//
//  NotificationInfoManager.h
//  GrowthCard
//
//  Created by Narender Kumar on 08/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationInfoManager : NSObject

+ (void)fetchNofificationList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;

@end
