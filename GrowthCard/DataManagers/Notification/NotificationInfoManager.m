//
//  NotificationInfoManager.m
//  GrowthCard
//
//  Created by Narender Kumar on 08/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "NotificationInfoManager.h"
#import "NotificationInfo+RemoteAccessor.h"

@implementation NotificationInfoManager

+ (void)fetchNofificationList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [NotificationInfo performFetchNotificationList:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSArray *dataArry = [userDict objectForKey:@"notifications"];
            NSMutableArray *notyList = [NSMutableArray new];
            for(NSDictionary *dict in dataArry) {
                NotificationInfo *dep = [NotificationInfo notificationWithDictnory:dict];
                [notyList addObject:dep];
            }
            completionHandler(notyList, nil);
            
        }
    }];
}


@end
