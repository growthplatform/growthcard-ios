//
//  CommentManager.h
//  GrowthCard
//
//  Created by Narender Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentManager : NSObject

+ (void)sendComment:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)fetchComment:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)createPost:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)fetchPost:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)deletePost:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;


@end
