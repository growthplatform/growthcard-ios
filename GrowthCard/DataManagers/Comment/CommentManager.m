//
//  CommentManager.m
//  GrowthCard
//
//  Created by Narender Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "CommentManager.h"
#import "Comment+RemoteAccessor.h"

@implementation CommentManager

+ (void)sendComment:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Comment performSendCommnt:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error);
            }
            else  /* A nil error indicates success! */ {
                
                NSDictionary *resultDict = [response resultDictionary];
                NSDictionary *commtDict = [resultDict objectForKey:@"result"];
                Comment *comt = [[Comment alloc]initWithAttributes:commtDict];
                completionHandler(comt, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError]);
        }
    }];
}

+ (void)fetchComment:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Comment performFetchCommnt:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error);
            }
            else  /* A nil error indicates success! */ {
                NSDictionary *resultDict = [response resultDictionary];
                NSDictionary *commtDict = [resultDict objectForKey:@"result"];
                NSMutableArray *commtArry = [commtDict objectForKey:@"comment"];
                NSMutableArray *arry = [NSMutableArray new];
                for(NSDictionary *dict in commtArry) {
                    Comment *comt = [[Comment alloc]initWithAttributes:dict];
                    [arry addObject:comt];
                }
                completionHandler(arry, nil);
                
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError]);
        }
    }];
}

+ (void)createPost:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Comment performCreatePost:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error);
            }
            else  /* A nil error indicates success! */ {
                NSDictionary *resultDict = [response resultDictionary];
                NSDictionary *commtDict = [resultDict objectForKey:@"result"];
                completionHandler(commtDict, nil);
                
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError]);
        }
    }];
}

+ (void)fetchPost:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler{
    [Comment performFetchPost:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error);
            }
            else  /* A nil error indicates success! */ {
                NSDictionary *resultDict = [response resultDictionary];
                NSDictionary *commtDict = [resultDict objectForKey:@"result"];
                NSMutableArray *commtArry = [commtDict objectForKey:@"post"];
                NSMutableArray *arry = [NSMutableArray new];
                for(NSDictionary *dict in commtArry) {
                    Comment *comt = [Comment getPostWithAttributes:dict];
                    [arry addObject:comt];
                }
                completionHandler(arry, nil);
                
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError]);
        }
    }];
}

+ (void)deletePost:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Comment performDeletePost:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error);
            }
            else  /* A nil error indicates success! */ {
                NSDictionary *resultDict = [response resultDictionary];
                NSDictionary *commtDict = [resultDict objectForKey:@"result"];
                completionHandler(commtDict, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError]);
        }
    }];
}


@end
