//
//  DashBoardFeedManager.h
//  GrowthCard
//
//  Created by Narender Kumar on 21/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DashBoardFeedManager : NSObject

+ (void)fetchPastEaList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)deletePastEaList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)fetchCurrentEa:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)likeFeed:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;

@end
