//
//  DashBoardFeedManager.m
//  GrowthCard
//
//  Created by Narender Kumar on 21/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DashBoardFeedManager.h"
#import "DashBoardFeed+RemoteAccessor.h"

@implementation DashBoardFeedManager

+ (void)fetchPastEaList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [DashBoardFeed performFetchPastEAs:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            NSArray *userArry = [userDict objectForKey:@"pastEffectiveAction"];
            NSMutableArray *userListArry = [NSMutableArray new];
            for(NSDictionary *dict in userArry) {
                DashBoardFeed *annumnt = [[DashBoardFeed alloc]initWithAttributes:dict];
                [userListArry addObject:annumnt];
            }
            
            completionHandler(userListArry, nil);
        }
    }];
}

+ (void)deletePastEaList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [DashBoardFeed performDeletePastEAs:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            completionHandler(userDict, nil);
        }
    }];
}


+ (void)fetchCurrentEa:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [DashBoardFeed performFetchCurrentEA:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            DashBoardFeed *feed = [DashBoardFeed currentFeed:userDict];
            completionHandler(feed, nil);
            
        }
    }];
}

+ (void)likeFeed:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [DashBoardFeed performLikeFeed:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            //"result":{"LikeCount":1,"isLiked":1}
            completionHandler(userDict, nil);
            
        }
    }];
}




@end
