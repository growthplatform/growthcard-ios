//
//  UserPreferences.h
//  GrowthCard
//
//  Created by Prateek prem on 13/09/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Announcement.h"

@interface UserPreferences : NSObject {

}

@property (nonatomic, strong) NSArray *departments;
@property (nonatomic, strong) Announcement *announcement;

-(id)init __attribute__((unavailable("Use 'sharedPreference' class method to create instance")));

+ (UserPreferences *)sharedPreference;

+ (NSInteger )batchCount ;
+ (void)saveBatchCount:(NSInteger )count ;

+ (void)saveDeviceToken:(NSString *)token;
+ (NSString *)deviceToken;

+ (NSString *)deviceUDID;

+ (NSMutableDictionary *)defaultUserParam;

+ (void)saveRandomNumber:(int)num ;
+ (int)getRandomNumber ;
+ (void)saveRandomNuberCounter:(int)num ;
+ (int)getRandomNuberCounter;
+ (void)saveRecentAnnoumentId:(int)num;
+ (int)getRecentAnnoumentId;
- (void)clearAnnouncements;

+ (NSMutableArray *)getGraphArray:(NSMutableArray *)array;

@end


