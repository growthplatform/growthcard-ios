//
//  UserManager.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "NetworkManager.h"
///**
// * Common request completion block that will retrun error if any and responsedata
// * responseData can be string, array, data or dictionary
// */

@interface UserManager : NSObject

@property (strong, nonatomic) User *activeUser;

+ (instancetype)sharedManager;

- (void)setUserData:(User *)user;
- (void)clearUserTokenAndInformation;
- (BOOL)isUserLoggedIn;

- (void)performUserLogin:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)performUserLogout:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)forgotPasswordForUserEmail:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)changeUserPasswordWithDict:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)performTerms:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)performAcceptTerms:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)performWorkDaysSelect:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)performUpdateProfile:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)updateProfileImage:(NSMutableArray *)postData AndFileData:(NSMutableArray *)fileData completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)getSubordinates:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)getEAList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)addEA:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)assignEA:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)changeEA:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)eADone:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)fetchSubordinate:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)fetchTeamMember:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)fetchSearchUser:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)fetchManagerDashboard:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)switchUser:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
- (void)fetchFeed:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;

@end
