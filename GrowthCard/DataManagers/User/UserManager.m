//
//  UserManager.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UserManager.h"
#import "User+RemoteAccessor.h"

#define kLoginKey         @"kUserLogin"
#define kUserInfoKey      @"kUserInfo"
#define kLoginToken       @"kLoginToken"


@interface UserManager ()

@end

@implementation UserManager

#pragma mark - Singleton Method
+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Init Method
- (id)init {
    if (self = [super init]) {
        // Load last logged user information if exists
        self.activeUser = [User lastLoggedUserInformation];
    }
    return self;
}

- (void)setUserData:(User *)user {
    self.activeUser = user;
    [self.activeUser saveUserInformation];
}

- (void)clearUserTokenAndInformation {
    [self.activeUser deleteUserInformation];
    self.activeUser = nil;
}

- (BOOL)isUserLoggedIn {
    return (self.activeUser != nil) ? YES : NO;
}

#pragma mark - Instance Methods
- (void)performUserLogin:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performUserLogin:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            User *user = [[User alloc] initWithAttributes:userDict];
            [[UserManager sharedManager] setUserData:user];
            
            NSArray *arry = [userDict objectForKey:@"days"];
            [user saveWorkingDay:arry];
            completionHandler(response, nil);
        }
    }];
}

- (void)performUserLogout:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performUserLogOut:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            completionHandler(userDict, nil);
        }
    }];
}

- (void)forgotPasswordForUserEmail:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performForgetPassword:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            
            completionHandler(response, nil);
        }
    }];
}

- (void)changeUserPasswordWithDict:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performChangePassword:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            
            completionHandler(response, nil);
        }
    }];
}

- (void)performTerms:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    
}

- (void)performAcceptTerms:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performAcceptTerms:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            User *user = [UserManager sharedManager].activeUser;
            user.userTermAccepted = [NSString formattedNumber:[NSNumber numberWithInt:1]];
            [UserManager sharedManager].activeUser = user;
            [user saveUserInformation];
            completionHandler(response, nil);
        }
        
    }];
}

- (void)performWorkDaysSelect:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performWorkDaysSelect:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSArray *arry = [resultDict objectForKey:@"result"];
            NSString *str =[arry componentsJoinedByString:@""];
            User *user = [UserManager sharedManager].activeUser;
            [user updateUserWorkingDays:str];
            completionHandler(response, nil);
        }
        
    }];
}

- (void)performUpdateProfile:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performUpdateProfile:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            completionHandler(response, nil);
        }
    }];
}

- (void)updateProfileImage:(NSMutableArray *)postData AndFileData:(NSMutableArray *)fileData completionHandler:(APIResponseCompletionHandler)completionHandler
{
    [User performProfileImageUpdate:postData fileData:fileData completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        @try {
            if (error) // Handle the error.
            {
                completionHandler(nil, error);
            }
            else // A nil error indicates success!
            {
                NSDictionary *resultDict = [response resultDictionary];
                NSString *userImage = [resultDict valueForKeyPath:@"result.profileImage"];
                User *user = [UserManager sharedManager].activeUser;
                user.userImageUrl = [NSString formattedValue:userImage];
                [user saveUserInformation];
                completionHandler(response, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError]);        }
    }];
}

- (void)getSubordinates:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performGetSubordinates:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSArray *userAry = [userDict objectForKey:@"subordinates"];
            NSMutableArray *temp = [NSMutableArray new];
            if(userAry.count) {
                for(NSDictionary *dict in userAry) {
                    User *subordnate = [User getSubordinatesWithAttributes:dict];
                    [temp addObject:subordnate];
                }
            }
            completionHandler(temp, nil);
        }
    }];
}

- (void)getEAList:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performGetEAList:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSArray *actions = [userDict objectForKey:@"effectiveActions"];
            NSMutableArray *temp = [NSMutableArray new];
            for (NSDictionary *dict in actions) {
                
                // NSError *error = nil;
                EAction *action = [EAction getEAWithData:dict]; //[MTLJSONAdapter modelOfClass:EAction.class fromJSONDictionary:dict error:&error];
                
                if (action) {
                    [temp addObject:action];
                }
            }
            
            completionHandler(temp, nil);
        }
    }];
}

- (void)addEA:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performAddEa:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSArray *actions = [userDict objectForKey:@"effectiveActions"];
            NSMutableArray *temp = [NSMutableArray new];
            for (NSDictionary *dict in actions) {
                
                // NSError *error = nil;
                EAction *action = [EAction getEAWithData:dict]; //[MTLJSONAdapter modelOfClass:EAction.class fromJSONDictionary:dict error:&error];
                
                if (action) {
                    [temp addObject:action];
                }
            }
            
            completionHandler(temp, nil);
        }
    }];
}

- (void)assignEA:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performAssignEa:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            completionHandler(userDict, nil);
        }
    }];
}

- (void)changeEA:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performChangeEa:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            completionHandler(userDict, nil);
        }
    }];
}

- (void)eADone:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performEaDone:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSArray *ary = [resultDict objectForKey:@"result"];
            NSDictionary *dict = [ary lastObject];
            completionHandler(dict, nil);
        }
    }];
}

- (void)fetchSubordinate:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performFetchSubordinate:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            User *menber = [User getSubordinatesFullData:userDict];
            completionHandler(menber, nil);
        }
    }];
}

- (void)fetchTeamMember:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performFetchTeamMember:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSDictionary *userDict1 = [userDict objectForKey:@"result"];
            NSArray *membersArry = [userDict1 objectForKey:@"members"];
            NSMutableArray *memberListArry = [NSMutableArray new];
            for(NSDictionary *dict in membersArry) {
                [memberListArry addObject:[User getMemberDetails:dict]];
            }
            
            completionHandler(memberListArry, nil);
        }
    }];
}

- (void)fetchSearchUser:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performFetchUsers:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSArray *userArry = [userDict objectForKey:@"users"];
            NSMutableArray *userListArry = [NSMutableArray new];
            for(NSDictionary *dict in userArry) {
                [userListArry addObject:[User getSearchUser:dict]];
            }
            
            completionHandler(userListArry, nil);
        }
    }];
}


- (void)fetchManagerDashboard:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performFetchManagerdashboard:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            NSArray *userArry = [userDict objectForKey:@"members"];
            NSMutableArray *userListArry = [NSMutableArray new];
            for(NSDictionary *dict in userArry) {
                [userListArry addObject:[User getSubordinateUser:dict]];
            }
            completionHandler(userListArry, nil);
        }
    }];
}

- (void)switchUser:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performSwitchUser:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            // Delete previous user data
            [[UserManager sharedManager] clearUserTokenAndInformation];
            
            // Create new user data with new information
            User *user = [[User alloc] initWithAttributes:userDict];
            [[UserManager sharedManager] setUserData:user];
            
            NSArray *arry = [userDict objectForKey:@"days"];
            [user saveWorkingDay:arry];
            completionHandler(response, nil);
        }
    }];
}


- (void)fetchFeed:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [User performFetchFeed:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDictq = [resultDict objectForKey:@"result"];
            
            NSDictionary *userDict = [userDictq objectForKey:@"result"];
            NSArray *fArry = [userDict objectForKey:@"feed"];
            NSMutableArray *feedArry = [NSMutableArray new];
            if(fArry.count) {
                for(NSDictionary *dict in fArry) {
                    [feedArry addObject:[[Feed alloc] initWithAttributes:dict]];
                }
            }
            if([userDict objectForKey:@"recentAnnouncement"] != [NSNull null]) {
                NSDictionary *annmntDict = [userDict objectForKey:@"recentAnnouncement"];
                Announcement *anumnt = [[Announcement alloc] initWithAttributes:annmntDict];
                [feedArry addObject:anumnt];
            }
            completionHandler(feedArry, nil);
        }
    }];
}

@end
