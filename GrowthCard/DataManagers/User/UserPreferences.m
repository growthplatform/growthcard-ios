//
//  UserPreferences.m
//  GrowthCard
//
//  Created by Prateek prem on 13/09/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UserPreferences.h"

#define kDeviceToken      @"kDeviceToken"
#define kRandomNumber    @"RandoneNumber"
#define kCounterRandom   @"CounterForRandomNumber"
#define kRecentAnnoumentID   @"RecentAnnounmentId"
#define kFilterData    @"FilterData"

@interface UserPreferences() {
    int photoTrackingCount;
}
@end

@implementation UserPreferences

#pragma mark - Singleton Method
+ (UserPreferences *)sharedPreference {
    static dispatch_once_t pred = 0;
    __strong static UserPreferences *_sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

+ (void)saveDeviceToken:(NSString *)token {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setObject:token forKey:kDeviceToken];
    [userDef synchronize];
}

+ (NSString *)deviceToken {
    
#if !(TARGET_IPHONE_SIMULATOR)
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return [userDef objectForKey:kDeviceToken];
#endif
    return @"1234512345";
}

// Device ID
+ (NSString *)deviceUDID {
    return  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+ (NSInteger )batchCount {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return [userDef integerForKey:@"batchCount"];
}

+ (void)saveBatchCount:(NSInteger )count {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setInteger:count forKey:@"batchCount"];
    [userDef synchronize];
}

+ (NSMutableDictionary *)defaultUserParam {
    User *user = [UserManager sharedManager].activeUser;
    NSString *deviceId    = [UserPreferences deviceUDID];
    NSString *userToken   = [NSString formattedValue:user.userToken];
    NSString *userId      = [NSString formattedValue:[NSString stringWithFormat:@"%@",user.userId]];
    
    NSMutableDictionary *loginData = [[NSMutableDictionary alloc] init];
    [loginData setObject:deviceId  forKey:@"deviceId"];
    [loginData setObject:userToken forKey:@"userToken"];
    [loginData setObject:userId    forKey:@"userId"];
    return loginData;
}

#pragma mark - Random Number
+ (void)saveRandomNumber:(int)num {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setInteger:num forKey:kRandomNumber];
    [userDef synchronize];
}

+ (int)getRandomNumber {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return (int)[userDef integerForKey:kRandomNumber];
}

+ (void)saveRandomNuberCounter:(int)num {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setInteger:num forKey:kCounterRandom];
    [userDef synchronize];
}

+ (int)getRandomNuberCounter {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return (int)[userDef integerForKey:kCounterRandom];
}

#pragma mark - Recent Annoument ID
+ (void)saveRecentAnnoumentId:(int)num {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setInteger:num forKey:kRecentAnnoumentID];
    [userDef synchronize];
}

+ (int)getRecentAnnoumentId {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return (int)[userDef integerForKey:kRecentAnnoumentID];
}

- (void)clearAnnouncements{
    self.announcement = nil;
}

#pragma mark - Graph Array
+ (NSMutableArray *)getGraphArray:(NSMutableArray *)array {
    NSMutableArray *graphDetails = [NSMutableArray new];
    NSMutableArray *graphAry = array;
    for(NSDictionary *dict in graphAry) {
        NSString *dayName       = [NSString formattedValue:[dict objectForKey:@"days"]];
        NSString *adhrenceCount = [NSString formattedNumber:[dict objectForKey:@"adhrenceCount"]];
        NSString *str = [NSString stringWithFormat:@"%@#%@",dayName,adhrenceCount];
        [graphDetails addObject:str];
    }
    return graphDetails;
}

@end
