//
//  DepartmentManager.h
//  GrowthCard
//
//  Created by Narender Kumar on 01/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepartmentManager : NSObject

+ (void)fetchDepmnt:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;

@end
