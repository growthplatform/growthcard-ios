//
//  DepartmentManager.m
//  GrowthCard
//
//  Created by Narender Kumar on 01/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DepartmentManager.h"
#import "Department+RemoteAccessor.h"

@implementation DepartmentManager

+ (void)fetchDepmnt:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Department performFetchDeptment:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSArray *userDict = [resultDict objectForKey:@"result"];
            
            NSMutableArray *depList = [NSMutableArray new];
            for(NSDictionary *dict in userDict) {
                Department *dep = [[Department alloc]initWithAttributes:dict];
                [depList addObject:dep];
            }
            completionHandler(depList, nil);
            
        }
    }];
}

@end
