//
//  AnnouncementManager.h
//  GrowthCard
//
//  Created by Narender Kumar on 20/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnnouncementManager : NSObject

+ (void)sendAnnoucement:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)fetchAnnoucement:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;
+ (void)deleteAnnoucement:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler;


@end
