//
//  AnnouncementManager.m
//  GrowthCard
//
//  Created by Narender Kumar on 20/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "AnnouncementManager.h"
#import "Announcement+RemoteAccessor.h"


@implementation AnnouncementManager

+ (void)sendAnnoucement:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Announcement performSendAnnoucement:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            completionHandler(userDict, nil);
        }
    }];
}

+ (void)fetchAnnoucement:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Announcement performFetchAnnounmnt:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            NSArray *userArry = [userDict objectForKey:@"announcements"];
            NSMutableArray *userListArry = [NSMutableArray new];
            for(NSDictionary *dict in userArry) {
                Announcement *annumnt = [[Announcement alloc]initWithAttributes:dict];
                [userListArry addObject:annumnt];
            }
            
            completionHandler(userListArry, nil);
        }
    }];
}

+ (void)deleteAnnoucement:(NSDictionary *)jsonDict completionHandler:(APIResponseCompletionHandler)completionHandler {
    [Announcement performDeleteAnnounmnt:jsonDict completionHandler:^(HTTPResponse *response, NSError *error, NSDictionary *userInfo) {
        if (error) /* Handle the error. */ {
            completionHandler(nil, error);
        }
        else  /* A nil error indicates success! */ {
            NSDictionary *resultDict = [response resultDictionary];
            NSDictionary *userDict = [resultDict objectForKey:@"result"];
            
            completionHandler(userDict, nil);
        }
    }];
}

@end
