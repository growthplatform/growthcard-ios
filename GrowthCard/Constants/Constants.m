//
//  Constants.m
//  GrowthCard
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#include "Constants.h"

NSString *const kAppName = @"GrowthCard";
int const kSeeMoreCharLimit=100;

NSString *const kFlurryKey = @"Y4HZ426SXZGVSBDZT35F";

// Date Format
NSString *const kISODateFormat     = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
NSString *const kDefaultDateFormat = @"yyyy-MM-dd";
NSString *const kTimeSlotFormat    = @"hh:mm a";
NSString *const kBookindDateFormat = @"MMM dd";
NSString *const kNotificationDate  = @"MMM dd, yyyy";

// Web Services
NSString *const kUSER_LOGIN_URL           = @"user/sign-in";
NSString *const kUSER_LOGOUT_URL          = @"user/log-out";
NSString *const kUSER_FORGOT_PASSWORD_URL = @"user/forgot-password";
NSString *const kUSER_TERMS_URL           = @"user/terms";
NSString *const kUSER_CHANGE_PWD          = @"user/change-password";
NSString *const kUSER_TERMS_STATUS        = @"user/terms-status";
NSString *const kUSER_SELECT_WORKDAYS     = @"user/work-days";
NSString *const kUSER_UPDATE_PROFILE      = @"user/user-profile";
NSString *const kUPDATE_PROFILE_IMAGE     = @"user/save-image";

NSString *const kGET_SUBORDINATE              = @"user/get-subordinates";
NSString *const kGET_EFFECTIVE_ACTION_LIST    = @"user/get-effective-actions";
NSString *const kGET_CREADTE_EFFECTIVE_ACTION = @"user/custom-effective-action";
NSString *const kASSIGN_EFFECTIVE_ACTION      = @"user/assign-effective-action";
NSString *const kCHANGE_EFFECTIVE_ACTION      = @"user/change-effective-action";
NSString *const kEFFECTIVE_ACTION_DONE        = @"user/mark-adhrence";
NSString *const kFETCH_SUBORDINATE            = @"user/get-subordinate-profile";
NSString *const kGET_TEAM_MEMBERS             = @"user/get-team-members";
NSString *const kGET_SEARCH_USER              = @"user/search-user";
NSString *const kSEND_ANNOUCEMENT             = @"user/announcement";
NSString *const kGET_ANNOUCEMENT              = @"user/get-announcement";
NSString *const kDELETE_ANNOUCEMENT           = @"user/delete-announcement";

NSString *const kFETCH_CURRENT_EA = @"user/get-current-effective-action";
NSString *const kFETCH_PAST_EA    = @"user/get-past-effective-actions";
NSString *const kDELETE_PAST_EA   = @"user/delete-past-effective-action";
NSString *const kSEND_COMMENT     = @"user/comment-on-effective-action";
NSString *const kFETCH_COMMENT    = @"user/get-comment-of-effective-action";

NSString *const kFETCH_MANAGER_DASHBOARD = @"user/get-manager-dashboard";

NSString *const kCREATE_POST = @"user/post";
NSString *const kFETCH_POST  = @"user/get-post";
NSString *const kDELETE_POST = @"user/delete-post";

NSString *const kSWITCH_USER      = @"user/switch-profile";
NSString *const kFETCH_DEPARTMENT = @"user/get-department";
NSString *const kFETCH_FEED       = @"user/generate-feed";

NSString *const kLIKE_FEED               = @"user/like-feed";
NSString *const kFETCH_NOTIFICATION_LIST = @"user/push-notification";

// Keys
NSString *const kEveryone = @"Everyone";

// Fonts
NSString *const kFont_Avenir_Next     = @"Avenir Next";
NSString *const kAvenir_Next_Semibold = @"Avenir Next-Semibold";
NSString *const kAvenir_Next_Bold     = @"Avenir Next-Bold";
NSString *const kAvenir_Next_Medium   = @"Avenir Next Medium";
NSString *const kAvenir_Next_DemiBold = @"AvenirNext-DemiBold";
NSString *const kAvenir_Next_italic   = @"AvenirNext-italic";

NSString *const kRubik_Bold    = @"Rubik-Bold";
NSString *const kRubik_Regular = @"Rubik-Regular";
NSString *const kRubik_italic  = @"Rubik-Italic";
NSString *const kRubik_Medium  = @"Rubik-Medium";
NSString *const kRubik_Light   = @"Rubik-Light";

// UserDefaults
NSString *const kIsFirstRun = @"IsFirstRun";

// Error in service
NSString *const kErrorInService   = @"There is some issue in service. Please try later";

// Invalid Email
NSString *const kEmailAndPwdEmpty = @"Please enter Email-ID & Password";
NSString *const kInvalidEmail     = @"Invalid Email-ID";
NSString *const kEnterEmail       = @"Please enter your Email-ID";
NSString *const kEnterPwd         = @"Please enter Password";
NSString *const kEmailSend        = @"Email sent successfully";

// Change Password
NSString *const kOldPwd           = @"Please enter old password";
NSString *const kNewPwd           = @"Please enter new password";
NSString *const kConfmPwd         = @"Please enter confirm password";
NSString *const kPwdNotMatch      = @"Old Password and Confirm Password does not match";
NSString *const kPwdLength        = @"Password should have minimum 6 characters";
NSString *const kPwdChanged       = @"Password changed successfully";

// Setting - update profile
NSString *const kFNameEmpty         = @"Please enter First Name";
NSString *const kLNameEmpty         = @"Please enter Last Name";
NSString *const kWeekDays           = @"Please select your working days";
NSString *const kProfileUpdated     = @"Profile updated successfully";
NSString *const kNoTeam             = @"Currently, you are not assigned to any team";

// Button Titles
NSString *const kOK     = @"OK";
NSString *const kCancel = @"Cancel";
NSString *const kYes    = @"Yes";
NSString *const kNo     = @"No";

// Subordinates List
NSString *const kEaList           = @"EAList";

// Search subordinates
NSString *const kEnterSomeText = @"Please enter some text";
NSString *const kEnterSubject = @"Please enter subject";
NSString *const kEnterAnnucmnt = @"Please enter message";
NSString *const kEnterPost     = @"Please enter some text";
NSString *const kEnterComment  = @"Please enter some text";

NSString *const kAddEA        = @"Please enter an Effective Action";
NSString *const kFeedArray    = @"FeedArray";
NSString *const kAnnouncement = @"AnnouncmentDict";
NSString *const UPDATE_BATCH  = @"UPDATEBATCH";

// No Working Day Msg
NSString *const kAreYouWorkMsg = @"Today is not your working day. Do you still want to mark your adherence?";
NSString *const kNoEAYet       = @"Currently no Effective Action assigned to you";
NSString *const kNoEACommt     = @"No Effective Action Assigned";

// Logout Msg
NSString *const kUserLogout    = @"There has been some changes in Team Structure at CMS by Admin. Please login again";

NSString *const kTechIssue     = @"There is some technical fault with server. Please try later";

// Alarm Msg
NSString *const kUSER_ALARM_MSG = @"This is alarm for user this need to change";

NSString *const kNottiff_Hide_Swap_Button = @"HideSwapNotification";
NSString *const kNottiff_Show_Swap_Button = @"showSwapNotification";

// Notifications
NSString *const kNotificationRecived      = @"notificationRecived";
NSString *const kNotificationCount        = @"NotificationCount";
NSString *const kRefreshEA                = @"RefreshUpdateEA";
NSString *const kRefreshEaDetailTableView = @"RefreshView";

//Share message
NSString *const kShareMessage = @"You seem to be doing Amazing! I see that your work is getting better and better each day and I like it. Keep it up hard work.";

// UILabel Url Notification Link
NSString *const kUILabelUrlNotification = @"UILabelUrlNotification";
NSString *const kAllDepartment = @"All Departments";

