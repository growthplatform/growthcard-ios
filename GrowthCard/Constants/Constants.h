//
//  Constants.h
//  GrowthCard
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef ConstantsHeader_h
#define ConstantsHeader_h

/**
 * Convenience method to replace NSLog output in DEBUG mode
 */

#ifdef DEBUG
    #define NSLog(args, ...) NSLog((@"%s [LineNumber: %d] " args), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
    #define NSLog(...);
#endif


/**
 * Convenience method to replace UIColor colorWithRead:RGB()
 */
#define kColor(_r_, _g_, _b_, _a_) [UIColor colorWithRed:_r_/255.0 green:_g_/255.0 blue:_b_/255.0 alpha:_a_]


typedef enum {
    UserRoleUndefined = 0,
    UserRoleManager,
    UserRoleSubordinate
} UserRole;


extern NSString *const kAppName;
extern int const kSeeMoreCharLimit;

extern NSString *const kFlurryKey;

// Date Format
extern NSString *const kISODateFormat;
extern NSString *const kDefaultDateFormat;
extern NSString *const kTimeSlotFormat;
extern NSString *const kBookindDateFormat;
extern NSString *const kNotificationDate;

// Web Services
extern NSString *const kUSER_LOGIN_URL;
extern NSString *const kUSER_LOGOUT_URL;
extern NSString *const kUSER_FORGOT_PASSWORD_URL;
extern NSString *const kUSER_CHANGE_PWD;
extern NSString *const kUSER_TERMS_URL;
extern NSString *const kUSER_TERMS_STATUS;
extern NSString *const kUSER_SELECT_WORKDAYS;
extern NSString *const kUSER_UPDATE_PROFILE ;
extern NSString *const kUPDATE_PROFILE_IMAGE;
extern NSString *const kGET_SUBORDINATE;
extern NSString *const kGET_EFFECTIVE_ACTION_LIST;
extern NSString *const kGET_CREADTE_EFFECTIVE_ACTION;
extern NSString *const kASSIGN_EFFECTIVE_ACTION;
extern NSString *const kCHANGE_EFFECTIVE_ACTION;
extern NSString *const kEFFECTIVE_ACTION_DONE;
extern NSString *const kFETCH_SUBORDINATE;
extern NSString *const kGET_TEAM_MEMBERS;
extern NSString *const kGET_SEARCH_USER;
extern NSString *const kSEND_ANNOUCEMENT;
extern NSString *const kGET_ANNOUCEMENT;
extern NSString *const kDELETE_ANNOUCEMENT;
extern NSString *const kFETCH_CURRENT_EA;
extern NSString *const kFETCH_PAST_EA;
extern NSString *const kDELETE_PAST_EA;
extern NSString *const kSEND_COMMENT;
extern NSString *const kFETCH_COMMENT;
extern NSString *const kFETCH_MANAGER_DASHBOARD;
extern NSString *const kCREATE_POST;
extern NSString *const kFETCH_POST;
extern NSString *const kDELETE_POST;
extern NSString *const kSWITCH_USER;
extern NSString *const kFETCH_DEPARTMENT;
extern NSString *const kFETCH_FEED;
extern NSString *const kLIKE_FEED;
extern NSString *const kFETCH_NOTIFICATION_LIST;

// Keys
extern NSString *const kEveryone;

// Fonts
extern NSString *const kFont_Avenir_Next;
extern NSString *const kAvenir_Next_Semibold;
extern NSString *const kAvenir_Next_Bold;
extern NSString *const kAvenir_Next_Medium;
extern NSString *const kAvenir_Next_DemiBold;
extern NSString *const kAvenir_Next_italic;

extern NSString *const kRubik_Bold;
extern NSString *const kRubik_Regular;
extern NSString *const kRubik_italic;
extern NSString *const kRubik_Medium;
extern NSString *const kRubik_Light;

// UserDefaults
extern NSString *const kIsFirstRun;

// Error in service
extern NSString *const kErrorInService;

// Invalid Email
extern NSString *const kEmailAndPwdEmpty;
extern NSString *const kInvalidEmail;
extern NSString *const kEnterEmail;
extern NSString *const kEnterPwd;
extern NSString *const kEmailSend;

// Change Password
extern NSString *const kOldPwd;
extern NSString *const kNewPwd;
extern NSString *const kConfmPwd;
extern NSString *const kPwdNotMatch;
extern NSString *const kPwdLength;
extern NSString *const kPwdChanged;

// Setting - update profile
extern NSString *const kFNameEmpty;
extern NSString *const kLNameEmpty ;
extern NSString *const kWeekDays;
extern NSString *const kProfileUpdated;
extern NSString *const kNoTeam;

// Button Titles
extern NSString *const kOK;
extern NSString *const kCancel;
extern NSString *const kYes;
extern NSString *const kNo;

// Search subordinates
extern NSString *const kEnterSomeText;
extern NSString *const kEnterSubject;
extern NSString *const kEnterAnnucmnt;
extern NSString *const kEnterPost;
extern NSString *const kEnterComment;

extern NSString *const kAddEA;

// Subordinates List
extern NSString *const kEaList;

// Share
extern NSString *const kShareMessage;

extern NSString *const kFeedArray;
extern NSString *const kAnnouncement;
extern NSString *const kNoEAYet;
extern NSString *const kNoEACommt;
extern NSString *const UPDATE_BATCH;

// Alarm Msg
extern NSString *const kUSER_ALARM_MSG;

// No Working Day Msg
extern NSString *const kAreYouWorkMsg;

// Logout Msg
extern NSString *const kUserLogout;
extern NSString *const kTechIssue;

extern NSString *const kNotificationRecived;
extern NSString *const kNotificationCount;
extern NSString *const kNottiff_Hide_Swap_Button;
extern NSString *const kNottiff_Show_Swap_Button;

// Notifications
extern NSString *const kRefreshEA;
extern NSString *const kRefreshEaDetailTableView;

extern NSString *const kUILabelUrlNotification;
extern NSString *const kAllDepartment;


#endif
