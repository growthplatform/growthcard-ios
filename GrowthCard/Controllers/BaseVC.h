//
//  BaseVC.h
//  GrowthCard
//
//  Created by Abhishek Tripathi on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

- (IBAction)presentMenuButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;

- (IBAction)backButtonPressedWithDismiss:(id)sender;

- (void)addTapGesture;
- (void)dismissKeyboard;

- (void) addBackButton;
- (IBAction)backBtnClicked:(id)sender;

- (void)seeMoreClick;


@end
