//
//  LoginVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

// Past Name, title

#import "LoginVC.h"

@interface LoginVC ()<UITextFieldDelegate> {
    __weak IBOutlet UITextField *emailTxtFld;
    __weak IBOutlet UITextField *pwdTxtFld;
    __weak IBOutlet UIButton *termsLbl;
}

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTapGesture];
    self.navigationController.navigationBarHidden = YES;
    NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:termsLbl.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:termsLbl.titleLabel.text attributes:linkAttributes];
    [termsLbl.titleLabel setAttributedText:attributedString];
    [self initializeView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"Device Token : %@",[UserPreferences deviceToken]);
    
   // [UIAlertController showAlertOn:self withMessage:[NSString formattedValue:[UserPreferences deviceToken]] andCancelButtonTitle:kOK];
    
   // emailTxtFld.text = @"albert@yopmail.com";
   // pwdTxtFld.text = @"123456";

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([APPManager isIphoneFour]){
        _contentViewHeightConstraint.constant = 400;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private Methods
- (void)initializeView {
    emailTxtFld.leftViewMode = UITextFieldViewModeAlways;
    UIView *view1 = [emailTxtFld paddingViewWithImage:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico_email"]] andPadding:10.0];
    emailTxtFld.leftView = view1;
    
    pwdTxtFld.leftViewMode = UITextFieldViewModeAlways;
    UIView *view2 = [emailTxtFld paddingViewWithImage:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico_pass"]] andPadding:10.0];
    pwdTxtFld.leftView = view2;
    
    NSLog(@"Frame size %f",self.view.frame.size.width);
}

- (BOOL)doValidation {
    if ([emailTxtFld.text isEmptyString] && [pwdTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEmailAndPwdEmpty andCancelButtonTitle:kOK];
        [emailTxtFld becomeFirstResponder];
        return NO;
    }
    else if ([emailTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterEmail andCancelButtonTitle:kOK];
        [emailTxtFld becomeFirstResponder];
        return NO;
    }
    else if (![NSString isEmailValid:emailTxtFld.text]) {
        [UIAlertController showAlertOn:self withMessage:kInvalidEmail andCancelButtonTitle:kOK];
        [emailTxtFld becomeFirstResponder];
        return NO;
    }
    else if ([pwdTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterPwd andCancelButtonTitle:kOK];
        [pwdTxtFld becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == emailTxtFld) {
        [pwdTxtFld becomeFirstResponder];
    }
    else {
        [self btnLoginClick:nil];
    }
    return YES;
}

#pragma mark - Controls Events
- (IBAction)termsBtnClicked:(id)sender {
    TermsVC  *viewController = (TermsVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[TermsVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)forgotPwdBtnclicked:(id)sender {
    ForgotPwdVC  *viewController = (ForgotPwdVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[ForgotPwdVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnLoginClick:(id)sender {
    if ([self doValidation]) {
        [self dismissKeyboard]; // dismiss the keyboard
        [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
        NSDictionary *dict = @{@"deviceId" : [NSString formattedValue:[UserPreferences deviceUDID]],
                               @"deviceType" : @"ios",
                               @"email" : [NSString formattedValue:emailTxtFld.text],
                               @"password" : [NSString formattedValue:pwdTxtFld.text],
                               @"deviceToken" : [NSString formattedValue:[UserPreferences deviceToken]]};
        
        [[UserManager sharedManager] performUserLogin:dict completionHandler:^(id response, NSError *error) {
            [GMDCircleLoader hideFromView:self.view animated:YES];
            if(!error) {
                    [APPManager presentHomeViewController];
            }
            else {
                [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
            }
        }];
    }
}

@end
