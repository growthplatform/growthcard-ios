//
//  TutorialView.h
//  GymApp
//
//  Created by Prashant Gautam on 13/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialView : UIView
-(void)setTutorialImages:(NSArray *)imageNameArray;
@end
