//
//  ForgotPwdVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 03/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ForgotPwdVC.h"

@interface ForgotPwdVC ()<UITextFieldDelegate> {
}
@property (weak, nonatomic) IBOutlet UITextField *emailTxtFld;

@end


@implementation ForgotPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super addTapGesture];
    [super addBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controls Events
- (IBAction)forgotBtnClicked:(id)sender {
    if (![NSString isEmailValid:_emailTxtFld.text] || [_emailTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kInvalidEmail andCancelButtonTitle:kOK];
        [_emailTxtFld becomeFirstResponder];
        return;
    }
    [self forgotPwd];
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - web Service
- (void)forgotPwd {
    [self dismissKeyboard];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    NSDictionary *dict = @{@"email" : self.emailTxtFld.text};
    [[UserManager sharedManager] forgotPasswordForUserEmail:dict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [UIAlertController showAlertOn:self withMessage:kEmailSend andCancelButtonTitle:kOK];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end
