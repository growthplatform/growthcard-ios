//
//  WorkdayVC.h
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@protocol WorkdayVCDelegate <NSObject>

@optional
- (void) getNewWorkingDays:(NSString *)workingDays;
@end

@interface WorkdayVC : BaseVC
@property (assign, nonatomic) BOOL isBack;
@property (nonatomic, weak) id<WorkdayVCDelegate> delegate;
+ (void) getWorkDaysView :(User *)user AndParentView:(UIView *)parentView;
@end
