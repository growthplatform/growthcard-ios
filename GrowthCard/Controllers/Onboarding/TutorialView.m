//
//  TutorialView.m
//  GymApp
//
//  Created by Prashant Gautam on 13/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "TutorialView.h"
@interface TutorialView(){
    
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIScrollView *imageScrollView;
    
    BOOL pageControlUsed;
}
@end
@implementation TutorialView


- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        UIView *v = [[[NSBundle mainBundle] loadNibNamed:@"TutorialView" owner:self options:nil] lastObject];
        v.frame = self.bounds;
        [self addSubview:v];
        
        [self initializeView];
    }
    return self;
}

-(void)initializeView {
    
    pageControlUsed=NO;
    
    // [imageScrollView setContentOffset:CGPointMake(0, 0)];
    
}


@end
