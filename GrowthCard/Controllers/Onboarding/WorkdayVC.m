//
//  WorkdayVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "WorkdayVC.h"
#import "WorkingDayCell.h"

//#define kDaysName

@interface WorkdayVC ()<UITableViewDataSource, UITableViewDelegate> {
    __weak IBOutlet UITableView *tblWeekDays;
    NSMutableArray *_dataSource;
    NSString *selectedDays;
    NSMutableArray *selectedDaysAry;
    UIButton *doneButton;
}
@property (weak, nonatomic) IBOutlet UILabel *workDayRemiderLbl;

@end

@implementation WorkdayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_isBack) {
        UIButton *backButton = [[UIButton alloc] init];
        backButton.frame=CGRectMake(0,0,22,15);
        [backButton setTitle:@" " forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"Back_arrow"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(leftMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        self.workDayRemiderLbl.hidden = YES;
    }
    else {
        [self.navigationItem setHidesBackButton:YES animated:YES];
        self.workDayRemiderLbl.hidden = NO;
    }
    
    doneButton = [[UIButton alloc] init];
    doneButton.frame=CGRectMake(0,0,50,30);
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:15]];
    [doneButton setTitleColor:[UIColor colorWithRed:33.0/255.0 green:161.0/255.0 blue:227.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [doneButton addTarget:self action:@selector(btnDoneClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:doneButton];
    [self checkSelection];
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"Work Days";
    [self showSelectedDays];
    [tblWeekDays reloadData];
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
    self.navigationController.navigationBarHidden = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controll Event
- (IBAction)btnDoneClick:(id)sender {
    NSString *string = [selectedDaysAry componentsJoinedByString:@""];
    if(_isBack) {
        if (_delegate && [_delegate respondsToSelector:@selector(getNewWorkingDays:)]) {
            [_delegate getNewWorkingDays:string];
        }
    }
    [self sendSelectedDaysOnServer:string];
}

- (IBAction)leftMenuClicked:(id)sender {
    if(_isBack) {
        [super backButtonClicked:sender];
    }
}

#pragma mark - Private Method
- (void)showSelectedDays {
    User *user = [UserManager sharedManager].activeUser;
    selectedDays = user.userWorkDays;
    selectedDaysAry = [NSMutableArray new];
    [selectedDays enumerateSubstringsInRange:NSMakeRange(0, [selectedDays length])
                                     options:(NSStringEnumerationByComposedCharacterSequences)
                                  usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                      [selectedDaysAry addObject:substring];
                                  }];
    NSArray *days = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
    _dataSource = [NSMutableArray new];
    
    for (int i=0; i<days.count; i++) {
        WorkingDaysInfo *dInfo;
        if([[selectedDaysAry objectAtIndex:i] isEqualToString:@"0"])
            dInfo = [WorkingDaysInfo setDayName:[days objectAtIndex:i]  andIsSeleted:NO];
        else
            dInfo = [WorkingDaysInfo setDayName:[days objectAtIndex:i]  andIsSeleted:YES];
        [_dataSource addObject:dInfo];
    }
    if ([selectedDaysAry containsObject:@"1"]) {
    }
}

- (void) checkSelection {
    BOOL isSelected = NO;
    if ([selectedDaysAry containsObject:@"1"]) {
        isSelected = YES;
    }
    doneButton.enabled = isSelected;
}

#pragma mark - Table View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"kDaysCellIdentifier";
    WorkingDayCell *cell = (WorkingDayCell *)[tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil) cell = [WorkingDayCell cell];
    WorkingDaysInfo *workDay = [_dataSource objectAtIndex:indexPath.row];
    [cell showDays:workDay];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WorkingDaysInfo *workDay = [_dataSource objectAtIndex:indexPath.row];
    workDay.isSelected = !workDay.isSelected;
    
    if([[selectedDaysAry objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
        [selectedDaysAry replaceObjectAtIndex:indexPath.row withObject:@"1"];
    }
    else {
        [selectedDaysAry replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    
    [self checkSelection];
    [tableView reloadData];
}

+ (void)getWorkDaysView :(User *)user AndParentView:(UIView *)parentView {
    NSArray *days   = @[@"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat", @"Sun"];
    NSString *selectedDaysStr = user.userWorkDays;
    NSMutableArray *array = [NSMutableArray new];
    for (int i = 0; i < [selectedDaysStr length]; i++) {
        NSString *chr = [selectedDaysStr substringWithRange:NSMakeRange(i, 1)];
        if([chr isEqualToString:@"1"]) {
            [array addObject:[days objectAtIndex:i]];
        }
    }
    if(array.count) {
        NSArray *viewsToRemove = [parentView subviews];
        for (UIView *v in viewsToRemove) {
            [v removeFromSuperview];
        }
        float viewWidth      = parentView.frame.size.width;
        float tempViewWidth  = 40*array.count;
        UIView *showDaysView = [[UIView alloc]initWithFrame:CGRectMake(viewWidth/2-tempViewWidth/2, 0, tempViewWidth, parentView.frame.size.height)];
        float xPos = 5;
        for(int i=0; i<array.count; i++) {
            UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(xPos, 10, 30, 20)];
            lbl.text = [array objectAtIndex:i];
            [lbl setFont:[UIFont fontWithName:kRubik_Regular size:14]];
            lbl.textAlignment = NSTextAlignmentCenter;
            [showDaysView addSubview:lbl];
            xPos += 35;
        }
        [parentView addSubview:showDaysView];
        //[parentView setBackgroundColor:[UIColor orangeColor]];
        [parentView setNeedsUpdateConstraints];
    }
}

#pragma mark - Web service
- (void) sendSelectedDaysOnServer:(NSString *)daysStr {
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:daysStr forKey:@"workDay"];
    [[UserManager sharedManager] performWorkDaysSelect:loginData completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            if(!_isBack) {
                
                [[NSUserDefaults standardUserDefaults] setBool: YES forKey:kIsFirstRun];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [APPManager presentHomeViewController];
                
                TermsVC  *viewController = (TermsVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[TermsVC class]];
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else {
                [self leftMenuClicked:nil];
            }
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}


@end

