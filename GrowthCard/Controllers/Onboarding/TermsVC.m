//
//  TermsVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "TermsVC.h"

@interface TermsVC ()<UIWebViewDelegate> {
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UIButton *accptBtn;
    __weak IBOutlet UIButton *delnBtn;
}

@end

@implementation TermsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    accptBtn.hidden = YES;
    delnBtn.hidden = YES;
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame=CGRectMake(0,0,22,15);
    [backButton setTitle:@" " forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"Back_arrow"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationController.navigationBarHidden = NO;
    webView.backgroundColor = [UIColor whiteColor];
    [self loadTermsAndConditions];
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    self.title=@"Terms & Conditions";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void) loadTermsAndConditions {
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_TERMS_URL]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodGET jsonDictionary:nil];
    [webView loadRequest:urlRequest];
}

#pragma mark - Controls Events
- (IBAction)btnAcceptClick:(id)sender {
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [[UserManager sharedManager]performAcceptTerms:loginData completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            TutorialVC *viewController = (TutorialVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[TutorialVC class]];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

- (IBAction)btnDeclineClick:(id)sender {
    [[UserManager sharedManager] clearUserTokenAndInformation];
    [APPManager presentLoginViewController];
}

- (IBAction)leftMenuClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [GMDCircleLoader hideFromView:self.view animated:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [GMDCircleLoader hideFromView:self.view animated:YES];
    accptBtn.enabled = YES;
}
- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}



@end

