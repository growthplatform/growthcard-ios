//
//  TutorialVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "TutorialVC.h"
#import "TutorialView.h"


@interface TutorialVC ()<UIScrollViewDelegate> {
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIScrollView *imageScrollView;
    __weak IBOutlet UIButton *nextBtn;
    __weak IBOutlet UIButton *skipBtn;
    
    BOOL pageControlUsed;
}
@end

@implementation TutorialVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    pageControlUsed=NO;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    NSArray *list = @[@"1",@"2",@"3",@"4"];
    [self setTutorialImages:list];
        
    if(_isBack) {
        nextBtn.hidden = YES;
        skipBtn.hidden = YES;
        UIButton *backButton = [[UIButton alloc] init];
        backButton.frame=CGRectMake(0,0,22,15);
        [backButton setTitle:@" " forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"Back_arrow"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(leftMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
        
        self.navigationController.navigationBarHidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Controlle Event
- (IBAction)btnNextClick:(id)sender {
    if([nextBtn.titleLabel.text isEqualToString:@"Next"]) {
        [self moveScrollViewToPage:pageControl.currentPage+1];
        pageControlUsed=YES;
        return;
    }
    WorkdayVC  *viewController = (WorkdayVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[WorkdayVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnSkipClick:(id)sender {
    WorkdayVC  *viewController = (WorkdayVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[WorkdayVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Private Method
-(void)setTutorialImages:(NSArray *)imageNameArray {
    CGFloat pageWidth = self.view.frame.size.width;
    NSLog(@"pageWidth %f",pageWidth);
    pageControl.numberOfPages = imageNameArray.count;
    for (NSInteger index = 0; index < imageNameArray.count; index++) {
        UIImageView *screenImageView=[self imageViewWithImageNamed:imageNameArray[index] pageNumber:index];
        [imageScrollView addSubview:screenImageView];
        [ imageScrollView setContentSize:CGSizeMake((index+1)*pageWidth,imageScrollView.frame.size.height-40)];
    }
}

- (UIImageView *)imageViewWithImageNamed:(NSString *)imageName pageNumber:(NSInteger)pageNumber {
    //float ww = imageScrollView.frame.size.width;
    CGFloat ww = self.view.frame.size.width;

    UIImageView  *screenImageView =[[UIImageView alloc] initWithFrame:CGRectMake(pageNumber*ww, 0, ww, imageScrollView.frame.size.height)];
    [screenImageView setImage:[UIImage imageNamed:imageName]];
    [screenImageView setContentMode:UIViewContentModeScaleToFill];
    screenImageView.backgroundColor=[UIColor clearColor];
    
    return screenImageView;
}
- (IBAction)pageChanged:(id)sender {
    [self moveScrollViewToPage:pageControl.currentPage];
    pageControlUsed=YES;
}

-(void)moveScrollViewToPage:(NSInteger)pageNo{
    CGRect frame = self.view.frame;
    frame.origin.x = frame.size.width * pageNo;
    [imageScrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = self.view.frame.size.width;
    int page = floor((imageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    
    skipBtn.hidden = NO;
    [nextBtn setTitle:@"Next" forState:UIControlStateNormal];
    if(page == pageControl.numberOfPages-1) {
       [nextBtn setTitle:@"Done" forState:UIControlStateNormal];
        skipBtn.hidden = YES;
    }
    if(_isBack)
        skipBtn.hidden = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (IBAction)leftMenuClicked:(id)sender {
    [self backButtonClicked:nil];
}

@end
