//
//  BaseVC.m
//  GrowthCard
//
//  Created by Abhishek Tripathi on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Control Events

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)presentMenuButtonClicked:(id)sender {
    [APPManager presentMenuonViewControlleronViewController:self];
}

- (IBAction)dismissButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)backButtonPressedWithDismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Public methods
- (void)addTapGesture {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Back Button 
- (void) addBackButton {
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame=CGRectMake(0,0,22,15);
    [backButton setTitle:@" " forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"Back_arrow"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
}

- (void)seeMoreClick {
    NSLog(@"seeMoreClick By Base VC");
}

@end
