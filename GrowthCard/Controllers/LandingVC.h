//
//  LandingVC.h
//  GrowthCard
//
//  Created by Narender Kumar on 26/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"


@protocol LandingVCDelegate <NSObject>

@optional
- (void)didLeftViewAnimationDone;

- (void)didRightViewCreateAnouncmntClicked;
- (void)didRightViewMakeComntClicked;
- (void)didRightViewCreatePostClicked;
- (void)didRightViewCancelClicked;

@end


@interface LandingVC : BaseVC

@property (nonatomic, assign) id <LandingVCDelegate> delegate;
@property (nonatomic, assign) BOOL isSlideMenuLock;

-(void) registerSwype;
- (void)setBringToFront;

@end
