
#import "NotificationController.h"
#import "NotificationInfo.h"
#import "FeedsVC.h"
#import "FullScreenImgView.h"
#import "UIWindow+CurrentView.h"
#import "CurrentEapopUp.h"
#import "EAPopUp.h"
#import "DashBoardFeedManager.h"
#import "SubSegmentedVC.h"

@implementation NotificationController

#pragma mark - Singleton Method

+ (instancetype)sharedController {
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NotificationController alloc] init];
    });
    return sharedInstance;
}

+ (void)registerPN {
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)recievedUserInfo:(NotificationInfo *)data {
    switch ([data.type integerValue]) {
        case kNotificationType_3PM:
            //Everyday 3 PM , SUBORDINATE should be reminded
            [self handlekNotificationType_3PM:data];
            break;
            
        case kNotificationType_EAAssign:
            //When an Effective Action is assigned to subordinate by his manager
            [self handlekNotificationType_EAAssign:data];
            break;
            
        case kNotificationType_CommentOnEA:
            //When assigned Effective Action has been commented by Manager
            [self handlekNotificationType_CommentOnEA:data];
            break;
            
        case kNotificationType_Likes:
            //When assigned Effective Action on Public Feed has been given a ThumbsUp by Other users
            [self handlekNotificationType_Likes:data];
            break;
            
        case kNotificationType_Announcement:
            //When an Announcement has been Made by Manager as well as where I am selected as a recipient
            [self handlekNotificationType_Announcement:data];
            break;
            
        case kNotificationType_TeamAssign:
            [self handlekNotificationType_TeamAssign:data];
            break;
            
        case kNotificationType_EADelete:
            [self handlekNotificationType_EADelete:data];
            break;
            
        case kNotificationType_AvgAdhareance:
            [self handlekNotificationType_AvgAdhareance:data];
            break;
        case kNotificationType_ShowVariablePop:
            [self handlekNotificationType_ShowVariablePop:data];
            break;
            
        default:
            break;
    }
}

-(void)handlekNotificationType_3PM:(NotificationInfo *)data {
    User *user = [UserManager sharedManager].activeUser;
    if ((user.userRole.integerValue == UserRoleSubordinate) && ((!(user.userEffectiveActionId.integerValue == 0)) || (user.userEffectiveActionId != nil))) {
        CurrentEapopUp *currentEa = [CurrentEapopUp currentEaView];
        [appDelegate.window addSubview:currentEa];
    }
}

-(void)handlekNotificationType_ShowVariablePop:(NotificationInfo *)data {
    FullScreenImgView *imageView=[FullScreenImgView showImageView];
    imageView.frame=[[UIApplication sharedApplication] keyWindow].frame;
    [[[(UINavigationController *)appDelegate.window.rootViewController topViewController] view] addSubview:imageView];
    [imageView showImageWithUrl:data.image AndTitle:data.markAdhereTitle AndMessage:data.markAdhereDesc];
}

-(void)handlekNotificationType_EAAssign:(NotificationInfo *)data {
    User *user = [UserManager sharedManager].activeUser;
    if (user.userRole.integerValue == UserRoleSubordinate) {
        {
            NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
            [loginData setObject:[NSString stringWithFormat:@"%@", user.userTeamId] forKey:@"teamId"];
            [loginData setObject:[NSString stringWithFormat:@"%@", data.effectiveActionId] forKey:@"effectiveActionId"];
            [loginData setObject:[NSString stringWithFormat:@"%@", user.userId] forKey:@"subordinateId"];
            [loginData setObject:[NSString stringWithFormat:@"%d",1] forKey:@"flag"];
            
            [DashBoardFeedManager fetchCurrentEa:loginData completionHandler:^(id response, NSError *error) {
                if(!error) {
                    DashBoardFeed *feed = (DashBoardFeed *)response;
                    user.userEffectiveActionId = [NSNumber numberWithInt: [feed.feedId intValue]];
                    user.userEffectiveActionTitle = feed.title;
                    user.userEffectiveActionDescription = feed.desc;
                    [UserManager sharedManager].activeUser = user;
                    [user saveUserInformation];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshEA object:nil];
                    
                    //EAPopUp *eaPop = [EAPopUp eaView];
                    //[appDelegate.window addSubview:eaPop];
                    
                    UIViewController *vc = [UIApplication sharedApplication].keyWindow.visibleViewController;
                    if([vc isKindOfClass:[SubEADetails class]]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshEaDetailTableView object:data userInfo:nil];
                        return;
                    }
                    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
                    viewController.notification=data;
                    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
                }
            }];
        }
    }
}

-(void)handlekNotificationType_CommentOnEA:(NotificationInfo *)data {
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.visibleViewController;
    if([vc isKindOfClass:[SubEADetails class]]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshEaDetailTableView object:data userInfo:nil];
        return;
    }
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.notification=data;
    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
}

-(void)handlekNotificationType_Likes:(NotificationInfo *)data{
    FeedsVC *viewController = (FeedsVC *)[[UIStoryboard homeStoryboard] instantiateViewControllerWithClass:[FeedsVC class]];
    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
}

-(void)handlekNotificationType_Announcement:(NotificationInfo *)data{
    AnnouncementsVC *viewController = (AnnouncementsVC *)[[UIStoryboard homeStoryboard] instantiateViewControllerWithClass:[AnnouncementsVC class]];
    viewController.isSubordinate = YES;
    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
}

-(void)handlekNotificationType_TeamAssign:(NotificationInfo *)data{
    NSLog(@"TeamAssign Notification for next update");
}

-(void)handlekNotificationType_EADelete:(NotificationInfo *)data{
    NSLog(@"EADelete Notification for next update");
}

-(void)handlekNotificationType_AvgAdhareance:(NotificationInfo *)data{
    NSLog(@"Mark Adhareance Notification for next update");
}

@end
