
#import <Foundation/Foundation.h>
#import "NotificationInfo.h"
typedef  enum {
    kNotificationType_3PM =1,
    kNotificationType_EAAssign,
    kNotificationType_CommentOnEA,
    kNotificationType_Likes,
    kNotificationType_Announcement,
    kNotificationType_TeamAssign,
    kNotificationType_EADelete,
    kNotificationType_AvgAdhareance,
    kNotificationType_ShowVariablePop=100
} NotificationType;

@interface NotificationController : NSObject

+ (instancetype)sharedController;
// register Push Notification.
+ (void)registerPN;
// push recieved.
- (void)recievedUserInfo:(NotificationInfo *)data;

@end
