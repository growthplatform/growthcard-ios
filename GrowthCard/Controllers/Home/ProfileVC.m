//
//  ProfileVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ProfileVC.h"
#import "SettingsVC.h"
#import "GroupMemberCell.h"
#import "SubSegmentedVC.h"
#import "ManagerSegmentedVC.h"
#import "UserPreferences.h"
#import "PostsCell.h"

@interface ProfileVC () {
    NSMutableArray *_userFeeds;
    NSMutableArray *_teamMembers;
    int _teamCurrentPage;
    BOOL _isMore;
    int _currentUser;
}

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompany;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartment;
@property (weak, nonatomic) IBOutlet UILabel *lblTeam;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblMembersCount;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberNotFound;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentNotFound;
@property (weak, nonatomic) IBOutlet UIView *viewWorkDay;
@property (weak, nonatomic) IBOutlet UICollectionView *collMembers;
@property (weak, nonatomic) IBOutlet UITableView *tblFeeds;
@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collMembers registerNib:[UINib nibWithNibName:NSStringFromClass([GroupMemberCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([GroupMemberCell class])];
    _userFeeds = [NSMutableArray new];
    _teamCurrentPage = 1;
    _currentUser = 1;
    _isMore = YES;
    self.lblMemberNotFound.hidden = YES;
    self.tblFeeds.estimatedRowHeight = 350;
    self.tblFeeds.rowHeight = UITableViewAutomaticDimension;
    [self.tblFeeds sizeToFit];
    self.tblFeeds.contentInset = UIEdgeInsetsMake(-17, 0, 0, 0);
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tblFeeds.bottomRefreshControl = refreshControl;
    self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.height/2;
    self.userProfileImage.layer.masksToBounds = YES;
    UIRefreshControl *refreshControl1 = [UIRefreshControl new];
    refreshControl1.triggerVerticalOffset = 100.;
    [refreshControl1 addTarget:self action:@selector(refreshPost) forControlEvents:UIControlEventValueChanged];
    self.tblFeeds.bottomRefreshControl = refreshControl1;
    [self refreshPost];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadData];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kUILabelUrlNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated    {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUILabelUrlNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tblFeeds.backgroundColor = [UIColor clearColor];
}

- (void)loadData {
    User *user = [UserManager sharedManager].activeUser;
    _teamCurrentPage = 1;
    _teamMembers = [NSMutableArray new];
    _lblCommentNotFound.hidden = _userFeeds.count;
    [WorkdayVC getWorkDaysView:user AndParentView:_viewWorkDay];
    [self.collMembers reloadData];
    [self.tblFeeds reloadData];
    self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",[user.userFName pascalCased], [user.userLName pascalCased]];
    self.lblCompany.text = [NSString stringWithFormat:@"%@",user.objCompany.name];
    self.lblDepartment.text = [NSString stringWithFormat:@"%@",user.userDeparmentName];
    self.lblTeam.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    self.lblEmail.text = [NSString stringWithFormat:@"%@",user.userEmail];
    [self.userProfileImage sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [self fetchSubordinateTeamList];
}

- (IBAction)leftMenuClick:(id)sender {
    SettingsVC *viewController = (SettingsVC *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithClass:[SettingsVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification {
    NSMutableArray *message = [notification object];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showUrls:message];
}

#pragma mark - Table View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _userFeeds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"PostsCell";
    PostsCell *cell = (PostsCell *)[self.tblFeeds dequeueReusableCellWithIdentifier:dayCellIdentifier];
    Comment *data=_userFeeds[indexPath.row];
    cell.backView.layer.cornerRadius = 3;
    cell.backView.layer.masksToBounds = YES;
    cell.imgView.layer.masksToBounds = YES;
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:data.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    User *user = [UserManager sharedManager].activeUser;
    if ([user.userId intValue] ==[data.commentUser.userId intValue] ) {
        cell.userNameLabel.text=@"Me";
    }
    else {
        cell.userNameLabel.text=[NSString stringWithFormat:@"%@ %@",[data.commentUser.userFName pascalCased], [data.commentUser.userLName pascalCased]];
    }
    cell.userTypeLabel.text=[NSString stringWithFormat:@"%@",data.commentUser.userDesignationTitle];
    [cell.dateLabel sizeToFit];
    cell.dateLabel.text = [NSDate getAgoFromString:data.commentAtTime];
    if (data.commentDesc.length> kSeeMoreCharLimit && !data.isExpended)
        [tableView addSeeMoreRecognizer:cell.descLabel AndDesc:data.commentDesc];
    else
        cell.descLabel.text=data.commentDesc;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

// Read more
- (void)tableView:(UITableView *)tableView didSeeMoreClickOnRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *data1 = _userFeeds[indexPath.row];
    data1.isExpended = YES;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone
     ];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
    [self fetchSubordinateTeamList];
}
- (void)refreshPost {
    [self fetchPost];
}

#pragma mark - UICollectionViewDatasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  _teamMembers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *channelCollectionViewCellIdentifier = @"GroupMemberCell";
    GroupMemberCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:channelCollectionViewCellIdentifier forIndexPath:indexPath];
    User *member=(User *)_teamMembers[indexPath.row];
    [cell configureCellWithMemberInfo:member];
    return cell;
}

#pragma mark - UICollectionViewDelegate Methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    User *member=(User *)_teamMembers[indexPath.row];
    [self fetchSubordinateUserDetails:member];
}

- (void)fetchSubordinateUserDetails :(User *)subUser {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",subUser.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",subUser.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",subUser.userTeamId] forKey:@"teamId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchSubordinate:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [self moveToNext:(User*)response];
        }
    }];
}

- (void)moveToNext :(User *)member {
    User *user = [UserManager sharedManager].activeUser;
    if (user.userRole.integerValue == UserRoleSubordinate) {
        SubSegmentedVC *viewController = (SubSegmentedVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubSegmentedVC class]];
        viewController.selectedUser=member;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else {
        ManagerSegmentedVC *viewController1 = (ManagerSegmentedVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ManagerSegmentedVC class]];
        viewController1.selectedUser=member;
        [self.navigationController pushViewController:viewController1 animated:YES];
    }
}

#pragma mark - UICollectionViewFlowLayoutDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(70+10, 100);
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Web Service
- (void)fetchSubordinateTeamList {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    User *user = [UserManager sharedManager].activeUser;
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",_teamCurrentPage] forKey:@"pageNo"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchTeamMember:dataDict completionHandler:^(id response, NSError *error) {
        [_collMembers.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            if([response isKindOfClass:[NSMutableArray class]]) {
                _isMore = NO;
                NSArray *arr = (NSArray *) response;
                if(arr.count) {
                    [_teamMembers addObjectsFromArray:arr];
                    _teamCurrentPage++;
                    _isMore = YES;
                }
            }
            _lblMembersCount.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)_teamMembers.count];
            _lblMemberNotFound.hidden = YES;
            if (!_teamMembers.count) {
                _lblMemberNotFound.hidden = NO;
            }
            _collMembers.hidden = !_lblMemberNotFound.hidden;
            [self.collMembers reloadData];
        }
        else
            if (!_teamMembers.count) {
                _lblMembersCount.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)_teamMembers.count];
                _lblMemberNotFound.hidden = NO;
            }
    }];
}

- (void)fetchPost {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    {
        [dataDict setObject:[NSString stringWithFormat:@"2"] forKey:@"flag"];
        [dataDict setObject:[NSString stringWithFormat:@"%d",_currentUser] forKey:@"pageNo"];
    }
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"subordinateId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager fetchPost:dataDict completionHandler:^(id response, NSError *error) {
        [self.tblFeeds.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                _lblCommentNotFound.hidden = YES;                
                for(Comment *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.commentId == %@", comt.commentId];
                    NSArray *filtered  = [_userFeeds filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [_userFeeds addObject:comt];
                    }
                }
                _currentUser = (int)(_userFeeds.count / 10) + 1;
                
                [self.tblFeeds reloadData];
            }
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end