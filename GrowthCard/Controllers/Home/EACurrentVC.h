//
//  EACurrentVC.h
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EASegmentedVC.h"

@interface EACurrentVC : BaseVC<SlideNavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) EASegmentedVC *eASegmentedVC;
@property (assign, nonatomic) BOOL isBack;

@end
