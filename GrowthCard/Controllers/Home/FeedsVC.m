//
//  FeedsVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "FeedsVC.h"
#import "TableViewCell.h"
#import "EAPopUp.h"
#import "DashBoardFeedManager.h"
#import "CurrentEADetail.h"
#import "SubFeedFilterView.h"
#import  "ManagerPastCell.h"
#import  "ManagerAnnunmentCell.h"
#import  "SubordinateAnnunmentCell.h"
#import  "CommntEACell.h"
#import  "MarkAdhereCell.h"
#import  "CurrentEapopUp.h"
#import "RecentAnnouncementCell.h"

#import "LazyHeader.h"


@interface FeedsVC () <SubFeedFilterViewDelegate, FeedLikeDelegate, UIScrollViewDelegate>{
    int currentPage;
    BOOL isMore;
    int currentFeedPage;
    DashBoardFeed *currentEA;
    CurrentEADetail *currentEADetail;
    UILabel *eaCountsLbl;
    SubFeedFilterView *overlayView;
    NSMutableArray *dataArray;
    int selectType;
    int deparmtType;
    UIRefreshControl *pullRefreshControl;
    __weak IBOutlet UITableView *_tableView;
    UIView *headerView;
}
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@property(nonatomic, strong) LazyHeader *objLazyHeader;
@property (weak, nonatomic) IBOutlet UIView *tempView;

@end

@implementation FeedsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    currentPage = 1;
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 150.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    _tableView.bottomRefreshControl = refreshControl;
    
    // Initialize the refresh control.
    pullRefreshControl = [[UIRefreshControl alloc] init];
    pullRefreshControl.backgroundColor = [UIColor clearColor];
    pullRefreshControl.tintColor = [UIColor grayColor];
    [pullRefreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:pullRefreshControl];
    
    isMore = YES;
    currentFeedPage = 1;
    currentEA = nil;
    selectType = 1;
    _tableView.estimatedRowHeight = 50;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView sizeToFit];
    [_tableView reloadData];
    
    [self resetHeader];
    [self getCurrentEA];
    [self getFeed];
    [self showSwapMarkadhereView];
    [self lazyHeader];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.objLazyHeader resizeView];
}

- (void) resetHeader {
    if(headerView == nil) {
        headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 178 + 20)];
        currentEADetail=[CurrentEADetail  currentEaView];
        User *user = [UserManager sharedManager].activeUser;
        currentEADetail.translatesAutoresizingMaskIntoConstraints = NO;
        [headerView addSubview:currentEADetail];
        NSDictionary *viewBindings = [NSDictionary dictionaryWithObjectsAndKeys:currentEADetail,@"subview", nil];
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subview]|" options:0 metrics:nil views:viewBindings]];
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subview]|" options:0 metrics:nil views:viewBindings]];
        [currentEADetail setUserData:user];
        currentEADetail.delegateView = self;
        eaCountsLbl.backgroundColor = [UIColor clearColor];
        headerView.backgroundColor = [UIColor colorWithRed:.91 green:.93 blue:.94 alpha:0.8];
        
        [_tempView setFrame:headerView.frame];
        [_tempView addSubview:headerView];
    }
}

- (void)lazyHeader {
    [_tableView setShowsVerticalScrollIndicator:NO];
    _tableView.contentInset = UIEdgeInsetsMake(CGRectGetHeight(_tempView.frame)-15, 0, 0, 0);
    self.objLazyHeader = [LazyHeader new];
    [self.objLazyHeader lazyHeader:_tempView tableView:_tableView];
}

- (IBAction)filterMenuClicked:(id)sender {
    if (!overlayView) {
        overlayView=[SubFeedFilterView filterView];
        overlayView.delegate = self;
        [self.view addSubview:overlayView];
        [overlayView resetData];
    }
    overlayView.frame=[UIScreen mainScreen].bounds;
    overlayView.hidden=false;
}

- (void)showSwapMarkadhereView {
    static BOOL isFirstTime = YES;
    if(isFirstTime) {
        User *user = [UserManager sharedManager].activeUser;
        if (((user.userEffectiveActionId.integerValue != 0) || (user.userEffectiveActionId != nil)) && (user.userRole.integerValue == UserRoleSubordinate)) {
            CurrentEapopUp *currentEa = [CurrentEapopUp currentEaView];
            [[(UINavigationController *)appDelegate.window.rootViewController topViewController].view addSubview:currentEa];
            if ([[self class] isSubclassOfClass:[LandingVC class]]) {
                [self setBringToFront];
            }
            isFirstTime = NO;
        }
    }
}

#pragma mark - SubFeedFilterViewDelegate
- (void)didApplyFilterClickedWithUserType:(int)userType AndDeprtmnt:(int)depmnt {
    overlayView.hidden=YES;
    currentFeedPage = 1;
    selectType = userType;
    deparmtType = depmnt;
    [self refreshTableByFilter];
}

- (void)didCrossButtonClicked{
    overlayView.hidden=YES;
}

- (void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getCurrentEA)
                                                 name:kRefreshEA
                                               object:nil];
    
    [self removeTableViewRefresher];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kRefreshEA
                                                  object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}



# pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    long count=dataArray.count;
    if([UserPreferences sharedPreference].announcement && ([UserPreferences getRecentAnnoumentId]!=[UserPreferences sharedPreference].announcement.announceId.intValue) && ([UserPreferences getRecentAnnoumentId]!=0)) {
        ++count;
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static  BOOL isAnnoucementCellCreated=NO;
    if (indexPath.row==0 ) {
        isAnnoucementCellCreated=NO;
    }
    if(([UserPreferences sharedPreference].announcement && indexPath.row==0) && ([UserPreferences getRecentAnnoumentId]!=[UserPreferences sharedPreference].announcement.announceId.intValue)) {
        [UserPreferences saveRecentAnnoumentId:[UserPreferences sharedPreference].announcement.announceId.intValue];
        isAnnoucementCellCreated=YES;
        return [self CreateAnnunmentCell:[UserPreferences sharedPreference].announcement];
    }
    Feed * feed = nil;
    long index=indexPath.row>= dataArray.count ?  dataArray.count-1 : indexPath.row;
    
    if (isAnnoucementCellCreated)
        feed = [dataArray objectAtIndex:indexPath.row-1];
    else
        feed = [dataArray objectAtIndex:index];
    switch (feed.feedType.integerValue) {
        case 1:
            // ManagerAnnunmentCell
            return [self CreateManagerAnnunmentCell:feed AtIndex:(int)indexPath.row];
            break;
        case 2:
            return [self CreateSubordinateAnnunmentCell:feed AtIndex:(int)indexPath.row];
            // SubordinateAnnunmentCell
            break;
        case 3:
            return [self CreateCommntEACell:feed AtIndex:(int)indexPath.row];
            // CommntEACell
            break;
        case 4:
            return [self CreateMarkAdhereCell:feed AtIndex:(int)indexPath.row];
            // MarkAdhereCell
            break;
        default:
            return nil;
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.objLazyHeader scrollViewDidScroll:scrollView];
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)refresh {
    [self getFeed];
}

- (void)pullToRefresh {
    [pullRefreshControl endRefreshing];
    currentFeedPage = 1;
    if(dataArray.count) {
        [dataArray removeAllObjects];
        [_tableView reloadData];
    }
    [self resetHeader];
    [self getCurrentEA];
    [self getFeed];
}
- (void)removeTableViewRefresher {
    [_tableView.bottomRefreshControl beginRefreshing];
    [_tableView.bottomRefreshControl endRefreshing];
}

#pragma mark - All Cells
//ManagerAnnunmentCell
//SubordinateAnnunmentCell
//CommntEACell
//MarkAdhereCell
- (UITableViewCell*)CreateAnnunmentCell:(Announcement *) announcement {
    static NSString *dayCellIdentifier = @"RecentAnnouncementCell";
    RecentAnnouncementCell *cell = (RecentAnnouncementCell *)[_tableView  dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [RecentAnnouncementCell cell];
    [cell setDataWithAnnoucement:announcement];
    cell.delegate=self;
    return cell;
}

- (UITableViewCell*)CreateManagerAnnunmentCell:(Feed *)feed AtIndex:(int)idx {
    static NSString *dayCellIdentifier = @"ManagerAnnunmentCell";
    ManagerAnnunmentCell *cell = (ManagerAnnunmentCell *)[_tableView  dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [ManagerAnnunmentCell cell];
    [cell setDataWithFeed:feed :idx];
    cell.delegate=self;
    return cell;
}

- (UITableViewCell*)CreateSubordinateAnnunmentCell:(Feed *)feed  AtIndex:(int)idx {
    static NSString *dayCellIdentifier = @"SubordinateAnnunmentCell";
    SubordinateAnnunmentCell *cell = (SubordinateAnnunmentCell *)[_tableView  dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [SubordinateAnnunmentCell cell];
    cell.delegate = self;
    cell.delegateView = self;
    [cell setDataWithFeed:feed :idx];
    return cell;
}

- (UITableViewCell*)CreateCommntEACell:(Feed *)feed  AtIndex:(int)idx {
    static NSString *dayCellIdentifier = @"CommntEACell";
    CommntEACell *cell = (CommntEACell *)[_tableView  dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [CommntEACell cell];
    cell.delegate = self;
    cell.delegateView = self;
    [cell setDataWithFeed:feed :idx];
    return cell;
}

- (UITableViewCell*)CreateMarkAdhereCell:(Feed *)feed  AtIndex:(int)idx {
    static NSString *dayCellIdentifier = @"MarkAdhereCell";
    MarkAdhereCell *cell = (MarkAdhereCell *)[_tableView  dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [MarkAdhereCell cell];
    cell.delegate = self;
    cell.delegateView = self;
    [cell setDataWithFeed:feed :idx];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Web Service
- (void)getCurrentEA {
    User *user = [UserManager sharedManager].activeUser;
    if(!(user.userEffectiveActionId.integerValue == 0) || (user.userEffectiveActionId != nil)) {
        NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
        [loginData setObject:[NSString stringWithFormat:@"%@", user.userTeamId] forKey:@"teamId"];
        [loginData setObject:[NSString stringWithFormat:@"%@", user.userEffectiveActionId] forKey:@"effectiveActionId"];
        [loginData setObject:[NSString stringWithFormat:@"%@", user.userId] forKey:@"subordinateId"];
        [loginData setObject:[NSString stringWithFormat:@"%d", currentPage] forKey:@"pageNo"];
        [loginData setObject:[NSString stringWithFormat:@"%d",1] forKey:@"flag"];
        
        [DashBoardFeedManager fetchCurrentEa:loginData completionHandler:^(id response, NSError *error) {
            if(!error) {
                currentEA = (DashBoardFeed *)response;
                currentEA.user = user;
                currentEA.user.graphDetails=currentEA.graphDetails;
                [currentEADetail setUserData:currentEA.user];
            }
        }];
    }
}

- (void)getFeed {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@", user.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", user.objCompany.companyId] forKey:@"companyId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", user.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%d", currentFeedPage] forKey:@"pageNo"];
    // Filter User Type
    [loginData setObject:[NSString stringWithFormat:@"%d", selectType] forKey:@"flag1"];
    // Filter Department Type
    [loginData setObject:[NSString stringWithFormat:@"%d", deparmtType] forKey:@"flag2"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchFeed:loginData completionHandler:^(id response, NSError *error) {
        [_tableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                isMore = YES;
                BOOL isRowFoud=NO;
                for(id iClass in response) {
                    if([iClass isKindOfClass:[Feed class]]) {
                        isRowFoud=YES;
                        Feed *feed = (Feed *)iClass;
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.feedId == %@", feed.feedId];
                        NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                        NSLog(@"Commt arrry %@", filtered);
                        if (filtered.count == 0) {
                            [dataArray addObject:feed];
                        }
                    }
                    if([iClass isKindOfClass:[Announcement class]]) {
                        [[UserPreferences sharedPreference] setAnnouncement:(Announcement *)iClass];
                    }
                }
                currentPage = (int)(dataArray.count / 10) + 1;
                if (isRowFoud) {
                    [_tableView reloadData];
                }
                [self showNoDataMessage];
            }
            if(dataArray.count)
                _noDataLbl.hidden = YES;
        }
    }];
}

- (void)likeFeed:(Feed *)feed AndIndex:(int)idx {
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@", feed.feedId] forKey:@"feedId"];
    [DashBoardFeedManager likeFeed:loginData completionHandler:^(id response, NSError *error) {
        if(!error) {
            if(!error) {
            }
        }
    }];
}

#pragma mark - FeedLikeDelegate
- (void)didLikeFeed:(Feed *)feed :(int)idx{
    [self likeFeed:feed AndIndex:idx];
}

#pragma mark - Controll Event
- (void) refreshTableByFilter {
    [dataArray removeAllObjects];
    [_tableView reloadData];
    [self getFeed];
}

- (void)showNoDataMessage{
    UILabel *label = nil;
    if(dataArray.count > 0) {
        id firstObject = [dataArray firstObject];
        if([firstObject isKindOfClass:[Feed class]]){
            _tableView.tableFooterView = [[UIView alloc] init];
        }
    } else {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 40)];
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentCenter;
        _tableView.tableFooterView = label;
    }
}

@end