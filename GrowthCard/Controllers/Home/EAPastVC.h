//
//  EAPastVC.h
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "LandingVC.h"
#import "EASegmentedVC.h"

@interface EAPastVC :  BaseVC<SlideNavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) EASegmentedVC *eASegmentedVC;

@end
