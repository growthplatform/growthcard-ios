//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Appster. All rights reserved.
//

#import "RightMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "ProfileVC.h"
#import "FeedsVC.h"
#import "EASegmentedVC.h"
#import "PostsVC.h"
#import "UIImage+CropImage.h"
#import "LatestAnnouncement.h"


static NSString *menuFeed = @" Feed";
static NSString *menuEA = @" Effective Action";
static NSString *menuPost = @" Posts";
static NSString *menuNotifications   = @" Notifications";
static NSString *menuDashboard = @" Dashboard";
static NSString *menuAnnoucements   = @" Annoucements";

/*
else if ([key isEqualToString:@" Effective Action"])//_active
else if ([key isEqualToString:@" Posts"])
else if ([key isEqualToString:@" Notifications"])
else if ([key isEqualToString:@" Dashboard"])
else  if([key isEqualToString:@" Annoucements"])
*/

@interface RightMenuViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSArray *menuItems;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrBackGround;
@property (strong, nonatomic)  UITableView *tblMenuView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;

@end


@implementation RightMenuViewController

#pragma mark - UIViewController Methods -
- (id)initWithCoder:(NSCoder *)aDecoder {
    self.slideOutAnimationEnabled = NO;
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [SlideNavigationController sharedInstance].panGestureSideOffset = 0;
    [SlideNavigationController sharedInstance].enableShadow = NO;
    [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
    self.scrBackGround.showsVerticalScrollIndicator = YES;
    [self designMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationRecived:)
                                                 name:UPDATE_BATCH
                                               object:nil];
    
    
    self.scrBackGround.bounces = NO;
    self.tblMenuView.bounces = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if ([self.navigationController.visibleViewController isKindOfClass:[UIAlertController class]]) {
        // UIAlertController is presenting.Here
        NSLog(@"Pop-up here");
    }
    
    CGFloat viewHeight = CGRectGetHeight(self.view.frame) - 60;
    if ([APPManager isIphoneFour]){
        self.scrollContentWidthConstraint.constant = 210;
        self.scrollContentHeightConstraint.constant = viewHeight;
    }else if ([APPManager isIphoneFive]){
        self.scrollContentWidthConstraint.constant = 240;
        self.scrollContentHeightConstraint.constant = viewHeight;
    }else if ([APPManager isIphoneSixe]){
        self.scrollContentWidthConstraint.constant = 280;
        self.scrollContentHeightConstraint.constant = viewHeight;
    }else if ([APPManager isIphoneSixPlus]){
        self.scrollContentWidthConstraint.constant = 330;
        self.scrollContentHeightConstraint.constant = CGRectGetHeight(self.view.frame) -  200 ;
    }
    [_scrBackGround setNeedsUpdateConstraints];
}

- (void)notificationRecived:(NSNotification *)notification{
    [self.tblMenuView reloadData];
}

- (IBAction)btnCrossClick:(id)sender {
    [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
    }];
}

- (void)designMenu {
    for (UIView *view in [self.scrollContentView subviews])
        [view removeFromSuperview];
    self.tblMenuView = nil;
    if ([UserManager sharedManager].activeUser.userRole.intValue==UserRoleManager) {
        [self designMangerMenu];
    }
    else {
        [self designSubordinateMenu];
    }
    [self.scrBackGround setNeedsUpdateConstraints];
}

-(void)designMangerMenu {
    self.tblMenuView=[self makeTableView];
    [self.scrollContentView addSubview:self.tblMenuView];
    self.tblMenuView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewBindings = [NSDictionary dictionaryWithObjectsAndKeys:self.tblMenuView,@"tableView", nil];
    [self.scrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:viewBindings]];
    [self.scrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]-10-|" options:0 metrics:nil views:viewBindings]];
    menuItems=@[@"UserName",menuDashboard,menuAnnoucements,menuNotifications];
    UIButton *addProject = [[UIButton alloc] initWithFrame:CGRectMake(10, 280+35, 190, 43)];
    [addProject setTitle:@"New Annoucement" forState:UIControlStateNormal];
    [addProject addTarget:self action:@selector(newAnnoucementPressed:) forControlEvents:UIControlEventTouchUpInside];
    [addProject  setBackgroundColor:[UIColor colorWithRed:0.0745 green:0.5922 blue:0.851 alpha:1.0]];
    [addProject setTintColor:[UIColor whiteColor]];
    [self.scrollContentView addSubview:addProject];
    [addProject.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:15.0f]];
    addProject.layer.cornerRadius = 5;
    addProject.layer.masksToBounds = YES;
    self.tblMenuView.tableFooterView = nil;
}

- (IBAction)newAnnoucementPressed:(id)sender {
    [self newAnnoumentClicked];
}

-(void)designSubordinateMenu {
    self.tblMenuView=[self makeTableView];
    [self.scrollContentView addSubview:self.tblMenuView];
    self.tblMenuView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *viewBindings = [NSDictionary dictionaryWithObjectsAndKeys:self.tblMenuView,@"tableView", nil];
    [self.scrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:viewBindings]];
    [self.scrollContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]-10-|" options:0 metrics:nil views:viewBindings]];
    menuItems = @[@"UserName",menuFeed,menuEA,menuNotifications];
    Announcement *anumnt = [[UserPreferences sharedPreference] announcement];
    if(anumnt)
        [self setAnnounsment:anumnt];
    [self.scrollContentView setNeedsUpdateConstraints];
}

- (void)setAnnounsment:(Announcement *)annunmect {
    LatestAnnouncement *latestAnnouncement=[LatestAnnouncement view];
    [latestAnnouncement.imgViewUser sd_setImageWithURL:[NSURL URLWithString:annunmect.announceUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [latestAnnouncement.imgViewUser roundCorner:latestAnnouncement.imgViewUser.frame.size.width/2 border:0 borderColor:nil];
    latestAnnouncement.userNameL.text = [NSString stringWithFormat:@"%@ %@",annunmect.announceUser.userFName, annunmect.announceUser.userLName];
    latestAnnouncement.userDegination.text = [NSString stringWithFormat:@"%@",annunmect.announceUser.userDesignationTitle];
    latestAnnouncement.userComment.text = [NSString stringWithFormat:@"%@",annunmect.announceDesc];
    latestAnnouncement.userTime.text = [NSDate getAgoFromString:annunmect.announceTime];
    [latestAnnouncement setFrame:CGRectMake(0, 0  , latestAnnouncement.frame.size.width, 250)];
    self.tblMenuView.tableFooterView = latestAnnouncement;
}

- (UITableView *)makeTableView {
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = 290;
    CGFloat height = 300;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.rowHeight = 54+20;
    tableView.sectionFooterHeight = 0;
    tableView.sectionHeaderHeight = 0;
    tableView.scrollEnabled = YES;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor=[UIColor clearColor];
    tableView.separatorColor = [UIColor clearColor];
    tableView.scrollEnabled = YES;
    return tableView;
}

#pragma mark - Table View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *backgroundView=[UIView new];
    backgroundView.backgroundColor=[UIColor clearColor];
    cell.selectedBackgroundView=backgroundView;
    cell.textLabel.highlightedTextColor = [UIColor whiteColor];
    cell.textLabel.textColor=[UIColor colorWithRed:0.4196 green:0.4353 blue:0.4471 alpha:1.0];
    cell.detailTextLabel.textColor=[UIColor colorWithRed:0.4196 green:0.4353 blue:0.4471 alpha:1.0];
    cell.detailTextLabel.highlightedTextColor = [UIColor colorWithRed:0.4196 green:0.4353 blue:0.4471 alpha:1.0];
    if (indexPath.row==0) {
        User *user=[UserManager sharedManager].activeUser;
        cell.textLabel.text=[NSString stringWithFormat:@"%@ %@",[user.userFName pascalCased], [user.userLName pascalCased]];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.detailTextLabel.text=@"Tap to view profile";
        UIImage *img = [UIImage imageNamed:@"profile_top"];
        img = [UIImage squareImageFromImage:img scaledToSize:30];
        [cell.imageView roundCorner:CGRectGetHeight(cell.imageView.frame)/2.0 border:0 borderColor:nil];
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:img completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            cell.imageView.image = [UIImage squareImageFromImage:image scaledToSize:30];
            [cell.imageView roundCorner:15 border:0 borderColor:nil];
        }];
        cell.textLabel.font=[UIFont fontWithName:kRubik_Medium size:15.0f];
        cell.detailTextLabel.font=[UIFont fontWithName:kRubik_Regular size:11.0f];        
        
        if(user.userSwitch.intValue == 1) {
            UIButton *switchButton = [cell.contentView viewWithTag:9999];
            if(!switchButton){
                switchButton = [[UIButton alloc] init];
                [switchButton setTitle:@"Switch" forState:UIControlStateNormal];
                [switchButton setTitleColor:UIColorMakeRGB(98,174,232) forState:UIControlStateNormal];
                [switchButton addTarget:self action:@selector(switchUser:) forControlEvents:UIControlEventTouchUpInside];
                [switchButton  setBackgroundColor:[UIColor clearColor]];
                [[switchButton titleLabel] setTextAlignment:NSTextAlignmentRight];
                [cell.contentView addSubview:switchButton];
                [switchButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];
                switchButton.translatesAutoresizingMaskIntoConstraints = NO;
                NSDictionary *viewBindings = [NSDictionary dictionaryWithObjectsAndKeys:switchButton,@"button", nil];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[button(65)]|" options:0 metrics:nil views:viewBindings]];
                [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[button]|" options:0 metrics:nil views:viewBindings]];
            }
        }
    }
    else {
        cell.textLabel.text=menuItems[indexPath.row];
        NSString *imgName=[self parseImageNameFromKey:menuItems[indexPath.row]];
        cell.imageView.image=[UIImage imageNamed:imgName];
        imgName=[NSString stringWithFormat:@"%@_active",imgName];
        cell.imageView.highlightedImage=[UIImage imageNamed:imgName];
        cell.textLabel.font=[UIFont fontWithName:kRubik_Medium size:15.0f];
        NSLog(@"Image name:%@",imgName);
    }
    if ([cell.textLabel.text isEqualToString:@"Notifications"]) {
        if ([UserPreferences batchCount]>0) {
            //show green icon
            UIView* accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 50)];
            UIImageView* accessoryViewImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_green_notification_big"]];
            accessoryViewImage.center = CGPointMake(12, 25);
            [accessoryView addSubview:accessoryViewImage];
            [cell setAccessoryView:accessoryView];
        }
        else {
            UIView* accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 50)];
            [cell setAccessoryView:accessoryView];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Class objClass=nil;
    if ([UserManager sharedManager].activeUser.userRole.intValue==UserRoleManager) {
        
        if (indexPath.row==0)
            objClass=[ProfileVC class];
        else if (indexPath.row==1)
            objClass=[DashboardVC class];
        else if (indexPath.row==2)
            objClass=[AnnouncementsVC class];
        else if (indexPath.row==3)
            objClass=[NotificationsVC class];
    }
    else {
        
        if (indexPath.row==0)
            objClass=[ProfileVC class];
        else if (indexPath.row==1)
            objClass=[FeedsVC class];
        else if (indexPath.row==2)
            objClass=[EASegmentedVC class];
        else if (indexPath.row==3)
            objClass=[PostsVC class];
        else if (indexPath.row==4)
            objClass=[NotificationsVC class];
        
    }
    if (!objClass) {
        return;
    }
    UIViewController *vc =[[UIStoryboard homeStoryboard] instantiateViewControllerWithClass:objClass];
    [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:vc withCompletion:nil];
}

- (NSString*)parseImageNameFromKey:(NSString*) key {
    if ([key isEqualToString:menuFeed])
        return @"ico_feed";
    else if ([key isEqualToString:menuEA])//_active
        return @"ico_eff-action";
    else if ([key isEqualToString:menuPost])
        return @"ico_post";
    else if ([key isEqualToString:menuNotifications])
        return @"ico_notifications";
    else if ([key isEqualToString:menuDashboard])
        return @"ico_eff-action";
    else  if([key isEqualToString:menuAnnoucements])
        return @"ico_announcement";
    return @"";
}


- (void)newAnnoumentClicked {
    SubNewAnnounce *viewController = (SubNewAnnounce *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubNewAnnounce class]];
    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
}

- (IBAction)switchUser:(id)sender {
    [[UserPreferences sharedPreference] clearAnnouncements];
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dict = [UserPreferences defaultUserParam];
    [dict setObject:[NSString stringWithFormat:@"%d",1] forKey:@"flag"];
    if(user.userRole.integerValue == UserRoleManager)
        [dict setObject:[NSString stringWithFormat:@"%d",2] forKey:@"flag"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] switchUser:dict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            User *user = [UserManager sharedManager].activeUser;
            {
                if([user.userWorkDays isEqualToString:@"0000000"]) {
                    // Send to Working days
                    WorkdayVC  *viewController = (WorkdayVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[WorkdayVC class]];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
                else {
                    [APPManager presentHomeViewController];
                }
            }
            [self.tblMenuView reloadData];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end
