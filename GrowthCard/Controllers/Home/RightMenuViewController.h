//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface RightMenuViewController : UIViewController 

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentWidthConstraint;

@end
