//
//  EAPastVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EAPastVC.h"
#import "TableViewCell.h"
#import "EAPopUp.h"
#import "DashBoardFeedManager.h"
#import "CurrentEADetail.h"
#import "MyPastEACell.h"
#import "DashBoardCell.h"

@interface EAPastVC () {
    int currentPage;
    BOOL isMore;
    /**
     * Pull to refresh manager
     */
    DashBoardFeed *currentEA;
    CurrentEADetail *currentEADetail;
    UILabel *eaCountsLbl;
    NSMutableArray *dataArray;
    User *user;
    
}
@property (weak, nonatomic) IBOutlet UILabel *pastEALabel;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

@implementation EAPastVC

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 1;
    isMore = YES;
    currentEA = nil;
    dataArray = [NSMutableArray new];
    self.tableView.estimatedRowHeight = 350;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView sizeToFit];
    self.tableView.contentInset = UIEdgeInsetsMake(-18, 0, 0, 0);
    
    [self.tableView reloadData];
    user = [UserManager sharedManager].activeUser;
    [self getPastEA];
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.title = @"";
    [self.tableView reloadData];
    [self removeTableViewRefresher];
}


- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"MyPastEACelly";
    MyPastEACell *cell = (MyPastEACell *)[tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    
    if (cell == nil)
        cell = [MyPastEACell cell];
    DashBoardFeed *dash = dataArray[indexPath.row];
    dash.user = user;
    cell.delegateView = self;
    [cell setData:dash AndUser:user];
     return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.selectedUser=user;
    viewController.eaData = dataArray[indexPath.row];
    viewController.isPast = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

- (void)refresh {
    [self getPastEA];
}

- (void)removeTableViewRefresher {
    [self.tableView.bottomRefreshControl beginRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - web service
- (void)getPastEA {
    //selectedUser
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@", user.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", user.userId] forKey:@"subordinateId"];
    [loginData setObject:[NSString stringWithFormat:@"%d", currentPage] forKey:@"pageNo"];
    
    [loginData setObject:[NSString stringWithFormat:@"1"] forKey:@"flag"];

    [DashBoardFeedManager fetchPastEaList:loginData completionHandler:^(id response, NSError *error) {
        [self.tableView.bottomRefreshControl endRefreshing];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(DashBoardFeed *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.pastEffectiveActionId == %@", comt.pastEffectiveActionId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    NSLog(@"Sub Past %@", filtered);
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                currentPage = (int)(dataArray.count / 10) + 1;
                isMore = YES;
                [_tableView reloadData];
            }
            
            if(dataArray.count)
                _noDataLbl.hidden = YES;
            eaCountsLbl.text = [NSString stringWithFormat:@"Past effective actions (%lu)",(unsigned long)dataArray.count];
        }
    }];
    _noDataLbl.hidden = YES;
}

@end
