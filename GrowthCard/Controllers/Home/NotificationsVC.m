//
//  NotificationsVC.m
//  LeftMenuDemo
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "NotificationsVC.h"
#import "NotificationCell.h"

@interface NotificationsVC () {
    NSMutableArray *notifications;
    __weak IBOutlet UILabel *lblEmptyData;
    UIRefreshControl *refreshCtrl;
    int currentPage;
    BOOL isMore;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

@implementation NotificationsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    notifications=[NSMutableArray new];
    currentPage = 1;
    isMore = YES;
    //Bottom to refresh page
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableview.bottomRefreshControl = refreshControl;
    // Pull to refresh
    refreshCtrl = [[UIRefreshControl alloc] init];
    [refreshCtrl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableview addSubview:refreshCtrl];
    self.tableview.estimatedRowHeight = 80;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.tableFooterView = [UIView new];
    self.tableview.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = false;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_BATCH object:nil];
    [self fetchDataFromServerWithRefresh:YES AndPageNo:1];
    [self removeTableViewRefresher];
    [UserPreferences saveBatchCount:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_BATCH object:nil];
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}

- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)handleRefresh:(id)sender{
    [self fetchDataFromServerWithRefresh:YES AndPageNo:1];
}

- (void)fetchDataFromServerWithRefresh:(BOOL) isRefresh AndPageNo:(NSInteger)index {
    currentPage = 1;
    if(notifications.count)
        [notifications removeAllObjects];
    [self getNotificationList];
}

#pragma mark - Private Merthods
#pragma mark - Private Merthods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [notifications count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationInfo *notificationInfo=[notifications objectAtIndex:indexPath.row];
    static NSString *cellIdentifier=@"NotificationCell";
    NotificationCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    [cell SetCellData:notificationInfo];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationInfo *notificationInfo=[notifications objectAtIndex:indexPath.row];
    notificationInfo.readStatus=[NSNumber numberWithInt:1];
    NotificationController *notificationCtrlObj=[NotificationController sharedController];
    [notificationCtrlObj recievedUserInfo:notificationInfo];
    [self.tableview reloadData];
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
    [self getNotificationList];
}

- (void)removeTableViewRefresher {
    [self.tableview.bottomRefreshControl beginRefreshing];
    [self.tableview.bottomRefreshControl endRefreshing];
}

#pragma mark - Web Service
- (void)getNotificationList {
    User *user = [UserManager sharedManager].activeUser;
    if ((!(user.userTeamId.integerValue == 0)) || (user.userTeamId != nil)) {
        NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
        [loginData setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
        
        [loginData setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
        
        [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
        [NotificationInfoManager fetchNofificationList:loginData completionHandler:^(id response, NSError *error) {
            [GMDCircleLoader hideFromView:self.view animated:YES];
            [self.tableview.bottomRefreshControl endRefreshing];
            [refreshCtrl endRefreshing];
            if(!error) {
                isMore = NO;
                NSArray *arr = (NSArray *) response;
                if(arr.count) {
                    [notifications addObjectsFromArray:arr];
                    currentPage = (int)(notifications.count / 10) + 1;
                    
                    isMore = YES;
                    [self.tableview reloadData];
                }
            }
            if(notifications.count)
                _noDataLbl.hidden = YES;
        }];
    }
    _noDataLbl.hidden = YES;
}

@end
