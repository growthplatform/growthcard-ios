//
//  EASegmentedVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EASegmentedVC.h"
#import "EACurrentVC.h"
#import "EAPastVC.h"



@interface EASegmentedVC ()
{
    EACurrentVC *embedEACurrentVC;
    EACurrentVC *embedEAPastVC;

}
@property (weak, nonatomic) IBOutlet UIView *SubContainer;
@property (weak, nonatomic) IBOutlet UIView *SubMasterContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrent;
@property (weak, nonatomic) IBOutlet UIButton *btnPast;
@end

@implementation EASegmentedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect rect=self.SubContainer.frame;
    rect.size.width=[UIScreen mainScreen].bounds.size.width;
    self.SubContainer.frame=rect;
    self.SubMasterContainer.frame=rect;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"EVCurrentVC"]) {
        embedEACurrentVC = segue.destinationViewController;
        embedEACurrentVC.eASegmentedVC = self;

    } else if ([[segue identifier] isEqualToString:@"EVPastVC"]) {
        embedEAPastVC= segue.destinationViewController;
        embedEAPastVC.eASegmentedVC = self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    return YES;
}

- (IBAction)segmentedButtonPressed:(UIButton *)sender {
    
    sender.selected=!sender.selected;
    if (sender.tag == 1) {
        self.btnCurrent.backgroundColor=[UIColor colorWithRed:0.1569 green:0.6392 blue:0.1804 alpha:1.0];
        [self.btnCurrent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

        self.btnPast.backgroundColor=[UIColor colorWithRed:32/255 green:32/255 blue:32/255 alpha:1.0];
        [self.btnPast setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        self.SubContainer.hidden = NO;
        self.SubMasterContainer.hidden = YES;
    }
    else if (sender.tag == 2) {
        
        self.btnPast.backgroundColor=[UIColor colorWithRed:0.1569 green:0.6392 blue:0.1804 alpha:1.0];
        [self.btnPast setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btnCurrent.backgroundColor=[UIColor colorWithRed:32/255 green:32/255 blue:32/255 alpha:1.0];
        [self.btnCurrent setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
        self.SubContainer.hidden = YES;
        self.SubMasterContainer.hidden = NO;
    }
}

@end
