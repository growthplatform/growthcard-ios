//
//  EACurrentVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EACurrentVC.h"
#import "TableViewCell.h"
#import "EAPopUp.h"
#import "DashBoardFeedManager.h"
#import "MyCurrentEAView.h"
#import "UserProfileCell.h"
#import "MYPastEADetailsView.h"
#import "MyCell.h"
#import "OtherCell.h"

#define DEFAULT_TEXT @"Would you like to respond?"

@interface EACurrentVC ()<UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UserProfileCellDelegat,UIViewDelegateSeeMore /*,NIAttributedLabelDelegate*/> {
    int currentPage;
    BOOL isMore;
    int currentCmmntPage;
    DashBoardFeed *currentEA;
    MyCurrentEAView *currentEADetail;
    NSMutableArray *dataArray;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCommentViewHight;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@property (assign,nonatomic) NSInteger numberOffline;
@property (assign,nonatomic) BOOL isExpended;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;

@end



@implementation EACurrentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 1;
    isMore = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
    currentCmmntPage = 1;
    self.numberOffline = 4;
    self.isExpended = NO;
    self.sendBtn.enabled = NO;
    dataArray =  [NSMutableArray new];
    currentEA = nil;
    currentEADetail=[MyCurrentEAView  currentView];
    NSLog(@"%f",self.tableView.tableHeaderView.frame.size.height);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [super addTapGesture];
    self.backBtn.hidden = YES;
    self.menuBtn.hidden = NO;
    if(_isBack) {
        self.backBtn.hidden = NO;
        self.menuBtn.hidden = YES;
    }
    self.txtComment.textColor = [UIColor lightGrayColor];
    self.txtComment.text = DEFAULT_TEXT;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    UIView *header = currentEADetail;
    header.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *headerWidthConstraint = [NSLayoutConstraint
                                                 constraintWithItem:header attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual
                                                 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:size.width
                                                 ];
    [header addConstraint:headerWidthConstraint];
    CGFloat height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    [header removeConstraint:headerWidthConstraint];
    
    header.frame = CGRectMake(0, 0, size.width, height);
    header.translatesAutoresizingMaskIntoConstraints = YES;
    self.tableView.tableHeaderView = header;
    self.tableView.tableHeaderView.frame = header.frame;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self viewWillTransitionToSize:self.tableView.bounds.size withTransitionCoordinator:self];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.navigationBarHidden = YES;;
    self.title = @"";
    [self getCurrentEA];
    self.tableView.tableHeaderView = currentEADetail;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [currentEADetail setUserDataWithEA:nil];
    [self.tableView reloadData];
    [self removeTableViewRefresher];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getCurrentEA)
                                                 name:kRefreshEA
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kUILabelUrlNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kRefreshEA
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUILabelUrlNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;  //set the -35.0f to your required value
        self.view.frame = f;
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNottiff_Hide_Swap_Button object:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNottiff_Show_Swap_Button object:nil];
}

#pragma mark - Controls Events
- (IBAction)btnSendClick:(id)sender {
    [self sendCmmnt];
}

- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification {
    NSMutableArray *message = [notification object];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showUrls:message];
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *comt = [dataArray objectAtIndex:(int)indexPath.row];
    User *user = [UserManager sharedManager].activeUser;
    if (!([user.userId intValue] ==[comt.commentUser.userId intValue]) ) {
        static NSString *dayCellIdentifier = @"OtherCell";
        OtherCell *cell = (OtherCell *)[self.tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
        [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:comt.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
        [cell.profileImageView roundCorner:cell.profileImageView.frame.size.width/2 border:0 borderColor:nil];
        if (comt.commentDesc.length > kSeeMoreCharLimit && !comt.isExpended)
            [tableView addSeeMoreRecognizer:cell.comment AndDesc:comt.commentDesc];
        else
            cell.comment.text=comt.commentDesc;
        
        cell.userDate.text= [NSDate getAgoTimeFromString:comt.commentAtTime];
        cell.userName.text=[NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
        cell.userTitle.text=comt.commentUser.userDesignationTitle;
        return cell;
    }
    else {
        static NSString *dayCellIdentifier = @"MyCell";
        MyCell *cell = (MyCell *)[self.tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
        if (comt.commentDesc.length > kSeeMoreCharLimit && !comt.isExpended)
            [tableView addSeeMoreRecognizer:cell.comment AndDesc:comt.commentDesc];
        else
            cell.comment.text=comt.commentDesc;
        
        cell.userDate.text= [NSDate getAgoTimeFromString:comt.commentAtTime];
        cell.userName.text=[NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
        cell.userTitle.text=@"Me";
        return cell;
    }
}


#pragma mark - See more on table
- (void)tableView:(UITableView *)tableView didSeeMoreClickOnRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *data1 = dataArray[indexPath.row];
    data1.isExpended = YES;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone
     ];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -
#pragma mark - NIAttributedLabelDelegate
- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    if (result.resultType == NSTextCheckingTypeLink) {
        [[UIApplication sharedApplication] openURL:result.URL];
    }
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

- (void)refresh {
    [self fetchCommnt];
}

- (void)removeTableViewRefresher {
    [self.tableView.bottomRefreshControl beginRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger length = [[textField text] length] - range.length + string.length;
    self.sendBtn.enabled = NO;
    if ((int)length > 0)  {
        self.sendBtn.enabled = YES;
    }
    return YES;
}

#pragma mark - UITextViewDelegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    self.sendBtn.enabled = NO;
    self.txtComment.text = @"";
    self.txtComment.textColor = [UIColor blackColor];
    return YES;
}

- (void) textViewDidChange:(UITextView *)textView {
    if(self.txtComment.text.length == 0) {
        self.txtComment.textColor = [UIColor lightGrayColor];
        self.txtComment.text = DEFAULT_TEXT;
        [self.txtComment resignFirstResponder];
        self.sendBtn.enabled = NO;
    }
    self.sendBtn.enabled = NO;
    if ((int)self.txtComment.text.length > 0)  {
        self.sendBtn.enabled = YES;
        if([self.txtComment.text isEqualToString:DEFAULT_TEXT]) {
            self.sendBtn.enabled = NO;
        }
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    self.sendBtn.enabled = YES;
    if((int)self.txtComment.text.length == 0) {
        [self setDefaultTextView];
    }
    if([self.txtComment.text isEqualToString:DEFAULT_TEXT]) {
        self.sendBtn.enabled = NO;
    }
    return YES;
}

- (void)setDefaultTextView {
    self.sendBtn.enabled = NO;
    self.txtComment.text = DEFAULT_TEXT;
    self.txtComment.textColor = [UIColor lightGrayColor];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Web services
- (void)getCurrentEA {
    User *user = [UserManager sharedManager].activeUser;
    if(!(user.userEffectiveActionId.integerValue == 0) || (user.userEffectiveActionId != nil)) {
        NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
        [loginData setObject:[NSString stringWithFormat:@"%@", user.userTeamId] forKey:@"teamId"];
        [loginData setObject:[NSString stringWithFormat:@"%@", user.userEffectiveActionId] forKey:@"effectiveActionId"];
        [loginData setObject:[NSString stringWithFormat:@"%@", user.userId] forKey:@"subordinateId"];
        [loginData setObject:[NSString stringWithFormat:@"%d", currentPage] forKey:@"pageNo"];
        [loginData setObject:[NSString stringWithFormat:@"%d",1] forKey:@"flag"];
        
        [DashBoardFeedManager fetchCurrentEa:loginData completionHandler:^(id response, NSError *error) {
            if(!error) {
                currentEA = (DashBoardFeed *)response;
                currentEA.user = user;
                currentEA.user.graphDetails=currentEA.graphDetails;
                [currentEADetail setUserDataWithEA:currentEA];
                [self addSeeMoreRecognizer:currentEADetail.eaDescLbl AndDesc:currentEA.user.userEffectiveActionDescription AndDelegate:self];
                self.tableView.tableHeaderView = currentEADetail;
                [currentEADetail updateConstraints];
                [self.tableView reloadData];
            }
            [self fetchCommnt];
        }];
    }
    else {
        [currentEADetail setUserData:user];
        [self.tableView reloadData];
    }
}

- (void)seeMoreClick {
    NSLog(@"seeMoreClick by EAcurrentVC");
    [currentEADetail refreshSize];
    [self.tableView reloadData];
}

- (void) fetchCommnt {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userEffectiveActionId] forKey:@"effectiveActionId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",currentCmmntPage] forKey:@"pageNo"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",currentEA.user.userTeamId] forKey:@"teamId"]; // Narender
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager fetchComment:dataDict completionHandler:^(id response, NSError *error) {
        [self.tableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            NSLog(@"Commt arrry %@", response);
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                
                for(Comment *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.commentId == %@", comt.commentId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                currentCmmntPage = (int)(dataArray.count / 10) + 1;
                isMore = YES;
                
                CGPoint contentOffset = self.tableView.contentOffset;
                [self.tableView reloadData];
                [self.tableView layoutIfNeeded];
                [self.tableView setContentOffset:contentOffset];
            }
            if(dataArray.count)
                _noDataLbl.hidden = YES;
        }
    }];
    
    _noDataLbl.hidden = YES;
}

// SendBTN
- (void)sendCmmnt {
    if ([self.txtComment.text isEmptyString]) {
        [self.txtComment becomeFirstResponder];
        return;
    }
    if((currentEA.user.userEffectiveActionId.integerValue == 0) || (currentEA.user.userEffectiveActionId == nil)) {
        [self.view endEditing:YES];
        [self setDefaultTextView];
        [UIAlertController showAlertOn:self withMessage:kNoEAYet andCancelButtonTitle:kOK];
        return;
    }
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",currentEA.user.userEffectiveActionId] forKey:@"effectiveActionId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.txtComment.text] forKey:@"content"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",currentEA.user.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",currentEA.user.userTeamId] forKey:@"teamId"]; // Narender
    if ([user.userRole intValue]==1)
        [dataDict setObject:[NSString stringWithFormat:@"0"] forKey:@"subordinateId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager sendComment:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(error) {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
        else {
            [self.view endEditing:YES];
            [self setDefaultTextView];
            [dataArray insertObject:response atIndex:0];
            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:dataArray];
            dataArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
            [self.tableView reloadData];
        }
    }];
}


@end