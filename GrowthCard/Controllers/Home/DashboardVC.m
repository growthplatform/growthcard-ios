//
//  DashboardVC.m
//  LeftMenuDemo
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DashboardVC.h"
#import "TableViewCell.h"
#import "DashboardVC.h"
#import "DashBoardCell.h"
#import "EAPopUp.h"
#import "AddEAView.h"
#import "RNBlurModalView.h"
#import <QuartzCore/QuartzCore.h>

@interface DashboardVC ()<DashBoardViewDelegate>//UIGestureRecognizerDelegate
{
    NSMutableArray *dataArray;
    User *subordinateUser;
    __weak IBOutlet UIButton *addEaBtn;
    int currentPage;
    BOOL isMore;
    UIRefreshControl *pullRefreshControl;
    UIRefreshControl *refreshControl;
}

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableWidthConstant;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    currentPage = 1;
    isMore = YES;
    
    self.myTableView.estimatedRowHeight = 280;
    self.myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.myTableView.rowHeight = UITableViewAutomaticDimension;
    [self.myTableView sizeToFit];
    [self.myTableView reloadData];
    User *user = [UserManager sharedManager].activeUser;
    if([user.userRole isEqualToNumber:[NSNumber numberWithInt:2]]) {
        [self showNewEaPopOverView];
        self.myTableView.canCancelContentTouches=NO;
    }
    
    refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.myTableView.bottomRefreshControl = refreshControl;
    
    // Initialize the refresh control.
    pullRefreshControl = [[UIRefreshControl alloc] init];
    pullRefreshControl.backgroundColor = [UIColor clearColor];
    pullRefreshControl.tintColor = [UIColor grayColor];
    [pullRefreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:pullRefreshControl];
    
    [self.view bringSubviewToFront:addEaBtn];
    
    [self managerDashboard];
}


- (void)leftViewOpen {
}

- (void)viewDidLayoutSubviews {
    self.tableWidthConstant.constant = self.view.frame.size.width;
    [self.myTableView updateConstraints];
    [self.myTableView layoutIfNeeded];
    [super viewDidLayoutSubviews];
    
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"Dashboard";
    User *user = [UserManager sharedManager].activeUser;
    if (user.userRole.integerValue == UserRoleSubordinate ) {
        addEaBtn.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self removeTableViewRefresher];
}

- (void)showNewEaPopOverView {
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"DashBoardCell";
    DashBoardCell *cell = (DashBoardCell *)[tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    
    if (cell == nil)
        cell = [DashBoardCell cell];
    cell.delegate=self;
    [cell resetFeedWithUser:dataArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
    [self managerDashboard];
}
- (void)pullToRefresh {
    [pullRefreshControl endRefreshing];
    currentPage = 1;
    if(dataArray.count) {
        [dataArray removeAllObjects];
        [self.myTableView reloadData];
    }
    [self managerDashboard];
}

- (void)removeTableViewRefresher {
    [self.myTableView.bottomRefreshControl beginRefreshing];
    [self.myTableView.bottomRefreshControl endRefreshing];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controls Events
- (IBAction)addEAButtonClick:(id)sender {
    NSLog(@"addEAButtonClick");
    AssignEAToVC *viewController = (AssignEAToVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[AssignEAToVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

#pragma mark - web service
- (void)managerDashboard {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchManagerDashboard:dataDict completionHandler:^(id response, NSError *error) {
        [self.myTableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(User *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.userId == %@", comt.userId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                currentPage = (int)(dataArray.count / 10) + 1;
                isMore = YES;
                _noDataLbl.hidden = YES;
                
                [self.myTableView.bottomRefreshControl endRefreshing];
                self.myTableView.bottomRefreshControl = nil;
                [NSTimer scheduledTimerWithTimeInterval:2.0
                                                 target:self
                                               selector:@selector(resetRefreshControl)
                                               userInfo:nil
                                                repeats:NO];
                [self.myTableView reloadData];
            }
            
        }
    }];
    
    _noDataLbl.hidden = YES;
}

- (void)resetRefreshControl {
    self.myTableView.bottomRefreshControl = refreshControl;
}



@end
