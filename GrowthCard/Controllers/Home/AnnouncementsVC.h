//
//  AnnouncementsVC.h
//  LeftMenuDemo
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LandingVC.h"
#import "SlideNavigationController.h"

@interface AnnouncementsVC : LandingVC<SlideNavigationControllerDelegate>

@property (assign, nonatomic) BOOL isSubordinate;

@end
