//
//  AnnouncementsVC.m
//  LeftMenuDemo
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "AnnouncementsVC.h"
#import "SubSegmentedVC.h"
#import "AnnounceCell.h"

@interface AnnouncementsVC () {
    NSMutableArray *dataArray;
    int currentPage;
    BOOL isMore;
    IBOutlet UIButton *addAnnumentBtn;
}
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

@implementation AnnouncementsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = nil;
    dataArray = [NSMutableArray new];
    self.myTableView.estimatedRowHeight = 280;
    self.myTableView.contentInset = UIEdgeInsetsMake(-10, 0, 0, 0);
    self.myTableView.rowHeight = UITableViewAutomaticDimension;
    [self.myTableView sizeToFit];
    [self.myTableView reloadData];
    currentPage = 1;
    isMore = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.myTableView.bottomRefreshControl = refreshControl;
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"Dashboard";
    
    if(self.isSubordinate) {
        [super addBackButton];
        addAnnumentBtn.hidden = YES;
    }
}
- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self removeTableViewRefresher];
    
    if(dataArray.count){
        [dataArray removeAllObjects];
    }
    currentPage = 1;
    [self fetchAnnoucment];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}
- (IBAction)rightMenuClick:(id)sender {
    
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

#pragma mark - UITableViewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"AnnounceCell";
    AnnounceCell *cell = (AnnounceCell *)[self.myTableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    Announcement *data=dataArray[indexPath.row];
    cell.backView.layer.masksToBounds = YES;
    cell.backView.layer.cornerRadius = 3;
    cell.imgView.layer.masksToBounds = YES;
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:data.announceUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    User *user= [UserManager sharedManager].activeUser;
    if ([user.userId intValue] ==[data.announceUser.userId intValue] ) {
        cell.userNameLabel.text=@"You";
    }
    else {
        cell.userNameLabel.text=[NSString stringWithFormat:@"%@ %@",[data.announceUser.userFName pascalCased], [data.announceUser.userLName pascalCased]];
    }
    cell.userNameLabel.text=@"You";
    if ([data.announceUser.userRole intValue]==1)
        cell.userTypeLabel.text=@"Subordinate";
    else
        cell.userTypeLabel.text=@"Manager";
    cell.dateLabel.text=[NSDate getAgoFromString:data.announceTime];
    [cell.dateLabel sizeToFit];
    cell.titleLabel.text=data.announceTitle;
    
    cell.descLabel.numberOfLines = 0;
    cell.descLabel.text=data.announceDesc;
    
    return cell;
}


-(void)readMoreLabelSetup:(ResponsiveLabel *)postDescriptionLabel :(NSString *)str {
    postDescriptionLabel.numberOfLines = 0;
    [postDescriptionLabel setFont:[UIFont systemFontOfSize:14.0]];
    postDescriptionLabel.userInteractionEnabled = YES;

    
    [postDescriptionLabel setUnAttributedTextTapResponder:^{
        NSLog(@"UnAttributedTextTapped full text");
    }];
    //HashTag
    PatternTapResponder hashTagTapAction = ^(NSString *tappedString) {
        NSLog(@"HashTag Tapped = %@",tappedString);
    };
    [postDescriptionLabel enableHashTagDetectionWithAttributes:@{NSForegroundColorAttributeName: [UIColor purpleColor], NSFontAttributeName: [UIFont systemFontOfSize:14.0], RLTapResponderAttributeName:hashTagTapAction}];
    
    //UserHandle
    PatternTapResponder userHandleTapAction = ^(NSString *tappedString){
        NSLog(@"Username Handler Tapped = %@",tappedString);

    };
    [postDescriptionLabel enableUserHandleDetectionWithAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:14.0/255.0 green:84.0/255.0 blue:135.0/255.0 alpha:1], NSFontAttributeName: [UIFont fontWithName:@"Baskerville" size:14.0],RLTapResponderAttributeName:userHandleTapAction}];
    
    //URL
    PatternTapResponder urlTapAction = ^(NSString *tappedString) {
        NSLog(@"URL Tapped = %@",tappedString);
    };
    [postDescriptionLabel enableURLDetectionWithAttributes: @{NSForegroundColorAttributeName:[UIColor lightGrayColor],NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1],RLTapResponderAttributeName:urlTapAction}];
    
    //ReadMore
    NSString *expansionToken = @"...Read More";
    NSMutableAttributedString *attribString = [[NSMutableAttributedString alloc] initWithString:expansionToken attributes:@{NSForegroundColorAttributeName:[UIColor blueColor],NSFontAttributeName:[UIFont systemFontOfSize:13.0],RLTapResponderAttributeName:^(NSString *string) {
        NSLog(@"Tap on read more");
    }}];
    [postDescriptionLabel setAttributedTruncationToken:attribString];
    [postDescriptionLabel setText:str withTruncation:YES];
}





- (void)addReadMoreStringToUILabel:(UILabel*)label :(int)tagValue{
    NSString *readMoreText = @"...Read More";
    NSInteger lengthForString = label.text.length;
    if (lengthForString >= 30) {
        NSInteger lengthForVisibleString = [self fitString:label.text intoLabel:label];
        NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
        NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (label.text.length - lengthForVisibleString)) withString:@""];
        NSInteger readMoreLength = readMoreText.length;
        NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
        NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{
                                                                                                                                        NSFontAttributeName : label.font
                                                                                                                                        }];
        
        NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{NSFontAttributeName: [UIFont fontWithName:kRubik_Bold size:14.0f]}];
        
        [answerAttributed appendAttributedString:readMoreAttributed];
        label.attributedText = answerAttributed;
        
        UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readMoreDidClickedGesture:)];
        readMoreGesture.view.tag = tagValue;
        readMoreGesture.numberOfTapsRequired = 1;
        [label addGestureRecognizer:readMoreGesture];
        
        label.userInteractionEnabled = YES;
    }
    else {
        NSLog(@"No need for 'Read More'...");
    }
}

- (void)readMoreDidClickedGesture:(UITapGestureRecognizer*)sender {
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag); //By tag, you can find out where you had tapped.
}

- (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label {
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = label.frame.size.width;
    CGFloat labelHeight    = label.frame.size.height;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        NSDictionary *attributes = @{ NSFontAttributeName : font };
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
        CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        {
            if (boundingRect.size.height > labelHeight)
            {
                NSUInteger index = 0;
                NSUInteger prev;
                NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                
                do
                {
                    prev = index;
                    if (mode == NSLineBreakByCharWrapping)
                        index++;
                    else
                        index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
                }
                
                while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
                
                return prev;
            }
        }
    }
    else
    {
        if ([string sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height > labelHeight)
        {
            NSUInteger index = 0;
            NSUInteger prev;
            NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            
            do
            {
                prev = index;
                if (mode == NSLineBreakByCharWrapping)
                    index++;
                else
                    index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
            }
            
            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height <= labelHeight);
            
            return prev;
        }
    }
    
    return [string length];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
    [self fetchAnnoucment];
}
- (void)removeTableViewRefresher {
    [self.myTableView.bottomRefreshControl beginRefreshing];
    [self.myTableView.bottomRefreshControl endRefreshing];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)AddAnnoucements:(id)sender {
    SubNewAnnounce *viewController = (SubNewAnnounce *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubNewAnnounce class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - web service
- (void)fetchAnnoucment {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *loginDict = [UserPreferences defaultUserParam];
    [loginDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [loginDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [loginDict setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
    
    [loginDict setObject:[NSString stringWithFormat:@"%@",user.userRole] forKey:@"role"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [AnnouncementManager fetchAnnoucement:loginDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        [self.myTableView.bottomRefreshControl endRefreshing];
        if(!error) {
            if([response isKindOfClass:[NSMutableArray class]]) {
                isMore = NO;
                NSArray *arr = (NSArray *) response;
                if(arr.count) {
                    //[dataArray addObjectsFromArray:arr];
                    
                    for(Announcement *comt in arr) {
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.announceId == %@", comt.announceId];
                        NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                        NSLog(@"Commt arrry %@", filtered);
                        
                        if (filtered.count == 0) {
                            [dataArray addObject:comt];
                        }
                    }
                    currentPage = (int)(dataArray.count / 10) + 1;
                    isMore = YES;
                    [self.myTableView reloadData];
                }
                
                if(dataArray.count)
                    _noDataLbl.hidden = YES;
            }
        }
    }];
}

- (void)deleteAnnoucmentWithId:(id)announcementId AndIndex:(int)idx {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *loginDict = [UserPreferences defaultUserParam];
    [loginDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [loginDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [loginDict setObject:[NSString stringWithFormat:@"%@",announcementId] forKey:@"announcementId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [AnnouncementManager deleteAnnoucement:loginDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [dataArray removeObjectAtIndex:idx];
            [self.myTableView reloadData];
            if(dataArray.count)
                _noDataLbl.hidden = YES;
            
        }
    }];
}

@end
