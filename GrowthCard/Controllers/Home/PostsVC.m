//
//  PostsVC.m
//  GrowthCard
//  Sub-Post
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "PostsVC.h"
#import "SubCreatePostVC.h"
#import "TableViewCell.h"
#import "PostsCell.h"

typedef enum {
    Page_Top = 0,
    Page_Bottom,
} PagePosition;

@interface PostsVC ()<UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *dataArray;
    int currentPageAll;
    int currentPageUser;
    BOOL isAllPost;
    BOOL isMoreAll;
    BOOL isMoreUser;
    NSMutableArray *allPostArray;
    NSMutableArray *myPostArray;
    BOOL _isMoreAllPost;
    BOOL _isMoreCurrentPost;
    UIView *_footerV;
}
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAllPosts;
@property (weak, nonatomic) IBOutlet UIButton *btnMyPosts;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

@implementation PostsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _footerV = [TableViewCell indicatorFooter];
    self.navigationItem.leftBarButtonItem = nil;
    dataArray = [NSMutableArray new];
    allPostArray = [NSMutableArray new];
    myPostArray  = [NSMutableArray new];
    self.myTableView.estimatedRowHeight = 350;
    self.myTableView.rowHeight = UITableViewAutomaticDimension;
    [self.myTableView sizeToFit];
    [self.myTableView addLongPressRecognizer];// Long press Gesture
    isAllPost = YES;
    currentPageAll = 1;
    currentPageUser = 1;
    isMoreAll = YES;
    isMoreUser = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.myTableView.bottomRefreshControl = refreshControl;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"Posts";
    currentPageUser = 1;
    [self fetchPostFrom:Page_Top];
}

- (void)viewDidAppear:(BOOL)animated    {
    [super viewDidAppear:animated];
    [self.myTableView reloadData];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kUILabelUrlNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUILabelUrlNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private methods
- (void)refreshUI {
    [self.myTableView reloadData];
}

- (void)loadAllPosts {
    isAllPost = YES;
    if(!allPostArray.count)
        [self fetchPostFrom:Page_Top];
    else {
        dataArray = [allPostArray mutableCopy];
        [self refreshUI];
    }
}

- (void)loadMyPosts {
    isAllPost = NO;
    if(!myPostArray.count)
        [self fetchPostFrom:Page_Top];
    else {
        dataArray = [myPostArray mutableCopy];
        [self refreshUI];
    }
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification {
    NSMutableArray *message = [notification object];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showUrls:message];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"PostsCell";
    PostsCell *cell = (PostsCell *)[self.myTableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    Comment *data=dataArray[indexPath.row];
    cell.backView.layer.masksToBounds = YES;
    cell.backView.layer.cornerRadius = 3;
    cell.imgView.layer.masksToBounds = YES;
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:data.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    User *user = [UserManager sharedManager].activeUser;
    if ([user.userId intValue] ==[data.commentUser.userId intValue] ) {
        cell.userNameLabel.text=@"Me";
    }
    else {
        cell.userNameLabel.text=[NSString stringWithFormat:@"%@ %@",[data.commentUser.userFName pascalCased], [data.commentUser.userLName pascalCased]];
    }
    cell.userTypeLabel.text=[NSString stringWithFormat:@"%@",data.commentUser.userDesignationTitle];
    [cell.dateLabel sizeToFit];
    cell.dateLabel.text = [NSDate getAgoTimeFromString:data.commentAtTime];
    if (data.commentDesc.length > kSeeMoreCharLimit && !data.isExpended)
        [tableView addSeeMoreRecognizer:cell.descLabel AndDesc:data.commentDesc];
    else
        cell.descLabel.text=data.commentDesc;
    return cell;
}

#pragma mark - See more on table
- (void)tableView:(UITableView *)tableView didSeeMoreClickOnRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *data1 = dataArray[indexPath.row];
    data1.isExpended = YES;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone
     ];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)tableView:(UITableView *)tableView didRecognizeLongPressOnRowAtIndexPath:(NSIndexPath *)indexPath {
    int idx = (int)indexPath.row;
    Comment *data=[dataArray objectAtIndex:idx];
    User *user = [UserManager sharedManager].activeUser;
    if ([user.userId intValue] != [data.commentUser.userId intValue] )
        return;
    
    [UIAlertController showAlertWithOkCancelAction:self withTitle:@"Do you want delete this post?" andOkButtonTitle:kOK andCancelButtonTitle:kCancel withHandler:^(UIAlertAction *okAction, UIAlertAction *cancelAction) {
        if(okAction) {
            if ([user.userId intValue] ==[data.commentUser.userId intValue] ) {
                [self deletePost:data AndPostIndex:idx];
            }
        }
    }];
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

- (void)refresh {
    [self fetchPostFrom:Page_Bottom];
}

#pragma mark - work done slider
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}

#pragma mark - Controls Events
- (IBAction)rightMenuClick:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

- (IBAction)AddNewPosts:(id)sender {
    User *user = [UserManager sharedManager].activeUser;
    if ((!(user.userTeamId.integerValue == 0)) || (user.userTeamId != nil)) {
        SubCreatePostVC *viewController = (SubCreatePostVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubCreatePostVC class]];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else {
        [UIAlertController showAlertOn:self withMessage:kNoTeam andCancelButtonTitle:kOK];
    }
}

- (IBAction)btnAllPostsClick:(id)sender {
    self.btnAllPosts.selected = YES;
    self.btnMyPosts.selected = NO;
    self.btnAllPosts.backgroundColor=[UIColor colorWithRed:0.1569 green:0.6392 blue:0.1804 alpha:1.0];
    [self.btnAllPosts setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.9] forState:UIControlStateNormal];
    self.btnMyPosts.backgroundColor=[UIColor colorWithRed:32/255 green:32/255 blue:32/255 alpha:1.0];
    [self.btnMyPosts setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];

    [self loadAllPosts];
}

- (IBAction)btnMyPostsClick:(id)sender {
    self.btnAllPosts.selected = NO;
    self.btnMyPosts.selected = YES;
    self.btnMyPosts.backgroundColor=[UIColor colorWithRed:0.1569 green:0.6392 blue:0.1804 alpha:1.0];
    [self.btnMyPosts setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.9] forState:UIControlStateNormal];
    self.btnAllPosts.backgroundColor=[UIColor colorWithRed:32/255 green:32/255 blue:32/255 alpha:1.0];
    [self.btnAllPosts setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];

    [self loadMyPosts];
}

#pragma mark - Web Service
- (void)fetchPostFrom:(PagePosition)position {
    int currentPage = 1;
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    if(isAllPost) {
        currentPage = (position == Page_Top)? 1: currentPageAll;
        [dataDict setObject:[NSString stringWithFormat:@"1"] forKey:@"flag"];
        [dataDict setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
    }
    else {
        [dataDict setObject:[NSString stringWithFormat:@"2"] forKey:@"flag"];
        [dataDict setObject:[NSString stringWithFormat:@"%d",currentPageUser] forKey:@"pageNo"];
    }
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"subordinateId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager fetchPost:dataDict completionHandler:^(id response, NSError *error) {
        [self.myTableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            NSArray *arr = (NSArray *) response;
            if(dataArray.count)
                [dataArray removeAllObjects];
            
            if(isAllPost) {
                isMoreAll = NO;
                if(arr.count) {
                    if(position != Page_Top ){
                        currentPageAll++;
                    }else if(allPostArray.count == 0){
                        currentPageAll++;
                    }
                    [self addCommentsFromArray:arr ofPosition:position];
                    isMoreAll = YES;
                }
                [dataArray addObjectsFromArray:allPostArray];
            } else {
                isMoreUser = NO;
                if(arr.count) {
                    [myPostArray addObjectsFromArray:arr];
                    currentPageUser++;
                    isMoreUser = YES;
                }
                [dataArray addObjectsFromArray:myPostArray];
            }
            if(dataArray.count)
                _noDataLbl.hidden = YES;
            
            [self refreshUI];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
    _noDataLbl.hidden = YES;
}

- (void)addCommentsFromArray:(NSArray *)commentList ofPosition:(PagePosition)position {
    if(position == Page_Bottom) {
        [allPostArray addObjectsFromArray:commentList];
        return;
    }
    Comment *lastComment = nil;
    if ([allPostArray count] == 0) {
        [allPostArray addObjectsFromArray:commentList];
        return;
    } else {
        lastComment = allPostArray[0];
    }
    if(lastComment) {
        for(id comment in commentList) {
            if([comment isKindOfClass:[Comment class]] && [[comment commentId] intValue] > [lastComment commentId].intValue)
                [allPostArray insertObject:comment atIndex:0];
        }
    }
}

- (void)deletePost:(Comment *)cmt AndPostIndex:(int)idx {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",cmt.commentId] forKey:@"postId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager deletePost:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [dataArray removeObjectAtIndex:idx];
            if(isAllPost) {
                if([allPostArray containsObject:cmt]) {
                    [allPostArray removeObject:cmt];
                }
            }
            else {
                if([myPostArray containsObject:cmt]) {
                    [myPostArray removeObject:cmt];
                }
            }
            [self refreshUI];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end
