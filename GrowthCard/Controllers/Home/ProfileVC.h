//
//  ProfileVC.h
//  LeftMenuDemo
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@class WorkdayVC;
@class User;
@class UserPreferences;
@interface ProfileVC : LandingVC <SlideNavigationControllerDelegate>

@end
