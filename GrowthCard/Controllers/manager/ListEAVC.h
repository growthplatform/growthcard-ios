//
//  ListEAVC.h
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"

@class User;
@class EAction;
@interface ListEAVC : BaseVC

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) EAction *eAction;


@end
