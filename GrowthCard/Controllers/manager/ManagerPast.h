//
//  ManagerPast.h
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"

@interface ManagerPast : BaseVC

@property (strong, nonatomic) User *selectedUser;

@end
