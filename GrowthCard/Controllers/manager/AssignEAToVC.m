//
//  M_AssignEAToVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "AssignEAToVC.h"
#import "AssignEACell.h"
#import "EAPopUp.h"

@interface AssignEAToVC ()<UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *teamMemberAry;
    NSMutableDictionary *dataDict;
    int currentPage;
    BOOL isMore;
}
@property (weak, nonatomic) IBOutlet UITableView *_tableView;

@end

@implementation AssignEAToVC

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 1;
    isMore = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self._tableView.bottomRefreshControl = refreshControl;
    teamMemberAry = [NSMutableArray new];
    dataDict = [UserPreferences defaultUserParam];
    [super addBackButton];
    [self getSubordinatesList];
    [self callEaOverView];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)callEaOverView {
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"Assign Effective Action to:";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
    self.navigationController.navigationBarHidden = YES;
    [self removeTableViewRefresher];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Control Event
- (IBAction)nextButtonClick:(id)sender {
    ListEAVC *viewController = (ListEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ListEAVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return teamMemberAry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *teamCellIdentifier = @"AssignEACell";
    AssignEACell *cell = (AssignEACell *)[tableView dequeueReusableCellWithIdentifier:teamCellIdentifier];
    if (cell == nil)
        cell = [AssignEACell cell];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell showUserBasicDetails:[teamMemberAry objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *user = [teamMemberAry objectAtIndex:indexPath.row];
    ListEAVC  *viewController = (ListEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ListEAVC class]];
    viewController.user = user;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

- (void)refresh {
    [self getSubordinatesList];
}

- (void)removeTableViewRefresher {
    [__tableView.bottomRefreshControl beginRefreshing];
    [__tableView.bottomRefreshControl endRefreshing];
}

#pragma mark - Web Service
- (void)getSubordinatesList {
    User *user = [UserManager sharedManager].activeUser;
    [dataDict setObject:user.userTeamId forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]getSubordinates:dataDict completionHandler:^(id response, NSError *error) {
        [__tableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(User *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.userId == %@", comt.userId];
                    NSArray *filtered  = [teamMemberAry filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [teamMemberAry addObject:comt];
                    }
                }
                currentPage = (int)(teamMemberAry.count / 10) + 1;
                isMore = YES;
                [__tableView reloadData];
            }
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end
