//
//  ViewOptherProfile.h
//  GrowthCard
//
//  Created by Pawan Kumar on 09/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewOtherProfile : BaseVC
@property (assign, nonatomic)BOOL isBack;
@property (nonatomic, strong) EAction *subUserEa;
@property (strong, nonatomic) User *selectedUser;

@end
