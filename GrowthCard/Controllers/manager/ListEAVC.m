//
//  ListEAVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ListEAVC.h"
#import "EffectiveActionCell.h"
#import "AddEAView.h"

@interface ListEAVC ()<UITableViewDelegate, UITableViewDataSource, AddEAViewDelegate> {
    NSMutableArray *_eaList;
    int currentPage;
    BOOL isMore;
}
@property (weak, nonatomic) IBOutlet UITableView *_tableViewEA;

@end


@implementation ListEAVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentPage = 1;
    isMore = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self._tableViewEA.bottomRefreshControl = refreshControl;
    [super addBackButton];
    _eaList = [NSMutableArray new];
    [self getEaList];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"New Effective Action";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self removeTableViewRefresher];
}


#pragma mark - Control Event
- (IBAction)nextButtonClick:(id)sender {
    CommentEAVC *viewController = (CommentEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[CommentEAVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)createNewEAClicked:(id)sender {
    AddEAView *vw = [AddEAView eaView];
    vw.delegate = self;
    [appDelegate.window addSubview:vw];
}

- (void)addNewEAInTable {
    NSArray *indexPath = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    [__tableViewEA beginUpdates];
    [__tableViewEA insertRowsAtIndexPaths:indexPath withRowAnimation:UITableViewRowAnimationTop];
    [__tableViewEA endUpdates];
}

#pragma mark - UITableViewDelegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _eaList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *EACellIdentifier = @"EACell";
    EffectiveActionCell *cell = (EffectiveActionCell *)[tableView dequeueReusableCellWithIdentifier:EACellIdentifier];
    if (cell == nil)
        cell = [EffectiveActionCell cell];
    
    EAction *action = (EAction *)[_eaList objectAtIndex:indexPath.row];
    [cell setEATitle:action.eaTitle];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentEAVC *vc = (CommentEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[CommentEAVC class]];
    vc.user = self.user;
    vc.eAction = [_eaList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

- (void)refresh {
    [self getEaList];
}

- (void)removeTableViewRefresher {
    [__tableViewEA.bottomRefreshControl beginRefreshing];
    [__tableViewEA.bottomRefreshControl endRefreshing];
}

#pragma mark - AddEADelegates
- (void)didAddEA:(NSString *)str {
    NSLog(@"Add New EA : %@", str);
    [self AddEaToServer:str];
}

#pragma mark - Web Service
- (void)AddEaToServer:(NSString *)eaStr {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:self.user.userDesignationId forKey:@"designationId"];
    [dataDict setObject:eaStr forKey:@"effectiveActionName"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]addEA:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            currentPage = 1;
            if(_eaList.count)
                [_eaList removeAllObjects];
            
            if ([response isKindOfClass:[NSMutableArray class]]) {
                NSMutableArray *arr = (NSMutableArray *)response;
                if(arr.count) {
                    for(EAction *ea in arr) {
                        [_eaList addObject:ea];
                    }
                    [__tableViewEA reloadData];
                }
            }
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

- (void)getEaList {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:self.user.userDesignationId forKey:@"designationId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]getEAList:dataDict completionHandler:^(id response, NSError *error) {
        [__tableViewEA.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(EAction *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.eaId == %@", comt.eaId];
                    NSArray *filtered  = [_eaList filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [_eaList addObject:comt];
                    }
                }
                currentPage = (int)(_eaList.count / 10) + 1;
                
                isMore = YES;
                [__tableViewEA reloadData];
            }
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end
