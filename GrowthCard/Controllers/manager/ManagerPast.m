//
//  ManagerPast.m
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ManagerPast.h"
#import "DashBoardCell.h"
#import "CurrentEADetail.h"
#import "ManagerPastCell.h"
#import "DashBoardFeedManager.h"

@interface ManagerPast () {
    NSMutableArray *dataArray;
    int currentPage;
    BOOL isMore;
}

@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *alphaBtn;
@property (weak, nonatomic) IBOutlet UIButton *latestBtn;
@property (weak, nonatomic) IBOutlet UILabel *noPastLbl;

@end

@implementation ManagerPast

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray new];
    _userNameLbl.text = [NSString stringWithFormat:@"%@ %@", self.selectedUser.userFName, self.selectedUser.userLName];
    _alphaBtn.selected = YES;
    currentPage = 1;
    isMore = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView addLongPressRecognizer];
    [self.tableView sizeToFit];
    [self.tableView reloadData];
    [self getPastEA];
    _noPastLbl.hidden = NO;
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self removeTableViewRefresher];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"ManagerPastCell";
    ManagerPastCell *cell = (ManagerPastCell *)[tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [ManagerPastCell cell];
    if(indexPath.row == 0) {
    }
    [cell setData:dataArray[indexPath.row] AndUser:self.selectedUser];
    return cell;
}

- (void)tableView:(UITableView *)tableView didRecognizeLongPressOnRowAtIndexPath:(NSIndexPath *)indexPath {
    [UIAlertController showAlertWithOkCancelAction:self withTitle:@"Are you sure, you want to delete?" andOkButtonTitle:kOK andCancelButtonTitle:kCancel withHandler:^(UIAlertAction *okAction, UIAlertAction *cancelAction) {
        if(okAction) {
            int idx = (int)indexPath.row;
            DashBoardFeed *feedData = dataArray[idx];
            [self  deletePastEA:feedData AndInedx:idx];
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
    [self getPastEA];
}

- (void)removeTableViewRefresher {
    [self.tableView.bottomRefreshControl beginRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didMoreClicked {
}

- (void)didLikeClicked {
    
}

#pragma mark - Controls Events
- (IBAction)latestBtnClicked:(id)sender {
    _alphaBtn.selected = NO;
    _latestBtn.selected = YES;
    [self resetTableData];
}

- (IBAction)alphaBtnClicked:(id)sender {
    _alphaBtn.selected = YES;
    _latestBtn.selected = NO;
    [self resetTableData];
}

- (IBAction)backBtnClicked:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)defaultShort {
    NSSortDescriptor *srtDesc = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    [dataArray sortUsingDescriptors:[NSArray arrayWithObject:srtDesc]];
    NSLog(@"%@",dataArray);
    [_tableView reloadData];
}

#pragma mark - Controll Event
- (void)resetTableData {
    currentPage = 1;
    [dataArray removeAllObjects];
    [self getPastEA];
}


#pragma mark - Web service
- (void)getPastEA {
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@", self.selectedUser.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", self.selectedUser.userId] forKey:@"subordinateId"];
    [loginData setObject:[NSString stringWithFormat:@"%d", currentPage] forKey:@"pageNo"];
    
    [loginData setObject:[NSString stringWithFormat:@"1"] forKey:@"flag"];
    if(!_alphaBtn.selected) {
        [loginData setObject:[NSString stringWithFormat:@"2"] forKey:@"flag"];
    }
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [DashBoardFeedManager fetchPastEaList:loginData completionHandler:^(id response, NSError *error) {
        [self.tableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            _noPastLbl.hidden = YES;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(DashBoardFeed *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.pastEffectiveActionId == %@", comt.pastEffectiveActionId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    NSLog(@"Manager Past %@", filtered);
                    
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                currentPage = (int)(dataArray.count / 10) + 1;
                
                isMore = YES;
                [self.tableView reloadData];
            }
            if(!dataArray.count) {
                _noPastLbl.hidden = NO;
                _alphaBtn.selected = NO;
            }
        }
    }];
}

// Delete Past EA
- (void)deletePastEA:(DashBoardFeed *)feedData  AndInedx:(int)idx {
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@", self.selectedUser.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", self.selectedUser.userId] forKey:@"subordinateId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", feedData.feedId] forKey:@"effectiveActionId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [DashBoardFeedManager deletePastEaList:loginData completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [dataArray removeObjectAtIndex:idx];
            [_tableView reloadData];
        }
    }];
}


@end
