//
//  M_EditEAVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EditEAVC.h"
#import "ViewOtherProfile.h"


@interface EditEAVC () <UITextViewDelegate> {
__weak IBOutlet UIImageView *userImg;
__weak IBOutlet UILabel *userName;
__weak IBOutlet UILabel *userjobProfile;
__weak IBOutlet UILabel *eaTitleLbl;
__weak IBOutlet UITextView *commntEATxtView;
}

@end

@implementation EditEAVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addBackButton];
    
    UIBarButtonItem *rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Done"
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(rightMenuClicked:)];
    
    [rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor colorWithRed:11.0/255.0 green:159.0/255.0 blue:233.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                               [UIFont fontWithName:kRubik_Regular size:16.0f], NSFontAttributeName, nil]forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem =rightBarButtonItem;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.translucent = YES;
    [super viewWillAppear:animated];
    self.title=@"Edit Effective Action";
    [self fillData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Control Event
- (void)fillData {
    [userImg roundCorner:userImg.frame.size.width/2 border:0 borderColor:nil];
    [userImg sd_setImageWithURL:[NSURL URLWithString:self.user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    userName.text = [NSString stringWithFormat:@"%@ %@",self.user.userFName, self.user.userLName];
    userjobProfile.text = [NSString stringWithFormat:@"%@",self.user.userDesignationTitle];
    eaTitleLbl.text = [NSString stringWithFormat:@"%@",self.eAction.eaTitle];
    commntEATxtView.text = self.eAction.eaDesc;
    [commntEATxtView becomeFirstResponder];
}

- (IBAction)deleteEaClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)changeClicked:(id)sender {
    NSString *str = commntEATxtView.text;
    if(!str.length)
        str = @"";
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:self.user.userId forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.eAction.eaId] forKey:@"effectiveActionId"];
    [dataDict setObject:str forKey:@"effectiveActionDescription"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.user.userTeamId] forKey:@"teamId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]changeEA:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            // GOTO  : subodrinate profile
            ListEAVC  *viewController = (ListEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ListEAVC class]];
            viewController.user = self.user;
            viewController.eAction = self.eAction;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

- (IBAction)rightMenuClicked:(id)sender {
    NSString *str = commntEATxtView.text;
    if(!str.length)
        str = @"";
     [self assignEAWithComment:str];    
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Web service
- (void)assignEAWithComment:(NSString *)str {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:self.user.userId forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.eAction.eaId] forKey:@"effectiveActionId"];
    [dataDict setObject:str forKey:@"effectiveActionDescription"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.user.userTeamId] forKey:@"teamId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]assignEA:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            // GOTO  : subodrinate profile
            ViewOtherProfile  *vc = (ViewOtherProfile *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ViewOtherProfile class]];
            vc.isBack    = YES;
            vc.selectedUser   = self.user;
            vc.subUserEa      = self.eAction;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

@end
