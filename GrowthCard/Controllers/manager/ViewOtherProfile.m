//
//  ViewOptherProfile.m
//  GrowthCard
//
//  Created by Pawan Kumar on 09/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ViewOtherProfile.h"
#import "UserProfileCell.h"
#import "Comment.h"
#import "APPManager.h"
#import "OtherCell.h"
#import "MyCell.h"

#define DEFAULT_TEXT @"Would you like to respond?"

@interface ViewOtherProfile ()<UserProfileCellDelegat, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>
{
    NSMutableArray *dataArray;
    DashBoardFeed *dasboard;
    User *subUserProfileData;;
    int CommntCurrentPage;
    BOOL isMore;
    BOOL keyboardIsShown;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *commntTxtFeld;
@property (weak, nonatomic) IBOutlet UITextView *commntTxtFeld1;
@property (weak, nonatomic) IBOutlet UIView *sendMsgTxtView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@end

@implementation ViewOtherProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sendBtn.enabled = NO;
    dataArray=[NSMutableArray new];
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView sizeToFit];
    CommntCurrentPage = 1;
    isMore = YES;
    refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
    [self fillSubordinateDetails];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    [self fetchCommnt];
    self.commntTxtFeld1.textColor = [UIColor lightGrayColor];
    self.commntTxtFeld1.text = DEFAULT_TEXT;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kUILabelUrlNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated    {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUILabelUrlNotification object:nil];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated {
    [self.view endEditing:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewWillDisappear:animated];
}


#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification {
    NSMutableArray *message = [notification object];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showUrls:message];
}



#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count +1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0) {
        static NSString *dayCellIdentifier = @"UserProfileCell";
        UserProfileCell *cell = (UserProfileCell *)[self.tableView  dequeueReusableCellWithIdentifier:dayCellIdentifier];
        if (cell == nil)
            cell = [UserProfileCell cell];
        cell.delegate = self;
        [cell setCellDataWithSubodnate:self.selectedUser withFeed:dasboard withNumberOfline:0 withTotalCount:0];
        if (self.selectedUser.userEffectiveActionDescription.length> kSeeMoreCharLimit && !self.selectedUser.isExpended)
            [tableView addSeeMoreRecognizer:cell.eaDescLbl AndDesc:self.selectedUser.userEffectiveActionDescription];
        else
            cell.eaDescLbl.text=self.selectedUser.userEffectiveActionDescription;
        return cell;
    }
    else {
        if(dataArray.count == 0)
            return nil;
        Comment *comt = [dataArray objectAtIndex:(int)indexPath.row-1];
        if (comt.commentUser.userRole.integerValue == UserRoleManager) {
            static NSString *dayCellIdentifier = @"OtherCell";
            OtherCell *cell = (OtherCell *)[self.tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
            [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:comt.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
            [cell.profileImageView roundCorner:cell.profileImageView.frame.size.width/2 border:0 borderColor:nil];
            if (comt.commentDesc.length > kSeeMoreCharLimit && !comt.isExpended)
                [tableView addSeeMoreRecognizer:cell.comment AndDesc:comt.commentDesc];
            else
                cell.comment.text=comt.commentDesc;
            
            cell.userDate.text=[NSDate getAgoFromString:comt.commentAtTime];
            cell.userName.text=[NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
            cell.userTitle.text=comt.commentUser.userDesignationTitle;
            return cell;
        }
        else {
            static NSString *dayCellIdentifier = @"MyCell";
            MyCell *cell = (MyCell *)[self.tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
            if (comt.commentDesc.length > kSeeMoreCharLimit && !comt.isExpended)
                [tableView addSeeMoreRecognizer:cell.comment AndDesc:comt.commentDesc];
            else
                cell.comment.text=comt.commentDesc;
            
            cell.userDate.text=[NSDate getAgoFromString:comt.commentAtTime];
            cell.userName.text=[NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
            cell.userTitle.text=comt.commentUser.userDesignationTitle;
            return cell;
        }
    }
    return nil;
}


#pragma mark - See more on table
- (void)tableView:(UITableView *)tableView didSeeMoreClickOnRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *data1 = dataArray[indexPath.row];
    data1.isExpended = YES;    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)offset {
    NSLog(@"offset: %f", offset->y+scrollView.frame.size.height);
    NSLog(@"Scroll view content size: %f", scrollView.contentSize.height);
    if (offset->y+scrollView.frame.size.height > scrollView.contentSize.height - 100) {
        NSLog(@"Load new rows when reaches 300px before end of content");
        [self fetchCommnt];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
    [self fetchCommnt];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger length = [[textField text] length] - range.length + string.length;
    self.sendBtn.enabled = NO;
    if ((int)length > 0)  {
        self.sendBtn.enabled = YES;
    }
    return YES;
}

#pragma mark - UITextViewDelegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    self.sendBtn.enabled = NO;
    self.commntTxtFeld1.text = @"";
    self.commntTxtFeld1.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    if(self.commntTxtFeld1.text.length == 0) {
        self.commntTxtFeld1.textColor = [UIColor lightGrayColor];
        self.commntTxtFeld1.text = DEFAULT_TEXT;
        [self.commntTxtFeld1 resignFirstResponder];
        self.sendBtn.enabled = NO;
    }
    self.sendBtn.enabled = NO;
    if ((int)self.commntTxtFeld1.text.length > 0)  {
        self.sendBtn.enabled = YES;
        if([self.commntTxtFeld1.text isEqualToString:DEFAULT_TEXT]) {
            self.sendBtn.enabled = NO;
        }
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    self.sendBtn.enabled = YES;
    if((int)self.commntTxtFeld1.text.length == 0) {
        [self setDefaultTextView];
    }
    if([self.commntTxtFeld1.text isEqualToString:DEFAULT_TEXT]) {
        self.sendBtn.enabled = NO;
    }
    return YES;
}

- (void)setDefaultTextView {
    self.sendBtn.enabled = NO;
    self.commntTxtFeld1.text = DEFAULT_TEXT;
    self.commntTxtFeld1.textColor = [UIColor lightGrayColor];
}

#pragma mark - UITableCell delegate
- (void)didBtnAssignEAClick:(DashBoardFeed *)info {
    [self assignNewBtnClicked:nil];
}

- (void)didbtnEditEAClick:(DashBoardFeed *)info {
}

#pragma mark - Controle Event
- (IBAction)assignNewBtnClicked:(id)sender {
    ListEAVC  *vc = (ListEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ListEAVC class]];
    vc.user = self.selectedUser;
    [self.navigationController pushViewController:vc animated:YES];}

- (IBAction)editBtnClicked:(id)sender {
    EAction *ea = [EAction new];
    ea.eaId = self.selectedUser.userEffectiveActionId;
    if(ea.eaId == nil) {
        [UIAlertController showAlertOn:self withMessage:@"No effective actions" andCancelButtonTitle:kOK];
        return ;
    }
    ea.eaTitle = self.selectedUser.userEffectiveActionTitle;
    ea.eaDesc = self.selectedUser.userEffectiveActionDescription;
    
    EditEAVC *vc = (EditEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[EditEAVC class]];
    vc.user = self.selectedUser;
    vc.eAction = ea;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backBtnClicked:(id)sender {
    [self backClicked:sender];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Web Service
- (void)fillSubordinateDetails {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userTeamId] forKey:@"teamId"]; // Manager Dashboard
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userId] forKey:@"subordinateId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]fetchSubordinate:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            self.selectedUser = (User *)response;
            [_tableView reloadData];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}

// SendBTN
- (IBAction)sendComment:(id)sender {
    if ([self.commntTxtFeld1.text isEmptyString]) {
        [self.commntTxtFeld1 becomeFirstResponder];
        return;
    }
    self.sendBtn.enabled = NO;
    User *user = [UserManager sharedManager].activeUser;
    if((user.userEffectiveActionId.integerValue == 0) || (user.userEffectiveActionId == nil)) {
        [self.view endEditing:YES];
        [self setDefaultTextView];
        [UIAlertController showAlertOn:self withMessage:kNoEAYet andCancelButtonTitle:kOK];
        return;
    }
    
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userEffectiveActionId] forKey:@"effectiveActionId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.commntTxtFeld1.text] forKey:@"content"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userId] forKey:@"subordinateId"]; // Send Selected user ID
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userTeamId] forKey:@"teamId"]; // Narender
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager sendComment:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(error) {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
        else {
            [self.view endEditing:YES];
            [self setDefaultTextView];
            [dataArray insertObject:response atIndex:0];
            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:dataArray];
            dataArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
            [self.tableView reloadData];
        }
    }];
}

- (void) fetchCommnt {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userEffectiveActionId] forKey:@"effectiveActionId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",CommntCurrentPage] forKey:@"pageNo"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager fetchComment:dataDict completionHandler:^(id response, NSError *error) {
        [self.tableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(Comment *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.commentId == %@", comt.commentId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                CommntCurrentPage = (int)(dataArray.count / 10) + 1;
                
                isMore = YES;
                [self.tableView.bottomRefreshControl endRefreshing];
                self.tableView.bottomRefreshControl = nil;
                
                [NSTimer scheduledTimerWithTimeInterval:2.0
                                                 target:self
                                               selector:@selector(resetRefreshControl)
                                               userInfo:nil
                                                repeats:NO];
                
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)resetRefreshControl {
    self.tableView.bottomRefreshControl = refreshControl;
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGFloat keyboardOffset = -keyboardSize.height;
    if([APPManager isIphoneSixPlus] || [APPManager isIphoneSixe]) {
        keyboardOffset = -keyboardSize.height + 35.0 ;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = keyboardOffset;  //set the -35.0f to your required value
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}


#pragma mark - Gesture
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if(![touch.view isKindOfClass:[UserProfileCell class]]){
        NSLog(@"Touches Work ");
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}



@end
