//
//  M_EditEAVC.h
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"

@interface EditEAVC : BaseVC

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) EAction *eAction;

@end
