//
//  M_SubordinateVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubordinateVC.h"

@interface SubordinateVC ()

@end

@implementation SubordinateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame=CGRectMake(0,0,22,15);
    [backButton setTitle:@" " forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"Back_arrow"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated  {
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
    self.title=@"Assign Effective Action to:";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - Control Event
- (IBAction)nextButtonClick:(id)sender {
    EditEAVC *viewController = (EditEAVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[EditEAVC class]];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)leftMenuClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
