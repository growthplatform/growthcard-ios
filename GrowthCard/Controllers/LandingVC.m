//
//  LandingVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 26/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "LandingVC.h"
#import "SubCreatePostVC.h"
#import "EACurrentVC.h"

@interface LandingVC ()<UIGestureRecognizerDelegate> {
    UIView *_leftView;
    UIView *_rightView;
    int pageWidth;
    int pageHight;
    BOOL isMenuOpen;
    NSTimer *myTimer;
    int firstX;
    int firstY;
}
@end

@implementation LandingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.userInteractionEnabled = YES;
    isMenuOpen=false;
    pageWidth=[[UIScreen mainScreen] applicationFrame].size.width;
    pageHight=[[UIScreen mainScreen] applicationFrame].size.height+20;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideSwapButtons:) name:kNottiff_Hide_Swap_Button object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSwapButtons:) name:kNottiff_Show_Swap_Button object:nil];
    [self registerSwype];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNottiff_Hide_Swap_Button object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNottiff_Show_Swap_Button object:nil];
}

- (void)registerSwype {
    User *user = [UserManager sharedManager].activeUser;
    if([user.userRole isEqualToNumber:[NSNumber numberWithInt:2]]) {
        [self createSlideMune];
        [self addSwipeGesture];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)createSlideMune {
    _leftView=[[UIView alloc]initWithFrame:CGRectMake(-pageWidth-70, 0, pageWidth+70, pageHight)];//+40
    [self.view addSubview:_leftView];
    
    UIView * _leftViewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, pageWidth, pageHight)];
    [_leftViewBackGround setBackgroundColor:[UIColor colorWithRed:0.149 green:0.6941 blue:0.2157 alpha:1.0]];
    [_leftView addSubview:_leftViewBackGround];
    
    UIImageView *leftIndicaterArrow =[[UIImageView alloc] initWithFrame:CGRectMake(pageWidth,0,70,pageHight)];
    leftIndicaterArrow.image=[UIImage imageNamed:@"green_slide"];
    [leftIndicaterArrow setContentMode:UIViewContentModeScaleToFill];
    [_leftView addSubview:leftIndicaterArrow];
    
    UIImageView *leftIndicaterImage =[[UIImageView alloc] initWithFrame:CGRectMake(pageWidth+60,(pageHight/2)-25,50,50)];
    leftIndicaterImage.image=[UIImage imageNamed:@"ic_arrow_right"];
    [leftIndicaterImage setContentMode:UIViewContentModeScaleToFill];
    [_leftView addSubview:leftIndicaterImage];
    
    _leftView.userInteractionEnabled=YES;
    _leftViewBackGround.userInteractionEnabled=YES;
    leftIndicaterArrow.userInteractionEnabled=YES;
    {
        UIButton *crossButton = [[UIButton alloc] initWithFrame:CGRectMake(pageWidth-50, 18, 45, 45)];
        [crossButton setImage:[UIImage imageNamed:@"ico_cross"] forState:UIControlStateNormal];
        [crossButton setImage:[UIImage imageNamed:@"ico_cross"] forState:UIControlStateHighlighted];
        [crossButton addTarget:self action:@selector(closeLeftView) forControlEvents:UIControlEventTouchUpInside];
        [_leftViewBackGround addSubview:crossButton];
        
        User *user = [UserManager sharedManager].activeUser;
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 80, pageWidth-10, 150)];
        fromLabel.text = user.userEffectiveActionTitle;
        fromLabel.font = [UIFont fontWithName:kRubik_Medium size:16.0f];
        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        fromLabel.adjustsFontSizeToFitWidth = YES;
        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        fromLabel.backgroundColor = [UIColor clearColor];
        fromLabel.textColor = [UIColor whiteColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        [_leftView addSubview:fromLabel];
    }
    
    UIImageView *circleImageView =[[UIImageView alloc] initWithFrame:CGRectMake((pageWidth/2)-100,(pageHight/2)-100,200,200)];
    circleImageView.image=[UIImage imageNamed:@"circle"];
    [circleImageView setContentMode:UIViewContentModeScaleToFill];
    [_leftView addSubview:circleImageView];
    
    _rightView=[[UIView alloc]initWithFrame:CGRectMake(pageWidth, 0, pageWidth+70, pageHight)];//+40
    [self.view addSubview:_rightView];
    
    UIView * _rightViewBackGround=[[UIView alloc]initWithFrame:CGRectMake(70+40, 0, pageWidth, pageHight)];
    [_rightViewBackGround setBackgroundColor:[UIColor colorWithRed:0.0784 green:0.6353 blue:0.9137 alpha:1.0]];
    [_rightView addSubview:_rightViewBackGround];
    
    UIImageView *rightIndicaterArrow =[[UIImageView alloc] initWithFrame:CGRectMake(40,0,70,pageHight)];
    rightIndicaterArrow.image=[UIImage imageNamed:@"blue_slide"];
    [rightIndicaterArrow setContentMode:UIViewContentModeScaleToFill];
    [_rightView addSubview:rightIndicaterArrow];
    
    UIImageView *rightIndicaterImage =[[UIImageView alloc] initWithFrame:CGRectMake(-40,(pageHight/2)-25,50,50)];
    rightIndicaterImage.image=[UIImage imageNamed:@"ic_comment"];
    [rightIndicaterImage setContentMode:UIViewContentModeScaleToFill];
    [_rightView addSubview:rightIndicaterImage];
    
    _rightView.userInteractionEnabled=YES;
    _rightViewBackGround.userInteractionEnabled=YES;
    rightIndicaterArrow.userInteractionEnabled=YES;
    {
        UIButton *crossButton = [[UIButton alloc] initWithFrame:CGRectMake(pageWidth-70-15, 55, 50, 50)];
        [crossButton setImage:[UIImage imageNamed:@"ico_cross"] forState:UIControlStateNormal];
        [crossButton setImage:[UIImage imageNamed:@"ico_cross"] forState:UIControlStateHighlighted];
        [crossButton addTarget:self action:@selector(closeRightView) forControlEvents:UIControlEventTouchUpInside];
    }
    
    {
        // Cancel
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(15, (60-pageHight)*-1, pageWidth-30, 50)];
        [cancelButton roundCorner:3.0 border:1.0 borderColor:[UIColor colorWithWhite:255 alpha:0.5]];
        [cancelButton setBackgroundColor:[UIColor clearColor]];
        [cancelButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];
        [cancelButton setTitle:kCancel forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rightViewBackGround addSubview:cancelButton];
        
        CGRect frm = cancelButton.frame;
        CGFloat yPos = 60;
        
        // Create a Post
//        UIButton *postButton = [[UIButton alloc] initWithFrame:CGRectMake(15, frm.origin.y-yPos, pageWidth-30, 50)];
//        [postButton setImage:[UIImage imageNamed:@"ic_Post"] forState:UIControlStateNormal];
//        [postButton setImage:[UIImage imageNamed:@"ic_Post"] forState:UIControlStateHighlighted];
//        [postButton roundCorner:3.0 border:1.0 borderColor:[UIColor colorWithWhite:255 alpha:0.8]];
//        [postButton setBackgroundColor:[UIColor clearColor]];
//        [postButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];
//        [postButton setTitle:@"  Create a Post" forState:UIControlStateNormal];
//        [postButton addTarget:self action:@selector(postClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [_rightViewBackGround addSubview:postButton];
//
//        frm = postButton.frame;
        
        // Make a Comment
        UIButton *commentButton = [[UIButton alloc] initWithFrame:CGRectMake(15, frm.origin.y-yPos, pageWidth-30, 50)];
        [commentButton setImage:[UIImage imageNamed:@"ic_Comnt"] forState:UIControlStateNormal];
        [commentButton setImage:[UIImage imageNamed:@"ic_Comnt"] forState:UIControlStateHighlighted];
        [commentButton roundCorner:3.0 border:1.0 borderColor:[UIColor colorWithWhite:255 alpha:0.8]];
        [commentButton setBackgroundColor:[UIColor clearColor]];
        [commentButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];
        [commentButton setTitle:@"  Make a Comment" forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(commentClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rightViewBackGround addSubview:commentButton];
        
        frm = commentButton.frame;
        
        // Create Announcement
        UIButton *announcementButton = [[UIButton alloc] initWithFrame:CGRectMake(15, frm.origin.y-yPos, pageWidth-30, 50)];
        [announcementButton setImage:[UIImage imageNamed:@"ic_Anumnt"] forState:UIControlStateNormal];
        [announcementButton setImage:[UIImage imageNamed:@"ic_Anumnt"] forState:UIControlStateHighlighted];
        [announcementButton roundCorner:3.0 border:1.0 borderColor:[UIColor colorWithWhite:255 alpha:0.8]];
        [announcementButton setBackgroundColor:[UIColor clearColor]];
        [announcementButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];
        [announcementButton setTitle:@"  Create Announcement" forState:UIControlStateNormal];
        [announcementButton addTarget:self action:@selector(announcementClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    [_leftView setAlpha:0.9];
    [_rightView setAlpha:0.9];
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - Private Methods
- (void)addSwipeGesture {
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightViewOpen)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRight.cancelsTouchesInView = NO;
    swipeRight.numberOfTouchesRequired=1;
    swipeRight.delegate=self;
    [self.view addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftMenuOpen)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionRight;
    swipeLeft.cancelsTouchesInView = NO;
    swipeLeft.numberOfTouchesRequired=1;
    swipeLeft.delegate=self;
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)leftMenuOpen {
    if (self.isSlideMenuLock)
        return;
    if (!isMenuOpen) {
        [self showLeftMenu:YES];
    }
}

- (void)rightViewOpen {
    if (self.isSlideMenuLock)
        return;
    if (!isMenuOpen) {
        [self showRightMenu:YES];
    }
}

- (void)handleLeftMenu {
    NSLog(@"handleLeftMenu");
    [self showLeftMenu:YES];
}

- (void)handleRightMenu {
    NSLog(@"handleRightMenu");
    [self showRightMenu:YES];
}

#pragma mark - Menu Bar
- (void)showLeftMenu:(BOOL)show {
    isMenuOpen = show;
    [self.view bringSubviewToFront:_leftView];
    CGRect fr = _leftView.frame;
    fr.origin.x = (isMenuOpen) ? 0:-pageWidth-70;
    [UIView animateWithDuration:0.3 animations:^{
        _leftView.frame = fr;
    } completion:^(BOOL finished) {
        NSLog(@"handleLeftMenu-----Open : %f", _leftView.frame.origin.x);
        if(_leftView.frame.origin.x < 10) {
            myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(closeLeftView) userInfo:nil repeats:YES];
            User *user = [UserManager sharedManager].activeUser;
            if(user.userEffectiveActionId.integerValue == 0) {
                [UIAlertController showAlertOn:self withMessage:kNoEAYet andCancelButtonTitle:kOK];
                return ;
            }
            if([self isWorkingDay]) {
                [self markJob];
            }
            else {
                [UIAlertController showAlertWithOkCancelAction:self withTitle:kAreYouWorkMsg andOkButtonTitle:kYes andCancelButtonTitle:kNo withHandler:^(UIAlertAction *okAction, UIAlertAction *cancelAction) {
                    if(okAction) {
                        [self markJob];
                    }
                }];
            }
        }
    }];
}

- (BOOL)isWorkingDay {
    User *user = [UserManager sharedManager].activeUser;
    NSArray *ary = [self convertToArray:user.userWorkDays];
    NSLog(@"---Mon--- %@", [ary objectAtIndex:0]);
    NSLog(@"---Tue--- %@", [ary objectAtIndex:1]);
    NSLog(@"---Wed--- %@", [ary objectAtIndex:2]);
    NSLog(@"---Thu--- %@", [ary objectAtIndex:3]);
    NSLog(@"---Fri--- %@", [ary objectAtIndex:4]);
    NSLog(@"---Sat--- %@", [ary objectAtIndex:5]);
    NSLog(@"---Sun--- %@", [ary objectAtIndex:6]);
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    int weekday = [comps weekday];
    NSLog(@"------ %d", weekday);
    // Sun = 1
    // Mon = 2
    // Tue = 3
    // Wed = 4
    // Thu = 5
    // Fri = 6
    // Sat = 7
    BOOL isWork = NO;
    
    switch (weekday) {
        case 1:
            if([[ary objectAtIndex:6] isEqual:@"1"])
                isWork = YES;
            break;
        case 2:
            if([[ary objectAtIndex:0] isEqual:@"1"])
                isWork = YES;
            break;
        case 3:
            if([[ary objectAtIndex:1] isEqual:@"1"])
                isWork = YES;
            break;
        case 4:
            if([[ary objectAtIndex:2] isEqual:@"1"])
                isWork = YES;
            break;
        case 5:
            if([[ary objectAtIndex:3] isEqual:@"1"])
                isWork = YES;
            break;
        case 6:
            if([[ary objectAtIndex:4] isEqual:@"1"])
                isWork = YES;
            break;
        case 7:
            if([[ary objectAtIndex:5] isEqual:@"1"])
                isWork = YES;
            break;
            
        default:
            break;
    }
    return isWork;
}

- (NSArray *)convertToArray:(NSString *)str {
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (int i=0; i < str.length; i++) {
        NSString *tmp_str = [str substringWithRange:NSMakeRange(i, 1)];
        [arr addObject:[tmp_str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    return arr;
}

- (void)markJob {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userEffectiveActionId] forKey:@"effectiveActionId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager]eADone:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshEA object:nil];
            if([AppDelegate checkAndResetRandomNum]) {
                NSDictionary *dict = (NSDictionary *)response;
                NotificationInfo *notificationObj = [[NotificationInfo alloc] init];
                notificationObj.type=[NSNumber numberWithInt:kNotificationType_ShowVariablePop];
                notificationObj.image=[dict objectForKey:@"name"];
                notificationObj.markAdhereTitle =[dict objectForKey:@"caption"];
                notificationObj.markAdhereDesc =[dict objectForKey:@"message"];
                NotificationController *notificationCtrlObj=[NotificationController sharedController];
                [notificationCtrlObj recievedUserInfo:notificationObj];
            }
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
    if (_delegate && [_delegate respondsToSelector:@selector(didLeftViewAnimationDone)]) {
        [_delegate didLeftViewAnimationDone];
    }
}

- (void)removeEADoneView {
    static int i = 1;
    if(i == 5) {
        i=0;
        [self closeLeftView];
    }
    i++;
}

- (void)showRightMenu:(BOOL)show {
    isMenuOpen = show;
    [self.view bringSubviewToFront:_rightView];
    CGRect fr = _rightView.frame;
    fr.origin.x = (isMenuOpen) ? -70-40: pageWidth;
    [UIView animateWithDuration:0.3 animations:^{
        _rightView.frame = fr;
    }];
}

- (void)closeLeftView {
    [myTimer invalidate];
    myTimer = nil;
    isMenuOpen=NO;
    CGRect fr = _leftView.frame;
    fr.origin.x = (isMenuOpen) ? 0:-pageWidth-70;
    [UIView animateWithDuration:0.3 animations:^{
        _leftView.frame = fr;
        
    } completion:^(BOOL finished) {
    }];
}

- (void)closeRightView {
    isMenuOpen=NO;
    [self.view bringSubviewToFront:_rightView];
    CGRect fr = _rightView.frame;
    fr.origin.x = (isMenuOpen) ? -70-40: pageWidth;
    [UIView animateWithDuration:0.3 animations:^{
        _rightView.frame = fr;
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Right Menu Button Delegates
- (IBAction)cancelClicked:(id)sender {
    NSLog(@"----------- 1");
    if (_delegate && [_delegate respondsToSelector:@selector(didRightViewCancelClicked)]) {
        [_delegate didRightViewCancelClicked];
    }
    [self showRightMenu:NO];
}

- (IBAction)postClicked:(id)sender {
    NSLog(@"----------- 2");
    if (_delegate && [_delegate respondsToSelector:@selector(didRightViewCreatePostClicked)]) {
        [_delegate didRightViewCreatePostClicked];
    }
    User *user = [UserManager sharedManager].activeUser;
    if ((!(user.userTeamId.integerValue == 0)) || (user.userTeamId != nil)) {
        
        SubCreatePostVC *viewController = (SubCreatePostVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubCreatePostVC class]];
        [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
        [self showRightMenu:NO];
    }
    else {
        [UIAlertController showAlertOn:self withMessage:kNoTeam andCancelButtonTitle:kOK];
    }
}

- (IBAction)commentClicked:(id)sender {
    NSLog(@"----------- 3");
    User *user = [UserManager sharedManager].activeUser;
    if((user.userEffectiveActionId.integerValue == 0) || (user.userEffectiveActionId == nil)) {
        [UIAlertController showAlertOn:self withMessage:kNoEACommt andCancelButtonTitle:kOK];
        return ;
    }
    if (_delegate && [_delegate respondsToSelector:@selector(didRightViewMakeComntClicked)]) {
        [_delegate didRightViewMakeComntClicked];
    }
    EACurrentVC *viewController = (EACurrentVC *)[[UIStoryboard homeStoryboard] instantiateViewControllerWithClass:[EACurrentVC class]];
    viewController.isBack = YES;
    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
    [self showRightMenu:NO];
}

- (IBAction)announcementClicked:(id)sender {
    NSLog(@"----------- 4");
    if (_delegate && [_delegate respondsToSelector:@selector(didRightViewCreateAnouncmntClicked)]) {
        [_delegate didRightViewCreateAnouncmntClicked];
    }
    [self showRightMenu:NO];
}

#pragma mark - UIGestureDelegates
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    int hight=(pageHight/2)-75;
    CGPoint location = [touch locationInView:self.view];
    if (location.x>50 && location.x<pageWidth-50) {
        return NO;
    }
    if (location.y<hight || location.y>hight+150) {
        return NO;
    }
    NSLog(@"gestureRecognizer %@",NSStringFromCGPoint(location));
    if ([touch.view isKindOfClass:[touch.view class]]) {
    }
    return YES;
}

- (IBAction) someMethod {
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [_rightView addGestureRecognizer:panRecognizer];
}

- (void)move:(id)sender {
    [self.view bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY);
    [[sender view] setCenter:translatedPoint];
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        CGFloat velocityX = (1.2*[(UIPanGestureRecognizer*)sender velocityInView:self.view].x);
        CGFloat finalX = translatedPoint.x + velocityX;
        CGFloat finalY = firstY;
        if (UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])) {
            if (finalX < 0) {
                //finalX = 0;
            } else if (finalX > 768) {
                //finalX = 768;
            }
            if (finalY < 0) {
                finalY = 0;
            } else if (finalY > 1024) {
                finalY = 1024;
            }
        } else {
            if (finalX < 0) {
                //finalX = 0;
            } else if (finalX > 1024) {
                //finalX = 768;
            }
            
            if (finalY < 0) {
                finalY = 0;
            } else if (finalY > 768) {
                finalY = 1024;
            }
        }
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        NSLog(@"the duration is: %f", animationDuration);
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [[sender view] setCenter:CGPointMake(finalX, finalY)];
        [UIView commitAnimations];
    }
}

#pragma mark - Notification -
- (void)hideSwapButtons:(NSNotification *)notiff{
    _leftView.hidden = YES;
    _rightView.hidden = YES;
}

- (void)showSwapButtons:(NSNotification *)notiff{
    _leftView.hidden = NO;
    _rightView.hidden = NO;
}

- (void)setBringToFront {
    [self.view bringSubviewToFront:_leftView];
    [self.view bringSubviewToFront:_rightView];
}

@end
