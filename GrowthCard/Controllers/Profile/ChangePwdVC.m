//
//  ChangePwdVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 05/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ChangePwdVC.h"

@interface ChangePwdVC ()<UITextFieldDelegate> {
    
}
@property (weak, nonatomic) IBOutlet UITextField *oldPwdTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *nPwdTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *confrmPwdTxtFld;

@end

@implementation ChangePwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addTapGesture];
    [super addBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}


- (BOOL)doValidation {
    if ([self.oldPwdTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kOldPwd andCancelButtonTitle:kOK];
        [self.oldPwdTxtFld becomeFirstResponder];
        return NO;
    }
    if ([self.nPwdTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kNewPwd andCancelButtonTitle:kOK];
        [self.nPwdTxtFld becomeFirstResponder];
        return NO;
    }
    if ([self.confrmPwdTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kConfmPwd andCancelButtonTitle:kOK];
        [self.confrmPwdTxtFld becomeFirstResponder];
        return NO;
    }
    
    if (self.nPwdTxtFld.text.length < 6) {
        [UIAlertController showAlertOn:self withMessage:kPwdLength andCancelButtonTitle:kOK];
        [self.nPwdTxtFld becomeFirstResponder];
        return NO;
    }
    if (self.confrmPwdTxtFld.text.length < 6) {
        [UIAlertController showAlertOn:self withMessage:kPwdLength andCancelButtonTitle:kOK];
        [self.confrmPwdTxtFld becomeFirstResponder];
        return NO;
    }
    if (![self.confrmPwdTxtFld.text isEqualToString:self.nPwdTxtFld.text]) {
        [UIAlertController showAlertOn:self withMessage:kPwdNotMatch andCancelButtonTitle:kOK];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextField
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.oldPwdTxtFld) {
        [self.nPwdTxtFld becomeFirstResponder];
        return NO;
    }
    else if (textField == self.nPwdTxtFld) {
        [self.confrmPwdTxtFld becomeFirstResponder];
        return NO;
    }
    else if (textField == self.confrmPwdTxtFld) {
        [textField resignFirstResponder];
        [self submitBtnClicked:nil];
        return NO;
    }
    return YES;
}

#pragma mark - Controls Events
- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitBtnClicked:(id)sender {
    if([self doValidation]) {
        [self changePwd];
    }
}

#pragma mark - webService
- (void)changePwd {
    [self dismissKeyboard];
    NSMutableDictionary *dict = [UserPreferences defaultUserParam];
    [dict setObject:self.oldPwdTxtFld.text forKey:@"oldPassword"];
    [dict setObject:self.nPwdTxtFld.text forKey:@"newPassword"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] changeUserPasswordWithDict:dict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            self.oldPwdTxtFld.text = @"";
            self.nPwdTxtFld.text = @"";
            self.confrmPwdTxtFld.text = @"";
            [UIAlertController showAlertWithAction:self withTitle:kPwdChanged andCancelButtonTitle:kOK withHandler:^(UIAlertAction *action) {
                if(action)
                    [self backBtnClicked:nil];
            }];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}


@end
