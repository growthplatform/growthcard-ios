//
//  SettingsVC.m
//  LeftMenuDemo
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SettingsVC.h"

@interface SettingsVC ()<UITextFieldDelegate, WorkdayVCDelegate, UIActionSheetDelegate> {
    UIButton *doneButton;
    __weak IBOutlet UIImageView *profileimgView;
    __weak IBOutlet UITextField *txtFld_email;
    __weak IBOutlet UITextField *txtFld_fName;
    __weak IBOutlet UITextField *txtFld_lName;
    __weak IBOutlet UITextField *txtFld_workDays;
    __weak IBOutlet UIButton *updatePicBtn;
    
}

@end


@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addTapGesture];
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame=CGRectMake(0,0,22,15);
    [backButton setTitle:@" " forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"Back_arrow"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(leftMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    doneButton = [[UIButton alloc] init];
    doneButton.frame=CGRectMake(0,0,50,30);
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont fontWithName:kRubik_Medium size:13]];
   // [doneButton setTitleColor:[UIColor colorWithRed:33.0/255.0 green:161.0/255.0 blue:227.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorMakeRGB(98,174,232) forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [doneButton addTarget:self action:@selector(btnDoneClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:doneButton];
    [profileimgView roundCorner:profileimgView.frame.size.height/2 border:0 borderColor:nil];
    
    txtFld_workDays.userInteractionEnabled = NO;
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.title=@"Settings";
    [self.navigationController.navigationBar setTitleTextAttributes:
    @{NSForegroundColorAttributeName:[UIColor blackColor],
      NSFontAttributeName:[UIFont fontWithName:kRubik_Medium size:15]}];
    //etFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];
    [self loadUserData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title=@"";
}

- (IBAction)leftMenuClicked:(id)sender {
    [super backButtonClicked:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadUserData {
    User *user = [UserManager sharedManager].activeUser;
    txtFld_email.text = [NSString stringWithFormat:@"%@", user.userEmail];
    txtFld_fName.text = [NSString stringWithFormat:@"%@", user.userFName];
    txtFld_lName.text = [NSString stringWithFormat:@"%@", user.userLName];
    [profileimgView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [self setWorkingDayInTxtFld:user];
}

#pragma mark - Controls Events
- (IBAction)workingDaysBtnClicked:(id)sender {
    WorkdayVC  *vc = (WorkdayVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[WorkdayVC class]];
    vc.isBack = YES;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)termBtnClicked:(id)sender {
    TermsVC  *vc = (TermsVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[TermsVC class]];
    [self.navigationController pushViewController:vc animated:YES];;
}

- (IBAction)tutorilaBtnClicked:(id)sender {
    TutorialVC  *vc = (TutorialVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[TutorialVC class]];
    vc.isBack = YES;
    [self.navigationController pushViewController:vc animated:YES];;
}

- (IBAction)changePwdBtnClicked:(id)sender {
    ChangePwdVC  *vc = (ChangePwdVC *)[[UIStoryboard profileStoryboard] instantiateViewControllerWithClass:[ChangePwdVC class]];
    [self.navigationController pushViewController:vc animated:YES];;
}

- (IBAction)feedbackBtnClicked:(id)sender {
}

- (IBAction)logoutBtnClicked:(id)sender {
    [UIAlertController showAlertWithOkCancelAction:self withTitle:@"Are you sure to logout?" andOkButtonTitle:kOK andCancelButtonTitle:kCancel withHandler:^(UIAlertAction *okAction, UIAlertAction *cancelAction) {
        if(okAction) {
            [self logout];
        }
    }];
}

- (IBAction)btnDoneClick:(id)sender {
    if([self doValidation]) {
        User *user = [UserManager sharedManager].activeUser;
        NSMutableDictionary *userData = [UserPreferences defaultUserParam];
        if(![user.userFName isEqualToString:txtFld_fName.text])
            [userData setObject:txtFld_fName.text forKey:@"firstName"];
        if(![user.userLName isEqualToString:txtFld_lName.text])
            [userData setObject:txtFld_lName.text forKey:@"lastName"];
        if(![user.userEmail isEqualToString:txtFld_email.text])
            [userData setObject:txtFld_email.text forKey:@"email"];
        
        [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
        [self.view setUserInteractionEnabled:NO];
        [[UserManager sharedManager] performUpdateProfile:userData completionHandler:^(id response, NSError *error) {
            [GMDCircleLoader hideFromView:self.view animated:YES];
            [self.view setUserInteractionEnabled:YES];
            if(!error) {
                if(![user.userFName isEqualToString:txtFld_fName.text])
                    user.userFName = txtFld_fName.text;
                if(![user.userLName isEqualToString:txtFld_lName.text])
                    user.userLName = txtFld_lName.text;
                if(![user.userEmail isEqualToString:txtFld_email.text])
                    user.userEmail = txtFld_email.text;
                
                [user saveUserInformation];
                //[UIAlertController showAlertOn:self withMessage:kProfileUpdated andCancelButtonTitle:kOK];
                [UIAlertController showAlertWithAction:self withTitle:kProfileUpdated andCancelButtonTitle:kOK withHandler:^(UIAlertAction *action) {
                    if(action)
                        [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else {
                [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
            }
        }];
    }
}

- (BOOL)doValidation {
    if ([txtFld_email.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterEmail andCancelButtonTitle:kOK];
        [txtFld_email becomeFirstResponder];
        return NO;
    }
    else if (![NSString isEmailValid:txtFld_email.text]) {
        [UIAlertController showAlertOn:self withMessage:kInvalidEmail andCancelButtonTitle:kOK];
        [txtFld_email becomeFirstResponder];
        return NO;
    }
    else if ([txtFld_fName.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kFNameEmpty andCancelButtonTitle:kOK];
        [txtFld_fName becomeFirstResponder];
        return NO;
    }
    else if ([txtFld_lName.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kLNameEmpty andCancelButtonTitle:kOK];
        [txtFld_lName becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (IBAction)updatePicBtnClicked:(id)sender {
    [self didPressAccessoryButton:sender];
}

#pragma mark - Private Method
- (void)setWorkingDayInTxtFld :(User *)user {
    NSArray *days   = @[@"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat", @"Sun"];
    NSString *selectedDaysStr = user.userWorkDays;
    NSMutableArray *array = [NSMutableArray new];
    for (int i = 0; i < [selectedDaysStr length]; i++) {
        NSString *chr = [selectedDaysStr substringWithRange:NSMakeRange(i, 1)];
        if([chr isEqualToString:@"1"]) {
            [array addObject:[days objectAtIndex:i]];
        }
    }
    if(array.count) {
        NSString *string = [array componentsJoinedByString:@", "];
        txtFld_workDays.text = string;
    }
    
}

- (void) getNewWorkingDays:(NSString *)workingDays {
    NSLog(@"Selected working days : %@", workingDays);
    User *user = [UserManager sharedManager].activeUser;
    [user updateUserWorkingDays:workingDays];
    [self setWorkingDayInTxtFld:user];
}


- (void)didPressAccessoryButton:(UIButton *)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Image Source"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Camera", @"Gallery", nil];
    [sheet showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    switch (buttonIndex) {
        case 0:
            [self handleMediaCapture:YES];
            break;
        case 1:
        {
            [self handleMediaCapture:NO];
            break;
        }
    }
}

#pragma mark - Web Service
- (void) handleMediaCapture:(BOOL)isCam {
    [ImagePickManager presentImageSource:isCam onController:self withCompletion:^(BOOL isSelected, UIImage *image) {
        if (isSelected) {
            User *user = [UserManager sharedManager].activeUser;
            NSMutableArray *fields = [NSMutableArray arrayWithObjects:@{@"key" : @"deviceId", @"value" : [UserPreferences deviceUDID]},
                                      @{@"key" : @"userToken", @"value" : [NSString formattedValue:[NSString stringWithFormat:@"%@",user.userToken]]},
                                      @{@"key" : @"userId", @"value" : [NSString formattedValue:[NSString stringWithFormat:@"%@",user.userId]]}, nil];
            NSMutableArray *files = [NSMutableArray arrayWithObjects:@{@"key" : @"image", @"fileName" : @"profileImage.png", @"contentType" : @"image/jpeg", @"data" : UIImageJPEGRepresentation(image, 0.5), }, nil];
            [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
            [[UserManager sharedManager] updateProfileImage:fields AndFileData:files completionHandler:^(id response, NSError *error) {
                [GMDCircleLoader hideFromView:self.view animated:YES];
                if(!error) {
                    [self loadUserData];
                }
            }];
        }
    }];
}

- (void)logout {
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] performUserLogout:loginData completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [UserPreferences sharedPreference].departments = nil;
            if([[UserPreferences sharedPreference] announcement]) {
                [[UserPreferences sharedPreference] clearAnnouncements];
            }
            [[UserManager sharedManager] clearUserTokenAndInformation];
            [APPManager presentLoginViewController];
        }
    }];
}

@end