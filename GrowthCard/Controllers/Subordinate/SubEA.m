//
//  SubEA.m
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubEA.h"
#import "DashBoardCell.h"
#import "CurrentEADetail.h"
#import "PastEaCell.h"
#import "DashBoardFeedManager.h"
#import "SubSegmentedVC.h"

@interface SubEA ()<PastEaCellDelegate, UIGestureRecognizerDelegate>
{
    NSMutableArray *dataArray;
    UILabel *eaCountsLbl;
    int currentPage;
    BOOL isMore;
    CurrentEADetail *currentEADetail;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

@implementation SubEA

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    currentPage = 1;
    isMore = YES;
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
    
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 215 + 20)];
    currentEADetail=[CurrentEADetail  currentEaView];
    currentEADetail.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 180);
    [headerView addSubview:currentEADetail];
    eaCountsLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(headerView.frame) - 45, [UIScreen mainScreen].bounds.size.width, 40)];
    eaCountsLbl.textAlignment = NSTextAlignmentCenter;
    [eaCountsLbl setFont:[UIFont fontWithName:kRubik_Medium size:12.0f]];//value lbl
    eaCountsLbl.text = @"Past effective actions (0)";
    eaCountsLbl.textColor = [UIColor colorWithRed:0.7373 green:0.749 blue:0.7608 alpha:1.0];
    [headerView addSubview:eaCountsLbl];
    
    eaCountsLbl.backgroundColor = [UIColor clearColor];
    _noDataLbl.hidden = NO;
    [self getPastEA];
    self.tableView.tableHeaderView=headerView;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    [self.tableView sizeToFit];
    [self.tableView reloadData];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self viewWillTransitionToSize:self.tableView.bounds.size withTransitionCoordinator:self];
}

- (void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:animated];
    [currentEADetail setUserDataForBasicDetail:self.subSegmentedVC.selectedUser];
    eaCountsLbl.text = [NSString stringWithFormat:@"Past effective actions (%lu)",(unsigned long)dataArray.count];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark -
#pragma mark Private Method
- (void) refreshData {
    [currentEADetail setUserDataForBasicDetail:self.subSegmentedVC.selectedUser];
    [self getPastEA];
}

- (void)refresh {
    [self getPastEA];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"PastEaCell";
    PastEaCell *cell = (PastEaCell *)[tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    if (cell == nil)
        cell = [PastEaCell cell];
    cell.delegate=self;
    [cell setDataDashBoard:dataArray[indexPath.row]];
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    cell.graphView.tag = indexPath.row;
    [cell.graphView addGestureRecognizer:singleFingerTap];
    cell.graphView.userInteractionEnabled=YES;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.selectedUser=self.subSegmentedVC.selectedUser;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)OpenUserProfile:(id)sender {
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    NSLog (@"%ld",[tapRecognizer.view tag]);
    int index = (int)[tapRecognizer.view tag];
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.selectedUser=self.subSegmentedVC.selectedUser;
    viewController.isPast = true;
    viewController.eaData = [DashBoardFeed new];
    viewController.eaData = dataArray[index];
    viewController.eaData.user = self.subSegmentedVC.selectedUser;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Web Service
- (void)getPastEA {
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@", self.subSegmentedVC.selectedUser.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%@", self.subSegmentedVC.selectedUser.userId] forKey:@"subordinateId"];
    [loginData setObject:[NSString stringWithFormat:@"%d", currentPage] forKey:@"pageNo"];
    [loginData setObject:[NSString stringWithFormat:@"1"] forKey:@"flag"];
    [DashBoardFeedManager fetchPastEaList:loginData completionHandler:^(id response, NSError *error) {
        [self.tableView.bottomRefreshControl endRefreshing];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(DashBoardFeed *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.feedId == %@", comt.feedId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                currentPage = (int)(dataArray.count / 10) + 1;
                isMore = YES;
                [_tableView reloadData];
            }
            eaCountsLbl.text = [NSString stringWithFormat:@"Past effective actions (%lu)",(unsigned long)dataArray.count];
            if(dataArray.count)
                _noDataLbl.hidden = YES;
        }
        [self getCurrentEA];
    }];
     _noDataLbl.hidden = YES;
}

- (void)getCurrentEA {
    if((!(self.subSegmentedVC.selectedUser.userEffectiveActionId.integerValue == 0)) || (self.subSegmentedVC.selectedUser.userEffectiveActionId != nil)) {
        NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
        [loginData setObject:[NSString stringWithFormat:@"%@", self.subSegmentedVC.selectedUser.userTeamId] forKey:@"teamId"];
        [loginData setObject:[NSString stringWithFormat:@"%@", self.subSegmentedVC.selectedUser.userEffectiveActionId] forKey:@"effectiveActionId"];
        [loginData setObject:[NSString stringWithFormat:@"%@", self.subSegmentedVC.selectedUser.userId] forKey:@"subordinateId"];
        [loginData setObject:[NSString stringWithFormat:@"%d", currentPage] forKey:@"pageNo"];
        [loginData setObject:[NSString stringWithFormat:@"%d",1] forKey:@"flag"];
        [DashBoardFeedManager fetchCurrentEa:loginData completionHandler:^(id response, NSError *error) {
            if(!error) {
                DashBoardFeed *currentEA = (DashBoardFeed *)response;
                currentEA.user = self.subSegmentedVC.selectedUser;
                currentEA.user.userFName = self.subSegmentedVC.selectedUser.userFName;
                currentEA.user.userLName = self.subSegmentedVC.selectedUser.userLName;
                currentEA.user.objCompany.name = self.subSegmentedVC.selectedUser.objCompany.name;
                currentEA.user.userDeparmentName = self.subSegmentedVC.selectedUser.userDeparmentName;
                currentEA.user.userDesignationTitle = self.subSegmentedVC.selectedUser.userDesignationTitle;
                currentEA.user.userEmail = self.subSegmentedVC.selectedUser.userEmail;
                currentEA.user.graphDetails=currentEA.graphDetails;
                [currentEADetail setUserData:currentEA.user];
            }
        }];
    }
    [self fetchSubordinateUserDetails];
}

- (void)fetchSubordinateUserDetails {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userTeamId] forKey:@"teamId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchSubordinate:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            self.subSegmentedVC.selectedUser = (User*)response;
            currentEADetail.delegateView = self;
            [currentEADetail setUserDataForBasicDetail:self.subSegmentedVC.selectedUser];
        }
    }];
}

@end
