//
//  SubSearchVC.m
//  GrowthCard
//
//  Created by Narender Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubSearchVC.h"
#import "AssignEACell.h"


@interface SubSearchVC ()<UITableViewDataSource, UITableViewDelegate, AssignEACellDelegate, UITextFieldDelegate> {
    NSMutableArray *userArry;
    NSMutableArray *userListArry;
    NSMutableArray *selectedUserArry;
    UIBarButtonItem *rightBarButtonItem;
    NSString *searchStr;
    int currentPage;
    BOOL isMore;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UITextField *searchTxtFld;
    
}
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@end


@implementation SubSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone
                                                                         target:self
                                                                         action:@selector(doneClicked:)];
    
    [rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor colorWithRed:11.0/255.0 green:159.0/255.0 blue:233.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                [UIFont fontWithName:kRubik_Regular size:16.0f], NSFontAttributeName, nil]forState:UIControlStateNormal];
    
    [rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor lightGrayColor], NSForegroundColorAttributeName,
                                                [UIFont fontWithName:kRubik_Regular size:16.0f], NSFontAttributeName, nil]forState:UIControlStateDisabled];
    
    
    self.navigationItem.rightBarButtonItem =rightBarButtonItem;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame=CGRectMake(0,0,16,17);
    [backButton setTitle:@" " forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"ico_cross_b"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    userArry = [NSMutableArray new];
    userListArry = [NSMutableArray new];
    selectedUserArry = [NSMutableArray new];
    searchStr = @"";
    currentPage = 1;
    isMore = YES;
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    _tableView.bottomRefreshControl = refreshControl;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Search Users";
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}


#pragma mark - Controls Events
- (IBAction)doneClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedUserList:)]) {
        [_delegate didSelectedUserList:selectedUserArry];
    }
    [self backBtnClicked:nil];
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return userArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *dayCellIdentifier = @"AssignEACell";
    AssignEACell *cell = (AssignEACell *)[tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    
    if (cell == nil)
        cell = [AssignEACell cell];
    cell.delegate = self;
    User *uu = [userArry objectAtIndex:indexPath.row];
    [cell showUserBasicDetails:uu WithShowButton:YES And:(int)indexPath.row];
    [cell showBtnSelected:NO];
    if([[userListArry objectAtIndex:(int)indexPath.row] isEqualToString:@"1"])
       [cell showBtnSelected:YES];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self selectRow:(int)indexPath.row];
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}
- (void)refresh {
}

#pragma mark - Cell Delagate
- (void)didSelectedBtnClicked:(int)idx {
    [self selectRow:idx];
}

- (void)selectRow:(int)idx {
    if([[userListArry objectAtIndex:idx] isEqualToString:@"0"]) {
        [userListArry replaceObjectAtIndex:idx withObject:@"1"];
    }
    else {
        [userListArry replaceObjectAtIndex:idx withObject:@"0"];
    }
    
    User *uu = [userArry objectAtIndex:idx];
    [selectedUserArry containsObject:uu] ? [selectedUserArry removeObject:uu]: [selectedUserArry addObject:uu];
    
    [_tableView reloadData];
    NSLog(@"%@",selectedUserArry);
    self.navigationItem.rightBarButtonItem.enabled = (selectedUserArry.count);
    if (selectedUserArry.count) {
        [rightBarButtonItem setTitle:[NSString stringWithFormat:@"Done(%lu)",(unsigned long)selectedUserArry.count]];
    }
    else
        [rightBarButtonItem setTitle:@"Done"];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"textField Should Return:"); {
        [textField resignFirstResponder];
        searchStr = textField.text;
        [self searchUser];
    }
    return YES;
}


#pragma mark - web service
- (void)searchUser {
    if ([searchTxtFld.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterSomeText andCancelButtonTitle:kOK];
        [searchTxtFld becomeFirstResponder];
        return;
    }
    if(searchTxtFld.text.length) {
        User *user = [UserManager sharedManager].activeUser;
        NSMutableDictionary *logData = [UserPreferences defaultUserParam];
        [logData setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
        [logData setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
        [logData setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
        [logData setObject:searchTxtFld.text forKey:@"searchText"];
        [[UserManager sharedManager]fetchSearchUser:logData completionHandler:^(id response, NSError *error) {
            [_tableView.bottomRefreshControl endRefreshing];
            if(!error) {
                if([response isKindOfClass:[NSMutableArray class]]) {
                    if(userArry.count) {
                        [userArry removeAllObjects];
                        [userListArry removeAllObjects];
                    }
                    userArry = response;
                    for(id a in userArry) {
                        [userListArry addObject:@"0"];
                    }
                    [_tableView reloadData];
                    if(userArry.count)
                        _noDataLbl.hidden = YES;
                }
            }
            else {
                [UIAlertController showAlertOn:self withMessage:@"No user found" andCancelButtonTitle:kOK];
            }
        }];
    }
    
    _noDataLbl.hidden = YES;
}

@end
