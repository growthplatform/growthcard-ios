//
//  SubCreatePostVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 25/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubCreatePostVC.h"
#import "SubPostOverlayView.h"

@interface SubCreatePostVC () <SubPostOverlayViewDelegate> {
    int selectFlag;
}

@property (weak, nonatomic) IBOutlet UIButton *publishBtn;
@property (weak, nonatomic) IBOutlet UITextView *commentTxtView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLbl;
@property (weak, nonatomic) IBOutlet UITextField *commentText;
@end

@implementation SubCreatePostVC

- (void)viewDidLoad {
    [super viewDidLoad];
    selectFlag = 1;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Controls Events
- (IBAction)btnPublishClick:(id)sender {
    [self.view endEditing:YES];
    SubPostOverlayView *overlayView=[SubPostOverlayView view];
    overlayView.delegate = self;
    [self.view addSubview:overlayView];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SubPostOverlayViewDelegate
- (void)didMyTeamClicked {
    selectFlag = 1;
    [self publicePost:1];
}

- (void)didMyDepartmentClicked {
    selectFlag = 2;
    [self publicePost:2];
}

- (void)didAllClicked {
    selectFlag = 3;
    [self publicePost:3];
}

- (void)textViewDidChange:(UITextView *)textView {
    self.placeholderLbl.hidden = textView.text.length;
}

#pragma mark - Web service
- (void)publicePost:(int)flag {
    [self.view endEditing:YES];
    if ([self.commentTxtView.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterPost andCancelButtonTitle:kOK];
        [self.commentTxtView becomeFirstResponder];
        return;
    }
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:self.commentTxtView.text forKey:@"content"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",selectFlag] forKey:@"flag"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userDeparmentId] forKey:@"departmentId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager createPost:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            self.commentTxtView.text = @"";
            [self backButtonClicked:nil];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
    }];
}


@end
