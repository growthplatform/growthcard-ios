        //
//  SubMasterVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubMasterVC.h"
#import "CurrentEADetail.h"
#import "PastEaCell.h"
#import "GroupMemberCell.h"
#import "SubSegmentedVC.h"
#import "SubordinateAnnunmentCell.h"
#import "SubMaster.h"

@interface SubMasterVC ()<UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *teamArry;
    int currentPage;
    BOOL isMore;
    User *subodrinateUser;
    __weak IBOutlet UIImageView *uerPicImgVew;
    __weak IBOutlet UILabel *nameLbl;
    __weak IBOutlet UILabel *compyLbl;
    __weak IBOutlet UILabel *departmentLbl;
    __weak IBOutlet UILabel *desigtionLbl;
    __weak IBOutlet UILabel *emailLbl;
    __weak IBOutlet UIView *workDaysView;
    __weak IBOutlet UILabel *noTeamLbl;
    __weak IBOutlet UICollectionView *teamCollectionView;
    __weak IBOutlet UILabel *lblMembersCount;
    __weak IBOutlet UILabel *noCommntLbl;
    __weak IBOutlet UITableView *commntTableView;
    CurrentEADetail *currentEADetai;
    int currentPostPage;
    NSMutableArray *postDataAry;
}

@end

@implementation SubMasterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 1;
    currentPostPage = 1;
    isMore = YES;
    noTeamLbl.hidden = YES;
    teamArry = [NSMutableArray new];
    postDataAry = [NSMutableArray new];
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refreshCommtTable) forControlEvents:UIControlEventValueChanged];
    commntTableView.bottomRefreshControl = refreshControl;
    UIRefreshControl *refreshControlTeam = [UIRefreshControl new];
    refreshControlTeam.triggerVerticalOffset = 100.;
    [refreshControlTeam addTarget:self action:@selector(refreshTeamMember) forControlEvents:UIControlEventValueChanged];
    teamCollectionView.bottomRefreshControl = refreshControlTeam;
    CGRect rect=commntTableView.frame;
    rect.size.width=[UIScreen mainScreen].bounds.size.width;
    commntTableView.frame=rect;

    
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 215+40)];
    currentEADetai=[CurrentEADetail  currentEaView];
    currentEADetai.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 215);
    [headerView addSubview:currentEADetai];
    [currentEADetai setUserDataForBasicDetail:self.subSegmentedVC.selectedUser];
    for(UIView *view in [headerView subviews]) {
        if([view isKindOfClass:[CurrentEADetail class]]) {
            noCommntLbl.hidden = YES;
            break;
        }
    }
    commntTableView.rowHeight = UITableViewAutomaticDimension;
    [commntTableView sizeToFit];
    [commntTableView reloadData];
    
    [teamCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([GroupMemberCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([GroupMemberCell class])];
    uerPicImgVew.image=nil;
    nameLbl.text = @"";
    compyLbl.text = @"";
    departmentLbl.text = @"";
    desigtionLbl.text = @"";
    emailLbl.text = @"";
    [self fetchSubordinateUserDetails];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Private Method
- (void)setWorkingDays :(User *)user {
    
    [[workDaysView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSArray *days   = @[@"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat", @"Sun"];
    NSString *selectedDaysStr = user.userWorkDays;
    NSMutableArray *array = [NSMutableArray new];
    for (int i = 0; i < [selectedDaysStr length]; i++) {
        NSString *chr = [selectedDaysStr substringWithRange:NSMakeRange(i, 1)];
        if([chr isEqualToString:@"1"]) {
            [array addObject:[days objectAtIndex:i]];
        }
    }
    if(array.count) {
        float viewWidth     = workDaysView.frame.size.width;
        float tempViewWidth = 40*array.count;
        UIView *showDaysView = [[UIView alloc]initWithFrame:CGRectMake(viewWidth/2-tempViewWidth/2, 0, tempViewWidth, workDaysView.frame.size.height)];
        float xPos = 5;
        for(int i=0; i<array.count; i++) {
            UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(xPos, 10, 30, 20)];
            lbl.text = [array objectAtIndex:i];
            [lbl setFont:[UIFont fontWithName:kRubik_Regular size:14]];
            lbl.textAlignment = NSTextAlignmentCenter;
            [showDaysView addSubview:lbl];
            xPos += 35;
        }
        [workDaysView addSubview:showDaysView];
    }
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return postDataAry.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *comt = postDataAry[indexPath.row];
    
    static NSString *dayCellIdentifier = @"SubMaster";
    SubMaster *cell = (SubMaster *)[commntTableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
    [cell.userImgView sd_setImageWithURL:[NSURL URLWithString:comt.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    cell.userImgView.layer.cornerRadius = cell.userImgView.frame.size.height/2;
    cell.userDecs.text= comt.commentDesc;
    cell.userDate.text=  [NSDate getAgoFromString:comt.commentAtTime];
    User *user = [UserManager sharedManager].activeUser;
    cell.userName.text=  [NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
    if(comt.commentUser.userId.integerValue == user.userId.integerValue)
    cell.userName.text=  [NSString stringWithFormat:@"Me"];
    cell.userTitle.text= comt.commentUser.userDesignationTitle;
    
    return cell;
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

- (void)refreshCommtTable {
    [self fetchPost];
}

- (void)refreshTeamMember {
    [self fetchSubordinateTeamList];
}

-(void) refreshData {
    [self fetchPost];
}

#pragma mark - UICollectionViewDatasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  teamArry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *channelCollectionViewCellIdentifier = @"GroupMemberCell";
    GroupMemberCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:channelCollectionViewCellIdentifier forIndexPath:indexPath];
    
    User *member=(User *)teamArry[indexPath.row];
    [cell configureCellWithMemberInfo:member];
    return cell;
}

#pragma mark - UICollectionViewFlowLayoutDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(70+10, 100);
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


#pragma mark - UICollectionViewDelegate Methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    User *member=(User *)teamArry[indexPath.row];
    self.subSegmentedVC.selectedUser = member;
    if(teamArry.count) {
        lblMembersCount.text = [NSString stringWithFormat:@"(0)"];
        [teamArry removeAllObjects];
        [teamCollectionView reloadData];
        
        currentPage = 1;
        currentPostPage = 1;
        if(postDataAry.count) {
            [postDataAry removeAllObjects];
        }
    }
    [self fetchSubordinateUserDetails];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didMoreClicked {
}
- (void)didLikeClicked {
}

#pragma mark - Web Services
- (void)fetchSubordinateUserDetails {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userTeamId] forKey:@"teamId"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchSubordinate:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        
        if(!error) {
            subodrinateUser = (User*)response;
            [uerPicImgVew sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",subodrinateUser.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
            [uerPicImgVew roundCorner:uerPicImgVew.frame.size.width/2 border:0 borderColor:nil];
            nameLbl.text = [NSString stringWithFormat:@"%@ %@",subodrinateUser.userFName, subodrinateUser.userLName];
            compyLbl.text = [NSString stringWithFormat:@"%@",subodrinateUser.objCompany.name];
            departmentLbl.text = [NSString stringWithFormat:@"%@",subodrinateUser.userDeparmentName];
            desigtionLbl.text = [NSString stringWithFormat:@"%@",subodrinateUser.userDesignationTitle];
            emailLbl.text = [NSString stringWithFormat:@"%@",subodrinateUser.userEmail];
            
            [self setWorkingDays:subodrinateUser];
        }
        [self fetchSubordinateTeamList];
    }];
}

- (void)fetchSubordinateTeamList {
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",currentPage] forKey:@"pageNo"];
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [[UserManager sharedManager] fetchTeamMember:dataDict completionHandler:^(id response, NSError *error) {
        [teamCollectionView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(User *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.userId == %@", comt.userId];
                    NSArray *filtered  = [teamArry filteredArrayUsingPredicate:predicate];
                    NSLog(@"User SubMasterVC arry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [teamArry addObject:comt];
                    }
                }
                currentPage = (int)(teamArry.count / 10) + 1;
                
                
                isMore = YES;
            }
            lblMembersCount.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)teamArry.count];
            noTeamLbl.hidden = YES;
            if (!teamArry.count) {
                noTeamLbl.hidden = NO;
            }
            teamCollectionView.hidden = !noTeamLbl.hidden;
            [teamCollectionView reloadData];
        }
        [self fetchPost];
    }];
}

- (void)fetchPost {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"2"] forKey:@"flag"];
    [dataDict setObject:[NSString stringWithFormat:@"%d",currentPostPage] forKey:@"pageNo"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.subSegmentedVC.selectedUser.userId] forKey:@"subordinateId"];

    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager fetchPost:dataDict completionHandler:^(id response, NSError *error) {
        [commntTableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(Comment *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.commentId == %@", comt.commentId];
                    NSArray *filtered  = [postDataAry filteredArrayUsingPredicate:predicate];
                    NSLog(@"Commt arrry %@", filtered);
                    
                    if (filtered.count == 0) {
                        [postDataAry addObject:comt];
                    }
                }
                currentPostPage = (int)(postDataAry.count / 10) + 1;
                
            }
            noCommntLbl.hidden = YES;
            if(!postDataAry.count) {
                noCommntLbl.hidden = YES;
            }
            [commntTableView reloadData];
        }
        else {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
        
    }];
    noCommntLbl.hidden = YES;
}


@end
