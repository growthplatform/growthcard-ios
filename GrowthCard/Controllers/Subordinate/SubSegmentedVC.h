//
//  SubSegmentedVC.h
//  GrowthCard
//
//  Created by Pawan Kumar on 17/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"

@interface SubSegmentedVC : BaseVC
@property (strong, nonatomic) User *selectedUser;
@property (strong, nonatomic) NSString *pastEffectiveActionId;
@end
