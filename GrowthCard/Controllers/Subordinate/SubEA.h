//
//  SubEA.h
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"

@class SubSegmentedVC;

@interface SubEA : BaseVC

@property (weak, nonatomic) SubSegmentedVC *subSegmentedVC;
-(void) refreshData;

@end
