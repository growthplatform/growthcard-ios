//
//  SubEADetails.h
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"
@class DashBoardFeed;
@class NotificationInfo;
@class DashBoardFeedManager;

@interface SubEADetails : BaseVC
@property (strong, nonatomic) User *selectedUser;
@property (strong, nonatomic) DashBoardFeed *eaData;;
@property (assign, nonatomic) BOOL isPast;
@property (strong, nonatomic) NotificationInfo *notification;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;

@end
