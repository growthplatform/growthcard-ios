//
//  SubCreatePost.m
//  GrowthCard
//
//  Created by Narender Kumar on 23/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubCreatePost.h"

@interface SubCreatePost ()<UITextViewDelegate> {
}
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLbl;

@end


@implementation SubCreatePost

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Create a Post";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Controls Events
- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView {
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
}

@end
