//
//  SubNewAnnounce.m
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubNewAnnounce.h"
#import "SubSearchVC.h"

@interface SubNewAnnounce ()<UITableViewDataSource, UITableViewDelegate, SubSearchVCDelegate, UIGestureRecognizerDelegate> {
    IBOutletCollection(UIButton) NSArray *allBtnClicked;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UITextField *subjectTxtFild;
    __weak IBOutlet UITextView *messageTxtView;
    int selectFlag;
    NSMutableArray *selectedUserArry;
}

@end

@implementation SubNewAnnounce

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    selectedUserArry = [NSMutableArray new];
    UIBarButtonItem *rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Create"
                                                                          style:UIBarButtonItemStyleDone
                                                                         target:self
                                                                         action:@selector(createClicked:)];
    
    [rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor colorWithRed:11.0/255.0 green:159.0/255.0 blue:233.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                [UIFont fontWithName:kRubik_Medium size:15.0f], NSFontAttributeName, nil]forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem =rightBarButtonItem;
    selectFlag = 1;
    UIButton *btn = [allBtnClicked objectAtIndex:0];
    btn.selected = YES;
    [super addBackButton];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"New Annoucement";
    [self.navigationController.title sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                         [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0], NSForegroundColorAttributeName,
                                                         [UIFont fontWithName:kRubik_Bold size:18.0f], NSFontAttributeName, nil]];
    
    if(selectedUserArry.count) {
        for(UIButton *btn in allBtnClicked) {
            btn.selected = NO;
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Private Method
- (void)didSelectedUserList:(NSMutableArray *)arry {
    NSLog(@"Selected user Arry list : %@",arry);
    
    for(User *user in arry) {
        if(![selectedUserArry containsObject:user]) {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF.userId = %@",
                                 user.userId];
            if (![selectedUserArry filteredArrayUsingPredicate:pred].count) {
                [selectedUserArry addObject:user];
            }
        }
    }
    [_tableView reloadData];
}


#pragma mark - UIButton Delegate
- (IBAction)createClicked:(id)sender {
    if ([subjectTxtFild.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterSubject andCancelButtonTitle:kOK];
        [subjectTxtFild becomeFirstResponder];
        return ;
    }
    
    if ([messageTxtView.text isEmptyString]) {
        [UIAlertController showAlertOn:self withMessage:kEnterAnnucmnt andCancelButtonTitle:kOK];
        [messageTxtView becomeFirstResponder];
        return ;
    }
    [self sendAnnouncement];
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectTypeCleicked:(UIButton *)sender {
    for(UIButton *btn in allBtnClicked) {
        btn.selected = NO;
    }
    sender.selected = YES;
    selectFlag = (int)sender.tag;
}

- (IBAction)searchUserClicked:(id)sender {
    SubSearchVC *vc1 = (SubSearchVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubSearchVC class]];
    vc1.delegate = self;
    [self.navigationController pushViewController:vc1 animated:YES];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return selectedUserArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *userCellIdentifier = @"UserCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:userCellIdentifier];
    User *user = [selectedUserArry objectAtIndex:(int)indexPath.row];
    UIImageView *userPicImgVw = (UIImageView *)[cell viewWithTag:10];
    [userPicImgVw roundCorner:userPicImgVw.frame.size.width/2 border:0 borderColor:nil];
    [userPicImgVw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_below"]];
    
    UILabel *nameLbl = (UILabel *)[cell viewWithTag:11];
    nameLbl.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    UILabel *postLbl = (UILabel *)[cell viewWithTag:12];
    postLbl.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [selectedUserArry removeObjectAtIndex:(int)indexPath.row];
    [_tableView reloadData];
}

#pragma mark - send announcement on server
- (void) sendAnnouncement {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *loginData = [UserPreferences defaultUserParam];
    [loginData setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [loginData setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [loginData setObject:[NSString stringWithFormat:@"%@",messageTxtView.text] forKey:@"content"];
    [loginData setObject:[NSString stringWithFormat:@"%@",subjectTxtFild.text] forKey:@"subject"];
    [loginData setObject:[NSString stringWithFormat:@"%d",selectFlag] forKey:@"flag"];
    if(selectedUserArry.count) {
        [loginData setObject:[NSString stringWithFormat:@"0"] forKey:@"flag"];
        NSMutableArray *idArry = [NSMutableArray new];
        for(User *u in selectedUserArry) {
            [idArry addObject:u.userId];
        }
        [loginData setObject:idArry forKey:@"senders"];
    }
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [AnnouncementManager sendAnnoucement:loginData completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            [self backBtnClicked:nil];
        }
        else {
            
        }
    }];
}

#pragma mark - Gesture
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if(![touch.view isKindOfClass:[UITableViewCell class]]){
        NSLog(@"Touches Work ");
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

@end
