//
//  SubSearchVC.h
//  GrowthCard
//
//  Created by Narender Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"

@protocol SubSearchVCDelegate <NSObject>

@optional
- (void)didSelectedUserList:(NSMutableArray *)arry;
@end

@interface SubSearchVC : BaseVC
@property (nonatomic, assign) id <SubSearchVCDelegate> delegate;
@end
