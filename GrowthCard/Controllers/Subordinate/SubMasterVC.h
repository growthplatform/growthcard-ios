//
//  SubMasterVC.h
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseVC.h"
@class SubSegmentedVC;
@interface SubMasterVC : BaseVC

@property (weak, nonatomic) SubSegmentedVC *subSegmentedVC;
@property (nonatomic, strong) EAction *eAction;
-(void) refreshData;

@end


