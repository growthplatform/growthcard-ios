//
//  SubEADetails.m
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubEADetails.h"
#import "CurrentEADetail.h"
#import "Comment.h"
#import "MYPastEADetailsView.h"
#import "DashBoardFeedManager.h"
#import "OtherCell.h"
#import "MyCell.h"

#define DEFAULT_TEXT @"Would you like to respond?"

@interface SubEADetails ()<UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate,UIViewControllerTransitionCoordinator /*, NIAttributedLabelDelegate*/> {
    NSMutableArray *dataArray;
    int CommntCurrentPage;
    BOOL isMore;
    MYPastEADetailsView *currentEADetail;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCommentViewHight;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@end

@implementation SubEADetails

- (void)viewDidLoad {
    [super viewDidLoad];
    CommntCurrentPage = 1;
    isMore = YES;
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
    dataArray = [NSMutableArray new];
    self.txtComment.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if (self.isPast) {
        self.constCommentViewHight.constant=0;
    }
    else {
        self.constCommentViewHight.constant=50;
    }
    currentEADetail=[MYPastEADetailsView  currentView];
    if(self.notification) {
        [self getCurrentEA];
    }
    else {
        [currentEADetail setUserDataWithEA:self.eaData WithUser:[UserManager sharedManager].activeUser];
    }
    
    self.tableView.estimatedRowHeight = 280;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView sizeToFit];
    self.tableView.tableHeaderView=currentEADetail;
    self.tableView.tableHeaderView.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,currentEADetail.frame.size.height);
    self.sendBtn.enabled = NO;
    _noDataLbl.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    [self fetchCommnt]; // Fetch All Commnts
    self.txtComment.textColor = [UIColor lightGrayColor];
    self.txtComment.text = DEFAULT_TEXT;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kUILabelUrlNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated    {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUILabelUrlNotification object:nil];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    UIView *header = currentEADetail;
    header.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *headerWidthConstraint = [NSLayoutConstraint
                                                 constraintWithItem:header attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual
                                                 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:size.width
                                                 ];
    [header addConstraint:headerWidthConstraint];
    CGFloat height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    [header removeConstraint:headerWidthConstraint];
    
    header.frame = CGRectMake(0, 0, size.width, height);
    header.translatesAutoresizingMaskIntoConstraints = YES;
    self.tableView.tableHeaderView = header;
    self.tableView.tableHeaderView.frame = header.frame;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self viewWillTransitionToSize:self.tableView.bounds.size withTransitionCoordinator:self];
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableWithNotification:) name:kRefreshEaDetailTableView object:nil];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [self.txtComment resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRefreshEaDetailTableView object:nil];
}

- (void)refreshTableWithNotification:(NSNotification *)notification {
    self.notification=(NotificationInfo *)notification.object;
    [self.tableView reloadData];
}

- (void)seeMoreClick {
    NSLog(@"seeMoreClick by EAcurrentVC");
    [currentEADetail refreshSize];
    [self.tableView reloadData];
}


#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification {
    NSMutableArray *message = [notification object];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showUrls:message];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *comt = [dataArray objectAtIndex:(int)indexPath.row];
    if(comt) {
        if (comt.commentUser.userRole.integerValue == UserRoleManager) {
             NSString *dayCellIdentifier = @"OtherCell";
            OtherCell *cell = (OtherCell *)[self.tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
            [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:comt.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
            [cell.profileImageView roundCorner:cell.profileImageView.frame.size.width/2 border:0 borderColor:nil];
            if (comt.commentDesc.length > kSeeMoreCharLimit && !comt.isExpended)
                [tableView addSeeMoreRecognizer:cell.comment AndDesc:comt.commentDesc];
            else
                cell.comment.text=comt.commentDesc;
            
            cell.userDate.text=[NSDate getAgoFromString:comt.commentAtTime];
            cell.userName.text=[NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
            cell.userTitle.text=comt.commentUser.userDesignationTitle;
            return cell;
        }
        else {
             NSString *dayCellIdentifier = @"MyCell";
            MyCell *cell = (MyCell *)[self.tableView dequeueReusableCellWithIdentifier:dayCellIdentifier];
            if (comt.commentDesc.length > kSeeMoreCharLimit && !comt.isExpended)
                [tableView addSeeMoreRecognizer:cell.comment AndDesc:comt.commentDesc];
            else
                cell.comment.text=comt.commentDesc;
            
            cell.userDate.text=[NSDate getAgoFromString:comt.commentAtTime];
            cell.userName.text=[NSString stringWithFormat:@"%@ %@", comt.commentUser.userFName, comt.commentUser.userLName];
            cell.userTitle.text=comt.commentUser.userDesignationTitle;
            return cell;
        }
    }
    else
        return nil;
}

#pragma mark - See more on table
- (void)tableView:(UITableView *)tableView didSeeMoreClickOnRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *data1 = dataArray[indexPath.row];
    data1.isExpended = YES;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone
     ];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -
#pragma mark - NIAttributedLabelDelegate
- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    if (result.resultType == NSTextCheckingTypeLink) {
        [[UIApplication sharedApplication] openURL:result.URL];
    }
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient
- (void)refresh {
    [self fetchCommnt];
}

#pragma mark - Paging
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)offset {
    return;
    NSLog(@"offset: %f", offset->y+scrollView.frame.size.height);
    NSLog(@"Scroll view content size: %f", scrollView.contentSize.height);
    
    if (offset->y+scrollView.frame.size.height > scrollView.contentSize.height - 300) {
        NSLog(@"Load new rows when reaches 300px before end of content");
        [self fetchCommnt];
    }
}

#pragma mark -
#pragma mark IBAction
- (IBAction)newSendcommt:(id)sender {
    [self sendComment:sender];
}

// SendBTN
- (IBAction)sendComment:(id)sender {
    if ([self.txtComment.text isEmptyString]) {
        [self.txtComment becomeFirstResponder];
        return;
    }
    [self sendCmmnt];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self sendComment:nil];
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger length = [[textField text] length] - range.length + string.length;
    self.sendBtn.enabled = NO;
    if ((int)length > 0)  {
        self.sendBtn.enabled = YES;
    }
    return YES;
}

#pragma mark - UITextViewDelegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    self.sendBtn.enabled = NO;
    self.txtComment.text = @"";
    self.txtComment.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    if(self.txtComment.text.length == 0) {
        self.txtComment.textColor = [UIColor lightGrayColor];
        self.txtComment.text = DEFAULT_TEXT;
        [self.txtComment resignFirstResponder];
        self.sendBtn.enabled = NO;
    }
    self.sendBtn.enabled = NO;
    if ((int)self.txtComment.text.length > 0)  {
        self.sendBtn.enabled = YES;
        if([self.txtComment.text isEqualToString:DEFAULT_TEXT]) {
            self.sendBtn.enabled = NO;
        }
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    self.sendBtn.enabled = YES;
    if((int)self.txtComment.text.length == 0) {
        [self setDefaultTextView];
    }
    if([self.txtComment.text isEqualToString:DEFAULT_TEXT]) {
        self.sendBtn.enabled = NO;
    }
    return YES;
}

- (void)setDefaultTextView {
    self.sendBtn.enabled = NO;
    self.txtComment.text = DEFAULT_TEXT;
    self.txtComment.textColor = [UIColor lightGrayColor];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y = -keyboardSize.height  ;  //set the -35.0f to your required value
        self.view.frame = viewFrame;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

#pragma mark -
#pragma mark Gesture
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if(![touch.view isKindOfClass:[UITableViewCell class]]){
        NSLog(@"Touches Work ");
        [self.view endEditing:YES];
        if([self.txtComment.text isEqualToString:DEFAULT_TEXT])
            [self.view endEditing:YES];
    }
    
    return NO;
}

#pragma mark -
#pragma mark Web Service
- (void) fetchCommnt {
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    if(self.notification) {
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.teamId] forKey:@"teamId"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.effectiveActionId] forKey:@"effectiveActionId"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.userId] forKey:@"subordinateId"];
        [dataDict setObject:[NSString stringWithFormat:@"%d",CommntCurrentPage] forKey:@"pageNo"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
        [dataDict setObject:[NSString stringWithFormat:@"%s","0"] forKey:@"pastEffectiveActionId"];
        if(self.notification.pastEffectiveActionId != nil) {
           [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.pastEffectiveActionId] forKey:@"pastEffectiveActionId"];
            self.constCommentViewHight.constant=0;
        }
    }
    else {
        [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.eaData.feedId] forKey:@"effectiveActionId"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.selectedUser.userId] forKey:@"subordinateId"];
        [dataDict setObject:[NSString stringWithFormat:@"%d",CommntCurrentPage] forKey:@"pageNo"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
        [dataDict setObject:[NSString stringWithFormat:@"%s","0"] forKey:@"pastEffectiveActionId"];
        if (self.isPast) {
            if(self.eaData.pastEffectiveActionId != nil) {
                NSString *str = [NSString formattedValue:self.eaData.pastEffectiveActionId];
                if(str.length > 0)
                [dataDict setObject:[NSString stringWithFormat:@"%@",self.eaData.pastEffectiveActionId] forKey:@"pastEffectiveActionId"];
            }
        }
    }
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager fetchComment:dataDict completionHandler:^(id response, NSError *error) {
        [self.tableView.bottomRefreshControl endRefreshing];
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(!error) {
            isMore = NO;
            NSArray *arr = (NSArray *) response;
            if(arr.count) {
                for(Comment *comt in arr) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.commentId == %@", comt.commentId];
                    NSArray *filtered  = [dataArray filteredArrayUsingPredicate:predicate];
                    if (filtered.count == 0) {
                        [dataArray addObject:comt];
                    }
                }
                CommntCurrentPage = (int)(dataArray.count / 10) + 1;                
                isMore = YES;
                [self.tableView reloadData];
            }
            _noDataLbl.hidden = YES;
            if(!dataArray.count){
                _noDataLbl.hidden = NO;
            }
        }
    }];
     _noDataLbl.hidden = YES;
}

- (void)sendCmmnt {
    User *user = [UserManager sharedManager].activeUser;
    if((user.userEffectiveActionId.integerValue == 0) || (user.userEffectiveActionId == nil)) {
        [self.view endEditing:YES];
        [self setDefaultTextView];
        [UIAlertController showAlertOn:self withMessage:kNoEAYet andCancelButtonTitle:kOK];
        return;
    }
    NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userTeamId] forKey:@"teamId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userEffectiveActionId] forKey:@"effectiveActionId"];
    if(!user.userEffectiveActionId)
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.eaData.feedId] forKey:@"effectiveActionId"];
    
    [dataDict setObject:[NSString stringWithFormat:@"%@",self.txtComment.text] forKey:@"content"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"subordinateId"];
    [dataDict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    
   // if ([user.userRole intValue]==1)
   // [dataDict setObject:[NSString stringWithFormat:@"0"] forKey:@"subordinateId"];
    
    [GMDCircleLoader setOnView:self.view withTitle:nil animated:YES];
    [CommentManager sendComment:dataDict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self.view animated:YES];
        if(error) {
            [UIAlertController showAlertOn:self withMessage:error.localizedFailureReason andCancelButtonTitle:kOK];
        }
        else {
            [self.view endEditing:YES];
            [self setDefaultTextView];
            [dataArray insertObject:response atIndex:0];
            
            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:dataArray];
            dataArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
            
            [self.tableView reloadData];
        }
    }];
}

- (void)getCurrentEA {
    User *user = [UserManager sharedManager].activeUser;
    if((!(user.userEffectiveActionId.integerValue == 0)) || (user.userEffectiveActionId != nil)) {
        NSMutableDictionary *dataDict = [UserPreferences defaultUserParam];
        [dataDict setObject:[NSString stringWithFormat:@"%d",1] forKey:@"flag"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.teamId] forKey:@"teamId"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.effectiveActionId] forKey:@"effectiveActionId"];
        [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.userId] forKey:@"subordinateId"];
        [dataDict setObject:[NSString stringWithFormat:@"%d",0] forKey:@"pastEffectiveActionId"];
        if((self.notification)  && (self.notification.pastEffectiveActionId != nil)) {
            [dataDict setObject:[NSString stringWithFormat:@"%@",self.notification.pastEffectiveActionId] forKey:@"pastEffectiveActionId"];
            [dataDict setObject:[NSString stringWithFormat:@"%d",2] forKey:@"flag"];
            self.constCommentViewHight.constant=0;
        }
        
        [DashBoardFeedManager fetchCurrentEa:dataDict completionHandler:^(id response, NSError *error) {
            if(!error) {
                if(!error) {
                    DashBoardFeed * currentEA = (DashBoardFeed *)response;
                    currentEA.user = user;
                    currentEA.user.graphDetails=currentEA.graphDetails;
                    [currentEADetail setUserDataWithEA:currentEA WithUser:[UserManager sharedManager].activeUser];
                    self.tableView.tableHeaderView = currentEADetail;
                    [currentEADetail updateConstraints];
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

@end