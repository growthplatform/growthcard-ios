//
//  SubSegmentedVC.m
//  GrowthCard
//
//  Created by Pawan Kumar on 17/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubSegmentedVC.h"

@interface SubSegmentedVC () {
    SubMasterVC *embedSubMasterVC;
    SubEA *embedSubEA;
}

@property (weak, nonatomic) IBOutlet UIView *SubContainer;
@property (weak, nonatomic) IBOutlet UIView *SubMasterContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrent;
@property (weak, nonatomic) IBOutlet UIButton *btnPast;

@end

@implementation SubSegmentedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect rect=self.SubContainer.frame;
    rect.size.width=[UIScreen mainScreen].bounds.size.width;
    self.SubContainer.frame=rect;
    self.SubMasterContainer.frame=rect;
    self.SubContainer.hidden = NO;
    self.SubMasterContainer.hidden = YES;
    self.btnCurrent.selected = YES;
    self.btnPast.selected = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"MasterEAVCSegue"]) {
        embedSubMasterVC = segue.destinationViewController;
        embedSubMasterVC.subSegmentedVC = self;
    } else if ([[segue identifier] isEqualToString:@"MasterVCSegue"]) {
        embedSubEA = segue.destinationViewController;
        embedSubEA.subSegmentedVC = self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Controls Events
- (IBAction)segmentedButtonPressed:(UIButton *)sender {
    sender.selected=!sender.selected;
    if (sender.tag == 1) {
        self.btnCurrent.backgroundColor = [UIColor colorWithRed:0.1569 green:0.6392 blue:0.1804 alpha:1.0];
        self.btnPast.backgroundColor = [UIColor colorWithRed:32/255 green:32/255 blue:32/255 alpha:1.0];
        self.SubContainer.hidden = NO;
        self.SubMasterContainer.hidden = YES;
        self.btnCurrent.selected = YES;
        self.btnPast.selected = NO;
    }
    else if (sender.tag == 2) {
        self.btnPast.backgroundColor=[UIColor colorWithRed:0.1569 green:0.6392 blue:0.1804 alpha:1.0];
        self.btnCurrent.backgroundColor=[UIColor colorWithRed:32/255 green:32/255 blue:32/255 alpha:1.0];
        self.SubContainer.hidden = YES;
        self.SubMasterContainer.hidden = NO;
        self.btnCurrent.selected = NO;
        self.btnPast.selected = YES;
    }
    [embedSubMasterVC refreshData];
    [embedSubEA refreshData];
}

@end
