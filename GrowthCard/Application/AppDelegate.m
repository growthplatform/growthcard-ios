//
//  AppDelegate.m
//  GrowthCard
//
//  Created by Abhishek Tripathi on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "AppDelegate.h"
#import "UserManager.h"
#import "APPManager.h"
#import "SubPostOverView.h"
#import "CurrentEapopUp.h"
#import "FeedsVC.h"
#import "Flurry.h"
#import "Constants.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "OnboardingViewController.h"
#import "OnboardingContentViewController.h"

@interface AppDelegate ()

@end

static NSString * const kUserHasOnboardedKey = @"user_onboarded";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    [APPManager initialStuff];
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    // Random Number for Image
    [AppDelegate setRandomNumber];
    [Fabric with:@[[Crashlytics class]]];
    [Flurry startSession:kFlurryKey];
    
    BOOL userHasOnboarded = [[NSUserDefaults standardUserDefaults] boolForKey:kUserHasOnboardedKey];
    
    if (userHasOnboarded) {
        NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(notification) {
            [self handleLoginAndHomePage];
            [self performSelector:@selector(handleNotification:) withObject:notification afterDelay:1.5];
        }
        else {
            [self performSelector:@selector(handleLoginAndHomePage) withObject:nil afterDelay:1.5];
        }
    } else {
        self.window.rootViewController = [self generateStandardOnboardingVC];
    }
    
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the chanages made on entering the background.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)handleOnboardingCompletion {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserHasOnboardedKey];
    [self performSelector:@selector(handleLoginAndHomePage) withObject:nil afterDelay:0];
}
 // 52 196 241
- (OnboardingViewController *)generateStandardOnboardingVC {
    OnboardingContentViewController *firstPage = [OnboardingContentViewController contentWithTitle:@"Welcome to GrowthCard." body:@"" image:[UIImage imageNamed:@"Logo_login"] buttonText:@"" action:^{  nil; }];
    
    firstPage.titleLabel.textColor = [UIColor colorWithRed:52.0/155 green:196.0/255 blue:241.0/255 alpha:1];
    firstPage.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    firstPage.topPadding = 200;
    
    OnboardingContentViewController *secondPage = [OnboardingContentViewController contentWithTitle:@"When you complete and Effective Action, swipe!" body:@"" image:[UIImage imageNamed:@"Logo_login"] buttonText:@"" action:^{ nil; }];
    secondPage.titleLabel.textColor = [UIColor colorWithRed:52.0/155 green:196.0/255 blue:241.0/255 alpha:1];
    secondPage.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    secondPage.topPadding = 200;
    
    OnboardingContentViewController *thirdPage = [OnboardingContentViewController contentWithTitle:@"To make a comment, swipe left." body:@"" image:[UIImage imageNamed:@"Logo_login"] buttonText:@"" action:^{ nil; }];
    thirdPage.titleLabel.textColor = [UIColor colorWithRed:52.0/155 green:196.0/255 blue:241.0/255 alpha:1];
    thirdPage.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    thirdPage.topPadding = 200;
    
    OnboardingContentViewController *fourthPage = [OnboardingContentViewController contentWithTitle:@"Share patterns you notice." body:@"Use GrowthCard to share what you see, what you need, what you did, and what you know. Then we can all have a shared true picture of what's really happening." image:[UIImage imageNamed:@"Logo_login"] buttonText:@"" action:^{ nil; }];
    fourthPage.titleLabel.textColor = [UIColor colorWithRed:52.0/155 green:196.0/255 blue:241.0/255 alpha:1];
    fourthPage.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    fourthPage.bodyLabel.textColor = [UIColor grayColor];
    fourthPage.bodyLabel.font = [UIFont systemFontOfSize:18];
    fourthPage.topPadding = 100;
    
    OnboardingContentViewController *fifthPage = [OnboardingContentViewController contentWithTitle:@"Share patterns you notice." body:@"Describe complications - and how you handled them! Call attention to a job well done." image:[UIImage imageNamed:@"Logo_login"] buttonText:@"" action:^{ nil; }];
    fifthPage.titleLabel.textColor = [UIColor colorWithRed:52.0/155 green:196.0/255 blue:241.0/255 alpha:1];
    fifthPage.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    fifthPage.bodyLabel.textColor = [UIColor grayColor];
    fifthPage.bodyLabel.font = [UIFont systemFontOfSize:18];
    fifthPage.topPadding = 100;
    
    OnboardingContentViewController *lastPage = [OnboardingContentViewController contentWithTitle:@"Let's Put The Big Picture Together." body:@"" image:[UIImage imageNamed:@"Logo_login"] buttonText:@"Get Started" action:^{
        [self handleOnboardingCompletion];
    }];
    lastPage.titleLabel.textColor = [UIColor colorWithRed:52.0/155 green:196.0/255 blue:241.0/255 alpha:1];
    lastPage.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    lastPage.topPadding = 100;
    [lastPage.actionButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    OnboardingViewController *onboardingVC = [OnboardingViewController onboardWithBackgroundImage:nil contents:@[firstPage, secondPage, thirdPage, fourthPage, fifthPage, lastPage]];
    onboardingVC.shouldMaskBackground = NO;
    onboardingVC.view.backgroundColor = [UIColor whiteColor];
    onboardingVC.shouldFadeTransitions = YES;
    onboardingVC.fadePageControlOnLastPage = YES;
    onboardingVC.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    onboardingVC.pageControl.pageIndicatorTintColor = [UIColor grayColor];
    
    return onboardingVC;
}

- (void)showEApopover {
    static bool isFirstTime = false;
    if(isFirstTime) {
        User *user = [UserManager sharedManager].activeUser;
        
        if([[(UINavigationController *)self.window.rootViewController topViewController] isKindOfClass:[FeedsVC class]]) {
            if (((user.userEffectiveActionId.integerValue != 0) || (user.userEffectiveActionId != nil)) && (user.userRole.integerValue == UserRoleSubordinate)) {
                NSArray *array = [(UINavigationController *)appDelegate.window.rootViewController topViewController].view.subviews;
                NSLog(@"array %@",array);
                for(UIView *tempView in array) {
                    if([tempView isKindOfClass:[CurrentEapopUp class]]) {
                        [tempView removeFromSuperview];
                        break;
                    }
                }
                CurrentEapopUp *currentEa = [CurrentEapopUp currentEaView];
                [[(UINavigationController *)appDelegate.window.rootViewController topViewController].view addSubview:currentEa];
            }
        }
    }
    else {
        isFirstTime = true;
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[(UINavigationController *)appDelegate.window.rootViewController topViewController].view endEditing:YES];
    if([[UserManager sharedManager] isUserLoggedIn]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm a"];
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        NSDateComponents *comps = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:[NSDate date]];
        comps.hour = 15; // 15 for at 03 PM in day
        comps.minute = 00;
        NSDate *alarmDateTime = [[NSCalendar currentCalendar] dateFromComponents:comps];
        localNotification.fireDate = alarmDateTime;
        User *user = [UserManager sharedManager].activeUser;
        NSString *userName=[NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
        NSString *strMsg= [NSString stringWithFormat:@"Hi %@, It's been 3 PM now, please mark your adherence for assigned effective action.", userName];
        localNotification.alertBody =strMsg;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.repeatInterval = NSCalendarUnitDay;
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [application setApplicationIconBadgeNumber:0];
    [self showEApopover];
}


#pragma mark - Push Notification
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    NSString *token = [[deviceToken.description stringByTrimmingCharactersInSet:characterSet] stringByReplacingOccurrencesOfString:@" " withString:@""];
    [UserPreferences saveDeviceToken:token];    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error.localizedDescription);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
        NotificationInfo *notificationObj = [[NotificationInfo alloc] init];
        notificationObj.type=[NSNumber numberWithInt:1];
        NotificationController *notificationCtrlObj=[NotificationController sharedController];
        [notificationCtrlObj recievedUserInfo:notificationObj];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self handleNotification:userInfo];
}


#pragma mark - Instance Methods

- (void)handleLoginAndHomePage {
    if ([[UserManager sharedManager] isUserLoggedIn]) {
        User *user = [UserManager sharedManager].activeUser;
        if([user.userWorkDays isEqualToString:@"0000000"]) {
            // Send to Working days
            [APPManager workingDays];
        }
        else {
            [APPManager presentHomeViewController];
        }
    }
    else {
        [APPManager presentLoginViewController];
    }
}

- (void)handleNotification:(NSDictionary *)userInfo {
    UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
    NSInteger badge=  [[userInfo valueForKeyPath:@"aps.badge"] integerValue];
    if(appState!=UIApplicationStateActive) {
        NSLog(@"ACtive: %@",[userInfo valueForKeyPath:@"aps.data"]);
        NotificationInfo *notificationObj = [[NotificationInfo alloc] initWithAttributes:[userInfo valueForKeyPath:@"aps.data"]];
        NotificationController *notificationCtrlObj=[NotificationController sharedController];
        [notificationCtrlObj recievedUserInfo:notificationObj];
    }
    else {
        NSString *msg=[userInfo valueForKeyPath:@"aps.alert"];
        [[[[iToast makeText:msg]
           setGravity:iToastGravityTop] setDuration:iToastDurationNormal] show];
        
        NotificationInfo *notificationObj = [[NotificationInfo alloc] initWithAttributes:[userInfo valueForKeyPath:@"aps.data"]];
        if([notificationObj.type integerValue] == kNotificationType_EAAssign) {
            NotificationController *notificationCtrlObj=[NotificationController sharedController];
            [notificationCtrlObj recievedUserInfo:notificationObj];
        }
    }
    [UserPreferences saveBatchCount:badge];
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_BATCH object:nil];
}

+ (void)setRandomNumber {
    if([UserPreferences getRandomNumber] == 0) {
        int rndValue = 1 + arc4random() % (4 - 1);//15
        [UserPreferences saveRandomNumber:rndValue];
        [UserPreferences saveRandomNuberCounter:0];
    }
}

+ (BOOL)checkAndResetRandomNum {
    if([UserPreferences getRandomNuberCounter] == [UserPreferences getRandomNumber]) {
        [UserPreferences saveRandomNumber:0];
        [AppDelegate setRandomNumber];
        return YES;
    }
    else {
        int counter = [UserPreferences getRandomNuberCounter];
        ++counter;
        [UserPreferences saveRandomNuberCounter:counter];
        return NO;
    }
}

+ (void)logOutCurrentUser {
    if([[UserPreferences sharedPreference] announcement]) {
        [[UserPreferences sharedPreference] clearAnnouncements];
    }
    [APPManager logout];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [UIAlertController showAlertWithAction:appDelegate.window.rootViewController withTitle:kUserLogout andCancelButtonTitle:kOK withHandler:^(UIAlertAction *action) {
            if(action)
                [APPManager presentLoginViewController];
        }];
    });
}


+ (void)techIssue:(NSString *)errMessage {
    [UIAlertController showAlertWithAction:appDelegate.window.rootViewController withTitle:errMessage andCancelButtonTitle:kOK withHandler:^(UIAlertAction *action) {
        if(action) {
            
        }
    }];
}

- (void)showUrls:(NSMutableArray *)array {
    if(self.urlTextArray == nil) {
        self.urlTextArray = [NSMutableArray new];
    }
    if(self.urlTextArray.count) {
        [self.urlTextArray removeAllObjects];
    }
    self.urlTextArray = array;
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Option" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    
    for (int i=0; i<array.count-1; i++){
        [popup addButtonWithTitle:[array objectAtIndex:i]];
    }
    [popup addButtonWithTitle:@"Copy Text"];
    [popup addButtonWithTitle:@"Cancel"];
    popup.cancelButtonIndex = popup.numberOfButtons - 1;
    
    [popup showInView:appDelegate.window.rootViewController.view];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if((int)buttonIndex == self.urlTextArray.count-1) {
        NSString *fullText = [self.urlTextArray lastObject];
        UIPasteboard *board = [UIPasteboard generalPasteboard];
        [board setString:fullText];
    }
    else {
        if((int)buttonIndex < self.urlTextArray.count-1){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.urlTextArray objectAtIndex:(int)buttonIndex]]];
        }
    }
}


@end

