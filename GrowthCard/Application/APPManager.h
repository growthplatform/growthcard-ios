//
//  APPManager.h
//  GrowthCard
//
//  Created by Prateek prem on 13/09/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "User.h"

@interface APPManager : NSObject

+ (void)initialStuff;
+ (void)logout;
+ (NSString *)sutableStrWithStr:(NSString *)str;
+ (BOOL)isIphoneSixPlus;
+ (BOOL)isIphoneFour;
+ (BOOL)isIphoneSixe;
+ (BOOL)isIphoneFive;
+ (BOOL)isIpade;
+ (void)presentHomeViewController;
+ (void)presentLoginViewController;
+ (void)termsPage ;
+ (void)workingDays;
+ (void)presentMenuonViewControlleronViewController:(UIViewController *)controller;
+ (void)setRootViewController:(UIViewController *)viewController animated:(BOOL)animated;
+ (NSArray *)getWorkDaysAry :(User *)user;
+ (void)addChart:(NSMutableArray *)graphDetails AndView:(UIView*)containView;
+ (void)addChartForTopView:(NSMutableArray *)graphDetails AndView:(UIView*)containView;

@end
