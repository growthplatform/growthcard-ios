//
//  AppDelegate.h
//  GrowthCard
//
//  Created by Abhishek Tripathi on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;
@interface AppDelegate : UIResponder <UIApplicationDelegate, UIActionSheetDelegate> {
}

@property (strong, nonatomic) NSMutableArray *urlTextArray;
@property (strong, nonatomic) UIWindow *window;

- (void)handleLoginAndHomePage;
+ (BOOL)checkAndResetRandomNum;
+ (void)logOutCurrentUser;
+ (void)techIssue:(NSString *)errMessage;
- (void)showUrls:(NSMutableArray *)array;

@end
