//
//  APPManager.m
//  GrowthCard
//
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "APPManager.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ProfileVC.h"
#import "FeedsVC.h"
#import "ChartContainView.h"

@implementation APPManager

+ (void)initialStuff {
    [[UITextField appearance] setTintColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0.36]];
    [NotificationController registerPN];
    [APPManager cocoaLumberjackInit];
    [APPManager initNavigationBar];
    [APPManager textFieldCursor];
}

+ (void)initNavigationBar {
    ///// remove the creepy black contents of status bar and set it to white color ///
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    // set navigation bar title color
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:232.0/255.0 green:237.0/255.0 blue:239.0/255.0 alpha:1.0]];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kRubik_Regular size:17], NSFontAttributeName,
                                [UIColor blackColor], NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                         [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                         [UIFont fontWithName:kRubik_Bold size:15.0f], NSFontAttributeName, nil]
                                               forState:UIControlStateNormal];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    NSDictionary* barButtonItemAttributes = @{NSFontAttributeName: [UIFont fontWithName:kRubik_Bold size:14.0f]};
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateHighlighted];
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateSelected];
    [[UIBarButtonItem appearance] setTitleTextAttributes: barButtonItemAttributes forState:UIControlStateDisabled];
}

+ (UINavigationController *)navControllerWithClass:(NSString *)className {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [[UINavigationController alloc] initWithRootViewController:[storyBoard instantiateViewControllerWithIdentifier:className]];
}

+ (void)textFieldCursor {
    [[UITextField appearance] setTintColor:[UIColor lightGrayColor]];
}

+ (void)logout {
    [[UserManager sharedManager] clearUserTokenAndInformation];
}

// @method : to return a sutable string (convert nit to @"").
+ (NSString *)sutableStrWithStr:(NSString *)str {
    if([[str class]isSubclassOfClass:NSClassFromString(@"NSNull")]) return @"";
    if(!str) return @"";
    str = [NSString stringWithFormat:@"%@", str];
    return (![str isEqualToString:@"<null>"] && ![str isEqualToString:@"(null)"]) ? str : @" ";
}

+ (void)cocoaLumberjackInit {
    [DDLog addLogger:[DDTTYLogger sharedInstance]]; // TTY = Xcode console
    [DDLog addLogger:[DDASLLogger sharedInstance]]; // ASL = Apple System Logs
    
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init]; // File Logger
    fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
    [DDLog addLogger:fileLogger];
}

+ (BOOL)isIphoneSixPlus {
    return ([UIScreen mainScreen].bounds.size.width == 414);
}

+ (BOOL)isIpade {
    return ([UIScreen mainScreen].bounds.size.width == 768);
}

+ (BOOL)isIphoneSixe {
    return ([UIScreen mainScreen].bounds.size.width == 375);
}

+ (BOOL)isIphoneFour {
    return ([UIScreen mainScreen].bounds.size.height == 480);
}

+ (BOOL)isIphoneFive {
    return ([UIScreen mainScreen].bounds.size.height == 568);
}

+ (void)presentMenuonViewControlleronViewController:(UIViewController *)controller {
    
}

+ (void)termsPage {
    TermsVC  *viewController = (TermsVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[TermsVC class]];
    [APPManager setRootViewController:viewController animated:NO];
}

+ (void)workingDays {
    WorkdayVC  *viewController = (WorkdayVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[WorkdayVC class]];
    [APPManager setRootViewController:viewController animated:NO];
}

+ (void)presentLoginViewController {
    LoginVC  *viewController = (LoginVC *)[[UIStoryboard onboardingStoryboard] instantiateViewControllerWithClass:[LoginVC class]];
    [APPManager setRootViewController:viewController animated:NO];
}

+ (void)presentHomeViewController {
    User *user = [UserManager sharedManager].activeUser;
    AppDelegate *appDelegateTemp = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UIStoryboard *MainStoryboard =[UIStoryboard homeStoryboard];
    SlideNavigationController *controller = (SlideNavigationController*)[MainStoryboard
                                                                         instantiateViewControllerWithIdentifier: @"SlideNavigationController"];
    if (user.userRole.integerValue == UserRoleManager) {
        DashboardVC *viewController=[MainStoryboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
        [controller setViewControllers:[NSArray arrayWithObject:viewController] animated:YES];
        
    } else {
        FeedsVC *viewController=[MainStoryboard instantiateViewControllerWithIdentifier:@"FeedsVC"];
        [controller setViewControllers:[NSArray arrayWithObject:viewController] animated:YES];
    }
    appDelegateTemp.window.rootViewController=controller;
    RightMenuViewController  *rightMenu = (RightMenuViewController *)[[UIStoryboard homeStoryboard] instantiateViewControllerWithClass:[RightMenuViewController class]];
    
    [SlideNavigationController sharedInstance].rightMenu = rightMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    // Creating a custom bar button for right menu
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 39)];
    [button setImage:[UIImage imageNamed:@"rightMenuBtn"] forState:UIControlStateNormal];
    [button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [SlideNavigationController sharedInstance].rightBarButtonItem = rightBarButtonItem;
    [SlideNavigationController sharedInstance].leftBarButtonItem = nil;
}


+ (void)setRootViewController:(UIViewController *)viewController animated:(BOOL)animated {
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:viewController];
    AppDelegate *appDelegateTemp = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (animated) {
        CATransition *animation = [CATransition animation];
        animation.duration = 0.5f;
        animation.timingFunction = UIViewAnimationOptionCurveEaseInOut;
        animation.fillMode = kCAFillModeForwards;
        animation.type = kCATransitionPush;
        animation.subtype = kCATransitionFromTop;
        [appDelegateTemp.window.layer addAnimation:animation forKey:@"animation"];
        [appDelegateTemp.window setRootViewController:nav];
    }
    else {
        [appDelegateTemp.window setRootViewController:nav];
    }
}

+ (NSArray *)getWorkDaysAry :(User *)user  {
    NSArray *days   = @[@"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat", @"Sun"];
    NSString *selectedDaysStr = user.userWorkDays;
    NSMutableArray *array = [NSMutableArray new];
    for (int i = 0; i < [selectedDaysStr length]; i++) {
        NSString *chr = [selectedDaysStr substringWithRange:NSMakeRange(i, 1)];
        if([chr isEqualToString:@"1"]) {
            [array addObject:[days objectAtIndex:i]];
        }
    }
    NSArray *daysAry = [array copy];
    return daysAry;
}

//buttom
+ (void)addChart:(NSMutableArray *)graphDetails AndView:(UIView*)containView   {
    
    [containView setBackgroundColor:[UIColor clearColor]];
    [[containView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGRect rect=containView.bounds;
    if (graphDetails.count==0) {
        CGRect rect=containView.bounds;
        rect.size.width=rect.size.width-10;
        UIImageView *defaultPlaceholder=[[UIImageView alloc] initWithFrame:rect];
        defaultPlaceholder.image=[UIImage imageNamed:@"Graph"];
        defaultPlaceholder.contentMode = UIViewContentModeScaleToFill;
        [containView addSubview:defaultPlaceholder];
        UILabel *lbl = [[UILabel alloc] initWithFrame:rect];
        lbl.textColor =[UIColor lightGrayColor];
        // lbl.text=@"No data";
        [lbl setFont:[UIFont boldSystemFontOfSize:16]];//country name
        lbl.textAlignment = NSTextAlignmentCenter;
        [containView addSubview:lbl];
        return;
    }
    int maxValu=0;
    for (int i=0; i<graphDetails.count; i++) {
        NSString *str=graphDetails[i];
        if (!([str containsString:@"#"] == NSNotFound)) {
            NSArray *strArray=[str componentsSeparatedByString:@"#"];
            int val = [strArray[1] intValue];
            if (val>maxValu) {
                maxValu=val;
            }
        }
    }
    
    int layoutMargin=1;
    int marginForXAxis=40;
    const int MAX_WIDTH=25;
    int blockSize=(rect.size.width-marginForXAxis-(graphDetails.count*2))/graphDetails.count;
    if (blockSize<=MAX_WIDTH) {
        rect.origin.x=marginForXAxis;
        int lableH=20;
        float totalH = rect.size.height - 2*(layoutMargin);
        int keyCount=totalH/lableH;
        int step=maxValu/keyCount;
        int startValu=maxValu;
        int yAxis=layoutMargin;
        for (int i=0; i<keyCount; i++) {
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, yAxis , marginForXAxis , lableH)];
            lbl.text=[NSString stringWithFormat:@"%d",startValu];
            [containView addSubview:lbl];
            [lbl setFont:[UIFont fontWithName:kRubik_Medium size:10.0f]];//country name
            lbl.textColor =[UIColor colorWithRed:0.7608 green:0.7647 blue:0.7804 alpha:1.0];
            yAxis+=lableH;
            startValu-=step;
        }
    }
    else
        rect.origin.x=marginForXAxis/2;
    //return;
    rect.size.width=rect.size.width-rect.origin.x;
    ChartContainView *chartV=[[ChartContainView alloc] initWithFrame:rect];
    [containView addSubview:chartV];
    chartV.chartWidth = blockSize>MAX_WIDTH?MAX_WIDTH :blockSize;
    chartV.gap = 1;
    chartV.marginvalue = layoutMargin;
    if (blockSize<=MAX_WIDTH) {
        chartV.topH=5;
        chartV.buttomH=0;
        chartV.isDetailsMode=NO;
    }
    else {
        chartV.topH=15;
        chartV.buttomH=30;
        chartV.isDetailsMode=YES;
        
    }
    chartV.LargestValue=maxValu;
    User *user = [UserManager sharedManager].activeUser;
    NSMutableArray *arr = [NSMutableArray new];
    
    for (int i=0; i<graphDetails.count; i++) {
        NSString *str=[graphDetails objectAtIndex:i];
        NSArray *strArray=[str componentsSeparatedByString:@"#"];
        int currentVakue=[strArray[1] intValue];
        float precentage = 0;
        
        if(currentVakue == 0 || maxValu == 0)
            precentage = 0;
        else
            precentage = (100 * currentVakue)/maxValu;
        
        NSString *title=strArray[0];
        NSString *dataText=strArray[1];
        title = [title stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
        UIColor *color=[UIColor colorWithRed:0.1725 green:0.6863 blue:0.2353 alpha:1.0];
        if ([title isEqualToString:@"Avg"]) {
            color=[UIColor colorWithRed:0.9922 green:0.8863 blue:0.0431 alpha:1.0];
        }else  if ([title isEqualToString:@"Today"]) {
            color=[UIColor colorWithRed:0.0784 green:0.6314 blue:0.9098 alpha:1.0];
        }
        ChartInfo *info1 = [ChartInfo infoWithPerHeight:precentage color:color dataText:dataText andTitle:title];
        if (user.userRole.integerValue==UserRoleSubordinate) {
            info1 = [ChartInfo infoWithPerHeight:precentage color:color dataText:@"" andTitle:title];
        }
        ChartInfo *info2 = [ChartInfo infoWithPerHeight:precentage color:color dataText:@"" andTitle:@""];
        if (blockSize>MAX_WIDTH)
            [arr addObject:info1];
        else
            [arr addObject:info2];
        
    }
    chartV.chartInfoList = arr;
    [chartV showCharts];
}

//Top view
+ (void)addChartForTopView:(NSMutableArray *)graphDetails AndView:(UIView*)containView   {
    [containView setBackgroundColor:[UIColor clearColor]];
    [[containView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGRect rect=containView.bounds;
    if (graphDetails.count==0) {
        CGRect rect=containView.bounds;
        rect.size.width=rect.size.width-10;
        UIImageView *defaultPlaceholder=[[UIImageView alloc] initWithFrame:rect];
        defaultPlaceholder.image=[UIImage imageNamed:@"Graph"];
        defaultPlaceholder.contentMode = UIViewContentModeScaleToFill;
        [containView addSubview:defaultPlaceholder];
        UILabel *lbl = [[UILabel alloc] initWithFrame:rect];
        lbl.textColor =[ UIColor lightGrayColor];
        // lbl.text=@"No data";
        [lbl setFont:[UIFont boldSystemFontOfSize:16]];//country name
        lbl.textAlignment = NSTextAlignmentCenter;
        [containView addSubview:lbl];
        return;
    }
    int maxValu=0;
    for (int i=0; i<graphDetails.count; i++) {
        NSString *str=graphDetails[i];
        if (!([str containsString:@"#"] == NSNotFound)) {
            NSArray *strArray=[str componentsSeparatedByString:@"#"];
            int val = [strArray[1] intValue];
            if (val>maxValu) {
                maxValu=val;
            }
        }
    }
    
    int layoutMargin=1;
    int marginForXAxis=40;
    const int MAX_WIDTH=25;
    int blockSize=(rect.size.width-marginForXAxis-(graphDetails.count*2))/graphDetails.count;
    rect.origin.x=marginForXAxis/2;
    rect.size.width=rect.size.width-rect.origin.x;
    ChartContainView *chartV=[[ChartContainView alloc] initWithFrame:rect];
    [containView addSubview:chartV];
    chartV.chartWidth = blockSize>MAX_WIDTH?MAX_WIDTH :blockSize;
    chartV.gap = 1;
    chartV.marginvalue = layoutMargin;
    chartV.topH=0;
    chartV.buttomH=0;
    chartV.LargestValue=maxValu;
    NSMutableArray *arr = [NSMutableArray new];
    for (int i=0; i<graphDetails.count; i++) {
        NSString *str=[graphDetails objectAtIndex:i];
        NSArray *strArray=[str componentsSeparatedByString:@"#"];
        int currentVakue=[strArray[1] intValue];
        float precentage = (100 * currentVakue)/maxValu;
        NSString *title=strArray[0];
        UIColor *color=[UIColor colorWithRed:0.1725 green:0.6863 blue:0.2353 alpha:1.0];
        if ([title isEqualToString:@"Avg"]) {
            color=[UIColor colorWithRed:0.9922 green:0.8863 blue:0.0431 alpha:1.0];
        }else  if ([title isEqualToString:@"Today"]) {
            color=[UIColor colorWithRed:0.0784 green:0.6314 blue:0.9098 alpha:1.0];
        }
        ChartInfo *info1 = [ChartInfo infoWithPerHeight:precentage color:color dataText:@"" andTitle:@""];
        [arr addObject:info1];
    }
    chartV.chartInfoList = arr;
    [chartV showCharts];
}

@end
