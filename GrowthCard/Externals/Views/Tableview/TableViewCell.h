//
//  TableViewCell.h
//  UUChartView
//
//  Created by shake on 15/1/4.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

- (void)configUI:(NSIndexPath *)indexPath;
+ (UIView *)indicatorFooter;

@end
