//
//  ChartView.h
//  GrowthCard
//
//  Created by Prakash Raj on 21/04/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChartView : UIView

- (void)addTagget:(id)target selector:(SEL)selector;
@end
