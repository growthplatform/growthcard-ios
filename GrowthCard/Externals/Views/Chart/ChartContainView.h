//
//  ChartContainView.h
//  GrowthCard
//
//  Created by Prakash Raj on 21/04/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChartInfo : NSObject
@property (nonatomic, assign) float    perHieght;  // Height in persentage.
@property (nonatomic, strong) UIColor  *chartColor;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *dataText;

+ (ChartInfo *)infoWithPerHeight:(float)pHeight color:(UIColor *)clr
                        dataText:(NSString *)dText andTitle:(NSString *)title;
@end



@protocol ChartContainViewDelegate;

@interface ChartContainView : UIScrollView
@property (nonatomic, assign) float initialMargin;
@property (nonatomic, assign) float gap;
@property (nonatomic, assign) float chartWidth;

@property (nonatomic, assign) float topH;
@property (nonatomic, assign) float buttomH;

@property (nonatomic, assign) float LargestValue;
@property (nonatomic, assign) float marginvalue;

@property (nonatomic, assign) BOOL isDetailsMode;



@property (nonatomic, strong) NSArray *chartInfoList;

@property (nonatomic, assign) id <ChartContainViewDelegate> chartDelegate;

- (void)showCharts;
- (void)hideCharts;

@end


@protocol ChartContainViewDelegate <NSObject>
- (void)didSelectChartAtIndex:(NSInteger)index;

@end
