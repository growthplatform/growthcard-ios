//
//  ChartContainView.m
//  GrowthCard
//
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "NSString+Additions.h"
#import "ChartContainView.h"
#import "ChartView.h"

#define kTitleFontSize 12
#define kValueFontSize 12

@implementation ChartInfo

+ (ChartInfo *)infoWithPerHeight:(float)pHeight color:(UIColor *)clr
                        dataText:(NSString *)dText andTitle:(NSString *)title {
    ChartInfo *info  = [[ChartInfo alloc] init];
    info.perHieght   = pHeight;
    info.chartColor  = clr;
    info.dataText    = dText;
    info.title       = title;
    return info;
}

@end


@interface ChartContainView () {
    NSMutableArray *_chartList;
    NSMutableArray *_topLabels;
    NSMutableArray *_titleLabels;
}

- (void)animateCharts;
@end


@implementation ChartContainView


- (void)showCharts {
    
    NSInteger verticalGap = 1;
    
    // init all arrays..
    if(!_chartList)
        _chartList = [[NSMutableArray alloc] initWithCapacity:_chartInfoList.count];
    
    if(!_topLabels)
        _topLabels = [[NSMutableArray alloc] initWithCapacity:_chartInfoList.count];
    
    if(!_titleLabels)
        _titleLabels = [[NSMutableArray alloc] initWithCapacity:_chartInfoList.count];
    
    
    // remove all chart lines..
    for(UIView *vv in self.subviews) {
        [vv removeFromSuperview];
    }
    
    [_chartList removeAllObjects];
    [_topLabels removeAllObjects];
    [_titleLabels removeAllObjects];
    
    // add fresh chart lines..
    CGRect bonds = self.bounds;
    float totalH = bonds.size.height - 2*(self.marginvalue);
    
    for(int k = 0; k < _chartInfoList.count; k++) {
        float hh = 3;
        float yy = totalH-hh+(self.marginvalue);
        
        ChartView *cView = [[ChartView alloc] initWithFrame:CGRectMake(k*(_chartWidth + _gap)+_initialMargin, yy, _chartWidth, hh)];
        [self addSubview:cView];
        
        ChartInfo *info = [_chartInfoList objectAtIndex:k];
        cView.backgroundColor = info.chartColor;
        cView.tag = k;
        
        [_chartList addObject:cView];
        // [cView addTagget:self selector:@selector(chartClicked:)];  //Narender
        
        float xx = k*(_chartWidth + _gap)+_initialMargin-_gap/2;
        
        //------------------ add top label----
        UILabel *topLbl = [[UILabel alloc] initWithFrame:CGRectMake(xx, (yy - (2 * verticalGap))+50, _chartWidth + _gap, 30)];
        topLbl.textAlignment = NSTextAlignmentCenter;
        [topLbl setFont:[UIFont fontWithName:@"Helvetica Neue" size:10.0f]];//value lbl
        topLbl.text = info.dataText;
        topLbl.textColor = [UIColor colorWithRed:0.7608 green:0.7647 blue:0.7804 alpha:1.0];
        topLbl.textColor = [UIColor colorWithRed:0.7325 green:0.2154 blue:0.4103 alpha:1.0];
        topLbl.textColor = [UIColor colorWithRed:0.7608 green:0.7647 blue:0.7804 alpha:1.0];

        
        [topLbl setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:topLbl];
        topLbl.backgroundColor = [UIColor clearColor];
        [_topLabels addObject:topLbl];
        topLbl.alpha = 0;
        // -----------------------------------

        
        // ---------------add bottom label-------
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(xx, yy + verticalGap , _gap+20 , 20)];
        [lbl setFont:[UIFont fontWithName:@"Helvetica Neue" size:9.0f]];//country name
        lbl.textColor =[UIColor colorWithRed:0.7608 green:0.7647 blue:0.7804 alpha:1.0];
        lbl.backgroundColor = [UIColor clearColor];
        [lbl setNumberOfLines:2];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.alpha = 0.0;
        [lbl sizeToFit];
        
        [self addSubview:lbl];
        [_titleLabels addObject:lbl];
        // --------------------------------------
    }
    
    [self setContentSize:CGSizeMake(_chartInfoList.count*(_chartWidth + _gap)+2*_initialMargin - _gap, bonds.size.height)];
    
    [self animateCharts];
    [self performSelector:@selector(animateTitles) withObject:Nil afterDelay:.5];
    [self performSelector:@selector(animateHeaders) withObject:Nil afterDelay:.5];

}

- (void)hideCharts
{
    // remove all chart lines..
    for(UIView *vv in self.subviews) {
        [vv removeFromSuperview];
    }
}


- (void)animateCharts
{
    CGRect bonds = self.bounds;
    float totalH = bonds.size.height - 2*(self.marginvalue);
    float chartH=totalH-self.topH-self.buttomH;
    
    for(int k = 0; k < _chartInfoList.count; k++) {
        ChartInfo *info = [_chartInfoList objectAtIndex:k];
        float hh = chartH*info.perHieght/100;
        
        if(isnan(chartH))
            return;
        
        float chartOrigin = totalH-self.buttomH-hh;
        [UIView animateWithDuration:.3 delay:k*.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            ChartView *vv = [_chartList objectAtIndex:k];
            CGRect fr = vv.frame;
            fr.size.height = hh;
            fr.origin.y = chartOrigin;
            vv.frame = fr;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)animateTitles {
    
    [UIView animateWithDuration:.6 animations:^{
        float xx = 0;
        float ww;
        NSInteger verticalGap = 10;
        CGRect bonds = self.bounds;
        float totalH = bonds.size.height - 2*(self.marginvalue);
        float yy = totalH-self.buttomH + verticalGap;
        if (!self.isDetailsMode) {
            yy=bonds.size.height;
        }
        for(int k = 0; k < _chartInfoList.count; k++) {

            UILabel *lbl = [_titleLabels objectAtIndex:k];
            int mar = (k) ? _gap : _initialMargin+_gap/2;
            ww = _chartWidth + mar;
            ChartInfo *info = [_chartInfoList objectAtIndex:k];
            NSString *str = ([info.title isKindOfClass:[NSNull class]])? @"": info.title;
            CGSize cn = [str actualSizeWithFont:lbl.font stickToWidth:ww];
            
            CGRect frame = lbl.frame;
            frame.origin.x = xx;
            frame.origin.y = yy-5;
            frame.size.width = ww-2;
            frame.size.height = cn.height;
            lbl.frame = frame;
            lbl.alpha = 1.0;
            xx += ww;
            lbl.text = info.title;
            lbl.textAlignment = NSTextAlignmentCenter;
            
            if ([lbl.text isEqualToString:@"Today"]) {
                lbl.textColor =[UIColor lightGrayColor];
                [lbl setFont:[UIFont fontWithName:kRubik_Bold size:8.0f]];//country name

            }

            
        }
        
    } completion:^(BOOL finished) {
        
    }];
}


- (void)animateHeaders {
    
    CGRect bonds = self.bounds;
    float totalH = bonds.size.height - 2*(self.marginvalue);
    float chartH=totalH-self.topH-self.buttomH;

    [UIView animateWithDuration:.6 animations:^{
        float xx = 0;
        float ww;
        for(int k = 0; k < _chartInfoList.count; k++) {
            
                UILabel *lbl = [_topLabels objectAtIndex:k];
                int mar = (k) ? _gap : _initialMargin+_gap/2;
                ww = _chartWidth + mar;
                ChartInfo *info = [_chartInfoList objectAtIndex:k];
                float hh = chartH*info.perHieght/100;
                if(isnan(chartH))
                    return;
                float chartOrigin = totalH-self.buttomH-hh;
                NSString *str = ([info.dataText isKindOfClass:[NSNull class]])? @"": info.dataText;
                CGSize cn = [str actualSizeWithFont:lbl.font stickToWidth:ww];
                
                CGRect frame = lbl.frame;
                frame.origin.x = xx;
                frame.origin.y = chartOrigin-self.topH;
                frame.size.width = ww-2;
                frame.size.height = cn.height;
                lbl.frame = frame;
                lbl.alpha = 1.0;
                xx += ww;
                lbl.text = info.dataText;
                lbl.textAlignment = NSTextAlignmentCenter;
            
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - Chart click Action

- (void)chartClicked:(ChartView *)chartView {
    if(_chartDelegate && [_chartDelegate respondsToSelector:@selector(didSelectChartAtIndex:)])
        [_chartDelegate didSelectChartAtIndex:chartView.tag];
}

@end
