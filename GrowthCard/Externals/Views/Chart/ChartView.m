//
//  ChartView.m
//  GrowthCard
//
//  Created by Prakash Raj on 21/04/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "ChartView.h"

#define k_OVERLAY_TAG 1987

@interface ChartView ()
@property (nonatomic, assign) id target;
@property (nonatomic, assign) SEL selector;
@end

@implementation ChartView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
