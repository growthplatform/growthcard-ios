//
//  Global.m
//  GrowthCard
//
//  Created by Prakash Raj on 21/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "Global.h"

@implementation Global

+ (instancetype)shared {
    static Global *_shGlobal = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shGlobal = [[Global alloc] init];
    });
    return _shGlobal;
}

- (void)clearAnnouncements{
    self.announcement = nil;
}

@end
