//
//  ImagePickManager.m
//  GrowthCard
//
//  Created by @Prakash on 26/12/13.
//  Copyright (c) 2013 Appster. All rights reserved.
//

#import "ImagePickManager.h"
#import <AssetsLibrary/AssetsLibrary.h>

/*
 * @block : to perform task on completion.
 * @arg : isSelected - YES/NO    (user selected an image/cancelled).
 * @arg : image - selected image (nil in case user cancelled).
 */
typedef void (^CompletionImageSelection)(BOOL isSelected, UIImage *image);

@interface ImagePickManager () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, copy) CompletionImageSelection block;
@end


@implementation ImagePickManager

// @method : to return shared instance.
+ (ImagePickManager *)sharedPicker {
    static ImagePickManager *picker = nil;
    @synchronized (self) {
        if (!picker) {
            picker = [[ImagePickManager alloc] init];
        }
    }
    return picker;
}

// check camera availability.
+ (BOOL)isCameraAuailable {
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

// show image library/camera..
+ (void)presentImageSource:(BOOL)isCam onController:(UIViewController *)controller
            withCompletion:(void (^)(BOOL isSelected, UIImage *image))block {
    
    // check if user ask for camera and its availability.
    if(isCam && ![ImagePickManager isCameraAuailable]) {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"Alert !" message:@"Device does not support camera." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alrt show]; alrt = nil; return;
    }
    
    ImagePickManager *picker = [ImagePickManager sharedPicker];
    picker.block = block;
    
    // open image library/camera.
   
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = picker;
    pickerController.allowsEditing = YES;
    pickerController.sourceType = (isCam) ? UIImagePickerControllerSourceTypeCamera : (UIImagePickerControllerSourceTypePhotoLibrary);
    
    [controller presentViewController:pickerController animated:YES completion:nil];
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *anImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    if(self.block) self.block(YES, anImage);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo {
    // handle error.
    if (error) {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alrt show]; alrt = nil; return;
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    if(self.block) self.block(NO, nil);
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController.navigationItem setTitle:@"Select Photo"];
}

@end
