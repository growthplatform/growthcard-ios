//
//  ImagePickManager.h
//  GrowthCard
//
//  Created by @Prakash on 26/12/13.
//  Copyright (c) 2013 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;
@interface ImagePickManager : NSObject

// @method : to return shared instance.
+ (ImagePickManager *)sharedPicker;

// check camera availability.
+ (BOOL)isCameraAuailable;

// show image library/camera..
+ (void)presentImageSource:(BOOL)isCam onController:(UIViewController *)controller withCompletion:(void (^)(BOOL isSelected, UIImage *image))block;

@end
