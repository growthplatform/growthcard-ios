//
//  ConfigurationManager.h
//
//  Created by Arvind Singh on 30/09/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationManager : NSObject

@property (strong, nonatomic) NSString *currentConfiguration;
@property (strong, nonatomic) NSString *APIEndPoint;
@property (assign, nonatomic) BOOL isLoggingEnabled;
@property (assign, nonatomic) BOOL isAnalyticsTrackingEnabled;

+ (ConfigurationManager *)sharedInstance;

// Mark following methods unavailable (produces compile time error)
+ (instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+ (instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));
- (instancetype) copy __attribute__((unavailable("copy not available, call sharedInstance instead")));

@end
