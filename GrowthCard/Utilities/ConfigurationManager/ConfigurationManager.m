//
//  ConfigurationManager.m
//
//  Created by Arvind Singh on 30/09/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "ConfigurationManager.h"

static NSString *const kEnvironmentsPlist = @"Environments";
static NSString *const kConfigurationKey = @"ActiveConfiguration";

static NSString *const kAPIEndPointKey = @"APIEndPoint";
static NSString *const kLoggingEnabledKey = @"LoggingEnabled";
static NSString *const kAnalyticsTrackingEnabled = @"AnalyticsTrackingEnabled";

@interface ConfigurationManager ()

@property (strong, nonatomic) NSDictionary *environment;

@end



@implementation ConfigurationManager


#pragma mark - Singleton Methods

+ (ConfigurationManager *) sharedInstance {
    
    static id shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
       shared = [[super alloc] initUniqueInstance];
    });
    
    return shared;
}


#pragma mark - Lifecycle Methods

- (instancetype) initUniqueInstance {
    ConfigurationManager *manager = [super init];
    [manager initializeSharedInstance];
    return manager;
}


#pragma mark - Instance Methods

- (void) initializeSharedInstance {
    
    // Load Configurations / Environments
    NSString *envsPListPath = [[NSBundle mainBundle] pathForResource:@"Environments" ofType:@"plist"];
    NSDictionary *environments = [NSDictionary dictionaryWithContentsOfFile:envsPListPath];
    
    // Load settings for current configuration
    self.environment = [environments objectForKey:self.currentConfiguration];
    
    if (!self.environment) {
        NSAssert(NSLocalizedString(@"Unable to load application configuration", @"Unable to load application configuration"), NSLocalizedString(@"Unable to load application configuration", @"Unable to load application configuration"));
    }
}


#pragma mark - Getter Methods

- (NSString *) currentConfiguration {
    // Fetch Current Configuration
    NSString *configuration = [[[NSBundle mainBundle] infoDictionary] objectForKey:kConfigurationKey];
    return configuration;
}

- (NSString *) APIEndPoint {
    return self.environment[kAPIEndPointKey];
}

- (BOOL) isLoggingEnabled {
    return self.environment[kLoggingEnabledKey];
}

- (BOOL) isAnalyticsTrackingEnabled {
    return self.environment[kAnalyticsTrackingEnabled];
}

@end
