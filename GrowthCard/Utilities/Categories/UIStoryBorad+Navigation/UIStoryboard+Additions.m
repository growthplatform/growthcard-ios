//
//  UIStoryboard+Additions.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UIStoryboard+Additions.h"

@implementation UIStoryboard (Additions)
 
+ (UIStoryboard *)onboardingStoryboard {
    return [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
}

+ (UIStoryboard *)homeStoryboard {
    return [UIStoryboard storyboardWithName:@"Home" bundle:nil];
}

+ (UIStoryboard *)profileStoryboard {
    return [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
}

+ (UIStoryboard *)notificationStoryboard {
    return [UIStoryboard storyboardWithName:@"Notification" bundle:nil];
}

+ (UIStoryboard *)managerStoryboard {
    return [UIStoryboard storyboardWithName:@"Manager" bundle:nil];
}

+ (UIStoryboard *)subordinateStoryboard {
    return [UIStoryboard storyboardWithName:@"Subordinate" bundle:nil];
}



- (id)instantiateViewControllerWithClass:(Class)viewControllerClass {
    return [self instantiateViewControllerWithIdentifier:NSStringFromClass([viewControllerClass class])];
}

@end
