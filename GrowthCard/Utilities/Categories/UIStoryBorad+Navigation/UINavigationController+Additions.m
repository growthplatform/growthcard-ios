//
//  UINavigationController+Additions.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UINavigationController+Additions.h"

@implementation UINavigationController (Additions)

- (void)showViewController:(UIViewController *)destination fromViewController:(UIViewController *)source animated:(BOOL)animated {
    [self pushViewController:destination animated:animated];
}

- (void)showViewController:(UIViewController *)viewController withAnimationType:(NSString *)type {
    [self showViewController:viewController withAnimationType:type withAnimationSubType:nil];
}

- (void)showViewController:(UIViewController *)viewController withAnimationType:(NSString *)type withAnimationSubType:(NSString *)subtype {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = type; // kCATransitionFade
    transition.subtype = subtype; // kCATransitionFromTop
    
    [self.view.layer addAnimation:transition forKey:kCATransition];
    [self pushViewController:viewController animated:NO];
}

- (void)showViewAsaPresentController:(UIViewController *)viewController {
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromTop];
    [animation setDuration:0.65f];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[self.view layer] addAnimation:animation forKey:@"AnimationFromBottomToTop"];
    [self popToRootViewControllerAnimated:NO];
}

- (void)hideViewController:(UIViewController *)viewController withAnimationType:(NSString *)type {
    [self hideViewController:viewController withAnimationType:type withAnimationSubType:nil];
}

- (void)hideViewController:(UIViewController *)viewController withAnimationType:(NSString *)type withAnimationSubType:(NSString *)subtype {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = type; // kCATransitionFade
    transition.subtype = subtype; // kCATransitionFromTop
    
    [self.view.layer addAnimation:transition forKey:kCATransition];
    [self popViewControllerAnimated:NO];
}

- (void)presentViewController:(UIViewController *)viewController withAnimationType:(NSString *)type {
    [self presentViewController:viewController withAnimationType:type withAnimationSubType:nil];
}

- (void)presentViewController:(UIViewController *)viewController withAnimationType:(NSString *)type withAnimationSubType:(NSString *)subtype {
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:viewController animated:YES completion:^{
        
    }];
}

+ (UINavigationController *)navigationControllerWithRootViewController:(UIViewController *)rootViewController {
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    [navigationController setNavigationBarHidden:YES];
    return navigationController;
}

@end
