//
//  UIStoryboard+Additions.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Additions)

+ (UIStoryboard *)onboardingStoryboard;
+ (UIStoryboard *)homeStoryboard;
+ (UIStoryboard *)profileStoryboard;
+ (UIStoryboard *)notificationStoryboard;
+ (UIStoryboard *)managerStoryboard;
+ (UIStoryboard *)subordinateStoryboard;

- (id)instantiateViewControllerWithClass:(Class)viewControllerClass;

@end
