//
//  UINavigationController+Additions.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Additions)

- (void)showViewController:(UIViewController *)destination fromViewController:(UIViewController *)source animated:(BOOL)animated;

- (void)showViewController:(UIViewController *)viewController withAnimationType:(NSString *)type;
- (void)hideViewController:(UIViewController *)viewController withAnimationType:(NSString *)type;

- (void)presentViewController:(UIViewController *)viewController withAnimationType:(NSString *)type;

- (void)showViewAsaPresentController:(UIViewController *)viewController;

@end
