//
//  UIAlertController+Additions.h
//  GrowthCard
//
//  Created by Abhishek Tripathi on 15/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AlertactionHandler)(UIAlertAction *action);

typedef void(^AlertactionOkCancelHandler)(UIAlertAction *okAction, UIAlertAction *cancelAction);

@interface UIAlertController (Additions)

+ (void)showAlertOn:(UIViewController *)viewController withTitle:(NSString *)title andCancelButtonTitle:(NSString *)string;
+ (void)showAlertOn:(UIViewController *)viewController withMessage:(NSString *)message andCancelButtonTitle:(NSString *)string;

+ (void)showAlertWithAction:(UIViewController *)viewController withTitle:(NSString *)title andCancelButtonTitle:(NSString *)string withHandler:(AlertactionHandler)handler;

+ (void)showAlertWithOkCancelAction:(UIViewController *)viewController withTitle:(NSString *)title andOkButtonTitle:(NSString *)ok andCancelButtonTitle:(NSString *)cancel withHandler:(AlertactionOkCancelHandler)handler;

@end
