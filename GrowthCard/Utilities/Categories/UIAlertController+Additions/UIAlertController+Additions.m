//
//  UIAlertController+Additions.m
//  GrowthCard
//
//  Created by Abhishek Tripathi on 15/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "UIAlertController+Additions.h"

@implementation UIAlertController (Additions)

+ (void)showAlertOn:(UIViewController *)viewController withTitle:(NSString *)title andCancelButtonTitle:(NSString *)string {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:kAppName
                                                                   message:title
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:string style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                          
                                                          
                                                          }];
    
    [alert addAction:defaultAction];
    [viewController presentViewController:alert animated:YES completion:nil];
}



+ (void)showAlertOn:(UIViewController *)viewController withMessage:(NSString *)message andCancelButtonTitle:(NSString *)string {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:kAppName
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:string style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              
                                                          }];
    
    [alert addAction:defaultAction];
    [viewController presentViewController:alert animated:YES completion:nil];
}

+ (void)showAlertWithAction:(UIViewController *)viewController withTitle:(NSString *)title andCancelButtonTitle:(NSString *)string withHandler:(AlertactionHandler)handler {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:kAppName
                                                                   message:title
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:string style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              handler (action);
                                                          }];
    
    [alert addAction:defaultAction];
    [viewController presentViewController:alert animated:YES completion:nil];
}

+ (void)showAlertWithOkCancelAction:(UIViewController *)viewController withTitle:(NSString *)title andOkButtonTitle:(NSString *)ok andCancelButtonTitle:(NSString *)cancel withHandler:(AlertactionOkCancelHandler)handler {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:kAppName
                                                                   message:title
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              handler (action,nil);
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         handler (nil,action);
                                                     }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    [viewController presentViewController:alert animated:YES completion:nil];
}
@end
