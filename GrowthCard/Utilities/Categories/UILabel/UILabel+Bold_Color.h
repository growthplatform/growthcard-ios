//
//  UILabel+Bold_Color.h
//  GrowthCard
//
//  Created by Pawan Kumar on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UILabel (Bold_Color)
- (void)boldSubstring:(NSString*)substring;
- (void)boldRange:(NSRange)range;
- (void)boldAndColorSubstring:(NSString*)substring forColor:(UIColor *)color;
- (void)boldAndColorRange:(NSRange)range forColor:(UIColor *)color;
@end