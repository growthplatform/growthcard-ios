//
//  UILabel+Bold_Color.m
//  GrowthCard
//
//  Created by Pawan Kumar on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "UILabel+Bold_Color.h"

@implementation UILabel (Bold_Color)
- (void)boldRange:(NSRange)range {
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText;
    if (!self.attributedText) {
        attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    } else {
        attributedText = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    }
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize]} range:range];
    self.attributedText = attributedText;
}
- (void)boldSubstring:(NSString*)substring {
    NSRange range = [self.text rangeOfString:substring];
    [self boldRange:range];
}
- (void)boldAndColorRange:(NSRange)range forColor:(UIColor *)color {
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText;
    if (!self.attributedText) {
        attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    } else {
        attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    }
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize], NSForegroundColorAttributeName:color} range:range];
    self.attributedText = attributedText;
}
- (void)boldAndColorSubstring:(NSString*)substring forColor:(UIColor *)color {
    NSRange range = [self.text rangeOfString:substring];
    [self boldAndColorRange:range forColor:color];
}
@end
