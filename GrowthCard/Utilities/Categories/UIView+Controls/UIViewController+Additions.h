//
//  UIViewController+Additions.h
//  GrowthCard
//
//  Created by Pawan Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol UIViewDelegateSeeMore;
@interface UIViewController (Additions)
- (void)addSeeMoreRecognizer:(FRHyperLabel*)lable AndDesc:(NSString*)comment AndDelegate:(BaseVC*)delegate;
@end


//@protocol UIViewDelegateSeeMore<NSObject>
//@optional
//- (void)seeMoreClick;
//@end
//
