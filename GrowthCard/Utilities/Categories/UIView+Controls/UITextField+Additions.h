//
//  UITextField+Additions.h
//
//  Created by Arvind Singh on 22/07/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, UITextFieldPadding) {
    UITextFieldPaddingLeft = 0,
    UITextFieldPaddingRight,
    
};

@interface UITextField (Additions)

- (NSString *)textByTrimmingWhiteSpacesAndNewline;

- (BOOL)isTextFieldEmpty;

- (void)trimWhiteSpacesAndNewline;
- (BOOL)isMobileNumberIsValid;
- (void)addPaddingInTextField:(UITextFieldPadding)padding withValue:(CGFloat)value;
- (BOOL)applyedFormettedMobilNumberOnTextField:(UITextField *)textField andrange:(NSRange)range;
- (BOOL)isUSAMobileNumberIsValid;

- (void)setPlaceholderColor:(UIColor *)placeholderColor;
- (void)setMargin:(float)margin;
- (UIView *)paddingViewWithImage:(UIImageView*)imageView andPadding:(float)padding;

- (CGRect)textRectForBounds:(CGRect)bounds AndLeftMargin:(CGFloat)leftMargin;
- (CGRect)editingRectForBounds:(CGRect)bounds AndLeftMargin:(CGFloat)leftMargin;

//- (void)drawPlaceholder:(CGRect)rect;

@end
