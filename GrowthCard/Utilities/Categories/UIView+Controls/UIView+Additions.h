//
//  UIView+Additions.h
//
//  Created by Arvind Singh on 26/04/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "FRHyperLabel.h"
@protocol UIViewDelegateSeeMore;

@interface UIView (Additions)



- (UIImage *)screenshot;
- (void)doEaseInEaseOutAnimation;

+ (id)viewFromXib:(NSString *)name classname:(Class)classname owner:(id)owner;

- (UIView *)subViewWithTag:(NSInteger)tag;
- (void)makeRoundCornerWithBorder:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

- (void)addBorder:(CGFloat)borderWidth color:(UIColor *)color;

- (void)addBordarOnButton:(UIColor *)color andBordarWidth:(int)value;

- (void)roundCorner:(float)radius border:(float)border borderColor:(UIColor *)clr;

- (void)scrollToY:(float)y;
- (void)scrollToView:(UIView *)view;
- (void)scrollElement:(UIView *)view toPoint:(float)y;

- (void)addSeeMoreRecognizer:(FRHyperLabel*)lable AndDesc:(NSString*)comment;
@property(nonatomic,assign)   id <UIViewDelegateSeeMore>   delegate;
@end


@protocol UIViewDelegateSeeMore
@optional
- (void)seeMoreClick;
@end
