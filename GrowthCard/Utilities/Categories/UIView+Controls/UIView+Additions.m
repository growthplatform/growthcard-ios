//
//  UIView+Additions.m
//
//  Created by Arvind Singh on 26/04/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "UIView+Additions.h"


@implementation UIView (Additions)

- (UIImage *)screenshot {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    if([self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    } else {
        [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSLog(@"SizeScale: %f",image.scale);
    return image;
}

- (void)doEaseInEaseOutAnimation {
    // remove previous animation if any
    [self.layer removeAllAnimations];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.fromValue = [NSNumber numberWithFloat:1.0f];
    animation.toValue = [NSNumber numberWithFloat:1.3f];
    animation.duration = 0.5f;
    animation.repeatCount = 3.0f; //FLT_MAX;
    animation.autoreverses = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.layer addAnimation:animation forKey:nil];
}

+ (id)viewFromXib:(NSString *)name classname:(Class)classname owner:(id)owner {
    id View = nil;
    NSArray *arrViews = [[NSBundle mainBundle] loadNibNamed:name owner:owner options:nil];
    for (id view in arrViews) {
        if([view isKindOfClass:classname]) {
            View = view;
            break;
        }
    }
    return View;
}

- (UIView *)subViewWithTag:(NSInteger)tag {
    for (UIView *v in self.subviews) {
        if (v.tag == tag) {
            return v;
        }
    }
    return nil;
}

- (void)makeRoundCornerWithBorder:(CGFloat)borderWidth borderColor:(UIColor *)borderColor {
    CGFloat radius = CGRectGetWidth(self.frame) / 2.0f;
    // Create your mask first
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)) cornerRadius:radius];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
    
    // And then create the outline layer
    CAShapeLayer *shape = [CAShapeLayer layer];
    shape.frame = self.bounds;
    shape.path = maskPath.CGPath;
    shape.lineWidth = borderWidth;
    shape.strokeColor = borderColor.CGColor;
    shape.fillColor = [UIColor clearColor].CGColor;
    [self.layer addSublayer:shape];
}

- (void)addBorder:(CGFloat)borderWidth color:(UIColor *)color {
    UIView *bottomLineView = [self viewWithTag:111];
    if (bottomLineView) {
        [bottomLineView removeFromSuperview];
        bottomLineView = nil;
    }
    bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, (self.bounds.size.height - borderWidth), self.bounds.size.width, borderWidth)];
    bottomLineView.backgroundColor = color;
    bottomLineView.tag = 111;
    [self addSubview:bottomLineView];
}

- (void)addBordarOnButton:(UIColor *)color andBordarWidth:(int)value {
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = value;
    self.layer.cornerRadius = 4.0;
    self.layer.masksToBounds = YES;
}

- (void)roundCorner:(float)radius border:(float)border borderColor:(UIColor *)clr {
    self.layer.cornerRadius = radius;
    self.clipsToBounds = YES;
    if (clr) self.layer.borderColor = clr.CGColor;
    self.layer.borderWidth = border;
}

-(void)scrollToY:(float)y {
    [UIView beginAnimations:@"registerScroll" context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.4];
    self.transform = CGAffineTransformMakeTranslation(0, y);
    [UIView commitAnimations];
}

-(void)scrollToView:(UIView *)view {
    CGRect theFrame = view.frame;
    float y = theFrame.origin.y - 15;
    y -= (y/1.7);
    [self scrollToY:-y];
}

-(void)scrollElement:(UIView *)view toPoint:(float)y {
    CGRect theFrame = view.frame;
    float orig_y = theFrame.origin.y;
    float diff = y - orig_y;
    if (diff < 0) {
        [self scrollToY:diff];
    }
    else {
        [self scrollToY:0];
    }
    
}

- (void)addSeeMoreRecognizer:(FRHyperLabel*)lable AndDesc:(NSString*)comment {
    if (comment.length<= kSeeMoreCharLimit ){
        lable.text=comment;
        return;
    }
    NSString *substring = [comment substringToIndex:kSeeMoreCharLimit-1];
    NSString *finalString=[NSString stringWithFormat:@"%@Read More",substring];
    NSDictionary *attributes = @{NSFontAttributeName: lable.font};
    lable.linkAttributeDefault = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0353 green:0.6588 blue:0.051 alpha:1.0],
                                   NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]};
    
    lable.attributedText = [[NSAttributedString alloc]initWithString:finalString attributes:attributes];
    [lable setLinkForSubstring:@"Read More" withLinkHandler:^(FRHyperLabel *labelL, NSString *substring) {
        labelL.text=comment;
        [(id<UIViewDelegateSeeMore>)self.delegate seeMoreClick];
    }];
}

@end


