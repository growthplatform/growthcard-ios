//
//  UIBorderLabel.m
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "UIBorderLabel.h"

@implementation UIBorderLabel

@synthesize topInset, leftInset, bottomInset, rightInset;

-(void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = {self.topInset, self.leftInset,
        self.bottomInset, self.rightInset};
    
    self.adjustsFontSizeToFitWidth=YES;
    
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end