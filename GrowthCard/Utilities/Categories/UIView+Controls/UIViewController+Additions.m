//
//  UIViewController+Additions.m
//  GrowthCard
//
//  Created by Pawan Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "UIViewController+Additions.h"

@implementation UIViewController (Additions)
- (void)addSeeMoreRecognizer:(FRHyperLabel*)lable AndDesc:(NSString*)comment AndDelegate:(BaseVC*)delegate
{
    if (comment.length<= kSeeMoreCharLimit ){
        lable.text=comment;
        return;
    }
    NSString *substring = [comment substringToIndex:kSeeMoreCharLimit-1];
    NSString *finalString=[NSString stringWithFormat:@"%@Read More",substring];
    NSDictionary *attributes = @{NSFontAttributeName: lable.font};
    lable.linkAttributeDefault = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0353 green:0.6588 blue:0.051 alpha:1.0],
                                   NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]};
    
    lable.attributedText = [[NSAttributedString alloc]initWithString:finalString attributes:attributes];
    
    
    [lable setLinkForSubstring:@"Read More" withLinkHandler:^(FRHyperLabel *label, NSString *substring) {
        NSLog(@"Clicked");
    }];
    
    [lable setLinkForSubstring:@"Read More" withLinkHandler:^(FRHyperLabel *labelL, NSString *substring) {
        NSLog(@"see more by pawan");

        labelL.text=comment;
        
        [delegate seeMoreClick];

    }];
}
@end
