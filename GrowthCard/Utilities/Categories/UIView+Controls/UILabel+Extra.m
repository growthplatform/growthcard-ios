//
//  UILabel+Extra.m
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "UILabel+Extra.h"

@implementation UILabel (Extra)

- (void)setFrameAsTextStickToWidth:(CGFloat)sWidth {
    [self setFrameAsTextStickToWidth:sWidth andHeight:MAXFLOAT];
}

- (void)setFrameAsTextStickToHeight:(CGFloat)sHeight {
    [self setFrameAsTextStickToWidth:MAXFLOAT andHeight:sHeight];
}

- (void)setFrameAsTextStickToWidth:(CGFloat)sWidth andHeight:(CGFloat)sHeight {
    
    CGSize sz = [self.text actualSizeWithFont:self.font stickToWidth:sWidth andHeight:sHeight];
    NSLog(@"%f %f", sz.height, sz.width);
    
    CGRect frame = self.frame;
    frame.size.width  =  sz.width+5;
    frame.size.height =  sz.height+5;
    self.frame = frame;
}

- (void)boldRange:(NSRange)range {
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText;
    if (!self.attributedText) {
        attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    } else {
        attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    }
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize]} range:range];
    self.attributedText = attributedText;
}

- (void)boldSubstring:(NSString*)substring {
    NSRange range = [self.text rangeOfString:substring];
    [self boldRange:range];
}

-(CGFloat)heightForLabel:(NSString *)text{
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:self.font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.frame.size.width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    return ceil(rect.size.height);
}




/////----------- copy to Clicp ---------------//
//- (void) attachTapHandler {
//    [self setUserInteractionEnabled:YES];
//    UIGestureRecognizer *touchy = [[UITapGestureRecognizer alloc]
//                                   initWithTarget:self action:@selector(handleTap:)];
//    [self addGestureRecognizer:touchy];
//}
//
//#pragma mark Clipboard
////- (void) copy: (id) sender {
////    NSLog(@"Copy handler, label: “%@”.", self.text);
////}
//
//- (void)copy:(id)sender {
//    NSLog(@"Copy handler, label: “%@”.", self.text);
//    UIPasteboard *board = [UIPasteboard generalPasteboard];
//    [board setString:self.text];
//    self.highlighted = NO;
//    [self resignFirstResponder];
//}
//
//- (BOOL) canPerformAction: (SEL) action withSender: (id) sender {
//    NSMutableArray *linkArray = [NSMutableArray new];
//    NSError *error = nil;
//    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
//    NSArray *matches = [detector matchesInString:self.text
//                                         options:0
//                                           range:NSMakeRange(0, [self.text length])];
//    for (NSTextCheckingResult *match in matches) {
//        NSRange matchRange = [match range];
//        if ([match resultType] == NSTextCheckingTypeLink) {
//            NSURL *url = [match URL];
//            NSLog(@"url : %@", url.description );
//            
//            NSArray* yourArray = [NSArray arrayWithArray:linkArray];
//            if ( [yourArray containsObject: url.absoluteString] ) {
//                // do found
//            } else {
//                // do not found
//                [linkArray addObject:url.absoluteString];
//            }
//        }
//    }
//    if(linkArray.count > 0) {
//        [linkArray addObject:self.text];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kUILabelUrlNotification object:linkArray];
//    }
//    
//    return (action == @selector(copy:));
//}
//
//
//- (void) handleTap: (UIGestureRecognizer*) recognizer {
//    [self becomeFirstResponder];
//    UIMenuController *menu = [UIMenuController sharedMenuController];
//    [menu setTargetRect:self.frame inView:self.superview];
//    [menu setMenuVisible:YES animated:YES];
//}
//
//- (BOOL) canBecomeFirstResponder {
//    return YES;
//}




@end
