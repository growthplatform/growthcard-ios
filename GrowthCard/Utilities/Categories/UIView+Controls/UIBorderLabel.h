//
//  UIBorderLabel.h
//  GrowthCard
//
//  Created by Pawan Kumar on 15/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIAttributedLabel.h"

@interface UIBorderLabel : /*NIAttributedLabel*/ UILabel
{
    CGFloat topInset;
    CGFloat leftInset;
    CGFloat bottomInset;
    CGFloat rightInset;
}

@property (nonatomic) CGFloat topInset;
@property (nonatomic) CGFloat leftInset;
@property (nonatomic) CGFloat bottomInset;
@property (nonatomic) CGFloat rightInset;

@end

