//
//  UILabel+Extra.h
//
//  Created by Pawan Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//
//  - A category class is responsible to set frame of label according to font & text.

#import <UIKit/UIKit.h>


@interface UILabel (Extra)

// @method : to set frame according to text & font.
// pass your desired width.
- (void)setFrameAsTextStickToWidth:(CGFloat)sWidth;

// @method : to set frame according to text & font.
// pass your desired width.
- (void)setFrameAsTextStickToHeight:(CGFloat)sHeight;

// @method : to set frame according to text & font.
// pass you max width & height.
- (void)setFrameAsTextStickToWidth:(CGFloat)sWidth andHeight:(CGFloat)sHeight;

// @method : to bold specific text(substring).
// pass you substring.
- (void) boldSubstring:(NSString *)substring;

// @method : to bold specific text(substring) on provided range.
// pass you range of text.
- (void) boldRange:(NSRange)range;
- (CGFloat)heightForLabel:(NSString *)text;


// @method : to select text in UILabel.
- (void) attachTapHandler;


@end
