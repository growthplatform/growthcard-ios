//
//  UITextField+Additions.m
//
//  Created by Arvind Singh on 22/07/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "UITextField+Additions.h"

@implementation UITextField (Additions)

- (NSString *)textByTrimmingWhiteSpacesAndNewline {
    NSCharacterSet *whitespaceAndNewline = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [self.text stringByTrimmingCharactersInSet:whitespaceAndNewline];
    self.text = trimmedString;
    
    return trimmedString;
}

- (BOOL)isTextFieldEmpty {
    NSString *str = self.text; //[self textByTrimmingWhiteSpacesAndNewline];
    return ([str length] == 0);
}

- (void)trimWhiteSpacesAndNewline {
    NSCharacterSet *whitespaceAndNewline = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [self.text stringByTrimmingCharactersInSet:whitespaceAndNewline];
    self.text = trimmedString;
}

- (BOOL)isMobileNumberIsValid {
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    return [mobileNumberPred evaluateWithObject:self.text];
}

- (BOOL)isUSAMobileNumberIsValid {
    //    ^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$
    //    and
    //    
    //    ^[0-9]{3}-[0-9]{3}-[0-9]{4}$
    
    NSString *mobileNumberPattern = @"[^[0-9]{3}-[0-9]{3}-[0-9]{4}$";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    return [mobileNumberPred evaluateWithObject:self.text];
}

- (void)addPaddingInTextField:(UITextFieldPadding)padding withValue:(CGFloat)value {
    if (padding == UITextFieldPaddingLeft) {
        self.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,value,value)];
        self.leftViewMode = UITextFieldViewModeAlways;
    }
    else if(padding == UITextFieldPaddingRight) {
        self.rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,value, value)];
        self.rightViewMode = UITextFieldViewModeAlways;
    }
}

- (BOOL)applyedFormettedMobilNumberOnTextField:(UITextField *)textField andrange:(NSRange)range {
    NSInteger length = [self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);
    
    if(length == 10) {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3) {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"(%@) ",num];
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6) {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber {
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    NSInteger length = [mobileNumber length];
    if(length > 10) {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
    }
    
    return mobileNumber;
}

- (NSInteger)getLength:(NSString *)mobileNumber {
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    
    return length;    
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    [self setValue:placeholderColor forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)setMargin:(float)margin {
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, margin, 20)];
    self.leftView = padView1;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (UIView *)paddingViewWithImage:(UIImageView*)imageView andPadding:(float)padding
{
    float height = CGRectGetHeight(imageView.frame);
    float width =  CGRectGetWidth(imageView.frame) + padding;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [paddingView addSubview:imageView];
    return paddingView;
}


- (void)setClearButtonImage:(UIImage *)image {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0.0f, 0.0f, 15.0f, 15.0f)]; // Required for iOS7
    self.rightView = button;
    self.rightViewMode = UITextFieldViewModeWhileEditing;
}

- (CGRect)textRectForBounds:(CGRect)bounds AndLeftMargin:(CGFloat)leftMargin {
    bounds.origin.x += leftMargin;
    return bounds;
}

- (CGRect)editingRectForBounds:(CGRect)bounds AndLeftMargin:(CGFloat)leftMargin {
    bounds.origin.x += leftMargin;
    return bounds;
}

@end
