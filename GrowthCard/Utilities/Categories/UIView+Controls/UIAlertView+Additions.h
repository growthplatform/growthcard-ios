//
//  UIAlertView+Additions.h
//
//  Created by Shipra Dhooper on 16/02/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Completion handler invoked when user taps a button.
 * buttonIndex The index of the button tapped.
 */
typedef void (^DismissBlock)(NSInteger buttonIndex);
typedef void (^CancelBlock)();

/**
 * Completion handler invoked when user taps a button.
 *
 * @param alertView The alert view being shown.
 * @param buttonIndex The index of the button tapped.
 */
typedef void(^UIAlertViewHandler)(UIAlertView *alertView, NSInteger buttonIndex);


@interface UIAlertView (Additions) <UIAlertViewDelegate>

// Shows Alert view with specified type
+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message;
+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle;
+ (UIAlertView *)alertViewWithTitle:(NSString*)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle
                  otherButtonTitles:(NSArray *)otherButtons onDismiss:(DismissBlock)dismissed onCancel:(CancelBlock)cancelled;
+ (UIAlertView *)showAlertViewWithMessage:(NSString*)message delegate:(id)delegate tag:(NSInteger)tag;

/**
 * Shows the receiver alert with the given handler.
 */
@property (copy, nonatomic) DismissBlock dismissBlock;
@property (copy, nonatomic) CancelBlock cancelBlock;

@end
