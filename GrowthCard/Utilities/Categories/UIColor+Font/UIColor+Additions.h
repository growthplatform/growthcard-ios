//
//  UIColor+Additions.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Convenience method to return a UIColor with RGB values based on 255
 */
#define UIColorMakeRGB(Red,Green,Blue) [UIColor colorWithRed:(Red)/255.0f green:(Green)/255.0f blue:(Blue)/255.0f alpha:1.0f]
#define UIColorMakeRGBA(Red,Green,Blue,Alpha) [UIColor colorWithRed:(Red)/255.0f green:(Green)/255.0f blue:(Blue)/255.0f alpha:Alpha]

@interface UIColor (Additions)

/**
 * Return a UIColor from a 3- or 6-digit hex string
 * @param hexString The hex color string value
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString;

/**
 * Return a UIColor from a 3- or 6-digit hex string and an alpha value
 * @param hexString The hex color string value
 * @param alpha The color's alpha value
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(CGFloat)alpha;

/**
 * Return a UIColor from a RGBA int
 * @param value The int value
 */
+ (UIColor *)colorWithRGBAValue:(uint)value;

/**
 * Return a UIColor from a ARGB int
 * @param value The int value
 */
+ (UIColor *)colorWithARGBValue:(uint)value;

/**
 * Return a UIColor from a RGB int
 * @param value The int value
 */
+ (UIColor *)colorWithRGBValue:(uint)value;

/**
 * Return text field color
 */
+ (UIColor *)textFieldTextColor;

/**
 * Return search bar color
 */
+ (UIColor *)searchBarTextColor;

/**
 * Return
 */
+ (UIColor *)textFieldPlaceholderColor;

/**
 * Return statusbar tint color
 */
+ (UIColor *)statusBarTintColor;

/**
 * Return image border color
 */
+ (UIColor *)imageBorderColor;

/**
 * Return navigation bar color
 */
+ (UIColor *)navigationBarColor;
+ (UIColor *)navigationBarColorWithAlpha;

/**
 * Return tableview seprator color
 */
+ (UIColor *)tableSepratorColor;

/**
 * Return previousSongArtistNameLabel color
 */
+ (UIColor *)previousSongArtistNameLabelColor;

/**
 * Return commentTextColor
 */
+ (UIColor *)commentTextColorWithAlpha;

/**
 * Return Label Placeholder Color
 */
+ (UIColor *)placeholderColor;

/**
 * Return Border Line Color
 */
+ (UIColor *)borderLineColor;

/**
 * Return Blur Color
 */
+ (UIColor *)blurImageColor;

/**
 * Return Divider Color
 */
+ (UIColor *)divColor;

/**
 * Return user feed Text color
 */
+ (UIColor *)feedTextColor;

/**
 * Return done button color with alpha
 */
+ (UIColor *)doneBarButtonColorWithAlpha;

/**
 * Return done button color
 */
+ (UIColor *)doneBarButtonColor;

/**
 * Return segment text color
 */
+ (UIColor *)segmentTextColor;

/**
 * Return background color
 */
+ (UIColor *)backgroundColor;
/**
 * Return grayBlackColor color
 */
+ (UIColor *)grayBlackColor;


@end
