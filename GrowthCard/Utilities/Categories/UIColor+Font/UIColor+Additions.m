//
//  UIColor+Additions.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UIColor+Additions.h"

@implementation UIColor (Additions)

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    return [UIColor colorWithHexString:hexString withAlpha:1.0];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(CGFloat)alpha {
    uint value = 0;
    
    if (hexString) {
        int startingIndex = ([hexString hasPrefix:@"#"]) ? 1 : 0;
        BOOL useSuppliedAlpha = YES;
        
        if ((hexString.length - startingIndex) == 3) {
            NSString *h1 = [hexString substringWithRange:NSMakeRange(0 + startingIndex, 1)];
            NSString *h2 = [hexString substringWithRange:NSMakeRange(1 + startingIndex, 1)];
            NSString *h3 = [hexString substringWithRange:NSMakeRange(2 + startingIndex, 1)];
            
            hexString = [NSString stringWithFormat:@"%@%@%@%@%@%@", h1, h1, h2, h2, h3, h3];
            
            startingIndex = 0;
        }
        else if ((hexString.length - startingIndex) == 4) {
            NSString *h1 = [hexString substringWithRange:NSMakeRange(0 + startingIndex, 1)];
            NSString *h2 = [hexString substringWithRange:NSMakeRange(1 + startingIndex, 1)];
            NSString *h3 = [hexString substringWithRange:NSMakeRange(2 + startingIndex, 1)];
            NSString *h4 = [hexString substringWithRange:NSMakeRange(3 + startingIndex, 1)];
            
            hexString = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@", h1, h1, h2, h2, h3, h3, h4, h4];
            
            startingIndex = 0;
            useSuppliedAlpha = NO;
        }
        else if ((hexString.length - startingIndex) == 8) {
            useSuppliedAlpha = NO;
        }
        
        [[NSScanner scannerWithString:[hexString substringFromIndex:startingIndex]] scanHexInt:&value];
        
        if (useSuppliedAlpha) {
            // clip to bounds
            alpha = MIN(MAX(0.0, alpha), 1.0);
            uint alphaByte = (uint) (alpha * 255.0);
            
            value |= (alphaByte << 24);
        }
    }
    
    return [UIColor colorWithARGBValue:value];
}

+ (UIColor *)colorWithARGBValue:(uint)value {
    uint a = (value & 0xFF000000) >> 24;
    uint r = (value & 0x00FF0000) >> 16;
    uint g = (value & 0x0000FF00) >> 8;
    uint b = (value & 0x000000FF);
    
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a/255.0];
}

+ (UIColor *)colorWithRGBAValue:(uint)value {
    uint r = (value & 0xFF000000) >> 24;
    uint g = (value & 0x00FF0000) >> 16;
    uint b = (value & 0x0000FF00) >> 8;
    uint a = (value & 0x000000FF);
    
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a/255.0];
}

+ (UIColor *)colorWithRGBValue:(uint)value {
    uint r = (value & 0x00FF0000) >> 16;
    uint g = (value & 0x0000FF00) >> 8;
    uint b = (value & 0x000000FF);
    
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0];
}

+ (UIColor *)textFieldTextColor {
    return UIColorMakeRGB(0.0, 0.0, 0.0);
}

+ (UIColor *)searchBarTextColor {
    return UIColorMakeRGB(23.0, 38.0, 71.0);
}

+ (UIColor *)textFieldPlaceholderColor {
    return UIColorMakeRGB(0.0, 71.0, 101.0);
}

+ (UIColor *)bufferProgressColor {
    return UIColorMakeRGB(159.0, 163.0, 164.0);
}

+ (UIColor *)audioProgressColor {
    return UIColorMakeRGB(38.0, 173.0, 206.0);
}

+ (UIColor *)statusBarTintColor {
    return UIColorMakeRGB(34, 173, 206);
}

+ (UIColor *)imageBorderColor {
    return UIColorMakeRGB(231.0, 232.0, 234.0);
}

+ (UIColor *)navigationBarColor {
    return UIColorMakeRGB(0, 172, 244);
}


+ (UIColor *)tableSepratorColor {
    return UIColorMakeRGB(113, 187, 244);
}

+ (UIColor *)navigationBarColorWithAlpha {
    return UIColorMakeRGB(0, 108.0, 165.0);
}

+ (UIColor *)leftMenuLoginButtonColor {
    return UIColorMakeRGB(56.0, 63.0, 66.0);
}

+ (UIColor *)previousSongArtistNameLabelColor {
    return UIColorMakeRGB (51, 153, 204);
}

+ (UIColor *)commentTextColorWithAlpha {
    return UIColorMakeRGBA(23.0, 38.0, 71.0, 0.8);
}

+ (UIColor *)placeholderColor {
    return UIColorMakeRGB (156.0, 161.0, 169.0);
}

+ (UIColor *)borderLineColor {
    return UIColorMakeRGB(233.0, 238.0, 239.0);
}

+ (UIColor *)blurImageColor {
    return UIColorMakeRGB(9.0, 41.0, 48.0);
}

+ (UIColor *)divColor {
    return UIColorMakeRGB (198.0, 200.0, 200.0);
}

+ (UIColor *)feedTextColor {
    return UIColorMakeRGB (23.0, 38.0, 38.0);
}

+ (UIColor *)doneBarButtonColorWithAlpha {
    return UIColorMakeRGBA(255.0, 255.0, 255.0, 0.7);
}

+ (UIColor *)doneBarButtonColor {
    return UIColorMakeRGB(255.0, 255.0, 255.0);
}

+ (UIColor *)segmentTextColor {
    return UIColorMakeRGB(201.0, 200.0, 203.0);
}

+ (UIColor *)backgroundColor {
    return UIColorMakeRGB(236.0, 2420.0, 246.0);
}

+ (UIColor *)grayBlackColor {
    return UIColorMakeRGB(51.0, 51.0, 51.0);
}



@end
