//
//  UIFont+Additions.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Additions)

/**
 * Convenience method to set the font name and size
 */
+ (UIFont *)regularApplicationFontOfSize:(CGFloat)fontSize;
+ (UIFont *)semiboldApplicationFontOfSize:(CGFloat)fontSize;
+ (UIFont *)boldApplicationFontOfSize:(CGFloat)fontSize;
+ (UIFont *)applicationMediumFontOfSize:(CGFloat)fontSize;
+ (UIFont *)demiBoldApplicationFontOfSize:(CGFloat)fontSize;
+ (UIFont *)italicApplicationFontOfSize:(CGFloat)fontSize;

@end
