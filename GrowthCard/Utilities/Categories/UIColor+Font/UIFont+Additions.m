//
//  UIFont+Additions.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UIFont+Additions.h"

@implementation UIFont (Additions)

+ (UIFont *)regularApplicationFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:kFont_Avenir_Next size:fontSize];
}

+ (UIFont *)semiboldApplicationFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:kAvenir_Next_Semibold size:fontSize];
}

+ (UIFont *)boldApplicationFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:kAvenir_Next_Bold size:fontSize];
}

+ (UIFont *)applicationMediumFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:kAvenir_Next_Medium size:fontSize];
}

+ (UIFont *)demiBoldApplicationFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:kAvenir_Next_DemiBold size:fontSize];
}

+ (UIFont *)italicApplicationFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:kAvenir_Next_italic size:fontSize];
}

@end
