//
//  UITableView+LongPress.m
//  GrowthCard
//
//  Created by Pawan Kumar on 30/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "UITableView+LongPress.h"

@implementation UITableView (LongPress)
@dynamic delegate;

- (void)addLongPressRecognizer {
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    lpgr.delegate = self;
    [self addGestureRecognizer:lpgr];
}

- (void)addSeeMoreRecognizer:(FRHyperLabel*)lable AndDesc:(NSString*)comment
{
    NSString *substring = [comment substringToIndex:kSeeMoreCharLimit-1];
    NSString *finalString=[NSString stringWithFormat:@"%@...Read More",substring];
    NSDictionary *attributes = @{NSFontAttributeName: lable.font};
    lable.linkAttributeDefault = @{NSForegroundColorAttributeName:[UIColor cyanColor] /*[UIColor colorWithRed:0.0353 green:0.6588 blue:0.051 alpha:1.0]*/,
                                   NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]};
    
    lable.attributedText = [[NSAttributedString alloc]initWithString:finalString attributes:attributes];
    
    
    [lable setLinkForSubstring:@"Read More" withLinkHandler:^(FRHyperLabel *label, NSString *substring) {
        
        
        CGPoint buttonPosition = [label convertPoint:CGPointZero
                                              toView: self];
        NSIndexPath *indexPath = [self indexPathForRowAtPoint:buttonPosition];
        
        //        if (self.delegate && [self.delegate respondsToSelector:@selector(didSeeMoreClickOnRowAtIndexPath:)]) {
        //            [(id<UITableViewDelegateLongPress>)self.delegate tableView:self didSeeMoreClickOnRowAtIndexPath:indexPath];
        //        }
        
        [(id<UITableViewDelegateLongPress>)self.delegate tableView:self didSeeMoreClickOnRowAtIndexPath:indexPath];
    }];
}


- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self];
    
    NSIndexPath *indexPath = [self indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        NSLog(@"long press on table view but not on a row");
    }
    else {
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            // I am not sure why I need to cast here. But it seems to be alright.
            //            if (self.delegate && [self.delegate respondsToSelector:@selector(didRecognizeLongPressOnRowAtIndexPath:)]) {
            //                [(id<UITableViewDelegateLongPress>)self.delegate tableView:self didRecognizeLongPressOnRowAtIndexPath:indexPath];
            //            }
            [(id<UITableViewDelegateLongPress>)self.delegate tableView:self didRecognizeLongPressOnRowAtIndexPath:indexPath];
        }
    }
}
@end