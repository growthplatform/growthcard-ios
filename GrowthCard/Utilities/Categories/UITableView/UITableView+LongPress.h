//
//  UITableView+LongPress.h
//  GrowthCard
//
//  Created by Pawan Kumar on 30/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//
#import <UIKit/UIKit.h>

@protocol UITableViewDelegateLongPress;

@interface UITableView (LongPress) <UIGestureRecognizerDelegate>
@property(nonatomic,assign)   id <UITableViewDelegateLongPress>   delegate;
- (void)addLongPressRecognizer;
- (void)addSeeMoreRecognizer:(FRHyperLabel*)lable AndDesc:(NSString*)comment;

@end

@protocol UITableViewDelegateLongPress <UITableViewDelegate>
@optional
- (void)tableView:(UITableView *)tableView didRecognizeLongPressOnRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSeeMoreClickOnRowAtIndexPath:(NSIndexPath *)indexPath;
@end
