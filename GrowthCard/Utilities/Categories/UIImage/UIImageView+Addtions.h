//
//  UIImageView+Addtions.h
//  GrowthCard
//
//  Created by Shipra Dhooper on 17/03/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import <Foundation/Foundation.h>

typedef void (^ImageDownloadCompletionHandler)(UIImage *image, UIImage *bluredImage);

@interface UIImageView (Addtions)

@property (nonatomic, retain) NSObject *property;

@end
