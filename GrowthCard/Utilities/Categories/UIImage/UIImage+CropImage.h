//
//  UIImage+CropImage.h
//  GrowthCard
//
//  Created by Shipra Dhooper on 10/03/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageExtension)

/*! Crop an image in square
 \param <CGFloat> size - dimension of targeted frame.
 \param <UIImage> inputImage - source image.
 \returns <UIImage> - cropped image.
 */

+ (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize;

+ (UIImage *)scaleImageWithAspectRatio:(UIImage *)image toSize:(CGSize)newSize;

+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(float)i_width;

+ (UIImage *)renderImageFromView:(UIView *)view withRect:(CGRect)frame;

+(UIImage *)captureScreen;

+ (UIImage *)roundedRectImageFromImage:(UIImage *)image withRadious:(CGFloat)radious;

- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;
- (void)addRoundedRectToPath:(CGRect)rect context:(CGContextRef)context ovalWidth:(CGFloat)ovalWidth ovalHeight:(CGFloat)ovalHeight;

@end
