//
//  UIImageView+Addtions.m
//  GrowthCard
//
//  Created by Shipra Dhooper on 17/03/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UIImageView+Addtions.h"

@implementation UIImageView (Addtions)


static char UIB_PROPERTY_KEY;

- (void)setProperty:(NSObject *)property {
    objc_setAssociatedObject(self, &UIB_PROPERTY_KEY, property, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSObject *)property {
    return (NSObject *)objc_getAssociatedObject(self, &UIB_PROPERTY_KEY);
}

// imageView.property = @"hello"; (any Object)
// id obj = imageView.property;   (get object)



@end
