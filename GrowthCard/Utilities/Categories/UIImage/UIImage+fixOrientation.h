//
//  UIImage+fixOrientation.h
//  GrowthCard
//
//  Created by Shipra Dhooper on 14/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
