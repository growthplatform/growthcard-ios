//
//  NSString+Additions.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

+ (NSString *)urlEncode:(NSString *)inputString {
    CFStringRef encodedCfStringRef = CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)inputString,NULL,(CFStringRef)@"!*'\"();@+$,%#[]% ",kCFStringEncodingUTF8 );
    NSString *endcodedString = (NSString *)CFBridgingRelease(encodedCfStringRef);
    return endcodedString;
}

+ (BOOL)isEmailValid:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (id)formattedValue:(id)value {
    if(value) {
        if([value isKindOfClass:[NSNull class]]) {
            return [NSString string];
        }
        else if([value isKindOfClass:[NSNumber class]]) {
            return [NSString stringWithFormat:@"%@",value];
        }
        else if(([value caseInsensitiveCompare:@"null"] == NSOrderedSame) || ([value caseInsensitiveCompare:@"(null)"] == NSOrderedSame)) {
            return [NSString string];
        }
        else {
            return value;
        }
    }
    return [NSString string];
}

+ (id)formattedNumber:(id)value {
    
    if(value==[NSNull null])
        return 0;
    
    return [NSNumber numberWithInteger:[value integerValue]];;
}

+ (NSInteger)formattedIntNumber:(id)value {
    
    if(value==[NSNull null])
        return 0;
    
    return [value integerValue];;
}

+ (NSString *)timeStamp {
    return [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
}

- (CGSize)actualSizeWithFont:(UIFont *)font stickToWidth:(CGFloat)sWidth {
    return [self actualSizeWithFont:font stickToWidth:sWidth andHeight:MAXFLOAT];
}

- (CGSize)actualSizeWithFont:(UIFont *)font stickToHeight:(CGFloat)sHeight {
    return [self actualSizeWithFont:font stickToWidth:MAXFLOAT andHeight:sHeight];
}

- (CGSize)actualSizeWithFont:(UIFont *)font
                stickToWidth:(CGFloat)sWidth andHeight:(CGFloat)sHeight {
    
    NSDictionary *attributes = @{NSFontAttributeName: font};
    
    // available only on ios7.0 sdk.
    CGRect rect = [self boundingRectWithSize:CGSizeMake(sWidth, sHeight)
                                     options: NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil];
    return rect.size;
}

- (NSString *)stringByTrimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet {
    NSUInteger location = 0;
    NSUInteger length = [self length];
    unichar charBuffer[length];
    [self getCharacters:charBuffer];
    
    for (; location < length; location++) {
        if (![characterSet characterIsMember:charBuffer[location]]) {
            break;
        }
    }
    return [self substringWithRange:NSMakeRange(location, length - location)];
}

- (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet {
    NSUInteger location = 0;
    NSUInteger length = [self length];
    unichar charBuffer[length];
    [self getCharacters:charBuffer];
    
    for (; length > 0; length--) {
        if (![characterSet characterIsMember:charBuffer[length - 1]]) {
            break;
        }
    }
    return [self substringWithRange:NSMakeRange(location, length - location)];
}

- (NSString*)stringByDeletingWhitespace {
    NSString* str = [self stringByTrimmingLeadingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    str = [str stringByTrimmingTrailingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return str;
}

- (BOOL)isEmptyString {
    NSString *str = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return ([str isEqualToString:@""] || str.length == 0);
}

- (NSString *)withoutWhiteSpaceString {
    NSString *sText = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return sText;
}

// @method : to return a sutable string (convert nit to @"").
+ (NSString *)sutableStrWithStr:(NSString *)str {
    if([[str class]isSubclassOfClass:NSClassFromString(@"NSNull")]) return @"";
    if(!str) return @"";
    str = [NSString stringWithFormat:@"%@", str];
    return (![str isEqualToString:@"<null>"] && ![str isEqualToString:@"(null)"]) ? str : @" ";
}


- (NSString *)camelCased  {
    NSMutableString *result = [NSMutableString new];
    NSArray *words = [self componentsSeparatedByString: @" "];
    for (uint i = 0; i < words.count; i++) {
        if (i==0) {
            [result appendString:((NSString *) words[i]).withLowercasedFirstChar];
        }
        else {
            [result appendString:((NSString *)words[i]).withUppercasedFirstChar];
        }
    }
    return result;
}

- (NSString *)pascalCased  {
    NSMutableString *result = [NSMutableString new];
    NSArray *words = [self componentsSeparatedByString: @" "];
    for (NSString *word in words) {
        [result appendString:word.withUppercasedFirstChar];
    }
    return result;
}

- (NSString *)withUppercasedFirstChar  {
    if (self.length <= 1) {
        return self.uppercaseString;
    } else {
        return [NSString stringWithFormat:@"%@%@",[[self substringToIndex:1] uppercaseString],[self substringFromIndex:1]];
    }
}

- (NSString *)withLowercasedFirstChar {
    if (self.length <= 1) {
        return self.lowercaseString;
    } else {
        return [NSString stringWithFormat:@"%@%@",[[self substringToIndex:1] lowercaseString],[self substringFromIndex:1]];
    }
}
@end
