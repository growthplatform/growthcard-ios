//
//  NSError+Additions.m
//  GrowthCard
//
//  Created by Shipra Dhooper on 18/03/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "NSError+Additions.h"

@implementation NSError (Additions)

+ (NSError *)networkConnectionError {
    NSDictionary *errorDict = @{ NSLocalizedDescriptionKey : NSLocalizedString(@"Connection Error", @"Connection Error"), NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"Network error occurred while performing this task. Please try again later.", @"Network error occurred while performing this task. Please try again later.") };
    NSError *errorReq = [NSError errorWithDomain:HTTPRequestDomain code:HTTPConnectionError userInfo:errorDict];
    return errorReq;
}

+ (NSError *)apiRequestGenericError {
    NSDictionary *errorDict = @{ NSLocalizedDescriptionKey : NSLocalizedString(kAppName, nil), NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"An error occurred while performing the operation. Please try again later.", @"An error occurred while performing the operation. Please try again later.") };
    NSError *errorReq = [NSError errorWithDomain:HTTPRequestDomain code:HTTPResultError userInfo:errorDict];
    
    return errorReq;
}

@end
