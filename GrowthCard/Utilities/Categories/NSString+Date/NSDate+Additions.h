//
//  NSDate+Additions.h
//  GrowthCard
//
//  Created by Shipra Dhooper on 15/04/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)

/*Date Format
-----------
< 1 minute       	= "Just now"
< 1 hour         	= "x minutes ago"
Today            	= "x hours ago"
Yesterday        	= "Yesterday at 1:28pm"
< Last 7 days    	= "Friday at 1:48am"
< Last 30 days   	= "March 30 at 1:14 pm"
< 1 year         	= "September 15"
Anything else    	= "September 9, 2011"

Formatted As Time Ago
Returns the time formatted as Time Ago (in the style of Facebook's mobile date formatting)
*/

+ (NSDate *)getUTCFormateDate:(NSString *)strDate;
- (NSString *)formattedAsTimeAgo:(NSDate *)now;
+ (NSString *)stringFromTimeInterval:(NSTimeInterval)timeInterval;
- (NSString *)formattedAsTimeAgoWithDate:(NSDate *)currentDate;
- (NSString *)formattedAsTimeAgoWithMonthFormat:(NSDate *)now;

+ (NSInteger)timeDurationForActivity:(NSTimeInterval)startInterval;
+ (NSString *)getCurrentDateWithFormat;
+ (BOOL)setDayOrNightMode;

+ (NSDate *)getCurrentDateWithISOFormat;
+ (NSDate *)getEndDateOfTheYear:(NSInteger)count date:(NSDate *)normalizedDate;
+ (NSDate *)convertISOStringIntoDate:(NSString *)dateString;

+ (NSString *)convertDateIntoStringWithISOFormat:(NSDate *)date;
+ (NSString *)convertDateFormat:(NSString *)dateString prevFormat:(NSString *)prevFormat newFormat:(NSString *)newFormat;
+ (NSString *)convertDateIntoString:(NSDate *)date;

+ (NSDate *)getDateFromString:(NSString *)strDate;

+ (NSString *)getHeaderDateStrFromString:(NSString *)strDate;
+ (NSString *)getStartEndDateStrFromString:(NSString *)startDate  AndEndDate:(NSString *)endDate;
+ (NSString *)getAgoFromString:(NSString *)strDate;
+ (NSString *)getAgoTimeFromString:(NSString *)strDate;

-(NSDate *) toLocalTime;
-(NSDate *) toGlobalTime;

@end
