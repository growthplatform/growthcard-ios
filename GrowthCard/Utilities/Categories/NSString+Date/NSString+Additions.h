//
//  NSString+Additions.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

+ (NSString *)urlEncode:(NSString *)inputString;

// @method : to check email string validation.
+ (BOOL)isEmailValid:(NSString *)email;
+ (id)formattedValue:(id)value;
+ (id)formattedNumber:(id)value ;
+ (NSInteger)formattedIntNumber:(id)value ;
+ (NSString *)timeStamp;

// @method : to get actual size with fixed width.
// pass you max width.
- (CGSize)actualSizeWithFont:(UIFont *)font stickToWidth:(CGFloat)sWidth;

// @method : to get actual size with fixed height.
// pass you max height.
- (CGSize)actualSizeWithFont:(UIFont *)font stickToHeight:(CGFloat)sHeight;

// @method : to get actual size with fixed width & height.
// pass you max width & height.
- (CGSize)actualSizeWithFont:(UIFont *)font stickToWidth:(CGFloat)sWidth
                   andHeight:(CGFloat)sHeight;

- (NSString*)stringByDeletingWhitespace;
- (NSString *)stringByTrimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet;
- (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet;

// @method : to check the empty string.
- (BOOL)isEmptyString;
- (NSString *)withoutWhiteSpaceString;

+ (NSString *)sutableStrWithStr:(NSString *)str;

- (NSString *)camelCased;
- (NSString *)pascalCased;
@end
