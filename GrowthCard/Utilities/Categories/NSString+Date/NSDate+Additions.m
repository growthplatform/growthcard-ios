//
//  NSDate+Additions.m
//  GrowthCard
//
//  Created by Shipra Dhooper on 15/04/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)


#define SECOND  1
#define MINUTE  (SECOND * 60)
#define HOUR    (MINUTE * 60)
#define DAY     (HOUR   * 24)
#define WEEK    (DAY    * 7)
#define MONTH   (DAY    * 31)
#define YEAR    (DAY    * 365.24)

/*
 Mysql Datetime Formatted As Time Ago
 Takes in a mysql datetime string and returns the Time Ago date format
 */
//+ (NSString *)mysqlDatetimeFormattedAsTimeAgo:(NSString *)mysqlDatetime
//{
//    //http://stackoverflow.com/questions/10026714/ios-converting-a-date-received-from-a-mysql-server-into-users-local-time
//    //If this is not in UTC, we don't have any knowledge about
//    //which tz it is. MUST BE IN UTC.
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//
//    NSDate *date = [formatter dateFromString:mysqlDatetime];
//
//    return [date formattedAsTimeAgo];
//
//}




+ (NSDate *)getUTCFormateDate:(NSString *)strDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:strDate];
    return date;
}

+ (NSDate *)getDateFromString:(NSString *)strDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:strDate];
    return date;
}

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)timeInterval {
    NSString *sText = @"";
    
    if (timeInterval < 60) {
        sText = @"just now";
    }
    else if (timeInterval < 3600) {
        timeInterval = round(timeInterval / 60);
        if (timeInterval == 0) timeInterval = 1;
        
        sText = [NSString stringWithFormat:@"%.0f %@ ago", timeInterval, (timeInterval > 1) ? @"m":@"m"];
    }
    else if (timeInterval < 86400) {
        timeInterval = round(timeInterval / 3600);
        if (timeInterval == 0) timeInterval = 1;
        
        sText = [NSString stringWithFormat:@"%.0f %@ ago", timeInterval, (timeInterval > 1) ? @"h":@"h"];
    }
    else {
        timeInterval = round(timeInterval / 86400);
        if (timeInterval == 0) timeInterval = 1;
        
        sText = [NSString stringWithFormat:@"%.0f %@ ago", timeInterval, (timeInterval > 1) ? @"d":@"d"];
    }
    
    return sText;
}

/**
 * Formatted As Time Ago
 * Returns the date formatted as Time Ago (in the style of the mobile time ago date formatting for Facebook)
 */
- (NSString *)formattedAsTimeAgo:(NSDate *)now {
    // Now
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    
    // Should never hit this but handle the future case
    if (secondsSince < 0)
        return @"Just now";
    
    
    // < 1 minute = "Just now"
    if (secondsSince < MINUTE)
        return @"Just now";
    
    
    // < 1 hour = "x minutes ago"
    if (secondsSince < HOUR)
        return [self formatMinutesAgo:secondsSince];
    
    
    // Today = "x hours ago"
    if ([self isSameDayAs:now])
        return [self formatAsToday:secondsSince];
    
    
    // Yesterday = "Yesterday at 1:28 PM"
    if ([self isYesterday:now])
        return [self formatAsYesterday];
    
    
    // < Last 7 days = "Friday at 1:48 AM"
    if ([self isLastWeek:secondsSince])
        return [self formatAsLastWeek];
    
    
    // < Last 30 days = "March 30 at 1:14 PM"
    if ([self isLastMonth:secondsSince])
        return [self formatAsLastMonth];
    
    // < 1 year = "September 15"
    if ([self isLastYear:secondsSince])
        return [self formatAsLastYear];
    
    // Anything else = "September 9, 2011"
    return [self formatAsOther];
    
}

- (NSString *)formattedAsTimeAgoWithMonthFormat:(NSDate *)now {
    // Now
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    
    // Should never hit this but handle the future case
    if (secondsSince < 0)
        return @"Just now";
    
    
    // < 1 minute = "Just now"
    if (secondsSince < MINUTE)
        return @"Just now";
    
    
    // < 1 hour = "x minutes ago"
    if (secondsSince < HOUR)
        return [self formatMinutesAgo:secondsSince];
    
    
    // Today = "x hours ago"
    if ([self isSameDayAs:now])
        return [self formatAsToday:secondsSince];
    
    
    // Yesterday = "Yesterday at 1:28 PM"
    if ([self isYesterday:now])
        return [self formatAsYesterday];
    
    
    // < Last 7 days = "Friday at 1:48 AM"
    if ([self isLastWeek:secondsSince])
        return [self formatAsLastWeek];
    
    
    // < Last 30 days = "March 30 at 1:14 PM"
    if ([self isLastMonth:secondsSince])
        return [self formatAsLastMonth];
    
    // < 1 year = "Sep 15"
    if ([self isLastYear:secondsSince])
        return [self formatAsLastMonthYear];
    
    // Anything else = "Sep 9, 2011"
    return [self formatAsOtherMonth];
    
}


- (NSString *)formattedAsTimeAgoWithDate:(NSDate *)currentDate {
    // Now
    NSDate *now = currentDate; //[NSDate date];
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    
    // Should never hit this but handle the future case
    if (secondsSince < 0)
        return @"Just now";
    
    
    // < 1 minute = "Just now"
    if(secondsSince < MINUTE)
        return @"Just now";
    
  
    // < 1 hour = "x minutes ago"
    if (secondsSince < HOUR)
        return [self formatMinutesAgo:secondsSince];
    
    
    // Today = "x hours ago"
    if ([self isSameDayAs:now])
        return [self formatAsToday:secondsSince];
    
    
    // Yesterday = "Yesterday at 1:28 PM"
    if ([self isYesterday:now])
        return [self formatAsYesterday];
    
    
    // < Last 7 days = "Friday at 1:48 AM"
    if ([self isLastWeek:secondsSince])
        return [self formatAsLastWeek];
    
    
    // < Last 30 days = "March 30 at 1:14 PM"
    if ([self isLastMonth:secondsSince])
        return [self formatAsLastMonth];
    
    // < 1 year = "September 15"
    if ([self isLastYear:secondsSince])
        return [self formatAsLastYear];
    
    // Anything else = "September 9, 2011"
    return [self formatAsOther];
}

/*
 ========================== Date Comparison Methods ==========================
 */

/**
 * Is Same Day As
 * Checks to see if the dates are the same calendar day
 */
- (BOOL)isSameDayAs:(NSDate *)comparisonDate {
    // Check by matching the date strings
    NSDateFormatter *dateComparisonFormatter = [[NSDateFormatter alloc] init];
    [dateComparisonFormatter setDateFormat:kDefaultDateFormat];
    
    // Return true if they are the same
    return [[dateComparisonFormatter stringFromDate:self] isEqualToString:[dateComparisonFormatter stringFromDate:comparisonDate]];
}

/**
 * If the current date is yesterday relative to now
 * Pasing in now to be more accurate (time shift during execution) in the calculations
 */
- (BOOL)isYesterday:(NSDate *)now {
    return [self isSameDayAs:[now dateBySubtractingDays:1]];
}


// From https://github.com/erica/NSDate-Extensions/blob/master/NSDate-Utilities.m
- (NSDate *) dateBySubtractingDays: (NSInteger) numDays {
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + DAY * -numDays;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

/**
 * Is Last Week
 * We want to know if the current date object is the first occurance of
 * that day of the week (ie like the first friday before today
 * - where we would colloquially say "last Friday")
 * ( within 6 of the last days)
 
 * TODO: make this more precise (1 week ago, if it is 7 days ago check the exact date)
 */
- (BOOL)isLastWeek:(NSTimeInterval)secondsSince {
    return secondsSince < WEEK;
}

/**
 * Is Last Month
 * Previous 31 days?
 * TODO: Validate on fb
 * TODO: Make last day precise
 */
- (BOOL)isLastMonth:(NSTimeInterval)secondsSince {
    return secondsSince < MONTH;
}

/**
 * Is Last Year
 * TODO: Make last day precise
 */
- (BOOL)isLastYear:(NSTimeInterval)secondsSince {
    return secondsSince < YEAR;
}

/*
 =============================================================================
 */





/*
 ========================== Formatting Methods ==========================
 */


// < 1 hour = "x minutes ago"
- (NSString *)formatMinutesAgo:(NSTimeInterval)secondsSince {
    // Convert to minutes
    int minutesSince = (int)secondsSince / MINUTE;
    
    // Handle Plural
    if(minutesSince == 1)
        return @"1 minute ago";
    else
        return [NSString stringWithFormat:@"%d minutes ago", minutesSince];
}


// Today = "x hours ago"
- (NSString *)formatAsToday:(NSTimeInterval)secondsSince {
    // Convert to hours
    int hoursSince = (int)secondsSince / HOUR;
    
    // Handle Plural
    if(hoursSince == 1)
        return @"1 hour ago";
    else
        return [NSString stringWithFormat:@"%d hours ago", hoursSince];
}


// Yesterday = "Yesterday at 1:28 PM"
- (NSString *)formatAsYesterday {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"h:mm a"];
    return [NSString stringWithFormat:@"Yesterday at, %@", [dateFormatter stringFromDate:self]];
    //return [NSString stringWithFormat:@"Yesterday"];

}


// < Last 7 days = "Friday at 1:48 AM"
- (NSString *)formatAsLastWeek {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"EEEE 'at,' h:mm a"];
    return [dateFormatter stringFromDate:self];
}


// < Last 30 days = "March 30 at 1:14 PM"
- (NSString *)formatAsLastMonth {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"d MMM 'at,' h:mm a"];
    return [dateFormatter stringFromDate:self];
}


// < 1 year = "September 15"
- (NSString *)formatAsLastYear {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"MMMM d"];
    return [dateFormatter stringFromDate:self];
}

// < 1 year = "Sep 15"
- (NSString *)formatAsLastMonthYear {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"d MMM"];
    return [dateFormatter stringFromDate:self];
}



// Anything else = "September 9, 2011"
- (NSString *)formatAsOther {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}

// Anything else = "Sep 9, 2011"
- (NSString *)formatAsOtherMonth {
    // Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Format
    [dateFormatter setDateFormat:@"d LLL, yyyy"];
    return [dateFormatter stringFromDate:self];
}

+ (NSInteger)timeDurationForActivity:(NSTimeInterval)startInterval {
    NSTimeInterval endInterval = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval timeDifference = endInterval - startInterval;
    
    NSInteger ti = (NSInteger)timeDifference;
    //    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    //    NSInteger hours = (ti / 3600);
    return minutes;
}


/*
 =======================================================================
 */
+ (BOOL)setDayOrNightMode {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH.mm"];
    NSString *strCurrentTime = [dateFormatter stringFromDate:[NSDate date]];
    if ([strCurrentTime floatValue] >= 20.00 || [strCurrentTime floatValue]  <= 6.00) {
        return NO;
    }
    else {
        return YES;
    }
}

/*
 =======================================================================
 */





/*
 ========================== Test Method ==========================
 */

/**
 * Test the format
 * TODO: Implement unit tests
 */
/*
 =======================================================================
 */

+ (NSString *)getCurrentDateWithFormat {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy"];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *theTime = [timeFormat stringFromDate:now];
    
    NSLog(@"\n"
          "theDate: |%@| \n"
          "theTime: |%@| \n", theDate, theTime);
    
    return theDate;
}

+ (NSDate *)getCurrentDateWithISOFormat {
    NSDate *const date = NSDate.date;
    NSCalendar *const calendar = NSCalendar.currentCalendar;
    NSCalendarUnit const preservedComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    NSDateComponents *const components = [calendar components:preservedComponents fromDate:date];
    NSDate *const normalizedDate = [calendar dateFromComponents:components];
    
    return normalizedDate;
}

+ (NSDate *)getEndDateOfTheYear:(NSInteger)count date:(NSDate *)normalizedDate {
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:count];
    NSDate *nextYear = [gregorian dateByAddingComponents:offsetComponents toDate:normalizedDate options:0];
    
    return nextYear;
}

+ (NSString *)convertDateIntoStringWithISOFormat:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kISODateFormat];
    
    NSString *dateStr = [formatter stringFromDate:date];
    
    return dateStr;
}


+ (NSString *)convertDateIntoString:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kISODateFormat];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    [formatter setTimeZone:timeZone];
    NSString *dateStr = [formatter stringFromDate:date];
    
    return dateStr;
}


+ (NSDate *)convertISOStringIntoDate:(NSString *)dateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kISODateFormat];
    
    //    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //    [formatter setLocale:posix];
    
    NSDate *date = [formatter dateFromString:dateString];
    
    //    [formatter setDateFormat:kISODateFormat];
    //    NSString *output = [formatter stringFromDate:date];
    //    NSDate *outputDate = [formatter dateFromString:output];
    
    return date;
}

+ (NSString *)convertDateFormat:(NSString *)dateString prevFormat:(NSString *)prevFormat newFormat:(NSString *)newFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:prevFormat];
    //    Then use it to parse the date string:
    
    NSDate *date = [formatter dateFromString:dateString];
    //    Now you can create a new formatter to output in the new format, or just reuse the existing one:
    
    [formatter setDateFormat:newFormat];
    NSString *output = [formatter stringFromDate:date];
    
    return output;
}

+ (NSString *)getHeaderDateStrFromString:(NSString *)strDate {
    NSString *dateStr=[NSDate convertDateFormat:strDate prevFormat:@"yyyy-MM-dd HH:mm:ss" newFormat:@"dd MMMM, yyyy"];
    return dateStr;
  }

+ (NSString *)getStartEndDateStrFromString:(NSString *)startDate  AndEndDate:(NSString *)endDate{
    NSString *startDateStr=[NSDate convertDateFormat:startDate prevFormat:@"yyyy-MM-dd HH:mm:ss" newFormat:@"dd MMMM, yyyy"];
   
    NSString *endDateStr=[NSDate convertDateFormat:endDate prevFormat:@"yyyy-MM-dd HH:mm:ss" newFormat:@"dd MMMM, yyyy"];

    NSString *dateStr=[NSString stringWithFormat:@"%@ - %@",startDateStr,endDateStr];

    return dateStr;
}

+ (NSString *)getAgoFromString:(NSString *)strDate{
    NSDate *dd = [NSDate getDateFromString:strDate];
    return [dd formattedAsTimeAgo:[NSDate date]];
}

+ (NSString *)getAgoTimeFromString:(NSString *)strDate{
    NSDate *dd = [NSDate getDateFromString:strDate];
    return [dd formattedAsTimeAgoWithMonthFormat:[NSDate date]];
}
 

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}


//+ (NSDate *)convertDateFormat:(NSDate *)date prevFormat:(NSString *)prevFormat newFormat:(NSString *)newFormat
//{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:prevFormat];
//    //    Then use it to parse the date string:
//
//    NSString *output = [formatter stringFromDate:date];
//    //    Now you can create a new formatter to output in the new format, or just reuse the existing one:
//
//    [formatter setDateFormat:newFormat];
//    NSDate *outputDate = [formatter dateFromString:output];
//    
//    return outputDate;
//}

@end
