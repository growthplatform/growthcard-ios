//
//  NSError+Additions.h
//  GrowthCard
//
//  Created by Shipra Dhooper on 18/03/2015.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Additions)

/**
 * Checks to see if api's response netwrok connection error
 */

+ (NSError *)networkConnectionError;
+ (NSError *)apiRequestGenericError;

@end
