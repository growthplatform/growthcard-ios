//
//  NSDictionary+Additions.h
//  GrowthCard
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

/**
 * Checks to see if the dictionary contains the given key
 */
- (BOOL)containsObjectForKey:(id)key;
/**
 * Checks to see if the dictionary is empty
 */
@property (nonatomic, readonly, getter=isEmpty) BOOL empty;

@end
