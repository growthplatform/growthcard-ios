//
//  NSDictionary+Additions.m
//  GrowthCard
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (BOOL)containsObjectForKey:(id)key {
    return [[self allKeys] containsObject:key];
}

- (BOOL)isEmpty {
    return (([self count] == 0) ? YES : NO);
}

@end
