//
//  UIWindow+CurrentView.h
//  GrowthCard
//
//  Created by Pawan Kumar on 07/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (CurrentView)
- (UIViewController *) visibleViewController;
@end
