//
//  NetworkManager.m
//
//  Created by Arvind Singh on 05/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "NetworkManager.h"
#import "Reachability.h"
#import <UIKit/UIKit.h>

NSString *const HTTPRequestDomain = @"com.httprequest";


@interface NetworkManager ()

@property (nonatomic, readonly) NSURLSession *URLSession;
@property (nonatomic, readonly) NSSet *runningURLRequests;

@end

@implementation NetworkManager
{
    NSURLSession *_URLSession;
    NSSet *_runningURLRequests;
}

#pragma mark - Class Methods

static NSUInteger networkFetchingCount = 0;

static void beginNetworkActivity() {
    
    networkFetchingCount++;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

static void endNetworkActivity() {
    
    if (networkFetchingCount > 0) {
        networkFetchingCount--;
        
        if (networkFetchingCount == 0) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }
    }
}


#pragma mark - Singleton Methods

+ (NetworkManager *) sharedInstance {
    
    static id shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    
    return shared;
}


#pragma mark - Lifecycle Methods

- (instancetype) initUniqueInstance {
    NetworkManager *manager = [super init];
    [manager initializeSharedInstance];
    return manager;
}


#pragma mark - Instance Methods

- (void) initializeSharedInstance {
    
}


#pragma mark - Getters Methods

- (NSURLSession *) urlSession {
    
    if (!_URLSession) {
        
        _URLSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        //        _URLSession.sessionDescription = @"networkmanager.nsurlsession";
    }
    
    return _URLSession;
}

- (NSSet *) runningURLRequests {
    
    if (!_runningURLRequests) {
        _runningURLRequests = [[NSSet alloc] init];
    }
    return _runningURLRequests;
}

- (BOOL) isProcessingURL:(NSURL *)URL {
    return ([self.runningURLRequests containsObject:URL]);
}


- (NSError *) errorForNilURL {
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:-1
                           userInfo:@{NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"URL must not be nil", @"URL must not be nil")}];
}


#pragma mark - Public Methods

- (void) performRequest:(NSMutableURLRequest *)request userInfo:(NSDictionary *)dict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    
    NSURL *URL = [request URL];
    if (!URL) {
       // NSAssert(URL, @"URL must not be nil here");
        if (completionHandler) {
            completionHandler(nil, [self errorForNilURL], dict);
        }
        return;
    }    
    if (![self checkInternetConnection]) {
        if (completionHandler) {
            completionHandler(nil, [self connectionError], dict);
        }
        
        return;
        
    }
    NSLog(@"performRequest service input:%@",dict);
    [self addRequestedURL:URL];
    
    beginNetworkActivity();

    NSURLSession *session = self.urlSession;
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
        endNetworkActivity();
        
        NSError *resError = error;
        NSInteger statusCode = 0;
        if ([response respondsToSelector:@selector(statusCode)]) {
            statusCode = [(NSHTTPURLResponse *)response statusCode];
        }
        if (statusCode >= 400) {
            NSMutableDictionary *errorUserInfo = [NSMutableDictionary dictionary];
            errorUserInfo[@"HTTP statuscode"] = @(statusCode);
            if (error) {
                errorUserInfo[@"underlying error"] = error;
            }
            resError = [NSError errorWithDomain:NSURLErrorDomain code:statusCode userInfo:errorUserInfo];
        }
        /*
        if (statusCode == 601) {
            if (completionHandler) {
                completionHandler(nil, resError, nil);
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:kLogOutNotification object:nil];
            [AppDelegate logOutCurrentUser];
            return;
        }
         */
        
        HTTPResponse *responseData = nil;
        
        if ( error ) {
            [self logError:error forRequest:request];
        }
        else {
            responseData = [HTTPResponse HTTPResponseWithData:data];
            
            [self logResponse:data forRequest:request];
            
            if (![responseData success]) {
                resError = responseData.error;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (completionHandler) {
                completionHandler(responseData, resError, dict);
            }
            [self removeRequestedURL:URL];
        });
        
        if (statusCode == 401 || statusCode == 604) {
            [AppDelegate logOutCurrentUser];
        }
        else if (statusCode == 606) {
            if(responseData.message != nil )
                [AppDelegate techIssue:responseData.message];
            else
                [AppDelegate techIssue:kTechIssue];
        }
    }] resume];
}

- (void) cancelRequestForURL:(NSURL *)URL {
    
    [_URLSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        NSInteger capacity = [dataTasks count] + [uploadTasks count] + [downloadTasks count];
        NSMutableArray *tasks = [NSMutableArray arrayWithCapacity:capacity];
        [tasks addObjectsFromArray:dataTasks];
        [tasks addObjectsFromArray:uploadTasks];
        [tasks addObjectsFromArray:downloadTasks];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"originalRequest.URL = %@", URL];
        [tasks filterUsingPredicate:predicate];
        
        for (NSURLSessionTask *task in tasks) {
            [task cancel];
        }
    }];
}

- (void) cancelAllRequests {
    
    [_URLSession invalidateAndCancel];
    _URLSession = nil;
    _runningURLRequests = nil;
}


#pragma mark - Private Methods

- (void) addRequestedURL:(NSURL *)URL {
    
    @synchronized(self) {
        NSMutableSet *requests = [self.runningURLRequests mutableCopy];
        if (URL) {
            [requests addObject:URL];
            _runningURLRequests = requests;
        }
    }
}

- (void) removeRequestedURL:(NSURL *)URL {
    
    @synchronized(self ) {
        NSMutableSet *requests = [self.runningURLRequests mutableCopy];
        if (URL && [requests containsObject:URL]) {
            [requests removeObject:URL];
            _runningURLRequests = requests;
        }
    }
}

- (NSError *) connectionError {
    
    NSDictionary *errorDict = @{ NSLocalizedDescriptionKey : NSLocalizedString(@"Connection Error", @"Connection Error"), NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"Network error occurred while performing this task. Please try again later.", @"Network error occurred while performing this task. Please try again later.") };
    NSError *error = [NSError errorWithDomain:HTTPRequestDomainKey code:HTTPConnectionError userInfo:errorDict];
    return error;
}

- (void) logError:(NSError *)error forRequest:(NSMutableURLRequest *)request {
    
#ifdef DEBUG
    NSLog(@"URL : %@\nError: %@", [[request URL] absoluteString], [error userInfo]);
#endif
}

- (void) logResponse:(NSData *)data forRequest:(NSMutableURLRequest *)request {
    
#ifdef DEBUG
    // There are 1024 bytes in a kilobyte and 1024 kilobytes in a megabyte, so...
    NSLog(@"Data Size: %.2f bytes", (float)data.length);
    
    NSString *output = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"URL : %@\nOutput: %@", [[request URL] absoluteString], output);
#endif
}

-(BOOL) checkInternetConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    else
        return YES;
}


@end
