//
//  NetworkManager.h
//
//  Created by Arvind Singh on 05/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPResponse.h"


FOUNDATION_EXPORT NSString *const HTTPRequestDomain;

typedef enum HTTPRequestErrorCode {
    HTTPConnectionError = 40, // Trouble connecting to Server.
    HTTPInvalidRequestError = 50, // Your request had invalid parameters.
    HTTPResultError = 60, // API result error (eg: Invalid username and password).
} HTTPRequestErrorCode;

/**
 * The completion callback type.
 * A nil error indicates success. Your callback should check to see if error is non-nil, and react accordingly.
 * request and userInfo are used for context in the callback.
 */
typedef void (^HTTPRequestCompletionHandler)(HTTPResponse *response, NSError *error, NSDictionary *userInfo);

/**
 * The completion callback type.
 * A nil error indicates success. Your callback should check to see if error is non-nil, and react accordingly.
 */
typedef void (^APIResponseCompletionHandler)(id response, NSError *error);


@interface NetworkManager : NSObject

// Mark following methods unavailable (produces compile time error)
+ (instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+ (instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));
- (instancetype) copy __attribute__((unavailable("copy not available, call sharedInstance instead")));


/**
 * Creates an autoreleased NetworkManager object
 * @return NetworkManager an autoreleased NetworkManager object.
 */
+ (NetworkManager *) sharedInstance;

/**
 * Perform HTTP request operation using NSMutableURLRequest in an Operation Queue. The userInfo dictionary is to pass along any context you'd like passed back to you upon completion or error.
 * @param request, userInfo and completionHandler.
 */
- (void) performRequest:(NSMutableURLRequest *)request userInfo:(NSDictionary *)dict completionHandler:(HTTPRequestCompletionHandler)completionHandler;

/**
 * Cancels the requests for an url.
 */
- (void) cancelRequestForURL:(NSURL *)URL;

/**
 * Cancels the requests.
 */
- (void) cancelAllRequests;

@end
