//
//  HTTPResponse.h
//
//  Created by Arvind Singh on 05/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * All API errors will be under this domain.
 */
extern NSString *const HTTPRequestDomainKey;

extern NSString *const APIResponseStatusKey;
extern NSString *const APIResponseCodeKey;
extern NSString *const APIResponseMessageKey;

/**
 * HTTPResponse used to store URL response.
 */
@interface HTTPResponse : NSObject

/**
 * The data received during the request.
 */
@property (strong, nonatomic) NSData *responseData;

/**
 * The dictionary received after parsing the received data.
 */
@property (strong, nonatomic) NSDictionary *resultDictionary;

/**
 * The response status after parsing the received data.
 */
@property (assign, nonatomic) BOOL success;

/**
 * The message received after parsing the received data.
 */
@property (strong, nonatomic) NSString *message;

/**
 * The responseCode received after parsing the received data.
 */
@property (strong, nonatomic) NSString *responseCode;

/**
 * The responseError received after parsing the received data.
 */
@property (strong, nonatomic) NSError *error;

/**
 * Creates an HTTPResponse object using response data
 */
+ (id)HTTPResponseWithData:(NSData *)data;

@end
