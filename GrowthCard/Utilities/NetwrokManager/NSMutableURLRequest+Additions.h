//
//  NSMutableURLRequest+Additions.h
//
//  Created by Arvind Singh on 06/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * HTTP Method
 */
typedef NS_ENUM(NSUInteger, HTTPMethod) {
    HTTPMethodGET = 0,
    HTTPMethodPOST,
    HTTPMethodPUT,
    HTTPMethodDELETE
};

@interface NSMutableURLRequest (Additions)

/**
 * Return HTTP Method string for method
 * @param method HTTP Method type.
 * @return HTTP Method string.
 */
+ (NSString *) HTTPMethodForType:(HTTPMethod)method;

/**
 * Creates an autoreleased NSMutableURLRequest object
 * @param URL, HTTPMethod and HTTPBody for creating mutable url request.
 * @return NSMutableURLRequest an autoreleased NSMutableURLRequest object.
 */
+ (NSMutableURLRequest *) requestWithURL:(NSURL *)URL HTTPMethod:(HTTPMethod)method HTTPBody:(NSString *)body;

/**
 * Creates an autoreleased NSMutableURLRequest object
 * @param URL, HTTPMethod and jsonDictionary for creating mutable url request.
 * @return NSMutableURLRequest an autoreleased NSMutableURLRequest object.
 */
+ (NSMutableURLRequest *) requestWithURL:(NSURL *)URL HTTPMethod:(HTTPMethod)method jsonDictionary:(NSDictionary *)jsonDictionary;

/**
 * Creates an autoreleased NSMutableURLRequest object
 * @param URL, httpMethod, httpHeaders, postDataArray and fileDataArray for creating mutable url request.
 * @return NSMutableURLRequest an autoreleased NSMutableURLRequest object.
 */
+ (NSMutableURLRequest *) requestMultipartFormDataWithURL:(NSURL *)URL HTTPMethod:(HTTPMethod)method additionalHeaders:(NSDictionary *)headersDict postDataArray:(NSMutableArray *)postDataArray fileDataArray:(NSMutableArray *)fileDataArray;

@end
