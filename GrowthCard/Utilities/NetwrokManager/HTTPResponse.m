//
//  HTTPResponse.m
//
//  Created by Arvind Singh on 05/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "HTTPResponse.h"

NSString *const HTTPRequestDomainKey = @"com.httprequest";

NSString *const APIResponseStatusKey = @"success";
NSString *const APIResponseCodeKey = @"statusCode";
NSString *const APIResponseMessageKey = @"message";

@implementation HTTPResponse

#pragma mark - Initialize Methods

- (id) init {
    
    if (self = [super init]) {
        
        self.responseData = nil;
        self.resultDictionary = nil;
    }
    
    return self;
}

- (id) initWithResponseData:(NSData *)data {
    
    if (self = [super init]) {
        
        if (data) {
            
            self.responseData = data;
            
            if ([HTTPResponse isNotNull:data]) {
                
                NSError *error;
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if ((error == nil) && (dict)) {
                    self.resultDictionary = dict; //[dict objectForKey:@"result"];
                }
            }
        }
    }
    
    return self;
}

- (void) dealloc {
    self.responseData = nil;
    self.resultDictionary = nil;
}


#pragma mark - Class Methods

+ (id)HTTPResponseWithData:(NSData *)data {
    HTTPResponse *response = [[HTTPResponse alloc] initWithResponseData:data];
    return response;
}

+ (BOOL)isNotNull:(id)data {
    
    if ([data isKindOfClass:[NSNull class]]) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Getters Methods

- (BOOL) success {
    
    if ((self.resultDictionary) && ([HTTPResponse isNotNull:self.resultDictionary[APIResponseStatusKey]])) {
        return [self.resultDictionary[APIResponseStatusKey] boolValue];
    }
    
    return NO;
}

- (NSString *) message {
    
    NSString *message = ([self success] ? NSLocalizedString(@"Action performed successfully.", @"Action performed successfully.") : NSLocalizedString(@"An error occurred while performing this request. Please try again later.", @"An error occurred while performing this request. Please try again later."));
    
    if (self.resultDictionary) {
        
        if ((self.resultDictionary[APIResponseMessageKey] != nil) && ([HTTPResponse isNotNull:self.resultDictionary[APIResponseMessageKey]])) {
            message = self.resultDictionary[APIResponseMessageKey];
        }
    }
    
    return message;
}

- (NSString *) responseCode {
    
    NSString *code = @"";
    
    if (self.resultDictionary) {
        
        if ((self.resultDictionary[APIResponseCodeKey] != nil) && ([HTTPResponse isNotNull:self.resultDictionary[APIResponseCodeKey]])) {
            code = self.resultDictionary[APIResponseCodeKey];
        }
    }
    
    return code;
}

- (NSError *) error {
    
    NSDictionary *bundleDict = [[NSBundle mainBundle] infoDictionary];
    NSDictionary *errorDict = @{ NSLocalizedDescriptionKey : [bundleDict objectForKey:@"CFBundleName"], NSLocalizedFailureReasonErrorKey : [self message] };
    NSError *errorReq = [NSError errorWithDomain:HTTPRequestDomainKey code:[[self responseCode] integerValue] userInfo:errorDict];
    return errorReq;
}

@end
