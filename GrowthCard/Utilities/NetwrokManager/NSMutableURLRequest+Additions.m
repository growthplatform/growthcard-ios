//
//  NSMutableURLRequest+Additions.m
//
//  Created by Arvind Singh on 06/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "NSMutableURLRequest+Additions.h"

@implementation NSMutableURLRequest (Additions)

#pragma mark - Class Methods

+ (double) requestTimeout {
    return 20.0f;
}

+ (NSString *) HTTPMethodForType:(HTTPMethod)method {
    NSString *httpMethod = @"";
    
    switch (method) {
        case HTTPMethodGET:
            httpMethod = @"GET";
            break;
        case HTTPMethodPOST:
            httpMethod = @"POST";
            break;
        case HTTPMethodPUT:
            httpMethod = @"PUT";
            break;
        case HTTPMethodDELETE:
            httpMethod = @"DELETE";
            break;
        default:
            httpMethod = @"GET";
            break;
    }
    
    return httpMethod;
}

+ (NSMutableURLRequest *) requestWithURL:(NSURL *)URL HTTPMethod:(HTTPMethod)method HTTPBody:(NSString *)body {
    
    //NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[NSMutableURLRequest requestTimeout]];
    // NSURLRequestUseProtocolCachePolicy
    NSMutableURLRequest *mutableURLRequest = [NSMutableURLRequest requestWithURL:URL];
    
    // Set the appkey http header field for authorization
    //[mutableURLRequest setValue:[NSMutableURLRequest authorizationValue] forHTTPHeaderField:[NSMutableURLRequest authorizationKey]];
    
    // Set the request's content type to application/x-www-form-urlencoded
    // In body data for the 'application/x-www-form-urlencoded' content type, form fields are separated by an ampersand. Note the absence of a leading ampersand.
    // eg: @"name=Arvind+Singh&address=123+Main+St"
    [mutableURLRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // Designate the request a POST request and specify its body data
    [mutableURLRequest setHTTPMethod:[NSMutableURLRequest HTTPMethodForType:method]];
    [mutableURLRequest setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:strlen([body UTF8String])]];
    
    return mutableURLRequest;
}

+ (NSMutableURLRequest *) requestWithURL:(NSURL *)URL HTTPMethod:(HTTPMethod)method jsonDictionary:(NSDictionary *)jsonDictionary {
    
    NSLog(@" ");
    NSLog(@"----------------------------------------");
    NSLog(@"URL for Api : %@",[URL absoluteString]);
    NSLog(@"Input Json Dict : %@",jsonDictionary);
    NSLog(@"----------------------------------------");
    NSLog(@" ");
    
    //NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[NSMutableURLRequest requestTimeout]];
    // NSURLRequestUseProtocolCachePolicy
    NSMutableURLRequest *mutableURLRequest = [NSMutableURLRequest requestWithURL:URL];
    //    [mutableURLRequest setTimeoutInterval:30.0];
    
    // Set the appkey http header field for authorization
    //[mutableURLRequest setValue:[NSMutableURLRequest authorizationValue] forHTTPHeaderField:[NSMutableURLRequest authorizationKey]];
    
    // Set the request's content type to application/json
    [mutableURLRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [mutableURLRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    // Designate the request a POST request and specify its body data
    [mutableURLRequest setHTTPMethod:[NSMutableURLRequest HTTPMethodForType:method]];
    
    if (jsonDictionary) {
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:kNilOptions error:&error];
        
        if (jsonData) {
            
            [mutableURLRequest setHTTPBody:jsonData];
        }
    }
    
    return mutableURLRequest;
}


#pragma mark - Multipart/Form-Data Body Methods

+ (NSMutableURLRequest *) requestMultipartFormDataWithURL:(NSURL *)URL HTTPMethod:(HTTPMethod)method additionalHeaders:(NSDictionary *)headersDict postDataArray:(NSMutableArray *)postDataArray fileDataArray:(NSMutableArray *)fileDataArray {
    
#ifdef DEBUG
    NSLog(@"%@", @"Building a multipart/form-data body");
#endif
    
    //NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:[NSMutableURLRequest requestTimeout]];
    // NSURLRequestUseProtocolCachePolicy
    NSMutableURLRequest *mutableURLRequest = [NSMutableURLRequest requestWithURL:URL];
    
    // Set the appkey http header field for authorization
   // [mutableURLRequest setValue:[NSMutableURLRequest authorizationValue] forHTTPHeaderField:[NSMutableURLRequest authorizationKey]];
    
    // Add http method
    [mutableURLRequest setHTTPMethod:[NSMutableURLRequest HTTPMethodForType:method]];
    
    
    NSStringEncoding stringEncoding = NSUTF8StringEncoding;
    NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(stringEncoding));
    
    // We don't bother to check if post data contains the boundary, since it's pretty unlikely that it does.
    CFUUIDRef uuid = CFUUIDCreate(nil);
    NSString *uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(nil, uuid));
    CFRelease(uuid);
    NSString *stringBoundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY-%@", uuidString];
    
    // Add default httpheader fields ***********
    [mutableURLRequest addValue:[NSString stringWithFormat:@"multipart/form-data; charset=%@; boundary=%@", charset, stringBoundary] forHTTPHeaderField:@"Content-Type"];
    
    // Add additional httpheader fields
    if (headersDict != nil) {
        NSArray *allKeys = [headersDict allKeys];
        
        for (NSString *key in allKeys) {
            [mutableURLRequest addValue:[headersDict objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    
    // Build multipart form data post body ***********
    NSMutableData *postBody = [NSMutableData data];
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:stringEncoding]];
    
    // Add post data ***********
    NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", stringBoundary];
    
    if (postDataArray) {
        
        for (NSDictionary *val in postDataArray) {
            
            [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[val objectForKey:@"key"]] dataUsingEncoding:stringEncoding]];
            [postBody appendData:[[NSString stringWithFormat:@"%@", [val objectForKey:@"value"]] dataUsingEncoding:stringEncoding]];
            
            // Only add the boundary if this is not the last item in the post body
            if ((val != [postDataArray lastObject]) || ([fileDataArray count] > 0)) {
                [postBody appendData:[endItemBoundary dataUsingEncoding:stringEncoding]];
            }
        }
    }
    
    // Add files to upload ***********
    if (fileDataArray) {
        
        for (NSDictionary *val in fileDataArray) {
            
            [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [val objectForKey:@"key"], [val objectForKey:@"fileName"]] dataUsingEncoding:stringEncoding]];
            [postBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [val objectForKey:@"contentType"]] dataUsingEncoding:stringEncoding]];
            
            id data = [val objectForKey:@"data"];
            if ([data isKindOfClass:[NSString class]]) {
                
                NSData *d = [NSData dataWithContentsOfFile:data];
                [postBody appendData:d];
            }
            else {
                [postBody appendData:data];
            }
            
            // Only add the boundary if this is not the last item in the post body
            if (val != [fileDataArray lastObject]) {
                [postBody appendData:[endItemBoundary dataUsingEncoding:stringEncoding]];
            }
        }
    }
    
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", stringBoundary] dataUsingEncoding:stringEncoding]];
    [mutableURLRequest setHTTPBody:postBody];
    
#ifdef DEBUG
    NSLog(@"%@", @"End of multipart/form-data body");
#endif
    
    return mutableURLRequest;
}

@end
