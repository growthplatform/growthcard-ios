//
//  LazyHeader.h
//  GrowthCard
//
//  Created by Sourabh Bhardwaj on 27/09/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LazyHeader : NSObject

- (void)lazyHeader:(UIView *)view tableView:(UITableView *)tableView;
- (void)scrollViewDidScroll:(UIScrollView*)scrollView;
- (void)resizeView;

@end
