//
//  LazyHeader.m
//  GrowthCard
//
//  Created by Sourabh Bhardwaj on 27/09/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "LazyHeader.h"

#define VELOCITY -0.50
#define THRESHHOLD_FROM_TOP 200
#define THRESHHOLD_VALUE THRESHHOLD_FROM_TOP-164

@interface LazyHeader () {
    CGRect initialFrame;
    CGFloat defaultViewHeight;

}

@property (nonatomic,retain) UITableView* tableView;
@property (nonatomic,retain) UIView* headerView;

@end

@implementation LazyHeader

- (void)lazyHeader:(UIView *)view tableView:(UITableView *)tableView {
    _tableView = tableView;
    _headerView = view;
    
    initialFrame       = _headerView.frame;
    defaultViewHeight  = initialFrame.size.height;    
}

- (void)scrollViewDidScroll:(UIScrollView*)scrollView {
    if (scrollView.contentOffset.y > THRESHHOLD_VALUE) {
        // NSLog(@"%f",scrollView.contentOffset.y);
        CGFloat offsetY = (scrollView.contentOffset.y + (scrollView.contentInset.top - THRESHHOLD_FROM_TOP)) * VELOCITY;
        initialFrame.size.height = defaultViewHeight + offsetY;
        _headerView.frame = initialFrame;
    }
}

- (void)resizeView {
    initialFrame.size.width = _tableView.frame.size.width;
    _headerView.frame = initialFrame;
}

@end
