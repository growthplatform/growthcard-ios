//
//  NotificationInfo+RemoteAccessor.h
//  GrowthCard
//
//  Created by Narender Kumar on 08/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "NotificationInfo.h"

@interface NotificationInfo (RemoteAccessor)

+ (void)performFetchNotificationList:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;


@end
