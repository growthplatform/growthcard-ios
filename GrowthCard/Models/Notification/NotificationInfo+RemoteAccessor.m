//
//  NotificationInfo+RemoteAccessor.m
//  GrowthCard
//
//  Created by Narender Kumar on 08/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "NotificationInfo+RemoteAccessor.h"

@implementation NotificationInfo (RemoteAccessor)

+ (void)performFetchNotificationList:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_NOTIFICATION_LIST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

@end
