//
//  NotificationInfo.h
//  GrowthCard
//
//  Created by Prashant Gautam on 06/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationInfo : NSObject

@property (copy, nonatomic) NSNumber *notificationId;
@property (copy, nonatomic) NSNumber *type;
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *message2;
@property (copy, nonatomic) NSString *message3;
@property (copy, nonatomic) NSNumber *effectiveActionId;
@property (copy, nonatomic) NSNumber *notificationCount;

@property (copy, nonatomic) NSNumber *pastEffectiveActionId;

@property (copy, nonatomic) NSNumber *teamId;
@property (copy, nonatomic) NSNumber *userId;
@property (copy, nonatomic) NSString *image;
@property (copy, nonatomic) NSString *timeSring;
@property (copy, nonatomic) NSNumber *readStatus;

@property (copy, nonatomic) NSString *markAdhereTitle;
@property (copy, nonatomic) NSString *markAdhereDesc;

- (id)initWithAttributes:(NSDictionary *)attributeDict;
+ (NotificationInfo *)notificationWithDictnory:(NSDictionary *)attributeDict;

@end

