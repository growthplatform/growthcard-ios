//
//  NotificationInfo.m
//  GrowthCard
//
//  Created by Prashant Gautam on 06/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "NotificationInfo.h"

@implementation NotificationInfo

- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self.notificationId = [NSString formattedNumber:[attributeDict objectForKey:@"notificationId"]];
        self.type   = [NSString formattedNumber:[attributeDict objectForKey:@"type"]];
        self.message = [NSString formattedValue:[attributeDict objectForKey:@"message"]];
        self.effectiveActionId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
        self.notificationCount = [NSString formattedNumber:[attributeDict objectForKey:@"notificationCount"]];
        self.teamId = [NSString formattedNumber:[attributeDict objectForKey:@"teamId"]];
        self.timeSring = [NSString formattedValue:[attributeDict objectForKey:@"timeSring"]];
        self.readStatus = [NSString formattedNumber:[attributeDict objectForKey:@"readStatus"]];
        self.userId = [NSString formattedNumber:[attributeDict objectForKey:@"userId"]];
        self.message2 = @"";
        self.message3 = @"";
    }
    return self;
}

+ (NotificationInfo *)notificationWithDictnory:(NSDictionary *)attributeDict {
    NotificationInfo *info = [NotificationInfo new];
    info.message = [NSString formattedValue:[attributeDict objectForKey:@"message1"]];
    info.message2 = [NSString formattedValue:[attributeDict objectForKey:@"message2"]];
    info.message3 = [NSString formattedValue:[attributeDict objectForKey:@"message3"]];
    info.image = [NSString formattedValue:[attributeDict objectForKey:@"profileImage"]];
    info.teamId = [NSString formattedNumber:[attributeDict objectForKey:@"teamId"]];
    info.readStatus = [NSString formattedNumber:[attributeDict objectForKey:@"read"]];
    info.effectiveActionId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
    info.notificationId   = [NSString formattedNumber:[attributeDict objectForKey:@"notificationId"]];
    info.type   = [NSString formattedNumber:[attributeDict objectForKey:@"type"]];
    info.userId = [NSString formattedNumber:[attributeDict objectForKey:@"userId"]];
    info.timeSring = [NSString formattedValue:[attributeDict objectForKey:@"createdAt"]];
    
    info.pastEffectiveActionId = [NSString formattedNumber:[attributeDict objectForKey:@"pastEffectiveActionId"]];

    
    return info;
}

@end
