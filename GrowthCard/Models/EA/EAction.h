//
//  EAction.h
//  GrowthCard
//
//  Created by Narender Kumar on 10/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EAction : NSObject

@property (copy, nonatomic) NSNumber *eaId;
@property (copy, nonatomic) NSString *eaTitle;
@property (copy, nonatomic) NSString *eaDesc;
@property (copy, nonatomic) NSNumber *eaDesignationId;

+ (EAction *)getEAWithData:(NSDictionary *)attributeDict;

@end
