//
//  EAction.m
//  GrowthCard
//
//  Created by Narender Kumar on 10/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EAction.h"

@implementation EAction

- (id)init {
    if (self = [super init]) {
        
        self.eaId = 0;
        self.eaTitle = @" ";
        self.eaDesc = @" ";
        self.eaDesignationId = 0;
    }
    
    return self;
}

+ (EAction *)getEAWithData:(NSDictionary *)attributeDict {
    EAction *ea = [[EAction alloc]init];
    ea.eaId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
    ea.eaTitle = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionTitle"]];
    ea.eaDesc = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionTitle"]];
    ea.eaDesignationId = [NSString formattedNumber:[attributeDict objectForKey:@"designationId"]];
    return ea;
}

@end