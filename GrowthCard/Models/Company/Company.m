//
//  Company.m
//  GrowthCard
//
//  Created by Narender Kumar on 26/10/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Company.h"

@implementation Company

#pragma mark - Init Methods
- (instancetype)init {
    if (self = [super init]) {
        self.companyId = 0;
        self.name = @"";
        self.logo = @"" ;
    }
    return self;
}

- (instancetype)initWithAttributes:(NSDictionary *)attributeDictionary {
    if (self = [super init]) {
        if(attributeDictionary && (![attributeDictionary isKindOfClass:[NSNull class]])) {
            self.companyId = [NSString formattedNumber:[attributeDictionary objectForKey:@"companyId"]];
            self.name = [NSString formattedValue:[attributeDictionary objectForKey:@"companyName"]];
            self.logo = [NSString formattedValue:[attributeDictionary objectForKey:@"companyLogo"]];
        }
        else {
            self = [[Company alloc]init];
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.companyId forKey:@"companyId"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.logo forKey:@"logo"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.companyId = [aDecoder decodeObjectForKey:@"companyId"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.logo = [aDecoder decodeObjectForKey:@"logo"];
    }
    return self;
}

@end
