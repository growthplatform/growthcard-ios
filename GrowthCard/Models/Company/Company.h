//
//  Company.h
//  GrowthCard
//
//  Created by Narender Kumar on 26/10/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Company : NSObject <NSCoding>

@property (strong, nonatomic) NSNumber *companyId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *logo;

- (instancetype)initWithAttributes:(NSDictionary *)attributeDictionary;

@end
