//
//  User+RemoteAccessor.h
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "User.h"

@interface User (RemoteAccessor)

+ (void)performUserLogin:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performUserLogOut:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performForgetPassword:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performChangePassword:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performAcceptTerms:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performWorkDaysSelect:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performProfileImageUpdate:(NSMutableArray *)postData fileData:(NSMutableArray *)fileData completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performUpdateProfile:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performGetSubordinates:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performGetEAList:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performAddEa:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performAssignEa:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performChangeEa:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performEaDone:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchSubordinate:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchTeamMember:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchUsers:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchManagerdashboard:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performSwitchUser:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchFeed:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;

@end
