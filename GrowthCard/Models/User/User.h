//
//  User.h
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2014 Appster. All rights reserved.
//


// role = 2 for subOrdinate
// role = 1 for manager

#import <Foundation/Foundation.h>
#import "Company.h"

@interface User : NSObject <NSCoding>

@property (copy, nonatomic) NSNumber *userId;
@property (copy, nonatomic) NSNumber *userRole;
@property (copy, nonatomic) NSNumber *userSwitch;
@property (copy, nonatomic) NSNumber *userTermAccepted;
@property (copy, nonatomic) NSString *userFName;
@property (copy, nonatomic) NSString *userLName;
@property (copy, nonatomic) NSString *userToken;
@property (copy, nonatomic) NSString *userImageUrl;
@property (copy, nonatomic) NSString *userEmail;

@property (strong, nonatomic) Company *objCompany;
@property (copy, nonatomic) NSNumber *userDeparmentId;
@property (copy, nonatomic) NSString *userDeparmentName;

@property (copy, nonatomic) NSNumber *userTeamId;
@property (copy, nonatomic) NSString *userTeamName;
@property (copy, nonatomic) NSNumber *userDesignationId;
@property (copy, nonatomic) NSString *userDesignationTitle;

@property (copy, nonatomic) NSNumber *userEffectiveActionId;
@property (copy, nonatomic) NSString *userEffectiveActionTitle;
@property (copy, nonatomic) NSString *userEffectiveActionDescription;

@property (copy, nonatomic) NSString *userEffectiveActionDate;
@property (copy, nonatomic) NSString *userWorkDays;

@property (strong, nonatomic) NSNumber *min;
@property (strong, nonatomic) NSNumber *max;
@property (strong, nonatomic) NSNumber *avg;
@property (strong, nonatomic) NSNumber *today;
 
@property (copy, nonatomic) NSString *startTimeStamp;
@property (copy, nonatomic) NSString *endTimeStamp;
@property (strong, nonatomic) NSMutableArray *graphDetails;
@property (assign, nonatomic) BOOL isExpended;

+ (User *)lastLoggedUserInformation;

#pragma mark - Basic
- (instancetype)initWithBasicAttributes:(NSDictionary *)attributeDict;

#pragma mark - Comment
- (instancetype )initWithCommentAttributes:(NSDictionary *)attributeDict postingComment:(BOOL)isComment;

- (id)initWithAttributes:(NSDictionary *)attributeDict;
- (void)updateUserBasicDetail:(NSDictionary *)attributeDict;

- (void)saveWorkingDay:(NSArray *)ary;
- (void)updateUserWorkingDays:(NSString *)str;

- (User *)saveWorkingDayForUser:(User *)user WithArry:(NSArray *)ary;
- (void)saveUserInformation;
- (void)deleteUserInformation;

+ (User *)getSubordinatesWithAttributes:(NSDictionary *)attributeDict;
+ (User *)getSubordinatesFullData:(NSDictionary *)attributeDict;

+ (User *)getMemberDetails:(NSDictionary *)attributeDict;
+ (User *)getSearchUser:(NSDictionary *)attributeDict;
+ (User *)getSubordinateUser:(NSDictionary *)attributeDict;

@end
