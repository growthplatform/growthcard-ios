//
//  User.m
//
//  Created by Shipra Dhooper on 05/07/15.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "User.h"

static NSString * const userXMLName = @"user_data.xml";

@implementation User

#pragma mark - Class Methods

+ (User *)lastLoggedUserInformation {
    User *userData = nil;
    NSString *sPath = [kDocumentsDirectory stringByAppendingPathComponent:userXMLName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:sPath]) {
        userData = [NSKeyedUnarchiver unarchiveObjectWithFile:sPath];
    }
    return userData;
}

#pragma mark - Init Methods
- (id)init {
    if (self = [super init]) {
        self.userId = 0;
        self.userRole = 0;
        self.userSwitch = 0;
        self.userTermAccepted = [NSNumber numberWithInt:0];
        self.userFName = @"";
        self.userLName = @"";
        self.userToken = @"";
        self.userImageUrl = @"";
        self.userEmail = @"";
        self.userDeparmentId  = 0;
        self.userDeparmentName = @"";
        
        self.userTeamId = 0;
        self.userTeamName = @"";
        self.userDesignationId = 0;
        self.userDesignationTitle  = @"";
        
        self.userEffectiveActionId = [NSNumber numberWithInt:-1];
        self.userEffectiveActionTitle = @"";
        self.userEffectiveActionDescription = @"";
        self.userWorkDays = @"0000000";
        self.userEffectiveActionDate = @"No Date Found";
        
        self.min = [NSNumber numberWithInt:0];
        self.max =[NSNumber numberWithInt:0];
        self.avg = [NSNumber numberWithInt:0];
        self.today = [NSNumber numberWithInt:0];
        self.startTimeStamp = @"";
        self.endTimeStamp = @"";
    }
    
    return self;
}

#pragma mark - Basic Info
- (instancetype )initWithBasicAttributes:(NSDictionary *)attributeDict {
    self = [super init];
    if(attributeDict && (![attributeDict isKindOfClass:[NSNull class]])) {
        self.userId = [NSString formattedNumber:[attributeDict objectForKey:@"userId"]];
        self.userEmail = [NSString formattedValue:[attributeDict objectForKey:@"email"]];
        self.userFName = [NSString formattedValue:[attributeDict objectForKey:@"firstName"]];
        self.userLName = [NSString formattedValue:[attributeDict objectForKey:@"lastName"]];
        self.userImageUrl = [NSString formattedValue:[attributeDict objectForKey:@"profileImage"]];
    }
    return self;
}

- (instancetype )initWithCommentAttributes:(NSDictionary *)attributeDict postingComment:(BOOL)isComment {
    self = [self initWithBasicAttributes:[attributeDict valueForKey:@"user"]];
    if (self) {
        if (isComment) {
            self.userRole   = [NSString formattedNumber:[attributeDict valueForKeyPath:@"role"]];
            self.userTermAccepted = [NSString formattedNumber:[attributeDict valueForKeyPath:@"user.termsAccepted"]];
            self.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.teamId"]];
            self.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.designation.designationId"]];
            self.userDesignationTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.designation.designationTitle"]];
            
            self.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKeyPath:@"member.designation"]];
        } else {
            self.userTermAccepted = [NSString formattedNumber:[attributeDict valueForKeyPath:@"user.termsAccepted"]];
            self.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.designationId"]];
            self.userEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effectiveActionId"]];
            self.objCompany.companyId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.companyId"]];
            self.userEffectiveActionDescription = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effectiveActionDescription"]];
            self.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.teamId"]];
            self.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"designation.designationId"]];
            self.objCompany.companyId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"designation.companyId"]];
            self.userDesignationTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.designation.designationTitle"]];
        }
    }
    return self;
}

- (void)updateUserBasicDetail:(NSDictionary *)attributeDict {
    self.userId = [NSString formattedNumber:[attributeDict objectForKey:@"userId"]];
    self.userEmail = [NSString formattedValue:[attributeDict objectForKey:@"email"]];
    self.userFName = [NSString formattedValue:[attributeDict objectForKey:@"firstName"]];
    self.userLName = [NSString formattedValue:[attributeDict objectForKey:@"lastName"]];
    self.userImageUrl = [NSString formattedValue:[attributeDict objectForKey:@"profileImage"]];
    self.userTermAccepted = [NSString formattedNumber:[attributeDict objectForKey:@"termsAccepted"]];
}

#pragma mark - Detailed Info
- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self = [[User alloc]initWithBasicAttributes:attributeDict];
        self.userRole = [NSString formattedNumber:[attributeDict objectForKey:@"role"]];
        self.userSwitch = [NSString formattedNumber:[attributeDict objectForKey:@"switch"]];
        self.userTermAccepted = [NSString formattedNumber:[attributeDict objectForKey:@"termsAccepted"]];
        
        self.userToken = [NSString formattedValue:[attributeDict objectForKey:@"userToken"]];
        self.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.teamId"]];
        self.userTeamName = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.teamName"]];
        self.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.designation.designationId"]];
        self.userDesignationTitle  = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.designation.designationTitle"]];
        
        self.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKeyPath:@"member.company"]];
        self.userDeparmentId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"userdepartment.department.departmentId"]];
        self.userDeparmentName = [NSString formattedValue:[attributeDict valueForKeyPath:@"userdepartment.department.departmentName"]];
        
        self.userEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effective.effectiveActionId"]];
        self.userEffectiveActionTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effective.effectiveActionTitle"]];
        self.userEffectiveActionDescription = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effectiveActionDescription"]];
        self.userEffectiveActionDate = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effective.assignEffectiveActionDate.date"]];
        self.min = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MIN"]];
        self.max = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MAX"]];
        self.avg = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.AVG"]];
        self.today = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.TODAY"]];
        self.startTimeStamp = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effective.assignEffectiveActionDate.date"]];
        self.endTimeStamp  = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effective.effectiveActionId"]];
    }
    return self;
}


- (void)saveWorkingDay:(NSArray *)ary {
    NSMutableArray *mArry = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0" ,@"0", @"0", @"0" ,@"0" ,nil];
    for(NSDictionary *dict in ary) {
        if([[NSString formattedNumber:[dict objectForKey:@"is_selected"]] intValue] == 1) {
            int index = [[NSString formattedNumber:[dict objectForKey:@"workDay"]] intValue];
            index--;
            [mArry replaceObjectAtIndex:index withObject:@"1"];
        }
    }
    NSString *str =[mArry componentsJoinedByString:@""];
    [self updateUserWorkingDays:str];
}

- (User *)saveWorkingDayForUser:(User *)user WithArry:(NSArray *)ary {
    NSMutableArray *mArry = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0" ,@"0", @"0", @"0" ,@"0" ,nil];
    for(NSDictionary *dict in ary) {
        if([[NSString formattedNumber:[dict objectForKey:@"is_selected"]] intValue] == 1) {
            int index = [[NSString formattedNumber:[dict objectForKey:@"workDay"]] intValue];
            index--;
            [mArry replaceObjectAtIndex:index withObject:@"1"];
        }
    }
    NSString *str =[mArry componentsJoinedByString:@""];
    user.userWorkDays = [NSString formattedValue:[NSString stringWithFormat:@"%@",str]];
    return user;
}


- (void)updateUserWorkingDays:(NSString *)str {
    self.userWorkDays = [NSString formattedValue:[NSString stringWithFormat:@"%@",str]];
    [UserManager sharedManager].activeUser = self;
    [self saveUserInformation];
}

- (void)saveUserInformation {
    NSString *xmlPath = [kDocumentsDirectory stringByAppendingPathComponent:userXMLName];
    [NSKeyedArchiver archiveRootObject:self toFile:xmlPath];
}

- (void)deleteUserInformation {
    // check user xml data is available
    NSString *xmlPath = [kDocumentsDirectory stringByAppendingPathComponent:userXMLName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:xmlPath]) {
        [fileManager removeItemAtPath:xmlPath error:nil];
    }
}


#pragma mark - NSCoding Methods
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.userRole forKey:@"role"];
    [aCoder encodeObject:self.userSwitch forKey:@"switch"];
    [aCoder encodeObject:self.userTermAccepted forKey:@"termsAccepted"];
    [aCoder encodeObject:self.userFName forKey:@"firstName"];
    [aCoder encodeObject:self.userLName forKey:@"lastName"];
    [aCoder encodeObject:self.userToken forKey:@"userToken"];
    [aCoder encodeObject:self.userImageUrl forKey:@"profileImage"];
    [aCoder encodeObject:self.userEmail forKey:@"email"];
    
    [aCoder encodeObject:self.objCompany forKey:@"objCompany"];
    [aCoder encodeObject:self.userDeparmentId forKey:@"departmentId"];
    [aCoder encodeObject:self.userDeparmentName  forKey:@"departmentName"];
    [aCoder encodeObject:self.userTeamId forKey:@"teamId"];
    [aCoder encodeObject:self.userTeamName forKey:@"teamName"];
    [aCoder encodeObject:self.userDesignationId forKey:@"designationId"];
    [aCoder encodeObject:self.userDesignationTitle forKey:@"designationTitle"];
    [aCoder encodeObject:self.userEffectiveActionId forKey:@"effectiveActionId"];
    [aCoder encodeObject:self.userEffectiveActionTitle forKey:@"effectiveActionTitle"];
    [aCoder encodeObject:self.userEffectiveActionDescription forKey:@"effectiveActionDescription"];
    [aCoder encodeObject:self.userWorkDays forKey:@"workingDays"];
    [aCoder encodeObject:self.userEffectiveActionDate forKey:@"date"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.userId     = [aDecoder decodeObjectForKey:@"userId"];
        self.userRole = [aDecoder decodeObjectForKey:@"role"];
        self.userSwitch = [aDecoder decodeObjectForKey:@"switch"];
        self.userTermAccepted = [aDecoder decodeObjectForKey:@"termsAccepted"];
        self.userFName = [aDecoder decodeObjectForKey:@"firstName"];
        self.userLName = [aDecoder decodeObjectForKey:@"lastName"];
        self.userToken = [aDecoder decodeObjectForKey:@"userToken"];
        self.userImageUrl = [aDecoder decodeObjectForKey:@"profileImage"];
        self.userEmail = [aDecoder decodeObjectForKey:@"email"];
        
        self.objCompany = [aDecoder decodeObjectForKey:@"objCompany"];
        self.userDeparmentId  = [aDecoder decodeObjectForKey:@"departmentId"];
        self.userDeparmentName  = [aDecoder decodeObjectForKey:@"departmentName"];
        self.userTeamId = [aDecoder decodeObjectForKey:@"teamId"];
        self.userTeamName = [aDecoder decodeObjectForKey:@"teamName"];
        self.userDesignationId = [aDecoder decodeObjectForKey:@"designationId"];
        self.userDesignationTitle  = [aDecoder decodeObjectForKey:@"designationTitle"];
        self.userEffectiveActionId = [aDecoder decodeObjectForKey:@"effectiveActionId"];
        self.userEffectiveActionTitle = [aDecoder decodeObjectForKey:@"effectiveActionTitle"];
        self.userEffectiveActionDescription = [aDecoder decodeObjectForKey:@"effectiveActionDescription"];
        self.userWorkDays  = [aDecoder decodeObjectForKey:@"workingDays"];
        self.userEffectiveActionDate =  [aDecoder decodeObjectForKey:@"date"];
    }
    return self;
}

+ (User *)getSubordinatesWithAttributes:(NSDictionary *)attributeDict {
    User *user = [[User alloc]initWithBasicAttributes:[attributeDict valueForKey:@"user"]];
    user.userEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionId"]];
    user.userEffectiveActionDescription = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionDescription"]];
    user.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"designation.designationId"]];
    user.userDesignationTitle  = [NSString formattedValue:[attributeDict valueForKeyPath:@"designation.designationTitle"]];
    user.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKey:@"designation"]];
    user.userTeamId   = [NSString formattedNumber:[attributeDict valueForKeyPath:@"teamId"]];
    
    return user;
}

+ (User *)getSubordinatesFullData:(NSDictionary *)attributeDict {
    User *user = [[User alloc]initWithBasicAttributes:attributeDict];
    user.userRole = [NSString formattedNumber:[attributeDict objectForKey:@"role"]];
    user.userTermAccepted = [NSString formattedNumber:[attributeDict objectForKey:@"termsAccepted"]];
    user.userToken = [NSString formattedValue:[attributeDict objectForKey:@"userToken"]];
    user.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKeyPath:@"member.company"]];
    
    user.userDeparmentId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"userdepartment.department.departmentId"]];
    user.userDeparmentName = [NSString formattedValue:[attributeDict valueForKeyPath:@"userdepartment.department.departmentName"]];
    user.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.teamId"]];
    user.userTeamName = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.teamName"]];
    user.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.designation.designationId"]];
    user.userDesignationTitle  = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.designation.designationTitle"]];
    
    user.userEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effective.effectiveActionId"]];
    user.userEffectiveActionTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effective.effectiveActionTitle"]];
    user.userEffectiveActionDescription = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effectiveActionDescription"]];
    user.userEffectiveActionDate = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effective.assignEffectiveActionDate.date"]];
    
    NSArray *arry = [attributeDict valueForKeyPath:@"days"];
    user = [user saveWorkingDayForUser:user WithArry:arry];
    user.min = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MIN"]];
    user.max = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MAX"]];
    user.avg = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.AVG"]];
    user.today = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.TODAY"]];
    user.startTimeStamp = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effective.assignEffectiveActionDate.date"]];
    
    NSMutableArray *graphAry = [attributeDict objectForKey:@"graph"];
    user.graphDetails = [UserPreferences getGraphArray:graphAry];
    
    return user;
}


+ (User *)getMemberDetails:(NSDictionary *)attributeDict {
    User *user = [[User alloc]initWithBasicAttributes:[attributeDict valueForKey:@"user"]];
    user.userDesignationId = [NSString formattedNumber:[attributeDict objectForKey:@"designationId"]];
    user.userEffectiveActionId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
    
    user.objCompany = [[Company alloc] initWithAttributes:attributeDict];
    user.userEffectiveActionDescription = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionDescription"]];
    user.userTeamId = [NSString formattedNumber:[attributeDict objectForKey:@"teamId"]];
    user.userTermAccepted = [NSString formattedNumber:[attributeDict valueForKeyPath:@"user.termsAccepted"]];
    
    NSMutableArray *graphAry = [attributeDict objectForKey:@"graph"];
    user.graphDetails = [UserPreferences getGraphArray:graphAry];
    
    return user;
}

+ (User *)getSearchUser:(NSDictionary *)attributeDict {
    User *user = [[User alloc]initWithBasicAttributes:attributeDict];
    user.userTermAccepted = [NSString formattedNumber:[attributeDict objectForKey:@"termsAccepted"]];
    user.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.designationId"]];
    
    user.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKey:@"member"]];
    user.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.teamId"]];
    user.userDesignationTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.designation.designationTitle"]];
    
    return user;
}

+ (User *)getSubordinateUser:(NSDictionary *)attributeDict {
    User *user = [[User alloc]initWithBasicAttributes:[attributeDict valueForKey:@"user"]];
    user.min = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MIN"]];
    user.max = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MAX"]];
    user.avg = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.AVG"]];
    user.today = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.TODAY"]];
    
    NSMutableArray *graphAry = [attributeDict objectForKey:@"graph"];
    user.graphDetails = [UserPreferences getGraphArray:graphAry];
    
    user.userTermAccepted = [NSString formattedNumber:[attributeDict valueForKeyPath:@"user.termsAccepted"]];
    user.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"designation.designationId"]];
    
    user.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKey:@"designation"]];
    user.userDesignationTitle  = [NSString formattedValue:[attributeDict valueForKeyPath:@"designation.designationTitle"]];
    user.userEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"effective.effectiveActionId"]];
    user.userEffectiveActionTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"effective.effectiveActionTitle"]];
    user.startTimeStamp = [attributeDict valueForKeyPath:@"effective.assignEffectiveActionDate.date"];
    
    user.objCompany = [[Company alloc] initWithAttributes:attributeDict];
    user.userEffectiveActionDescription = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionDescription"]];
    user.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"teamId"]];
    
    return user;
}

@end
