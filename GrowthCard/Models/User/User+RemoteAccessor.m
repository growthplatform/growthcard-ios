//
//  User+RemoteAccessor.m
//  GrowthCard
//
//  Created by Narender Kumar on 19/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "User+RemoteAccessor.h"

@implementation User (RemoteAccessor)

+ (void)performUserLogin:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_LOGIN_URL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        
        @try {
            
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performUserLogOut:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_LOGOUT_URL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        
        @try {
            
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performChangePassword:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_CHANGE_PWD]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        
        @try {
            
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performForgetPassword:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_FORGOT_PASSWORD_URL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        
        @try {
            
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void) performAcceptTerms:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_TERMS_STATUS]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPUT jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        
        @try {
            
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void) performWorkDaysSelect:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_SELECT_WORKDAYS]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    
    // [request addValue:VALUE forHTTPHeaderField:@"Field You Want To Set"];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        
        @try {
            
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performUpdateProfile:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_UPDATE_PROFILE]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPUT jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];

}

+ (void)performProfileImageUpdate:(NSMutableArray *)postData fileData:(NSMutableArray *)fileData completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUPDATE_PROFILE_IMAGE]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestMultipartFormDataWithURL:URL HTTPMethod:HTTPMethodPOST additionalHeaders:nil postDataArray:postData fileDataArray:fileData];
    
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError* error, id responseData) {
        
        @try {
            if (error) // Handle the error.
            {
                completionHandler(nil, error , nil);
            }
            else // A nil error indicates success!
            {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];

}


+ (void)updateUserImage:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kUSER_UPDATE_PROFILE]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPUT jsonDictionary:jsonDict];
    
    // Create a connection and perform request
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
    
}

+ (void)performGetSubordinates:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kGET_SUBORDINATE]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];

}

+ (void)performGetEAList:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kGET_EFFECTIVE_ACTION_LIST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
    
}


+ (void)performAddEa:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kGET_CREADTE_EFFECTIVE_ACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performAssignEa:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kASSIGN_EFFECTIVE_ACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];

}
+ (void)performChangeEa:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kASSIGN_EFFECTIVE_ACTION]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
    
}

+ (void)performEaDone:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kEFFECTIVE_ACTION_DONE]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchSubordinate:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_SUBORDINATE]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchTeamMember:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kGET_TEAM_MEMBERS]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchUsers:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kGET_SEARCH_USER]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}


+ (void)performFetchManagerdashboard:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_MANAGER_DASHBOARD]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performSwitchUser:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kSWITCH_USER]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchFeed:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_FEED]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];

}


@end
