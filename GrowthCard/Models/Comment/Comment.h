//
//  Comment.h
//  GrowthCard
//
//  Created by Narender Kumar on 21/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject

@property (strong, nonatomic) NSNumber *commentId;
@property (strong, nonatomic) NSNumber *compnyId;
@property (strong, nonatomic) NSString *commentDesc;
@property (strong, nonatomic) NSString *commentAtTime;
@property (strong, nonatomic) User *commentUser;
@property (assign, nonatomic) BOOL isExpended;

- (id)initWithAttributes:(NSDictionary *)attributeDict;
+ (Comment *)getPostWithAttributes:(NSDictionary *)attributeDict;
- (instancetype)initWithBasicAttributes:(NSDictionary *)attributeDict;

@end
