//
//  Comment+RemoteAccessor.h
//  GrowthCard
//
//  Created by Narender Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Comment.h"

@interface Comment (RemoteAccessor)

+ (void)performSendCommnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchCommnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performCreatePost:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchPost:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performDeletePost:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;


@end
