//
//  Comment+RemoteAccessor.m
//  GrowthCard
//
//  Created by Narender Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Comment+RemoteAccessor.h"

@implementation Comment (RemoteAccessor)

+ (void)performSendCommnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kSEND_COMMENT]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchCommnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_COMMENT]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performCreatePost:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kCREATE_POST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchPost:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_POST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performDeletePost:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kDELETE_POST]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

@end
