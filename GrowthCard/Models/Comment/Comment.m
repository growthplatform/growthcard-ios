//
//  Comment.m
//  GrowthCard
//
//  Created by Narender Kumar on 21/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Comment.h"

@implementation Comment

- (id)init {
    if (self = [super init]) {
        self.commentId = 0;
        self.compnyId = 0;
        self.commentDesc = @"";
        self.commentAtTime = @"";
        self.commentUser = [User new];
    }
    return self;
}

- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self = [[Comment alloc]initWithBasicAttributes:attributeDict];
        self.compnyId   = [NSString formattedNumber:[attributeDict objectForKey:@"compnayId"]];
        self.commentUser = [[User alloc]initWithCommentAttributes:attributeDict postingComment:YES];
    }
    return self;
}

+ (Comment *)getPostWithAttributes:(NSDictionary *)attributeDict {
    Comment *post = [Comment new];
    post.commentId   = [NSString formattedNumber:[attributeDict objectForKey:@"postId"]];
    post.commentDesc = [NSString formattedValue:[attributeDict objectForKey:@"content"]];
    post.commentAtTime = [NSString stringWithFormat:@"%@",[NSString formattedValue:[attributeDict valueForKeyPath:@"createdAt.date"]]];
    post.commentUser = [[User alloc] initWithCommentAttributes:attributeDict postingComment:NO];
    
    return post;
}

- (instancetype)initWithBasicAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        if(attributeDict && (![attributeDict isKindOfClass:[NSNull class]])) {
            self.commentId   = [NSString formattedNumber:[attributeDict objectForKey:@"commentId"]];
            self.commentDesc = [NSString formattedValue:[attributeDict objectForKey:@"content"]];
            self.commentAtTime = [NSString stringWithFormat:@"%@",[NSString formattedValue:[attributeDict valueForKeyPath:@"createdAt.date"]]];
        }
        else {
            self = [[Comment alloc]init];
        }
    }
    return self;
}

@end
