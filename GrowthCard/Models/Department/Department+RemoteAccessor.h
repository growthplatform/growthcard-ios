//
//  Department+RemoteAccessor.h
//  GrowthCard
//
//  Created by Narender Kumar on 01/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Department.h"

@interface Department (RemoteAccessor)

+ (void)performFetchDeptment:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;


@end
