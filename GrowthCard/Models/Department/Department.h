//
//  Department.h
//  GrowthCard
//
//  Created by Narender Kumar on 01/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Department : NSObject

@property (strong, nonatomic) NSNumber *depmntId;
@property (strong, nonatomic) NSString *depmntName;

- (id)initWithAttributes:(NSDictionary *)attributeDict;

@end
