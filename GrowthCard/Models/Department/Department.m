//
//  Department.m
//  GrowthCard
//
//  Created by Narender Kumar on 01/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Department.h"

@implementation Department

- (id)init {
    if (self = [super init]) {
        self.depmntId = 0;
        self.depmntName = @"";
    }
    return self;
}

- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self.depmntId   = [NSString formattedNumber:[attributeDict objectForKey:@"departmentId"]];
        self.depmntName = [NSString formattedValue:[attributeDict objectForKey:@"departmentName"]];
    }
    return self;
}


@end
