//
//  Announcement.h
//  GrowthCard
//
//  Created by Pawan Kumar on 16/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Announcement : NSObject

@property (strong, nonatomic) NSNumber *announceId;
@property (strong, nonatomic) NSString *announceTitle;
@property (strong, nonatomic) NSString *announceDesc;
@property (strong, nonatomic) NSString *announceTime;
@property (strong, nonatomic) User *announceUser;

- (id)initWithAttributes:(NSDictionary *)attributeDict;
- (instancetype)initWithBasicAttributes:(NSDictionary *)attributeDict;

@end
