//
//  Announcement+RemoteAccessor.h
//  GrowthCard
//
//  Created by Narender Kumar on 20/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Announcement.h"

@interface Announcement (RemoteAccessor)

+ (void)performSendAnnoucement:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchAnnounmnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performDeleteAnnounmnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;

@end
