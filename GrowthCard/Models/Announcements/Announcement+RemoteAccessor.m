//
//  Announcement+RemoteAccessor.m
//  GrowthCard
//
//  Created by Narender Kumar on 20/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Announcement+RemoteAccessor.h"

@implementation Announcement (RemoteAccessor)

+ (void)performSendAnnoucement:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kSEND_ANNOUCEMENT]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}


+ (void)performFetchAnnounmnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kGET_ANNOUCEMENT]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performDeleteAnnounmnt:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kDELETE_ANNOUCEMENT]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

@end
