//
//  Announcement.m
//  GrowthCard
//
//  Created by Pawan Kumar on 16/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Announcement.h"

@implementation Announcement


#pragma mark - Init Methods
- (id)init {
    if (self = [super init]) {
        self.announceId = 0;
        self.announceTitle = @"Title";
        self.announceDesc = @"Description";
        self.announceTime = @"";
        self.announceUser = [User new];
    }
    return self;
}

- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self = [[Announcement alloc]initWithBasicAttributes:attributeDict];
        self.announceId   = [NSString formattedNumber:[attributeDict objectForKey:@"announcementId"]];
        self.announceUser = [[User alloc]initWithBasicAttributes:[attributeDict valueForKey:@"user"]];
        self.announceUser.startTimeStamp =[NSString formattedValue:[attributeDict valueForKeyPath:@"member.assign_at"]];
        self.announceUser.userDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.designationId"]];
        self.announceUser.userEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.effectiveActionId"]];
        self.announceUser.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKey:@"member"]];
        self.announceUser.userEffectiveActionDescription = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.effectiveActionDescription"]];
        self.announceUser.userTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"member.teamId"]];
        self.announceUser.userDesignationTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"member.designation.designationTitle"]];
    }
    return self;
}

- (instancetype)initWithBasicAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        if(attributeDict && (![attributeDict isKindOfClass:[NSNull class]])) {
            self.announceTitle = [NSString formattedValue:[attributeDict objectForKey:@"subject"]];
            self.announceDesc = [NSString formattedValue:[attributeDict objectForKey:@"content"]];
            self.announceTime = [NSString formattedValue:[attributeDict valueForKeyPath:@"createdAt.date"]];
        }
        else {
            self = [[Announcement alloc]init];
        }
    }
    return self;
}

@end
