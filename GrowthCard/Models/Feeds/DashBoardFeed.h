//
//  DashBoardFeed.h
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;
@interface DashBoardFeed : NSObject
@property (strong, nonatomic) NSString *feedId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSMutableArray *graphDetails;
@property (strong, nonatomic) NSString *pastEffectiveActionId;

@property (strong, nonatomic) NSNumber *min;
@property (strong, nonatomic) NSNumber *max;
@property (strong, nonatomic) NSNumber *avg;
@property (strong, nonatomic) NSNumber *today;

@property (nonatomic,strong) NSString *startTimeStamp;
@property (nonatomic,strong) NSString *endTimeStamp;
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) NSNumber *likesCount;
@property (assign, nonatomic) BOOL isExpended;

- (id)initWithAttributes:(NSDictionary *)attributeDict;
+ (DashBoardFeed *)currentFeed:(NSDictionary *)attributeDict;
- (void)configUI:(NSIndexPath *)indexPath;




@end
