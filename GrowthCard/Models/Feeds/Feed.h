//
//  Feed.h
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Company.h"
#import "Announcement.h"
#import "Comment.h"

@class User;
@class Announcement;
@class Comment;
@class EAction;

@interface Feed : NSObject
@property (strong, nonatomic) NSNumber *feedId;
@property (strong, nonatomic) NSNumber *feedType;
@property (strong, nonatomic) NSNumber *feedIsLiked;
@property (strong, nonatomic) NSNumber *pastEffectiveActionId;

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSNumber *postID;
@property (strong, nonatomic) NSNumber *likesCount;
@property (strong, nonatomic) NSNumber *commntCount;
@property (strong, nonatomic) User *rUser;

@property (strong, nonatomic) NSNumber *pEffectiveActionId;
@property (strong, nonatomic) NSString *pEAstartDate;
@property (strong, nonatomic) NSString *pEAendDate;
@property (strong, nonatomic) NSString *pEffectiveActionDescription;
@property (strong, nonatomic) NSNumber *ppastEffectiveActionId;


@property (strong, nonatomic) NSString *rAssignAt;
@property (strong, nonatomic) NSNumber *rDesignationId;
@property (strong, nonatomic) NSNumber *rEaId;
@property (strong, nonatomic) Company *objCompany;
@property (strong, nonatomic) NSString *rEaDescpton;
@property (strong, nonatomic) NSNumber *rTeamId;

@property (strong, nonatomic) NSString *rDesignationTitle;
@property (strong, nonatomic) Comment *objComment;
@property (strong, nonatomic) NSNumber *rCommentUserRole;
@property (strong, nonatomic) User *sUser;

@property (strong, nonatomic) NSMutableArray *rGraphDetails;
@property (strong, nonatomic) NSString *sUserEffectiveActionTitle;
@property (strong, nonatomic) NSString *sUserEffectiveActionDate;

@property (strong, nonatomic) NSString *postDesc;
@property (strong, nonatomic) NSString *postDate;
@property (strong, nonatomic) Announcement *objAnnouncement;

@property (strong, nonatomic) NSNumber *isPast;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;
@property (assign, nonatomic) BOOL isExpended;

- (id)initWithAttributes:(NSDictionary *)attributeDict;

@end
