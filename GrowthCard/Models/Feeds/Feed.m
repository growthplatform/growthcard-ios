//
//  Feed.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "Feed.h"
#import "UserPreferences.h"


@implementation Feed

- (id)init {
    if (self = [super init]) {
        self.feedId = 0;
        self.pastEffectiveActionId = 0;
        self.feedType = 0;
        self.feedIsLiked = 0;
        self.title = @"";
        self.desc = @"";
        self.postID = 0;
        self.likesCount = 0;
        self.commntCount = 0;
        self.rAssignAt = @"";
        self.rDesignationId = 0;
        self.rEaId = 0;
        self.rEaDescpton = @"";
        self.rTeamId = 0;
        self.rDesignationTitle = @"";
        self.rCommentUserRole = 0;
        self.rGraphDetails = [NSMutableArray new];
        self.sUserEffectiveActionTitle  = @"";
        self.sUserEffectiveActionDate = @"";
        self.postDesc = @"";
        self.postDate = @"";
        self.isPast = [NSNumber numberWithInt:0];
        self.startDate = @"";
        self.endDate = @"";
    }
    return self;
}

- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self.feedId = [NSString formattedNumber:[attributeDict objectForKey:@"feedId"]];
        self.feedType = [NSString formattedNumber:[attributeDict objectForKey:@"type"]];
        self.feedIsLiked = [NSString formattedNumber:[attributeDict objectForKey:@"isLiked"]];
        self.pastEffectiveActionId = [NSString formattedNumber:[attributeDict objectForKey:@"pastEffectiveActionId"]];
        
        self.postID      = [NSString formattedNumber:[attributeDict objectForKey:@"postId"]];
        self.likesCount  = [NSString formattedNumber:[attributeDict objectForKey:@"likeCount"]];
        self.commntCount = [NSString formattedNumber:[attributeDict objectForKey:@"commentCount"]];
        self.rUser = [[User alloc]initWithBasicAttributes:[attributeDict valueForKey:@"user"]];
        
        self.rAssignAt = [NSString formattedValue:[attributeDict valueForKeyPath:@"user.member.assign_at"]];
        self.rDesignationId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"user.member.designationId"]];
        self.rEaId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
        self.objCompany = [[Company alloc] initWithAttributes:[attributeDict valueForKeyPath:@"user.member"]];
        self.rEaDescpton = [NSString formattedValue:[attributeDict valueForKeyPath:@"user.member.effectiveActionDescription"]];

        self.rTeamId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"user.member.teamId"]];
        self.rDesignationTitle= [NSString formattedValue:[attributeDict valueForKeyPath:@"user.member.designation.designationTitle"]];
        self.rCommentUserRole = [NSString formattedNumber:[attributeDict valueForKeyPath:@"comment.role"]];
        self.objComment = [[Comment alloc]initWithBasicAttributes:[attributeDict valueForKey:@"comment"]];
        self.sUser = [[User alloc]initWithBasicAttributes:[attributeDict valueForKeyPath:@"comment.user"]];
        
        if([attributeDict valueForKeyPath:@"effectiveaction.graph"] != [NSNull null]) {
            NSMutableArray *graphAry = [attributeDict valueForKeyPath:@"effectiveaction.graph"];
            self.rGraphDetails = [UserPreferences getGraphArray:graphAry];
        }
        self.sUserEffectiveActionTitle = [NSString formattedValue:[attributeDict valueForKeyPath:@"effectiveaction.effectiveActionTitle"]];
        self.sUserEffectiveActionDate = [NSString formattedValue:[attributeDict valueForKeyPath:@"effectiveaction.assignEffectiveActionDate.date"]];
        self.postDesc = [NSString formattedValue:[attributeDict valueForKeyPath:@"post.content"]];
        self.postDate = [NSString formattedValue:[attributeDict valueForKeyPath:@"post.createdAt.date"]];
        
        self.objAnnouncement = [[Announcement alloc]initWithBasicAttributes:[attributeDict valueForKey:@"announcement"]];
        self.pEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"pasteffectiveaction.effectiveActionId"]];
        self.pEAstartDate = [NSString formattedValue:[attributeDict valueForKeyPath:@"pasteffectiveaction.startDate"]];
        self.pEAendDate = [NSString formattedValue:[attributeDict valueForKeyPath:@"pasteffectiveaction.endDate.date"]];
        self.pEffectiveActionDescription = [NSString formattedValue:[attributeDict valueForKeyPath:@"pasteffectiveaction.effectiveActionDescription"]];
        self.ppastEffectiveActionId = [NSString formattedNumber:[attributeDict valueForKeyPath:@"pasteffectiveaction.pastEffectiveActionId"]];
        
        if(self.pEffectiveActionId != nil && self.pEffectiveActionDescription != nil){
            self.rEaDescpton = self.pEffectiveActionDescription;
        }
    }
    return self;
}

@end
