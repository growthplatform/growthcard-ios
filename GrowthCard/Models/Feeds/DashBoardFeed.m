//
//  DashBoardFeed.m
//  GrowthCard
//
//  Created by Pawan Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DashBoardFeed.h"
#import "UserPreferences.h"
#import "NSDate+Additions.h"

@implementation DashBoardFeed

- (id)init {
    if (self = [super init]) {
        self.feedId = @"";
        self.title=@"";
        self.desc=@"";
        self.startTimeStamp=@"Not Found";
        self.endTimeStamp=@"Not Found";
        self.likesCount = 0;
        self.pastEffectiveActionId = @"0";
    }
    return self;
}

- (id)initWithAttributes:(NSDictionary *)attributeDict {
    if (self = [super init]) {
        self.feedId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
        self.title  = [NSString formattedValue:[attributeDict valueForKeyPath:@"effectiveaction.effectiveActionTitle"]];;
        self.desc   = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionDescription"]];;
        self.pastEffectiveActionId   = [NSString formattedValue:[attributeDict objectForKey:@"pastEffectiveActionId"]];;
        self.likesCount  = [NSString formattedNumber:[attributeDict objectForKey:@"likeCount"]];
        self.min=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MIN"]];;
        self.max=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MAX"]];
        self.avg=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.AVG"]];
        self.today=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.TODAy"]];
        self.startTimeStamp = [NSString stringWithFormat:@"%@",[NSString formattedValue:[attributeDict objectForKey:@"startDate"]]];
        self.endTimeStamp=[NSString stringWithFormat:@"%@",[NSString formattedValue:[attributeDict valueForKeyPath:@"endDate.date"]]];
        NSMutableArray *graphAry = [attributeDict objectForKey:@"graph"];
        self.graphDetails=[UserPreferences getGraphArray:graphAry];
    }
    return self;
}

+ (DashBoardFeed *)currentFeed:(NSDictionary *)attributeDict {
    DashBoardFeed *dbFeed = [DashBoardFeed new];
    dbFeed.feedId = [NSString formattedNumber:[attributeDict objectForKey:@"effectiveActionId"]];
    dbFeed.title  = [NSString formattedValue:[attributeDict valueForKeyPath:@"effectiveActionTitle"]];;
    dbFeed.desc   = [NSString formattedValue:[attributeDict objectForKey:@"effectiveActionDescription"]];;
    
    NSMutableArray *graphAry = [attributeDict objectForKey:@"graph"];
    dbFeed.graphDetails=[UserPreferences getGraphArray:graphAry];
    
    dbFeed.min=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MIN"]];;
    dbFeed.max=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.MAX"]];
    dbFeed.avg=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.AVG"]];
    dbFeed.today=[NSString formattedNumber:[attributeDict valueForKeyPath:@"effectiveActionSummary.TODAy"]];
    dbFeed.startTimeStamp = [NSString stringWithFormat:@"%@",[NSString formattedValue:[attributeDict objectForKey:@"startDate"]]];
    dbFeed.endTimeStamp=[NSString stringWithFormat:@"%@",[NSString formattedValue:[attributeDict valueForKeyPath:@"endDate"]]];
    return dbFeed;
}

- (void)configUI:(NSIndexPath *)indexPath {
    NSLog(@"Row number : %ld", (long)indexPath.row);
}

@end
