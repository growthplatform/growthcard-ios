//
//  DashBoardFeed+RemoteAccessor.h
//  GrowthCard
//
//  Created by Narender Kumar on 21/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DashBoardFeed.h"

@interface DashBoardFeed (RemoteAccessor)

+ (void)performFetchPastEAs:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performDeletePastEAs:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performFetchCurrentEA:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;
+ (void)performLikeFeed:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler;

@end
