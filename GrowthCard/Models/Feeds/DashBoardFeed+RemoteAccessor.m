//
//  DashBoardFeed+RemoteAccessor.m
//  GrowthCard
//
//  Created by Narender Kumar on 21/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DashBoardFeed+RemoteAccessor.h"

@implementation DashBoardFeed (RemoteAccessor)

+ (void)performFetchPastEAs:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_PAST_EA]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

// kDELETE_PAST_EA
+ (void)performDeletePastEAs:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kDELETE_PAST_EA]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performFetchCurrentEA:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kFETCH_CURRENT_EA]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];
}

+ (void)performLikeFeed:(NSDictionary *)jsonDict completionHandler:(HTTPRequestCompletionHandler)completionHandler {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [[ConfigurationManager sharedInstance] APIEndPoint], kLIKE_FEED]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL HTTPMethod:HTTPMethodPOST jsonDictionary:jsonDict];
    [[NetworkManager sharedInstance] performRequest:request userInfo:nil completionHandler:^(HTTPResponse *response, NSError *error,  id responseData) {
        @try {
            if (error) /* Handle the error. */ {
                completionHandler(nil, error, nil);
            }
            else  /* A nil error indicates success! */ {
                completionHandler(response, nil, nil);
            }
        }
        @catch (NSException *exception) {
            
            NSLog(@"ExceptionName : %@, ExceptionReason : %@", exception.name, exception.reason);
            completionHandler(nil, [NSError apiRequestGenericError], nil);
        }
    }];;
}

@end
