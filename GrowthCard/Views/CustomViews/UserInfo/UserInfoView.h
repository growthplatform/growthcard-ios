//
//  UserInfoView.h
//
//  Created by Pawan Kumar on 08/10/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@protocol UserInfoViewDelegate

- (void)didSelectProfile:(NSString *)userId;

@end


@interface UserInfoView : UIView

@property(nonatomic,weak)id<UserInfoViewDelegate> delegate;

-(void)resetView:(User *)userData;

@end
