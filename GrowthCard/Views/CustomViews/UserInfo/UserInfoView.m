//
//  UserInfoView.m
//
//  Created by Pawan Kumar on 08/10/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "UserInfoView.h"
#import "User.h"

@interface UserInfoView() <UIGestureRecognizerDelegate> {
    __weak IBOutlet UILabel *lblUserName;
    __weak IBOutlet UILabel *lblDepart;
    NSString *userId;
}
@property (weak, nonatomic) IBOutlet UIImageView *profIleImage;

@end


@implementation UserInfoView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIView *v = [[NSBundle mainBundle] loadNibNamed:@"UserInfoView" owner:self options:nil][0];
        v.frame = self.bounds;
        [self addSubview:v];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark - Public Method
-(void)resetView:(User *)userData {
    [self.profIleImage sd_setImageWithURL:[NSURL URLWithString:userData.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    lblUserName.text      = [NSString stringWithFormat:@"%@ %@",[userData.userFName pascalCased], [userData.userLName pascalCased]];
    lblDepart.text   = [NSString stringWithFormat:@"%@",userData.userDeparmentName];
    userId = [NSString stringWithFormat:@"%@", userData.userId];
}

#pragma mark - Action Method
- (IBAction)profileTapped {
    if(self.delegate && [(UIViewController*)self.delegate respondsToSelector:@selector(didSelectProfile:)]) {
        [self.delegate didSelectProfile:userId];
    }
}

@end
