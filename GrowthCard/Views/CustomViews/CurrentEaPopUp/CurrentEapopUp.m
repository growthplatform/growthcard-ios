//
//  CurrentEapopUp.m
//  GrowthCard
//
//  Created by Narender Kumar on 07/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "CurrentEapopUp.h"

@interface CurrentEapopUp () {
}
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UIButton *cloaseBtn;

@end


@implementation CurrentEapopUp

+ (instancetype)currentEaView {
    CurrentEapopUp *header = (CurrentEapopUp *)[UIView viewFromXib:@"CurrentEapopUp" classname:[CurrentEapopUp class] owner:self];
    User *user = [UserManager sharedManager].activeUser;
    header.frame=[UIScreen mainScreen].bounds;
    header.eaTitleLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    [header.eaTitleLbl sizeToFit];
    [header setNeedsUpdateConstraints];
    header.tag = 786543 ;
    return header;
}

- (void)awakeFromNib {
    // Initialization code
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:swiperight];
}

#pragma mark -
#pragma mark Private Method
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer {
    [self removeFromSuperview];
    //Do what you want here
}

#pragma mark -
#pragma mark IBAction
- (IBAction)closeBtnClicked:(id)sender {
    [self removeFromSuperview];
}


@end
