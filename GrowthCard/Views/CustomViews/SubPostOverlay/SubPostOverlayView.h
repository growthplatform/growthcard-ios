//
//  SubPostOverlayView.h
//  GrowthCard
//
//  Created by Pawan Kumar on 25/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SubPostOverlayViewDelegate;

@interface SubPostOverlayView : UIView
@property (nonatomic, assign) id<SubPostOverlayViewDelegate> delegate;
+ (instancetype)view;
@end

@protocol SubPostOverlayViewDelegate <NSObject>
@optional
- (void)didMyTeamClicked;
- (void)didMyDepartmentClicked;
- (void)didAllClicked;
@end
