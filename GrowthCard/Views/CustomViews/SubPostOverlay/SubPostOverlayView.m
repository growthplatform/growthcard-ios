//
//  SubPostOverlayView.m
//  GrowthCard
//
//  Created by Pawan Kumar on 25/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubPostOverlayView.h"

@interface SubPostOverlayView() {
}
@property (weak, nonatomic) IBOutlet UIButton *btnMyTeam;
@property (weak, nonatomic) IBOutlet UIButton *btnMyDepart;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupsOfPeople;
@property (weak, nonatomic) IBOutlet UIButton *btnAll;

@end


@implementation SubPostOverlayView

+ (instancetype)view {
    SubPostOverlayView *header = (SubPostOverlayView *)[UIView viewFromXib:@"SubPostOverlay" classname:[SubPostOverlayView class] owner:self];
    header.frame=[UIScreen mainScreen].bounds;
    [header handleButtonRadiousAndColor:header.btnMyTeam];
    [header handleButtonRadiousAndColor:header.btnMyDepart];
    [header handleButtonRadiousAndColor:header.btnGroupsOfPeople];
    [header handleButtonRadiousAndColor:header.btnAll];
    return header;
}

-(void)handleButtonRadiousAndColor:(UIButton*)btn {
    btn.backgroundColor=[UIColor clearColor];
    btn.layer.cornerRadius = 5;
    btn.clipsToBounds = YES;
    btn.layer.borderColor = [UIColor colorWithRed:0.349 green:0.3569 blue:0.3725 alpha:1.0].CGColor;
    btn.layer.borderWidth = 1;

}

#pragma mark -
#pragma mark IBAction
- (IBAction)btnMyTeamClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didMyTeamClicked)]) {
        [_delegate didMyTeamClicked];
    }
    [self btnCrossClick:sender];
}

- (IBAction)btnMyDepartClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didMyDepartmentClicked)]) {
        [_delegate didMyDepartmentClicked];
    }
    [self btnCrossClick:sender];
}

- (IBAction)btnGroupsOfPeopleClick:(id)sender {
}

- (IBAction)btnAllClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didAllClicked)]) {
        [_delegate didAllClicked];
    }
    [self btnCrossClick:sender];
}

- (IBAction)btnCrossClick:(id)sender {
    [self removeFromSuperview];
}

@end
