//
//  MYPastEADetailsView.m
//  GrowthCard
//
//  Created by Pawan Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "MYPastEADetailsView.h"
#import "DashBoardFeed.h"
#import "User.h"
#import "UIView+Additions.h"

@interface MYPastEADetailsView() {
    
}
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaTimeLbl;
@property (weak, nonatomic) IBOutlet UIView *graphView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descHConst;

@end


@implementation MYPastEADetailsView

+ (instancetype)currentView {
    MYPastEADetailsView *header = (MYPastEADetailsView *)[UIView viewFromXib:@"MYPastEADetailsView" classname:[MYPastEADetailsView class] owner:self];
    return header;
}

#pragma mark -
#pragma mark Public Methods
- (void)setUserDataWithEA:(DashBoardFeed *)info  WithUser:(User*)user {
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",info.title];
    _eaDescLbl.text = [NSString stringWithFormat:@"%@",info.desc];
    _eaTimeLbl.text=[NSDate getHeaderDateStrFromString:info.startTimeStamp];
    [APPManager addChart:info.graphDetails AndView:self.graphView];
}

#pragma mark - 
#pragma mark Private Methods
-(CGSize)refreshSize {
    int viewHight=241;
    int defaultLabelHight1=29;
    int defaultLabelHight2=20;
    
    int labelHight1=[self.eaTitleLbl  heightForLabel:self.eaTitleLbl.text];
    int labelHight2=[self.eaDescLbl  heightForLabel:self.eaDescLbl.text];
    
    self.titleHConst.constant=labelHight1;
    self.descHConst.constant=labelHight2;
    
    CGRect rect=self.bounds;
    rect.size.height=viewHight+(labelHight1-defaultLabelHight1)+(labelHight2-defaultLabelHight2);
    [self setBounds:rect];
    [self setFrame:rect];
    
    return self.frame.size;    
}

@end
