//
//  MYPastEADetailsView.h
//  GrowthCard
//
//  Created by Pawan Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYPastEADetailsView : UIView

@property (weak, nonatomic) IBOutlet FRHyperLabel *eaDescLbl;

+ (instancetype)currentView;
- (void)setUserDataWithEA:(DashBoardFeed *)info  WithUser:(User*)user;
- (CGSize)refreshSize;

@end
