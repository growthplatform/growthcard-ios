//
//  SubFeedFilterView.m
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubFeedFilterView.h"
#import "department.h"

#define BTN_HEIGHT 40

@interface SubFeedFilterView() {
    __weak IBOutlet UIButton *allUserBtn;
    __weak IBOutlet UIButton *feedBtn;
    __weak IBOutlet UIButton *managerBtn;
    __weak IBOutlet UIButton *subordinateBtn;
    __weak IBOutlet UIScrollView *scrollVew;
    
    __weak IBOutlet UIView *_topV;
    UIImageView *_tickImgV1, *_tickImgV2;
    NSMutableArray *departmentsButton;
    NSMutableArray *typesButton;
    int deptCellH;
}
@end


@implementation SubFeedFilterView

+ (instancetype)filterView {
    SubFeedFilterView *header = (SubFeedFilterView *)[UIView viewFromXib:@"SubFeedFilterView" classname:[SubFeedFilterView class] owner:self];
    header.frame=[UIScreen mainScreen].bounds;
    [header innovate];
    return header;
}

#pragma mark -
#pragma mark Private Method
- (void)innovate {
    _tickImgV1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [_tickImgV1 setImage:[UIImage imageNamed:@"ico_checkbox"]];
    [_topV addSubview:_tickImgV1];
    
    _tickImgV2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [_tickImgV2 setImage:[UIImage imageNamed:@"ico_checkbox"]];
    [scrollVew addSubview:_tickImgV2];
    _tickImgV2.tag=20000;
    _selectedUserType = 1;
    _selectedDeptType = 0;
    departmentsButton = [NSMutableArray new];
    typesButton = [NSMutableArray new];
    
    [typesButton addObject:allUserBtn];
    [typesButton addObject:feedBtn];
    [typesButton addObject:managerBtn];
    [typesButton addObject:subordinateBtn];
    
    [self depmentlist];
}

- (void)resetData {
    for (int i=0; i<typesButton.count; i++) {
        [self setBorder:typesButton[i]];
    }
    for (int i=0; i<[[UserPreferences sharedPreference] departments].count; i++) {
        [self createButtonAtIndex:i];
        [self setBorder:departmentsButton[i]];
    }
    
    [scrollVew setContentSize:CGSizeMake(scrollVew.frame.size.width, deptCellH+BTN_HEIGHT)];
    [scrollVew bringSubviewToFront:_tickImgV2];
    
    [self resetCheckImageAt:1 withBtn:typesButton[0]];
    if(departmentsButton.count>0) [self resetCheckImageAt:2 withBtn:departmentsButton[0]];
    [self checkHiglight:1];
    [self checkHiglight:2];
    
}

- (void)createButtonAtIndex:(int)index {
    Department *department = [[UserPreferences sharedPreference] departments][index];
    int cellW=([UIScreen mainScreen].bounds.size.width-20)/2;
    int x=index%2==0? 0: cellW;
    deptCellH=(index/2)*BTN_HEIGHT;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(btnUserDeptClick:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:department.depmntName forState:UIControlStateNormal];
    button.frame = CGRectMake(x==0?0:cellW-2, deptCellH, cellW, BTN_HEIGHT+2);
    [scrollVew addSubview:button];
    button.tag=department.depmntId.intValue;
    button.titleLabel.font = allUserBtn.titleLabel.font;
    [departmentsButton addObject:button];
}

- (void)setBorder:(UIButton*)btn {
    [[btn layer] setBorderWidth:1.0f];
    [[btn layer] setBorderColor:[UIColor colorWithRed:0.7333 green:0.7608 blue:0.7647 alpha:1.0].CGColor];
    [btn setTitleColor:[UIColor colorWithRed:0.6392 green:0.6588 blue:0.6745 alpha:1.0] forState:UIControlStateNormal];
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    btn.imageView.image=[UIImage imageNamed:@"ico_checkbox"];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 50);
}

- (void)resetCheckImageAt:(NSInteger)indx withBtn:(UIButton *)btn {
    UIImageView *imgV = (indx == 1) ? _tickImgV1 : _tickImgV2;
    CGRect fr = imgV.frame;
    fr.origin.x = btn.frame.origin.x + btn.frame.size.width - fr.size.width - 5;
    fr.origin.y = btn.frame.origin.y + 10;
    imgV.frame = fr;
}

- (void)updateBackgroundColor:(UIButton*)btn {
    allUserBtn.backgroundColor=[UIColor colorWithRed:0.8784 green:0.9059 blue:0.9137 alpha:1.0];
}

- (void)checkHiglight :(NSInteger)indx  {
    if (indx==1) {
        for (UIButton *btn in typesButton) {
            btn.backgroundColor = (btn.tag != self.selectedUserType) ? [UIColor colorWithRed:0.8784 green:0.9059 blue:0.9137 alpha:1.0] : [UIColor colorWithRed:0.149 green:0.6902 blue:0.2118 alpha:1.0];
            btn.selected = (btn.tag == self.selectedUserType);
        }
    }
    else  {
        for (UIButton *btn in departmentsButton) {
            btn.backgroundColor = (btn.tag != self.selectedDeptType) ? [UIColor colorWithRed:0.8784 green:0.9059 blue:0.9137 alpha:1.0] : [UIColor colorWithRed:0.149 green:0.6902 blue:0.2118 alpha:1.0];
            btn.selected = (btn.tag == self.selectedDeptType);
        }
    }
}

#pragma mark -
#pragma mark IBAction
- (IBAction)btnUserTypeClick:(id)sender {
    UIButton *btn=(UIButton*)sender;
    self.selectedUserType = btn.tag;
    [self resetCheckImageAt:1 withBtn:btn];
    [self checkHiglight:1];
}

- (void)btnUserDeptClick:(id)sender {
    UIButton *btn=(UIButton*)sender;
    self.selectedDeptType = btn.tag;
    [self resetCheckImageAt:2 withBtn:btn];
    [self checkHiglight:2];
}

- (IBAction)btnCrossClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didCrossButtonClicked)]) {
        [_delegate didCrossButtonClicked];
    }
}

- (IBAction)btnApplyFilterClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didApplyFilterClickedWithUserType: AndDeprtmnt:)]) {
        [_delegate didApplyFilterClickedWithUserType:(int)self.selectedUserType AndDeprtmnt:(int)self.selectedDeptType];
    }
}


#pragma mark -
#pragma mark web service
- (void)depmentlist {
    if ([[[UserPreferences sharedPreference] departments] count] > 1) {
        [self resetData]; return;
    }
    User *user = [UserManager sharedManager].activeUser;
    NSMutableDictionary *dict = [UserPreferences defaultUserParam];
    [dict setObject:[NSString stringWithFormat:@"%@",user.objCompany.companyId] forKey:@"companyId"];
    [GMDCircleLoader setOnView:self withTitle:nil animated:YES];
    [DepartmentManager fetchDepmnt:dict completionHandler:^(id response, NSError *error) {
        [GMDCircleLoader hideFromView:self animated:YES];
        NSMutableArray *depts = [NSMutableArray new];
        if(!error) {
            NSArray *arr = (NSArray *) response;
            [depts addObjectsFromArray:arr];
        }
        Department *dep = [[Department alloc] init];
        dep.depmntId = [NSNumber numberWithInt:0];
        dep.depmntName = kAllDepartment;
        [depts insertObject:dep atIndex:0];
        _selectedDeptType = 0;
        [[UserPreferences sharedPreference] setDepartments:depts];
        [self resetData];
        
    }];
}

@end

