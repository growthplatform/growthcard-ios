//
//  SubFeedFilterView.h
//  GrowthCard
//
//  Created by Pawan Kumar on 26/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SubFeedFilterViewDelegate <NSObject>
@optional
- (void)didApplyFilterClickedWithUserType:(int)userType AndDeprtmnt:(int)depmnt ;
- (void)didCrossButtonClicked;
@end

@interface SubFeedFilterView : UIView
@property (nonatomic, assign) id<SubFeedFilterViewDelegate> delegate;
@property (nonatomic, assign) long selectedUserType;
@property (nonatomic, assign) long selectedDeptType;
+ (instancetype)filterView;
- (void)resetData;
@end
