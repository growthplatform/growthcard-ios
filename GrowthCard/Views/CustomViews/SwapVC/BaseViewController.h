//
//  BaseViewController.h
//  GrowthCard
//
//  Created by Prateek Prem on 26/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
