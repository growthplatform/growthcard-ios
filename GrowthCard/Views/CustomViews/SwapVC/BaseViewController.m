//
//  BaseViewController.m
//  GrowthCard
//
//  Created by Prateek Prem on 26/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController () {
    IBOutlet __weak UIView *_hideView;
    IBOutlet __weak UIView *_leftView;
    IBOutlet __weak UIView *_rightView;
    IBOutlet __weak UIView *_circleView;

    IBOutlet __weak UIView *_leftMenuSliderView;
    BOOL isMenuOpen;

}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addLeftSwipeGesture];
    [self addRightSwipeGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view addSubview: _leftMenuSliderView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Private Methods
- (void)addLeftSwipeGesture {
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipe)];
    swipe.direction = UISwipeGestureRecognizerDirectionLeft;
    //[self.view addGestureRecognizer:swipe];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController].view addGestureRecognizer:swipe];
    
    //swipe = nil;
}

- (void)addRightSwipeGesture {
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipe)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    // [self.view addGestureRecognizer:swipe];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController].view addGestureRecognizer:swipe];
    //swipe = nil;
}

- (void)leftSwipe {

    [self showLeftMenu:NO];
    
}

- (void)rightSwipe {

    [self showLeftMenu:YES];
}


- (void)addTapGesture {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideLeftMenu)];
   // [self.view addGestureRecognizer:tap];
    
}

#pragma mark - Menu Bar
- (IBAction)leftMenuClicked:(id)sender {
    [self showLeftMenu:(isMenuOpen)?NO:YES];
}

- (void)hideLeftMenu {
    isMenuOpen = YES;
    [self showLeftMenu:isMenuOpen];
}

- (void)showLeftMenu:(BOOL)show {
    isMenuOpen = show;
    //self.view.userInteractionEnabled = !show;
    [self.view bringSubviewToFront:_leftView];
    
    CGRect fr = _leftView.frame;
    fr.origin.x = (show) ? 0:-self.view.frame.size.width-50;
    
    CGRect hFr = _hideView.frame;
    hFr.size.width = (show)? 0 : 150;
    
    CGRect cFr = _circleView.frame;
    cFr.origin.x = (show)? self.view.frame.size.width/2-cFr.size.width/2 : (_leftView.frame.size.width * 50/100);
    
    UIView *vv = self.navigationController.view;
    
    __block CGRect fr1 = vv.frame;
    fr1.origin.x = fr.origin.x + fr.size.width;
    
    _leftMenuSliderView.hidden = YES;
    [UIView animateWithDuration:1 animations:^{
        _leftView.frame = fr;
        _circleView.frame = cFr;
        _hideView.frame = hFr;

        vv.frame = fr1;

    } completion:^(BOOL finished) {
        _leftMenuSliderView.hidden = NO;
    }];
}

- (void)showRightMenu:(BOOL)show {
    isMenuOpen = show;
    self.view.userInteractionEnabled = !show;
    
    CGRect fr = _leftView.frame;
    fr.origin.x = (show) ? 0:-fr.size.width;
    
    UIView *vv = self.navigationController.view;
    
    __block CGRect fr1 = vv.frame;
    fr1.origin.x = fr.origin.x + fr.size.width;
    
    [UIView animateWithDuration:0.3 animations:^{
        _leftView.frame = fr;
        vv.frame = fr1;
        
    }];
}


@end
