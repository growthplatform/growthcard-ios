//
//  ExpandingTextView.m
//  GrowthCard
//
//  Created by Arvind Singh on 14/04/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "ExpandingTextView.h"

#define kTopContentInset -4
#define lBottonContentInset 12
#define kTextLength  140

@implementation ExpandingTextViewInternal

- (void)setContentOffset:(CGPoint)offset {
    // Check if user scrolled
    if (self.tracking || self.decelerating) {
        self.contentInset = UIEdgeInsetsMake(kTopContentInset, 0, 0, 0);
    }
    else {
        float bottomContentOffset = (self.contentSize.height - self.frame.size.height + self.contentInset.bottom);
        if (offset.y < bottomContentOffset && self.scrollEnabled) {
            self.contentInset = UIEdgeInsetsMake(kTopContentInset, 0, lBottonContentInset, 0);
        }
    }
    
    [super setContentOffset:offset];
}

- (void)setContentInset:(UIEdgeInsets)inset {
    UIEdgeInsets edgeInsets = inset;
    edgeInsets.top = kTopContentInset;
    
    if (inset.bottom > 12) {
        edgeInsets.bottom = 4;
    }
    
    [super setContentInset:edgeInsets];
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

#define kTextInsetX 4
#define kTextInsetBottom 0

@interface ExpandingTextView ()

@property (strong, nonatomic) UILabel *placeholderLabel;

@property (assign, nonatomic) CGFloat minimumHeight;
@property (assign, nonatomic) CGFloat maximumHeight;

@property (assign, nonatomic) BOOL forceSizeUpdate;

@end

@implementation ExpandingTextView

#pragma mark - Init Method

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.forceSizeUpdate = NO;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        CGRect backgroundFrame = frame;
        backgroundFrame.origin.y = 0;
        backgroundFrame.origin.x = 0;
        
        CGRect textViewFrame = CGRectInset(backgroundFrame, kTextInsetX, 0);
        
        // Internal Text View component
        self.internalTextView = [[ExpandingTextViewInternal alloc] initWithFrame:textViewFrame];
        self.internalTextView.delegate        = self;
        self.internalTextView.font            = [UIFont regularApplicationFontOfSize:14.0];
        self.internalTextView.contentInset    = UIEdgeInsetsMake(-4,0,-4,0);
        self.internalTextView.text            = @"-";
        self.internalTextView.scrollEnabled   = NO;
        self.internalTextView.opaque          = NO;
        self.internalTextView.backgroundColor = [UIColor clearColor];
        self.internalTextView.textColor = [UIColor commentTextColorWithAlpha];
        self.internalTextView.showsHorizontalScrollIndicator = NO;
        self.internalTextView.enablesReturnKeyAutomatically = YES;
        self.internalTextView.autocorrectionType = UITextAutocorrectionTypeNo;
        [self.internalTextView sizeToFit];
        self.internalTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        // set placeholder
        self.placeholderLabel = [[UILabel alloc]initWithFrame:CGRectMake(5,-7,self.bounds.size.width - 16,self.bounds.size.height)];
        self.placeholderLabel.text = self.placeholder;
        self.placeholderLabel.font = self.internalTextView.font;
        self.placeholderLabel.backgroundColor = [UIColor clearColor];
        self.placeholderLabel.textColor = [UIColor placeholderColor];
        [self.internalTextView addSubview:self.placeholderLabel];
        
        [self addSubview:self.internalTextView];
        
        // Calculate the text view height
        UIView *internal = (UIView *)[[self.internalTextView subviews] objectAtIndex:0];
        self.minimumHeight = internal.frame.size.height;
        [self setMinimumNumberOfLines:1];
        self.animateHeightChange = YES;
        self.internalTextView.text = @"";
        [self setMaximumNumberOfLines:13];
        
        [self sizeToFit];
    }
    return self;
}

-(void)sendButtonTaped {
    if ([self.delegate respondsToSelector:@selector(expandingTextViewShouldReturn:)]) {
        [self.delegate performSelector:@selector(expandingTextViewShouldReturn:) withObject:self];
    }
}

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = [placeholder copy];
    self.placeholderLabel.text = placeholder;
    self.placeholderLabel.font = [UIFont regularApplicationFontOfSize:13.0];
    self.placeholderLabel.textColor = [UIColor colorWithHexString:@"8b9297"];
}

- (void)sizeToFit
{
    CGRect r = self.frame;
    if ([self.text length] > 0) {
        // No need to resize is text is not empty
        return;
    }
    r.size.height = self.minimumHeight + kTextInsetBottom;
    self.frame = r;
}

- (void)setFrame:(CGRect)aframe {
    CGRect backgroundFrame   = aframe;
    backgroundFrame.origin.y = 0;
    backgroundFrame.origin.x = 0;
    CGRect textViewFrame = CGRectInset(backgroundFrame, kTextInsetX, 0);
    self.internalTextView.frame   = textViewFrame;
    backgroundFrame.size.height  -= 8;
    /*self.textViewBackgroundImage.frame = backgroundFrame;*/
    self.forceSizeUpdate = YES;
    [super setFrame:aframe];
}

- (void)clearText {
    self.text = @"";
    [self textViewDidChange:self.internalTextView];
}

/*
 * Code and comments thanks to https://github.com/sanjerali
 * Based on http://stackoverflow.com/a/19047464/296888 by http://stackoverflow.com/users/1003533/tarmes
 * Function to measure the textView Height in ios7
 */
- (CGFloat)measureHeightOfUITextView:(UITextView *)textView {
    if ([textView respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
        // This is the code for iOS 7. contentSize no longer returns the correct value, so
        // we have to calculate it.
        //
        // This is partly borrowed from HPGrowingTextView, but I've replaced the
        // magic fudge factors with the calculated values (having worked out where
        // they came from)
        
        CGRect frame = textView.bounds;
        
        // Take account of the padding added around the text.
        
        UIEdgeInsets textContainerInsets = textView.textContainerInset;
        UIEdgeInsets contentInsets = textView.contentInset;
        
        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + textView.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
        
        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;
        
        NSString *textToMeasure = textView.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
        }
        
        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *attributes = @{ NSFontAttributeName: textView.font, NSParagraphStyleAttributeName : paragraphStyle };
        
        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
        
        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight+8.0f;
    }
    else {
        return textView.contentSize.height;
    }
}

- (void)setMaximumNumberOfLines:(NSUInteger)n {
    BOOL didChange            = NO;
    NSString *saveText        = self.internalTextView.text;
    NSString *newText         = @"-";
    self.internalTextView.hidden   = YES;
    self.internalTextView.delegate = nil;
    for (int i = 2; i < n; ++i) {
        newText = [newText stringByAppendingString:@"\n|W|"];
    }
    self.internalTextView.text     = newText;
    
    if(floor(NSFoundationVersionNumber)>NSFoundationVersionNumber_iOS_6_1) {
        didChange = (self.maximumHeight != [self measureHeightOfUITextView:self.internalTextView]);
    }
    else {
        didChange = (self.maximumHeight != self.internalTextView.contentSize.height);
    }
    if(floor(NSFoundationVersionNumber)>NSFoundationVersionNumber_iOS_6_1) {
        self.maximumHeight             = [self measureHeightOfUITextView:self.internalTextView];
    } else {
        self.maximumHeight             = self.internalTextView.contentSize.height;
    }
    
    _maximumNumberOfLines      = n;
    self.internalTextView.text     = saveText;
    self.internalTextView.hidden   = NO;
    self.internalTextView.delegate = self;
    if (didChange) {
        self.forceSizeUpdate = YES;
        [self textViewDidChange:self.internalTextView];
    }
}

- (void)setMinimumNumberOfLines:(NSUInteger)m {
    NSString *saveText        = self.internalTextView.text;
    NSString *newText         = @"-";
    self.internalTextView.hidden   = YES;
    self.internalTextView.delegate = nil;
    for (int i = 2; i < m; ++i) {
        newText = [newText stringByAppendingString:@"\n|W|"];
    }
    self.internalTextView.text     = newText;
    if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.minimumHeight = [self measureHeightOfUITextView:self.internalTextView];
    }
    else {
        self.minimumHeight = self.internalTextView.contentSize.height;
    }
    self.internalTextView.text     = saveText;
    self.internalTextView.hidden   = NO;
    self.internalTextView.delegate = self;
    [self sizeToFit];
    _minimumNumberOfLines = m;
}

- (void)textViewDidChange:(UITextView *)textView {
    if(textView.text.length == 0)
        self.placeholderLabel.alpha = 1;
    else
        self.placeholderLabel.alpha = 0;
    
    NSInteger newHeight;
    
    if(floor(NSFoundationVersionNumber)>NSFoundationVersionNumber_iOS_6_1) {
        newHeight = [self measureHeightOfUITextView:self.internalTextView];
    }
    else {
        newHeight = self.internalTextView.contentSize.height;
    }
    
    if(newHeight < self.minimumHeight || !self.internalTextView.hasText) {
        newHeight = self.minimumHeight;
    }
    
    if (self.internalTextView.frame.size.height != newHeight || self.forceSizeUpdate) {
        self.forceSizeUpdate = NO;
        if (newHeight > self.maximumHeight && self.internalTextView.frame.size.height <= self.maximumHeight) {
            newHeight = self.maximumHeight;
        }
        if (newHeight <= self.maximumHeight) {
            if(self.animateHeightChange) {
                [UIView beginAnimations:@"" context:nil];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDidStopSelector:@selector(growDidStop)];
                [UIView setAnimationBeginsFromCurrentState:YES];
            }
            
            if ([self.delegate respondsToSelector:@selector(expandingTextView:willChangeHeight:)]) {
                [self.delegate expandingTextView:self willChangeHeight:(newHeight+ kTextInsetBottom)];
            }
            
            // Resize the frame
            CGRect r = self.frame;
            r.size.height = newHeight + kTextInsetBottom;
            self.frame = r;
            r.origin.y = 0;
            r.origin.x = 0;
            self.internalTextView.frame = CGRectInset(r, kTextInsetX, 0);
            r.size.height -= 8;
            //self.textViewBackgroundImage.frame = r;
            
            if(self.animateHeightChange) {
                [UIView commitAnimations];
            }
            else if ([self.delegate respondsToSelector:@selector(expandingTextView:didChangeHeight:)]) {
                [self.delegate expandingTextView:self didChangeHeight:(newHeight+ kTextInsetBottom)];
            }
        }
        
        if (newHeight >= self.maximumHeight) {
            // Enable vertical scrolling
            if(!self.internalTextView.scrollEnabled) {
                self.internalTextView.scrollEnabled = YES;
                [self.internalTextView flashScrollIndicators];
            }
        }
        else {
            // Disable vertical scrolling
            self.internalTextView.scrollEnabled = NO;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(expandingTextViewDidChange:)]) {
        [self.delegate expandingTextViewDidChange:self];
    }
}

- (void)growDidStop {
    if ([self.delegate respondsToSelector:@selector(expandingTextView:didChangeHeight:)]) {
        [self.delegate expandingTextView:self didChangeHeight:self.frame.size.height];
    }
}

- (BOOL)resignFirstResponder {
    [super resignFirstResponder];
    return [self.internalTextView resignFirstResponder];
}


#pragma mark UITextView properties Methods

- (void)setText:(NSString *)atext {
    self.internalTextView.text = atext;
    [self performSelector:@selector(textViewDidChange:) withObject:self.internalTextView];
}

- (NSString *)text {
    return self.internalTextView.text;
}

- (void)setFont:(UIFont *)afont {
    self.internalTextView.font= afont;
    [self setMaximumNumberOfLines:self.maximumNumberOfLines];
    [self setMinimumNumberOfLines:self.minimumNumberOfLines];
    
    self.placeholderLabel.font = afont;
}

- (UIFont *)font {
    return self.internalTextView.font;
}

- (void)setTextColor:(UIColor *)color {
    self.internalTextView.textColor = color;
}

- (UIColor *)textColor {
    return self.internalTextView.textColor;
}

- (void)setTextAlignment:(NSTextAlignment)aligment {
    self.internalTextView.textAlignment = aligment;
}

- (NSTextAlignment)textAlignment {
    return self.internalTextView.textAlignment;
}

- (void)setSelectedRange:(NSRange)range {
    self.internalTextView.selectedRange = range;
}

- (NSRange)selectedRange {
    return self.internalTextView.selectedRange;
}

- (void)setEditable:(BOOL)beditable {
    self.internalTextView.editable = beditable;
}

- (BOOL)isEditable {
    return self.internalTextView.editable;
}

- (void)setReturnKeyType:(UIReturnKeyType)keyType {
    self.internalTextView.returnKeyType = keyType;
}

- (UIReturnKeyType)returnKeyType {
    return self.internalTextView.returnKeyType;
}

- (void)setDataDetectorTypes:(UIDataDetectorTypes)datadetector {
    self.internalTextView.dataDetectorTypes = datadetector;
}

- (UIDataDetectorTypes)dataDetectorTypes {
    return self.internalTextView.dataDetectorTypes;
}

- (BOOL)hasText {
    return [self.internalTextView hasText];
}

- (void)scrollRangeToVisible:(NSRange)range {
    [self.internalTextView scrollRangeToVisible:range];
}


#pragma mark - UITextViewDelegate Methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(expandingTextViewShouldBeginEditing:)]) {
        return [self.delegate expandingTextViewShouldBeginEditing:self];
    }
    else {
        return YES;
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(expandingTextViewShouldEndEditing:)])  {
        return [self.delegate expandingTextViewShouldEndEditing:self];
    }
    else {
        return YES;
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(expandingTextViewDidBeginEditing:)]) {
        [self.delegate expandingTextViewDidBeginEditing:self];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(expandingTextViewDidEndEditing:)]) {
        [self.delegate expandingTextViewDidEndEditing:self];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)atext {
    if (range.location == 0 && [atext isEqualToString:@" "]) {
        return NO;
    }
    
    if(![textView hasText] && [atext isEqualToString:@""]) {
        return NO;
    }
    
    if ([atext isEqualToString:@"\n"]) {
        if ([self.delegate respondsToSelector:@selector(expandingTextViewShouldReturn:)]) {
            if (![self.delegate performSelector:@selector(expandingTextViewShouldReturn:) withObject:self]) {
                return YES;
            }
            else {
                [textView resignFirstResponder];
                return NO;
            }
        }
    }
    if (textView.text.length + atext.length > kTextLength) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(expandingTextViewDidChangeSelection:)]) {
        [self.delegate expandingTextViewDidChangeSelection:self];
    }
}

@end
