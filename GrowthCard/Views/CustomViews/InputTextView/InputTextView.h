//
//  InputTextView.h
//  GrowthCard
//
//  Created by Arvind Singh on 14/04/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpandingTextView.h"

@protocol InputTextViewDelegate;

@interface InputTextView : UIView

@property (strong, nonatomic) ExpandingTextView *textView;
@property (assign, nonatomic) id<InputTextViewDelegate> inputDelegate;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol InputTextViewDelegate <ExpandingTextViewDelegate>

@optional

- (void)inputButtonDidPressed:(NSString *)inputText;

@end