//
//  InputTextView.m
//  GrowthCard
//
//  Created by Arvind Singh on 14/04/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "InputTextView.h"

@interface InputTextView () <ExpandingTextViewDelegate>

@end

@implementation InputTextView

#pragma mark - UIControl Methods

- (void)inputButtonPressed:(id)sender {
    if ([self.inputDelegate respondsToSelector:@selector(inputButtonPressed:)]) {
        [self.inputDelegate inputButtonDidPressed:self.textView.text];
    }
    
    // Remove the keyboard and clear the text
    [self.textView resignFirstResponder];
    [self.textView clearText];
}


#pragma mark - Init Methods

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self setupView:NSLocalizedString(@"Send", @"Send") frame:frame];
    }
    return self;
}

- (id)init {
    if ((self = [super init])) {
        [self setupView:NSLocalizedString(@"Send", @"Send") frame:CGRectZero];
    }
    return self;
}

- (void)setupView:(NSString *)buttonLabel frame:(CGRect)frame {
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
    self.tintColor = [UIColor lightGrayColor];
    
    self.textView = [[ExpandingTextView alloc] initWithFrame:CGRectMake(5.0f, 10.0f, self.bounds.size.width - 60.0f, 50)];
    
    self.textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(4.0f, 0.0f, 10.0f, 0.0f);
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    self.textView.delegate = self;
    self.textView.returnKeyType = UIReturnKeySend;
    self.textView.maximumNumberOfLines = 3.0;
    self.textView.font = [UIFont regularApplicationFontOfSize:13.0f];
    self.textView.textColor = [UIColor colorWithHexString:@"8b9297"];
    [self addSubview:self.textView];
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(TScreenWidth*0.834375, 10, 46, 30)];
    [button addTarget:self action:@selector(sendButtonTaped) forControlEvents:    UIControlEventTouchUpInside];
    [button setTitle:@"Send" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont applicationMediumFontOfSize:13];
    [button setTitleColor:[UIColor colorWithHexString:@"3b3e41"] forState:UIControlStateNormal];
    [self addSubview:button];
}

-(void)sendButtonTaped {
    
    if ([self.inputDelegate respondsToSelector:@selector(expandingTextViewShouldReturn:)]) {
        [self.inputDelegate expandingTextViewShouldReturn:self.textView];
    }
}


#pragma mark - ExpandingTextViewDelegate Methods

- (void)expandingTextView:(ExpandingTextView *)expandingTextView willChangeHeight:(float)height {
    /* Adjust the height of the toolbar when the input component expands */
    float diff = (self.textView.frame.size.height - height);
    CGRect r = self.frame;
    r.origin.y += diff;
    r.size.height -= diff;
    self.frame = r;
    
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        [self.inputDelegate expandingTextView:expandingTextView willChangeHeight:height];
    }
}

- (void)expandingTextViewDidChange:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:@selector(expandingTextViewDidChange:)]) {
        [self.inputDelegate expandingTextViewDidChange:expandingTextView];
    }
}

- (BOOL)expandingTextViewShouldReturn:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        return [self.inputDelegate expandingTextViewShouldReturn:expandingTextView];
    }
    
    return YES;
}

- (BOOL)expandingTextViewShouldBeginEditing:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        return [self.inputDelegate expandingTextViewShouldBeginEditing:expandingTextView];
    }
    return YES;
}

- (BOOL)expandingTextViewShouldEndEditing:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        return [self.inputDelegate expandingTextViewShouldEndEditing:expandingTextView];
    }
    return YES;
}

- (void)expandingTextViewDidBeginEditing:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        [self.inputDelegate expandingTextViewDidBeginEditing:expandingTextView];
    }
}

- (void)expandingTextViewDidEndEditing:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        [self.inputDelegate expandingTextViewDidEndEditing:expandingTextView];
    }
}

- (BOOL)expandingTextView:(ExpandingTextView *)expandingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        return [self.inputDelegate expandingTextView:expandingTextView shouldChangeTextInRange:range replacementText:text];
    }
    return YES;
}

- (void)expandingTextView:(ExpandingTextView *)expandingTextView didChangeHeight:(float)height {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        [self.inputDelegate expandingTextView:expandingTextView didChangeHeight:height];
    }
}

- (void)expandingTextViewDidChangeSelection:(ExpandingTextView *)expandingTextView {
    if ([self.inputDelegate respondsToSelector:_cmd]) {
        [self.inputDelegate expandingTextViewDidChangeSelection:expandingTextView];
    }
}

@end
