//
//  ExpandingTextView.h
//  GrowthCard
//
//  Created by Arvind Singh on 14/04/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpandingTextViewInternal : UITextView

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ExpandingTextViewDelegate;

@interface ExpandingTextView : UIView  <UITextViewDelegate>

@property (strong, nonatomic) UITextView *internalTextView;

@property (assign, nonatomic) NSUInteger maximumNumberOfLines;
@property (assign, nonatomic) NSUInteger minimumNumberOfLines;
@property (assign, nonatomic) BOOL animateHeightChange;

@property (assign, nonatomic) id<ExpandingTextViewDelegate> delegate;
@property (copy, nonatomic) NSString *text;
@property (copy, nonatomic) UIFont *font;
@property (copy, nonatomic) UIColor *textColor;
@property (assign, nonatomic) NSTextAlignment textAlignment;
@property (assign, nonatomic) NSRange selectedRange;
@property (assign, nonatomic, getter=isEditable) BOOL editable;
@property (assign, nonatomic) UIDataDetectorTypes dataDetectorTypes;
@property (assign, nonatomic) UIReturnKeyType returnKeyType;
//@property (strong, nonatomic) UIImageView *textViewBackgroundImage;
@property (copy, nonatomic) NSString *placeholder;


- (BOOL)hasText;
- (void)scrollRangeToVisible:(NSRange)range;
- (void)clearText;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol ExpandingTextViewDelegate <NSObject>

@optional

- (BOOL)expandingTextViewShouldBeginEditing:(ExpandingTextView *)expandingTextView;
- (BOOL)expandingTextViewShouldEndEditing:(ExpandingTextView *)expandingTextView;

- (void)expandingTextViewDidBeginEditing:(ExpandingTextView *)expandingTextView;
- (void)expandingTextViewDidEndEditing:(ExpandingTextView *)expandingTextView;

- (BOOL)expandingTextView:(ExpandingTextView *)expandingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
- (void)expandingTextViewDidChange:(ExpandingTextView *)expandingTextView;

- (void)expandingTextView:(ExpandingTextView *)expandingTextView willChangeHeight:(float)height;
- (void)expandingTextView:(ExpandingTextView *)expandingTextView didChangeHeight:(float)height;

- (void)expandingTextViewDidChangeSelection:(ExpandingTextView *)expandingTextView;
- (BOOL)expandingTextViewShouldReturn:(ExpandingTextView *)expandingTextView;

@end