//
//  FullScreenImgView.h
//  GrowthCard
//
//  Created by Narender Kumar on 06/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenImgView : UIView

+ (instancetype)showImageView;
- (void)showImageWithUrl:(NSString *)url AndTitle:(NSString *)tilte AndMessage:(NSString *)msg;

@end
