//
//  FullScreenImgView.m
//  GrowthCard
//
//  Created by Narender Kumar on 06/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "FullScreenImgView.h"
#import "UCZProgressView.h"

@interface FullScreenImgView() {
    float _progress;
}

@property (nonatomic) IBOutlet UCZProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *mgsTitle;
@property (weak, nonatomic) IBOutlet UILabel *msgDesc;
@property (weak, nonatomic) NSString *imgUrl;

@end


@implementation FullScreenImgView
 
+ (instancetype)showImageView {
    FullScreenImgView *header = (FullScreenImgView *)[UIView viewFromXib:@"FullScreenImgView" classname:[FullScreenImgView class] owner:self];
    return header;
}

#pragma mark -
#pragma mark Public Method
- (void)showImageWithUrl:(NSString *)url AndTitle:(NSString *)tilte AndMessage:(NSString *)msg {
    _progress = 1;
    self.imgUrl = url;
    self.mgsTitle.text = tilte;
    self.msgDesc.text = msg;
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:self.imgView.image];
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCheck) userInfo:nil repeats:YES];
    self.progressView.showsText = YES;
    self.progressView.tintColor = [UIColor whiteColor];
    self.progressView.backgroundColor=[UIColor clearColor];
    self.progressView.progress =  _progress;
    self.progressView.textColor = [UIColor whiteColor];
    self.progressView.usesVibrancyEffect = NO;
    self.progressView.hidden=YES;
}


- (void)timerCheck {
    _progress-=0.1;
    self.progressView.hidden=NO;
    self.progressView.progress =_progress;
    if (_progress<0) {
        _progress=1;
        [self cloaseWindow];
    }
}

- (void)cloaseWindow {
    [_myTimer invalidate];
    _myTimer = nil;
    [self removeFromSuperview];
}


#pragma mark -
#pragma mark Share
- (void)shareAppWithUrlStr:(NSString *)imgURL AndMsg:(NSString *)msg AndImage:(UIImage *)img {
    NSString *textToShare = msg;
    NSURL *myWebsite = [NSURL URLWithString:imgURL];
    
    NSArray *objectsToShare = nil;
    if(img) {
        objectsToShare = @[kShareMessage, img];
    }
    else {
        objectsToShare = @[kShareMessage, textToShare, myWebsite];
    }
  
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [activityVC setValue:kAppName forKey:@"subject"];
    [[(UINavigationController *)appDelegate.window.rootViewController topViewController] presentViewController:activityVC animated:YES completion:^{
    }];    
}

#pragma mark -
#pragma mark IBAction
- (IBAction)shareBtnClicked:(id)sender {
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:self.imgUrl]];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            // WARNING: is the cell still using the same data by this point??
           UIImage* img = [UIImage imageWithData: data];
            if(img) {
                [self shareAppWithUrlStr:[NSString stringWithFormat:@"%@",self.imgUrl] AndMsg:self.msgDesc.text AndImage:img];
            }
            else {
                [self shareAppWithUrlStr:[NSString stringWithFormat:@"%@",self.imgUrl] AndMsg:self.msgDesc.text AndImage:nil];
            }
        });
    });
}

- (IBAction)closeBtnClicked : (id)sender {
    [self cloaseWindow];
}

@end
