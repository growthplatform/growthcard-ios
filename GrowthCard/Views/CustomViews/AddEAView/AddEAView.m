//
//  AddEAView.m
//  GrowthCard
//
//  Created by Narender Kumar on 08/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "AddEAView.h"

#define MAX_LENGTH 140

@interface AddEAView()<UITextViewDelegate> {
    
}
@property (weak, nonatomic) IBOutlet UIView *backBgView;
@property (weak, nonatomic) IBOutlet UILabel *limitLbl;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property (weak, nonatomic) IBOutlet UILabel *plceholderLbl;

@end


@implementation AddEAView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)eaView {
    AddEAView *header = (AddEAView *)[UIView viewFromXib:@"AddEAView" classname:[AddEAView class] owner:self];
    header.frame=[UIScreen mainScreen].bounds;
    [header tapGestureForRemoveKeyboard];
    [header.txtView becomeFirstResponder];
    header.txtView.text = @"";
    header.plceholderLbl.hidden = NO;
    return header;
}

#pragma mark -
#pragma mark Private Method
- (void)tapGestureForRemoveKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [_backBgView addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self endEditing:YES];
    [self removeFromSuperview];
}

#pragma mark - 
#pragma mark IBAction
- (IBAction)addBtnClicked:(id)sender {
    if(_txtView.text.length) {
        if (_delegate && [_delegate respondsToSelector:@selector(didAddEA:)]) {
            [_delegate didAddEA:_txtView.text];
            [self removeFromSuperview];
        }
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:kAppName
                                  message:kAddEA
                                  delegate:self
                                  cancelButtonTitle:kOK
                                  otherButtonTitles:nil, nil];
        
        [alertView show];
    }
}

#pragma mark - 
#pragma mark UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    //Compare backSpace....
    if(textView.text.length >= MAX_LENGTH && ![text isEqualToString:@""]) {
        return FALSE;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    if(textView.text.length > MAX_LENGTH) {
        textView.text = [textView.text substringToIndex:MAX_LENGTH];
    }
    // text changed..
    _limitLbl.text = [NSString stringWithFormat:@"%i Left", (int)MAX_LENGTH - (int)textView.text.length];
    _plceholderLbl.hidden = textView.text.length;
}

@end
