//
//  AddEAView.h
//  GrowthCard
//
//  Created by Narender Kumar on 08/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol AddEAViewDelegate <NSObject>

@optional
- (void)didAddEA:(NSString *)str;

@end


@interface AddEAView : UIView
@property (nonatomic, assign) id <AddEAViewDelegate> delegate;
+ (instancetype)eaView;

@end

