//
//  BannerAlert.m
//  GrowthCard
//
//  Created by Prakash Raj on 23/07/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "BannerAlert.h"
#import "UILabel+Extra.h"

@interface BannerAlert () {
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UIImageView *imgView;
    __weak IBOutlet UILabel *titleLabel;
}
- (IBAction)closeClickd:(id)sender;

@end


@implementation BannerAlert

// @method : to return shared instance.
+ (instancetype)sharedBaner {
    static BannerAlert *shBaner = nil;
    @synchronized (self) {
        if (!shBaner) {
            shBaner = [[BannerAlert alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 55)];
            shBaner.appeared = NO;
        }
    }
    return shBaner;
}

+ (void)showOnView:(UIView *)aView byReducingView:(UIView *)rView atY:(CGFloat)yy
     withbackColor:(UIColor *)clr andMessage:(NSString *)message textColor:(UIColor *)tClr name:(NSString *)name image:(UIImage *)image sendBackToViews:(NSArray *)views {
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [[BannerAlert sharedBaner] setAppeared:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kBannerShown object:nil userInfo:nil];
    
    if (!name) name = @"";
    if (!message) message = @"";
    
    BannerAlert *banner = [BannerAlert sharedBaner];
    [NSObject cancelPreviousPerformRequestsWithTarget:banner];
    [banner refreshBackColor:clr andMessage:message textColor:tClr name:name image:image];
    [banner performSelector:@selector(hideAlertByExpendingView:) withObject:rView afterDelay:kAlertHideInterval];
    
    // alert view is already is in same view..
    if( [banner superview] == aView) return;     // do nothing.
    
    // alert view is added anywhere else. remove it.
    if([banner superview] != nil) [banner removeFromSuperview];
    
    // add alert on desired view
    [aView addSubview:banner];
    
    for(UIView *av in views)
        [[av superview] bringSubviewToFront:av];
    
    // set frame less to make animation from top.
    CGRect frame = banner.frame;
    frame.origin.y = yy - frame.size.height;
    banner.frame = frame;
    
    // set desired type, message, frame..
    frame.origin.y = yy;
    
    // reset r view frame.
    CGRect rFrame = rView.frame;
    if(rView && frame.origin.y+frame.size.height>rFrame.origin.y) {
        CGFloat margine = frame.size.height;
        rFrame.origin.y = frame.origin.y+frame.size.height;
        rFrame.size.height -= margine;
    }
    
    // set frame with animation.
    [UIView animateWithDuration:0.4 animations:^{
        banner.frame = frame;
        if(rView) rView.frame = rFrame;
    } completion:nil];
}

+ (void)showOnView:(UIView *)vv WithName:(NSString *)name text:(NSString *)text image:(UIImage *)image {
    [BannerAlert showOnView:vv byReducingView:nil atY:0 withbackColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.7] andMessage:text textColor:nil name:name image:image sendBackToViews:nil];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"BannerAlert" owner:self options:nil];
        UIView *tv = (UIView *)[nibs objectAtIndex:0];
        [self addSubview:tv];
        tv.center = self.center;
        tv.frame = self.bounds;
        // tv.backgroundColor = [UIColor clearColor];
        tv.alpha = .6;
        
        imgView.layer.cornerRadius = imgView.frame.size.height/2.0;
    }
    return self;
}

- (void)refreshBackColor:(UIColor *)clr andMessage:(NSString *)msg textColor:(UIColor *)tClr name:(NSString *)name image:(UIImage *)image {
    // color update.
    if(clr) self.backgroundColor = clr;
    if(tClr) titleLabel.textColor = tClr;
    
    // content update.
    titleLabel.text = msg;
    nameLabel.text = name;
    imgView.image = image;
}

- (void)hideAlertByExpendingView:(UIView *)rView {
    // BannerAlert *baner = [BannerAlert sharedBaner];
    // set frame less to make animation from top.
    CGRect frame = self.frame;
    frame.origin.y -= frame.size.height;
    
    // reset r view frame.
    CGRect rFrame;
    if(rView) {
        rFrame = rView.frame;
        CGFloat margine = frame.size.height;
        rFrame.origin.y -= margine;
        rFrame.size.height += margine;
    }
    
    [[BannerAlert sharedBaner] setAppeared:NO];
    // set frame with animation.
    [UIView animateWithDuration:0.4 animations:^{
        self.frame = frame;
        if(rView) rView.frame = rFrame;
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [[NSNotificationCenter defaultCenter] postNotificationName:kBannerShown object:nil userInfo:nil];
    }];
}


#pragma mark - Controls Events
- (IBAction)closeClickd:(id)sender {
    [self hideAlertByExpendingView:nil];
}

@end
