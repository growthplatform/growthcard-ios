//
//  BannerAlert.h
//  GrowthCard
//
//  Created by Prakash Raj on 23/07/14.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kAlertHideInterval 5.0

// device tracking
static NSString *kBannerShown     = @"kBannerAppear";

@interface BannerAlert : UIView
@property (nonatomic, assign) BOOL appeared;

// @method : to return shared instance.
+ (instancetype)sharedBaner;

+ (void)showOnView:(UIView *)aView byReducingView:(UIView *)rView atY:(CGFloat)yy
     withbackColor:(UIColor *)clr andMessage:(NSString *)message textColor:(UIColor *)tClr name:(NSString *)name image:(UIImage *)image sendBackToViews:(NSArray *)views;
+ (void)showOnView:(UIView *)vv WithName:(NSString *)name text:(NSString *)text
             image:(UIImage *)image;

- (void)refreshBackColor:(UIColor *)clr andMessage:(NSString *)msg textColor:(UIColor *)tClr name:(NSString *)name image:(UIImage *)image;
- (void)hideAlertByExpendingView:(UIView *)rView;

@end
