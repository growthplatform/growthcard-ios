//
//  InputAccessoryView.h
//  GrowthCard
//
//  Created by Abhishek Tripathi on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputAccessoryView : UIView

- (void)addTarget:(id)target doneButtonAction:(SEL)donePressed withTitle:(NSString *)title;

@end
