//
//  InputAccessoryView.m
//  GrowthCard
//
//  Created by Abhishek Tripathi on 14/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "InputAccessoryView.h"

@interface InputAccessoryView ()

@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation InputAccessoryView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [_doneButton setTitle:NSLocalizedString(@"SIGN UP",@"SIGN UP") forState:UIControlStateNormal];
}

- (void)addTarget:(id)target doneButtonAction:(SEL)donePressed withTitle:(NSString *)title {
    self.frame = CGRectMake(0, 0, TScreenWidth, TScreenHeight*0.08695652174);
    [self.doneButton addTarget:target action:donePressed forControlEvents:UIControlEventTouchUpInside];
    [self.doneButton setTitle:title forState:UIControlStateNormal];
}


@end
