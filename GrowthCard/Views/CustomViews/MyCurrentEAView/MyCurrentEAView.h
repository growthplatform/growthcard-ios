//
//  MyCurrentEAView.h
//  GrowthCard
//
//  Created by Pawan Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCurrentEAView : UIView {
    
}
@property (weak, nonatomic) IBOutlet FRHyperLabel *eaDescLbl;

+ (instancetype)currentView;
- (void)setUserData:(User *)user;
- (void)setUserDataWithEA:(DashBoardFeed *)user;
- (CGSize)refreshSize;
@end
