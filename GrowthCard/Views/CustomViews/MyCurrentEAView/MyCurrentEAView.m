//
//  MyCurrentEAView.m
//  GrowthCard
//
//  Created by Pawan Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "MyCurrentEAView.h"

@interface MyCurrentEAView() {
}

@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *designationLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaTimeLbl;
@property (weak, nonatomic) IBOutlet UIView *graphView;

@end


@implementation MyCurrentEAView

+ (instancetype)currentView {
    MyCurrentEAView *header = (MyCurrentEAView *)[UIView viewFromXib:@"MyCurrentEAView" classname:[MyCurrentEAView class] owner:self];
    header.userImgView.image = [UIImage imageNamed:@"profile_top"];
    [header.userImgView roundCorner:header.userImgView.frame.size.width/2 border:0.0 borderColor:nil];
    
    header.nameLbl.text = @"No Name";;
    header.designationLbl.text = @"No desigation";
    header.eaTitleLbl.text = @"No EA";
    header.eaDescLbl.text = @"No EA Detail";
    header.eaTimeLbl.text=@"No Time";
    return header;
}

#pragma mark -
#pragma mark Public Methods
- (void)setUserData:(User *)user {
    [self.eaDescLbl setFrameAsTextStickToWidth:self.eaDescLbl.frame.size.width];
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _nameLbl.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    _designationLbl.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    _eaDescLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionDescription];
     _eaTimeLbl.text=[NSDate getHeaderDateStrFromString:user.startTimeStamp];
    [APPManager addChart:user.graphDetails AndView:self.graphView];
}

- (void)setUserDataWithEA:(DashBoardFeed *)info {
    if(info ) {
        [_userImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",info.user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
        _nameLbl.text = [NSString stringWithFormat:@"%@ %@",info.user.userFName, info.user.userLName];
        _designationLbl.text = [NSString stringWithFormat:@"%@",info.user.userDesignationTitle];
        _eaTitleLbl.text = [NSString stringWithFormat:@"%@",info.user.userEffectiveActionTitle];
        [_eaDescLbl clearActionDictionary];
        _eaDescLbl.text = [NSString stringWithFormat:@"%@",info.user.userEffectiveActionDescription];
        _eaTimeLbl.text=[NSDate getHeaderDateStrFromString:info.startTimeStamp];
        // Current EA (On EA tap in menu)
        //[APPManager addChartForTopView:info.user.graphDetails AndView:self.graphView];
        [APPManager addChart:info.user.graphDetails AndView:self.graphView];
    }
    else {
        CGRect rect=self.graphView.bounds;
        rect.size.width=rect.size.width-10;
        UIImageView *defaultPlaceholder=[[UIImageView alloc] initWithFrame:rect];
        defaultPlaceholder.image=[UIImage imageNamed:@"Graph"];
        defaultPlaceholder.contentMode = UIViewContentModeScaleToFill;
        [self.graphView addSubview:defaultPlaceholder];
        UILabel *lbl = [[UILabel alloc] initWithFrame:rect];
        lbl.textColor =[UIColor colorWithRed:0.0784 green:0.0784 blue:0.0784 alpha:1.0];
        // lbl.text=@"No data";
        [lbl setFont:[UIFont boldSystemFontOfSize:16]];//country name
        lbl.textAlignment = NSTextAlignmentCenter;
        [self.graphView addSubview:lbl];
    }
  }

-(CGSize)refreshSize {
    int viewHight=208;
    int defaultLabelHight=22;
    int labelHight=[self.eaDescLbl  heightForLabel:self.eaDescLbl.text];
    int diff=labelHight-defaultLabelHight;
    CGRect rect=self.bounds;
    rect.size.height=viewHight+diff;
    [self setBounds:rect];
    return self.frame.size;
}

@end
