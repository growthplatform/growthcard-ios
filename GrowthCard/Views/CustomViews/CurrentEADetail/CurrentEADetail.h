//
//  CurrentEADetail.h
//  GrowthCard
//
//  Created by Narender Kumar on 16/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentEADetail : UIView

@property (nonatomic, weak)  UIViewController *delegateView;

+ (instancetype)currentEaView;
- (void)setUserData:(User *)user;
- (void)setUserDataForBasicDetail:(User *)user;
- (void)setUserDataWithEaDetails:(DashBoardFeed *)dashBoard WithUser:(User *)user;

@end
