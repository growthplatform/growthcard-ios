//
//  CurrentEADetail.m
//  GrowthCard
//
//  Created by Narender Kumar on 16/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "CurrentEADetail.h"
#import "EACurrentVC.h"
#import "SlideNavigationController.h"

@interface CurrentEADetail()<UIGestureRecognizerDelegate> {
    UITapGestureRecognizer *singleFingerTap;
    User *currentEAUser;
}

@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *designationLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UIView *graphView;

@end


@implementation CurrentEADetail

+ (instancetype)currentEaView {
    CurrentEADetail *header = (CurrentEADetail *)[UIView viewFromXib:@"CurrentEADetail" classname:[CurrentEADetail class] owner:self];
    header.userImgView.image = [UIImage imageNamed:@"profile_top"];
    header.eaTitleLbl.text = @"";
    header.eaTitleLbl.hidden = YES;
    header.userImgView.hidden = YES;
    header.nameLbl.hidden = YES;
    header.designationLbl.hidden = YES;
    [header.userImgView roundCorner:header.userImgView.frame.size.width/2 border:0.0 borderColor:nil];
    return header;
}

#pragma mark -
#pragma mark Public Method
- (void)setUserData:(User *)user {
    [self setBackgroundColor:[UIColor colorWithRed:.89 green:.90 blue:.90 alpha:1]];
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _nameLbl.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    _designationLbl.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    _eaTitleLbl.numberOfLines = 2;
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    _eaTitleLbl.hidden = NO;
    _userImgView.hidden = NO;
    _nameLbl.hidden = NO;
    _designationLbl.hidden = NO;
    [APPManager addChart:user.graphDetails AndView:self.graphView];
    if(singleFingerTap) {
        [self removeGestureRecognizer:singleFingerTap];
        singleFingerTap = nil;
    }
    singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(currentEa)];
    [singleFingerTap setDelegate:self];
    currentEAUser = user;
    [self addGestureRecognizer:singleFingerTap];
    self.userInteractionEnabled=YES;
    [self setNeedsUpdateConstraints];
}

- (void)setUserDataWithEaDetails:(DashBoardFeed *)dashBoard WithUser:(User *)user {
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _nameLbl.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    _designationLbl.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",dashBoard.title];
    if ((user.userRole.integerValue == UserRoleSubordinate) && (!(user.userEffectiveActionId.integerValue == 0) || (user.userEffectiveActionId != nil))) {
        _eaTitleLbl.hidden = NO;
        _userImgView.hidden = NO;
        _nameLbl.hidden = NO;
        _designationLbl.hidden = NO;
    }
    [APPManager addChart:dashBoard.graphDetails AndView:self.graphView];
}

- (void)setUserDataForBasicDetail:(User *)user {
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _nameLbl.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    if(user.userDesignationTitle.length < 1) {
        _designationLbl.text = @"";
    } else {
        _designationLbl.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    }
    if(user.userEffectiveActionTitle.length < 1) {
        _eaTitleLbl.text = @"";
    } else {
        _eaTitleLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    }
    _eaTitleLbl.hidden = NO;
    _userImgView.hidden = NO;
    _nameLbl.hidden = NO;
    _designationLbl.hidden = NO;
    currentEAUser = user;
    [APPManager addChart:user.graphDetails AndView:self.graphView];
}

- (void)currentEa {
    // Feed > SelectProfilePic > EA Tap Bar(Profile/EA list) > select EA List > Select Current top EA Graph
    if(self.delegateView)
        [self openEaDetails];
}

- (void)openEaDetails {
    DashBoardFeed *eaData = [DashBoardFeed new];
    eaData.feedId = [NSString stringWithFormat:@"%@",currentEAUser.userEffectiveActionId];
    eaData.title = currentEAUser.userEffectiveActionTitle;
    eaData.desc = currentEAUser.userEffectiveActionDescription;
    eaData.graphDetails = currentEAUser.graphDetails;
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.selectedUser = currentEAUser;
    viewController.eaData = eaData;
    viewController.isPast = NO;
    UIViewController *vc=(UIViewController*)self.delegateView;
    [vc.navigationController pushViewController:viewController animated:YES];
}

@end
