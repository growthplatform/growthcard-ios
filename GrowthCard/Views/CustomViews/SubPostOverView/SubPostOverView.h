//
//  SubPostOverView.h
//  GrowthCard
//
//  Created by Narender Kumar on 23/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SubPostOverViewDelegate <NSObject>

@optional
- (void)didMyTeamClicked;
- (void)didMyDepartmentClicked;
- (void)didAllClicked;

@end


@interface SubPostOverView : UIView

+ (instancetype)subPostOver;
@property (nonatomic, assign) id<SubPostOverViewDelegate> delegate;

@end
