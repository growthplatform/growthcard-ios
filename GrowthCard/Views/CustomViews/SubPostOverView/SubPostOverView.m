//
//  SubPostOverView.m
//  GrowthCard
//
//  Created by Narender Kumar on 23/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubPostOverView.h"

@interface SubPostOverView() {
    
}

@property (weak, nonatomic) IBOutlet UIButton *myTeamBtn;
@property (weak, nonatomic) IBOutlet UIButton *myDepartmentBtn;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;

@end


@implementation SubPostOverView

+ (instancetype)subPostOver {
    SubPostOverView *header = (SubPostOverView *)[UIView viewFromXib:@"SubPostOverView" classname:[SubPostOverView class] owner:self];
    header.frame=[UIScreen mainScreen].bounds;
    [header.myTeamBtn roundCorner:3 border:1 borderColor:[UIColor grayColor]];
    [header.myDepartmentBtn roundCorner:3 border:1 borderColor:[UIColor grayColor]];
    [header.allBtn roundCorner:3 border:1 borderColor:[UIColor grayColor]];
    return header;
}

#pragma mark -
#pragma mark IBAction
- (IBAction)myTeamClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didMyTeamClicked)]) {
        [_delegate didMyTeamClicked];
    }
    [self closeBtnClicked:sender];
}

- (IBAction)myDepartmentClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didMyDepartmentClicked)]) {
        [_delegate didMyDepartmentClicked];
    }
    [self closeBtnClicked:sender];
}

- (IBAction)allClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didAllClicked)]) {
        [_delegate didAllClicked];
    }
    [self closeBtnClicked:sender];
}

- (IBAction)closeBtnClicked:(id)sender {
    [self removeFromSuperview];
}

@end
