//
//  PostAccessoryView.m
//  GrowthCard
//
//  Created by Abhishek Tripathi on 23/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "PostAccessoryView.h"

@implementation PostAccessoryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)addTarget:(id)target doneButtonAction:(SEL)donePressed {
    self.frame = CGRectMake(0, 0, TScreenWidth, TScreenHeight*0.08695652174);
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.layer.shadowOpacity = .10f;
    self.layer.shadowPath = shadowPath.CGPath;
    [self.cameraButton addTarget:target action:donePressed forControlEvents:UIControlEventTouchUpInside];
}

@end
