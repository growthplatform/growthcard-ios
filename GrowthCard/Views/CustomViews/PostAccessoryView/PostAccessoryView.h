//
//  PostAccessoryView.h
//  GrowthCard
//
//  Created by Abhishek Tripathi on 23/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostAccessoryView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UILabel *inputLablel;

- (void)addTarget:(id)target doneButtonAction:(SEL)donePressed;

@end
