//
//  LatestAnnouncement.m
//  GrowthCard
//
//  Created by Pawan Kumar on 04/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "LatestAnnouncement.h"


@interface LatestAnnouncement() {
}

@end


@implementation LatestAnnouncement

+ (instancetype)view {
    LatestAnnouncement *header = (LatestAnnouncement *)[UIView viewFromXib:@"LatestAnnouncement" classname:[LatestAnnouncement class] owner:self];
    header.frame=[UIScreen mainScreen].bounds;
    return header;
}

- (IBAction)btnShowAllClick:(id)sender {
    AnnouncementsVC *viewController = (AnnouncementsVC *)[[UIStoryboard homeStoryboard] instantiateViewControllerWithClass:[AnnouncementsVC class]];
        viewController.isSubordinate = YES;
    [[SlideNavigationController sharedInstance] pushViewController:viewController animated:YES];
}

@end
