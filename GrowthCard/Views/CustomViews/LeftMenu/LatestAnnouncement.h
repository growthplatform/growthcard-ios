//
//  LatestAnnouncement.h
//  GrowthCard
//
//  Created by Pawan Kumar on 04/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LatestAnnouncement : UIView {
    
}

+ (instancetype)view;
@property (weak, nonatomic) IBOutlet UIButton *btnShowAll;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUser;
@property (weak, nonatomic) IBOutlet UILabel *userNameL;
@property (weak, nonatomic) IBOutlet UILabel *userDegination;
@property (weak, nonatomic) IBOutlet UILabel *userComment;
@property (weak, nonatomic) IBOutlet UILabel *userTime;

@end
