//
//  EAPopUp.h
//  GrowthCard
//
//  Created by Narender Kumar on 11/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EAPopUpDelegate <NSObject>
@optional
- (void)didClose:(NSString *)str;
@end


@interface EAPopUp : UIView

@property (nonatomic, assign) id <EAPopUpDelegate> delegate;
+ (instancetype)eaView;

@end
