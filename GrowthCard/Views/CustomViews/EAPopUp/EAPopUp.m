//
//  EAPopUp.m
//  GrowthCard
//
//  Created by Narender Kumar on 11/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EAPopUp.h"


@interface EAPopUp() {
}
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIView *profileBgView;

@end


@implementation EAPopUp

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)eaView {
    EAPopUp *header = (EAPopUp *)[UIView viewFromXib:@"EAPopUp" classname:[EAPopUp class] owner:self];
    User *user = [UserManager sharedManager].activeUser;
    header.frame=[UIScreen mainScreen].bounds;
    [header.userImgView roundCorner:header.userImgView.frame.size.width/2 border:0 borderColor:nil];
    header.nameLbl.text = [NSString stringWithFormat:@"%@ %@", user.userFName,user.userLName];
    [header.userImgView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [header.profileBgView roundCorner:5.0 border:1.0 borderColor:[UIColor grayColor]];
    header.eaTitleLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    return header;
}

- (IBAction)closeBtnClicked:(id)sender {
    [self removeFromSuperview];
}



@end
