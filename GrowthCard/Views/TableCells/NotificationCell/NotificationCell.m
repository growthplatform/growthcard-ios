//
//  NotificationCell.m
//
//  Created by Pawan Kumar on 02/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "NotificationCell.h"
#import "UILabel+Bold_Color.h"

@interface NotificationCell (){
    __weak IBOutlet NSLayoutConstraint *userName_leftConst;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgVirwCep;
@property (weak, nonatomic) IBOutlet UIImageView *imgVirwUnread;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end


@implementation NotificationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Public Method
- (void)SetCellData:(NotificationInfo*) notificationInfo {
    [self formatNotificationMsg:notificationInfo];
    self.lblTime.text=[NSDate getAgoFromString:notificationInfo.timeSring];
    [self.imgVirwCep sd_setImageWithURL:[NSURL URLWithString:notificationInfo.image] placeholderImage:[UIImage imageNamed:@"profileDefault"]];
    if (notificationInfo.readStatus.intValue==1) {
        [userName_leftConst setConstant:53];
        self.imgVirwUnread.hidden=YES;
    }
    else {
        [userName_leftConst setConstant:68];
        self.imgVirwUnread.hidden=NO;
    }
}

- (void)formatNotificationMsg:(NotificationInfo*) data {
    NSMutableString *strMsg=[NSMutableString new];
    [strMsg appendString:data.message];
    [strMsg appendString:@""];
    [strMsg appendString:data.message2];
    [strMsg appendString:@""];
    [strMsg appendString:data.message3];
    self.lblUserName.text=strMsg;
    [self.lblUserName boldAndColorSubstring:data.message forColor:[UIColor colorWithRed:0.2118 green:0.2235 blue:0.2392 alpha:1.0]];
    [self.lblUserName boldAndColorSubstring:data.message3 forColor:[UIColor colorWithRed:0.2196 green:0.6627 blue:0.9176 alpha:1.0]];
}

@end
