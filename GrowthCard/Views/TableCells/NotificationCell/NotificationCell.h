//
//  NotificationCell.h
//
//  Created by Pawan Kumar on 02/12/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
-(void)SetCellData:(NotificationInfo*) notificationInfo;

@end
