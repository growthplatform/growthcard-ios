//
//  MyCurrentEACell.m
//  GrowthCard
//
//  Created by Abhishek Tripathi on 31/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "MyCurrentEACell.h"

@interface MyCurrentEACell ()

@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *designationLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaTimeLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphBottemConstraint;
@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet UILabel *eaDescLbl;
@property (weak, nonatomic) IBOutlet UIButton *readMoreButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *readMoreButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLableHeight;
@property (assign, nonatomic) NSInteger numberOfLine;
@property (strong, nonatomic)  User *userInfo;

@end


@implementation MyCurrentEACell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

+ (MyCurrentEACell *)cell {
    MyCurrentEACell *cell = (MyCurrentEACell *)[UIView viewFromXib:@"MyCurrentEACell" classname:[MyCurrentEACell class] owner:self];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

#pragma mark - Public Method
- (void)setCellData:(User *)user withNumberOfline:(NSInteger )lines withTotalCount:(NSInteger )count {
    user.userEffectiveActionDescription = @"";
    self.userInfo = user;
    self.titleLableHeight.constant = [self getheightoftitlelable];
    self.eaDescLbl.numberOfLines = lines;
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",user.userImageUrl]] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _nameLbl.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    _designationLbl.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    _eaDescLbl.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionDescription];
    self.graphBottemConstraint.constant = 0;
    [self readMoreAndReadLessButtonPressed];
    [self checkRedMoreButton];
    [APPManager addChart:user.graphDetails AndView:self.graphView];
}

#pragma mark - Private Method
- (int)getNumberOfLineOfAText {
    NSString *str = [NSString stringWithFormat:@"%@",self.userInfo.userEffectiveActionDescription];
    CGRect frame = [str  boundingRectWithSize:CGSizeMake(self.eaDescLbl.frame.size.width,999)
                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes:@{NSFontAttributeName:self.eaDescLbl.font}
                                      context:nil];
    
    int numLines = frame.size.height / self.eaDescLbl.font.lineHeight;
    return numLines;
    
}

- (CGFloat)getheightoftitlelable {
    NSString *str = [NSString stringWithFormat:@"%@",self.userInfo.userEffectiveActionTitle];;
    CGRect frame = [str  boundingRectWithSize:CGSizeMake(self.eaTitleLbl.frame.size.width,999)
                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes:@{NSFontAttributeName:self.eaTitleLbl.font}
                                      context:nil];
    
    return frame.size.height + 5;
}

- (void)checkRedMoreButton {
    if ([self getNumberOfLineOfAText] > 4) {
        self.readMoreButton.hidden = NO;
        self.readMoreButtonConstraint.constant = 30;
    }
    else {
        self.readMoreButton.hidden = YES;
        self.readMoreButtonConstraint.constant = 10;
    }
}

- (void)readMoreAndReadLessButtonPressed {
    if ((int)self.numberOfLine  == 4) {
        [self.readMoreButton setTitle:@"Continue reading" forState:UIControlStateNormal];
    }
    else{
        [self.readMoreButton setTitle:@"Read less" forState:UIControlStateNormal];
    }
}

#pragma mark - Controls Events
- (IBAction)tapToContinueReadingButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(continueReadingButtonPressed)]) {
        [self.delegate continueReadingButtonPressed];
    }
}

- (IBAction)tapTouserProfileButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(userImageButtonPressed)]) {
        [self.delegate userImageButtonPressed];
    }
}

@end
