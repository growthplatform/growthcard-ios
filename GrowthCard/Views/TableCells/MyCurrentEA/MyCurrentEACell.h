//
//  MyCurrentEACell.h
//  GrowthCard
//
//  Created by Abhishek Tripathi on 31/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyCurrentEACellDelegate <NSObject>

- (void)userImageButtonPressed;
- (void)continueReadingButtonPressed;

@end


@interface MyCurrentEACell : UITableViewCell

@property (weak, nonatomic) id <MyCurrentEACellDelegate> delegate;
- (void)setCellData:(User *)user withNumberOfline:(NSInteger )lines withTotalCount:(NSInteger )count;
+ (MyCurrentEACell *)cell;

@end
