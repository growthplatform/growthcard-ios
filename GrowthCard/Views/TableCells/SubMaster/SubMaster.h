//
//  SubMaster.h
//  GrowthCard
//
//  Created by Narender Kumar on 20/05/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubMaster : UITableViewCell {
}

@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userTitle;
@property (weak, nonatomic) IBOutlet UILabel *userDecs;
@property (weak, nonatomic) IBOutlet UILabel *userDate;

@end
