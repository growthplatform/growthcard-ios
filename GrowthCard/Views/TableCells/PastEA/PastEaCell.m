//
//  PastEaCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 16/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "PastEaCell.h"
#import "NAUIViewWithBorders.h"

@interface PastEaCell () {
}
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *eaTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *eaDescLbl;
@property (weak, nonatomic) IBOutlet UIButton *readmoreBtn;
@property (weak, nonatomic) IBOutlet UILabel *startDateLbl;
@property (weak, nonatomic) IBOutlet UIImageView *heardImgView;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphHightConst;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *eiSummeryView;

@end


@implementation PastEaCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

+ (PastEaCell *)cell {
    PastEaCell *cell = (PastEaCell *)[UIView viewFromXib:@"PastEaCell" classname:[PastEaCell class] owner:self];
    [cell.bgView roundCorner:3.0 border:0 borderColor:nil];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Public Method
- (void)setData:(User *)info {
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",info.userEffectiveActionTitle];
    _eaDescLbl.text = [NSString stringWithFormat:@"%@",info.userEffectiveActionDescription];
    _startDateLbl.text = [NSDate getStartEndDateStrFromString:info.startTimeStamp AndEndDate:info.endTimeStamp];
    [APPManager addChart:info.graphDetails AndView:self.graphView];
}

- (void)setDataDashBoard:(DashBoardFeed *)info {
    self.eiSummeryView.borderWidthsAll = 1.0;
    self.eiSummeryView.borderColorTop = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
    _eaTitleLbl.text = [NSString stringWithFormat:@"%@",info.title];
    _eaDescLbl.text = [NSString stringWithFormat:@"%@",info.desc];
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)info.likesCount.integerValue] forState:UIControlStateNormal];
    _startDateLbl.text = [NSDate getStartEndDateStrFromString:info.startTimeStamp AndEndDate:info.endTimeStamp];
    if (info.graphDetails.count>0) {
        self.graphHightConst.constant=140;
    }
    else
        self.graphHightConst.constant=0;
    [APPManager addChart:info.graphDetails AndView:self.graphView];
}

#pragma mark - Control Event
- (IBAction)readMoreBtnClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didMoreClicked)]) {
        [_delegate didMoreClicked];
    }
}

- (IBAction)likeBtnClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didLikeClicked)]) {
        [_delegate didLikeClicked];
    }
}

@end