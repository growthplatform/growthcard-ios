//
//  PastEaCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 16/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PastEaCellDelegate <NSObject>

@optional
- (void)didMoreClicked;
- (void)didLikeClicked;

@end


@interface PastEaCell : UITableViewCell

@property (nonatomic, assign) id <PastEaCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *graphView;

- (void)setDataDashBoard:(DashBoardFeed *)info;
+ (PastEaCell *)cell;
- (void)setData:(User *)info;

@end
