//
//  AssignEACell.h
//  GrowthCard
//
//  Created by Narender Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AssignEACellDelegate <NSObject>

@optional
- (void)didSelectedBtnClicked:(int)idx;
@end

@interface AssignEACell : UITableViewCell
@property (nonatomic, assign) id <AssignEACellDelegate> delegate;
+ (AssignEACell *)cell;
- (void)showUserBasicDetails:(User *)info;
- (void)showUserBasicDetails:(User *)info WithShowButton:(BOOL)isShow And:(int)inx;
- (void)showBtnSelected :(BOOL)selected;
@end
