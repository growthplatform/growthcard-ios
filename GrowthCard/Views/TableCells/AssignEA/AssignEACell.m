//
//  AssignEACell.m
//  GrowthCard
//
//  Created by Narender Kumar on 01/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "AssignEACell.h"

@interface AssignEACell () {
    __weak IBOutlet UIImageView *imgView;
    __weak IBOutlet UILabel     *nameLbl;
    __weak IBOutlet UILabel     *profileTitleLbl;
    __weak IBOutlet UIButton    *selecteBtn;
    
    int index;
}
@end


@implementation AssignEACell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

+ (AssignEACell *)cell {
    AssignEACell *cell = (AssignEACell *)[UIView viewFromXib:@"AssignEACell" classname:[AssignEACell class] owner:self];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Public Method
- (void)showUserBasicDetails:(User *)info WithShowButton:(BOOL)isShow And:(int)inx {
    selecteBtn.hidden = !isShow;
    nameLbl.text    = [NSString stringWithFormat:@"%@ %@", info.userFName, info.userLName];
    profileTitleLbl.text = info.userDesignationTitle;
    [imgView roundCorner:imgView.frame.size.width/2 border:0 borderColor:nil];
    [imgView sd_setImageWithURL:[NSURL URLWithString:info.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    index = inx;
}

- (void)showBtnSelected :(BOOL)selected {
    selecteBtn.selected = selected;
}

- (void)showUserBasicDetails:(User *)info {
     User *user = [UserManager sharedManager].activeUser;
    if(info.userId.integerValue == user.userId.integerValue)
        nameLbl.text = @"Me";
    else
        nameLbl.text    = [NSString stringWithFormat:@"%@ %@", info.userFName, info.userLName];
    profileTitleLbl.text = info.userDesignationTitle;
    [imgView roundCorner:imgView.frame.size.width/2 border:0 borderColor:nil];
    [imgView sd_setImageWithURL:[NSURL URLWithString:info.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
}

#pragma mark - Controls Events
- (IBAction)selectBtnClicked:(UIButton *)sender {
    selecteBtn.selected = !sender.selected;
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedBtnClicked:)]) {
        [_delegate didSelectedBtnClicked:index];
    }
}


@end




