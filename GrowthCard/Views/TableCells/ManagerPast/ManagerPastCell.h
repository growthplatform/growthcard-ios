//
//  ManagerPastCell.h
//  GrowthCard
//
//  Created by Pawan Kumar on 18/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManagerPastCell : UITableViewCell

+ (ManagerPastCell *)cell;
- (void)setData:(DashBoardFeed *)info AndUser:(User *)user;

@end
