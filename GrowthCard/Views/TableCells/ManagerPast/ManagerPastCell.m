//
//  ManagerPastCell.m
//  GrowthCard
//
//  Created by Pawan Kumar on 18/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "ManagerPastCell.h"
#import "NAUIViewWithBorders.h"


@interface ManagerPastCell () {
}
@property (weak, nonatomic) IBOutlet UIView *uiUserInfo;
@property (weak, nonatomic) IBOutlet UIView *commentsView;
@property (weak, nonatomic) IBOutlet UIView *dateTimeView;
@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *eiSummeryView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userDeptLabel;
@property (weak, nonatomic) IBOutlet UILabel *eITitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eICommentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphHightConst;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end


@implementation ManagerPastCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (ManagerPastCell *)cell {
    ManagerPastCell *cell = (ManagerPastCell *)[UIView viewFromXib:@"ManagerPastCell" classname:[ManagerPastCell class] owner:self];
    [cell.bgView roundCorner:4.0 border:0.25 borderColor:[UIColor lightGrayColor]];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark -
#pragma mark Public Method
- (void)setData:(DashBoardFeed *)info AndUser:(User *)user {
    [_profileImgView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [_profileImgView roundCorner:_profileImgView.frame.size.width/2 border:0 borderColor:nil];
    _userNameLabel.text = [NSString stringWithFormat:@"%@ %@",user.userFName, user.userLName];
    _userDeptLabel.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    _eITitleLabel.text = [NSString stringWithFormat:@"%@",info.title];
    _eICommentsLabel.text = [NSString stringWithFormat:@"%@",info.desc];
    _dateLabel.text =  [NSDate getStartEndDateStrFromString:info.startTimeStamp AndEndDate:info.endTimeStamp];
     [APPManager addChart:info.graphDetails AndView:self.graphView];
    [self createWeekDetails:info];
}
 
#pragma mark -
#pragma mark Control Event
-(void) createWeekDetails:(DashBoardFeed *)useFeed {
    for (UIView *view in [self.eiSummeryView subviews]) {
        [view removeFromSuperview];
    }
    self.eiSummeryView.borderWidthsAll = 1.0;
    self.eiSummeryView.borderColorTop = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:0.8];
    float cellCount=3.0;
    float cellWidth=([[UIScreen mainScreen] applicationFrame].size.width-10)/cellCount;
    float x=0.0;
    NSLog(@"width %f",self.eiSummeryView.frame.size.width);
    NSLog(@"cellWidth %f",cellWidth);
    NSMutableArray *headers=[NSMutableArray new];
    [headers addObject:@"MIN"];
    [headers addObject:@"MAX"];
    [headers addObject:@"AVG"];
    NSMutableArray *values=[NSMutableArray new];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.min]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.max]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.avg]];
    for (int i=0; i<cellCount; i++) {
        UILabel *cellTitle=[[UILabel alloc] initWithFrame:CGRectMake(x, 13, cellWidth,20)];
        [cellTitle setFont:[UIFont fontWithName:kRubik_Bold size:9]];
        cellTitle.textColor = [UIColor colorWithRed:0.5137 green:0.5216 blue:0.5333 alpha:1.0];
        cellTitle.textAlignment = NSTextAlignmentCenter;
        cellTitle.text = headers[i];
        [self.eiSummeryView addSubview:cellTitle];
        UILabel *cellValue=[[UILabel alloc] initWithFrame:CGRectMake(x, 40, cellWidth,15)];
        [cellValue setFont:[UIFont fontWithName:kRubik_Regular size:16]];
        cellValue.textColor = [UIColor colorWithRed:0.3137 green:0.3294 blue:0.3686 alpha:1.0];
        cellValue.textAlignment = NSTextAlignmentCenter;
        cellValue.text = values[i];
        [self.eiSummeryView addSubview:cellValue];
        if (i!=cellCount-1) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(x+cellWidth-1, 0, 1, self.eiSummeryView.frame.size.height)];
            [self.eiSummeryView addSubview:view];
            view.backgroundColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:0.7];
        }
        //x+=cellWidth;
        x = x + cellWidth ;
    }
}


@end
