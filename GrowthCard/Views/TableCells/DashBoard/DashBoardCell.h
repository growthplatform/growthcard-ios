//
//  DashBoardCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 02/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DashBoardViewDelegate
@optional
- (void)reloadCell:(UITableViewCell *)cell;
@end

@interface DashBoardCell : UITableViewCell
@property (weak, nonatomic) id<DashBoardViewDelegate> delegate;
@property (strong, nonatomic) DashBoardFeed *useFeed;
-(void)resetFeed:(DashBoardFeed *)useFeed;
- (void)resetFeedWithUser:(User *)user;
+ (DashBoardCell *)cell;
@end
