//
//  DashBoardCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 02/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "DashBoardCell.h"
#import "UserInfoView.h"
#import "UILabel+Extra.h"
#import "ChartContainView.h"
#import "NAUIViewWithBorders.h"
#import "ViewOtherProfile.h"
#import "SubSegmentedVC.h"


@interface DashBoardCell () {
    IBOutlet __weak ChartContainView *chartV;
}

@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *middleVIew;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *buttomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *middleViewHight;//default hight 50
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartment;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphHightConstraint;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *graphView;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *baseView;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *topView;
@property (weak, nonatomic) User *feedUser;

@end


@implementation DashBoardCell

+ (DashBoardCell *)cell {
    DashBoardCell *cell = (DashBoardCell *)[UIView viewFromXib:@"DashBoardCell" classname:[DashBoardCell class] owner:self];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) layoutSubviews {
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Private Method
- (void)OpenUserProfile:(id)sender {
    NSLog(@"Open User Profile click");
    ManagerSegmentedVC  *viewController = (ManagerSegmentedVC *)[[UIStoryboard managerStoryboard] instantiateViewControllerWithClass:[ManagerSegmentedVC class]];
    viewController.selectedUser   = self.feedUser;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Public Method
- (void)resetFeed:(DashBoardFeed *)useFeed {
    [self.buttomView sizeToFit];
    self.feedUser = useFeed.user;
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    [self.topView addGestureRecognizer:singleFingerTap];
    self.topView.userInteractionEnabled=YES;
    self.buttomView.borderWidthsAll     = 1.0;
    self.buttomView.borderColorTop      = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
    self.baseView.layer.borderColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0].CGColor;
    self.baseView.layer.borderWidth = 1.0f;
    self.baseView.layer.cornerRadius = 10;
    self.baseView.layer.masksToBounds = YES;
    self.middleVIew.backgroundColor = [UIColor clearColor];
    self.buttomView.backgroundColor = [UIColor clearColor];
    self.middleVIew.backgroundColor = [UIColor clearColor];
    self.graphView.backgroundColor = [UIColor clearColor];
    [self updateConstraints];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:useFeed.user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    self.imgView.layer.cornerRadius = 17;
    self.imgView.layer.masksToBounds=YES;
    self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",[useFeed.user.userFName pascalCased], [useFeed.user.userLName pascalCased]];
    self.lblTitle.text = useFeed.title;
    if (useFeed.description.length>0)
        self.lblDesc.text = useFeed.desc;
    else
        self.lblDesc.text = @"";
    [APPManager addChart:useFeed.graphDetails AndView:self.graphView];
}

- (void)resetFeedWithUser:(User *)user {
    [self.buttomView sizeToFit];
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    [self.topView addGestureRecognizer:singleFingerTap];
    self.topView.userInteractionEnabled=YES;
    self.feedUser = user;
    self.buttomView.borderWidthsAll = 1.0;
    self.buttomView.borderColorTop = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:0.4];
    self.baseView.layer.borderColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0].CGColor;
    self.baseView.layer.borderWidth = 0.5f;
    self.baseView.layer.cornerRadius = 10-6;
    self.baseView.layer.masksToBounds = YES;
    self.middleVIew.backgroundColor = [UIColor clearColor];
    self.buttomView.backgroundColor = [UIColor clearColor];
    self.middleVIew.backgroundColor = [UIColor clearColor];
    self.graphView.backgroundColor = [UIColor clearColor];
    [self updateConstraints];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    self.imgView.layer.cornerRadius = 17;
    self.imgView.layer.masksToBounds=YES;
    User *currentUser = [UserManager sharedManager].activeUser;
    if(currentUser.userId.integerValue == user.userId.integerValue)
        self.lblUserName.text   = @"Me";
    else
        self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",[user.userFName pascalCased], [user.userLName pascalCased]];
    self.lblDepartment.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    self.lblTitle.text = user.userEffectiveActionTitle;
    if (user.userEffectiveActionDescription.length>0)
        self.lblDesc.text = user.userEffectiveActionDescription;
    else
        self.lblDesc.text = @"";
    [self createWeekDetails:user];
    [APPManager addChart:user.graphDetails AndView:self.graphView];
}

-(void) createWeekDetails:(User *)useFeed {
    for (UIView *view in [self.buttomView subviews]) {
        [view removeFromSuperview];
    }
    int cellCount=4;
    float cellWidth=([[UIScreen mainScreen] applicationFrame].size.width-30)/cellCount;
    int x=0;
    NSLog(@"width %f",self.buttomView.frame.size.width);
    NSLog(@"cellWidth %f",cellWidth);
    NSMutableArray *headers=[NSMutableArray new];
    [headers addObject:@"MIN"];
    [headers addObject:@"MAX"];
    [headers addObject:@"AVG"];
    [headers addObject:@"TODAY"];
    NSMutableArray *values=[NSMutableArray new];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.min]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.max]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.avg]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.today]];
    for (int i=0; i<cellCount; i++) {
        UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(x, 10, cellWidth,20)];
        [cellTitle setFont:[UIFont fontWithName:kRubik_Bold size:11]];
        //cellTitle.textColor = [UIColor colorWithRed:0.5137 green:0.5216 blue:0.5333 alpha:1.0];
        cellTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:171.0/255.0 blue:173.0/255.0 alpha:1];

        cellTitle.textAlignment = NSTextAlignmentCenter;
        cellTitle.text = headers[i];
        [self.buttomView addSubview:cellTitle];
        UILabel *cellValue = [[UILabel alloc] initWithFrame:CGRectMake(x, 40-8, cellWidth,15)];
        [cellValue setFont:[UIFont fontWithName:kRubik_Regular size:16]];
        cellValue.textColor = [UIColor colorWithRed:0.3137 green:0.3294 blue:0.3686 alpha:1.0];
        cellValue.textAlignment = NSTextAlignmentCenter;
        cellValue.text = values[i];
        [self.buttomView addSubview:cellValue];
        if (i!=cellCount-1) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(x+cellWidth-1, 0, 1, self.buttomView.frame.size.height)];
            [self.buttomView addSubview:view];
            view.backgroundColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:0.27];
        }
        x+=cellWidth;
    }
}

@end
