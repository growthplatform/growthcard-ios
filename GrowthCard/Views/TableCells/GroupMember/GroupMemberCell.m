//
//  GroupMemberCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 25/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import "GroupMemberCell.h"

@interface GroupMemberCell(){
    __weak IBOutlet UILabel *lblMemberName;
    __weak IBOutlet UIImageView *imgMember;
}

@end


@implementation GroupMemberCell

- (void)awakeFromNib {
    // Initialization code
    [imgMember.layer setCornerRadius:imgMember.frame.size.height/2];
    [imgMember.layer setMasksToBounds:YES];
}

#pragma mark - Public Method
-(void)configureCellWithMemberInfo:(User *)memberInfo{
    [imgMember sd_setImageWithURL:[NSURL URLWithString:memberInfo.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_below"]];
    lblMemberName.text      = [NSString stringWithFormat:@"%@ %@",[memberInfo.userFName pascalCased], [memberInfo.userLName pascalCased]];
}

@end
