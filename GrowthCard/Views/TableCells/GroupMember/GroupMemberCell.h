//
//  GroupMemberCell.h
//  GrowthCard
//
//  Created by Prashant Gautam on 25/10/15.
//  Copyright © 2015 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupMemberCell : UICollectionViewCell
-(void)configureCellWithMemberInfo:(User *)memberInfo;
@end
