//
//  MyPastEACell.m
//  GrowthCard
//
//  Created by Pawan Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "MyPastEACell.h"
#import "NAUIViewWithBorders.h"


@interface MyPastEACell () {
    DashBoardFeed *dashBoardFeed;
}
@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *eiSummeryView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UILabel *eITitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eICommentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commntBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphHightConst;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *userDetailsLabel;
@property (weak, nonatomic) IBOutlet NAUIViewWithBorders *likesView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentedUserConstant;

@end


@implementation MyPastEACell

+ (MyPastEACell *)cell {
    MyPastEACell *cell = (MyPastEACell *)[UIView viewFromXib:@"MyPastEACell" classname:[MyPastEACell class] owner:self];
    [cell.bgView roundCorner:5.0 border:0 borderColor:nil];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        // Your initializations here
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Public Method
- (void)setData:(DashBoardFeed *)info AndUser:(User *)user {
    self.commentedUserConstant.constant=0;
    [_profileImgView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [_profileImgView roundCorner:_profileImgView.frame.size.width/2 border:0 borderColor:nil];
    _eITitleLabel.text = [NSString stringWithFormat:@"%@",info.title];
    _eICommentsLabel.text = [NSString stringWithFormat:@"%@",info.desc];
    _dateLabel.text = [NSDate getStartEndDateStrFromString:info.startTimeStamp AndEndDate:info.endTimeStamp];
    self.profileImgView.image = [UIImage imageNamed:@"profile_top"];
    [self.profileImgView roundCorner:self.profileImgView.frame.size.width/2 border:0.0 borderColor:nil];
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)info.likesCount.integerValue] forState:UIControlStateNormal];
    self.likesView.borderWidthsAll = 1.0;
    self.likesView.borderColorTop = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
    self.likesView.borderColorBottom = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
    [APPManager addChart:info.graphDetails AndView:self.graphView];
    [self createWeekDetails:info];
    UITapGestureRecognizer *singleFingerTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEaDetails)];
    [singleFingerTap1 setDelegate:self];
    [self.graphView addGestureRecognizer:singleFingerTap1];
    self.graphView.userInteractionEnabled=YES;
    dashBoardFeed = info;
}

- (void)setDataWithUser:(User *)user {
    [_profileImgView sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [_profileImgView roundCorner:_profileImgView.frame.size.width/2 border:0 borderColor:nil];
    _eITitleLabel.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionTitle];
    _eICommentsLabel.text = [NSString stringWithFormat:@"%@",user.userEffectiveActionDescription];
    _dateLabel.text = [NSDate getStartEndDateStrFromString:user.startTimeStamp AndEndDate:user.endTimeStamp];
    [APPManager addChart:user.graphDetails AndView:self.graphView];
    [self createWeekDetailsWithUser:user];
}

- (void) createWeekDetailsWithUser:(User *)useFeed {
    for (UIView *view in [self.eiSummeryView subviews]) {
        [view removeFromSuperview];
    }
    self.eiSummeryView.borderWidthsAll = 1.0;
    self.eiSummeryView.borderColorTop = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
    int cellCount=3;
    float cellWidth=([[UIScreen mainScreen] applicationFrame].size.width-30)/cellCount;
    int x=0;
    NSLog(@"width %f",self.eiSummeryView.frame.size.width);
    NSLog(@"cellWidth %f",cellWidth);
    NSMutableArray *headers=[NSMutableArray new];
    [headers addObject:@"MIN"];
    [headers addObject:@"MAX"];
    [headers addObject:@"AVG"];
    //[headers addObject:@"TODAY"];
    NSMutableArray *values=[NSMutableArray new];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.min]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.max]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.avg]];
    //[values addObject:[NSString stringWithFormat:@"%@",useFeed.today]];
    for (int i=0; i<cellCount; i++) {
        UILabel *cellTitle=[[UILabel alloc] initWithFrame:CGRectMake(x, 13, cellWidth,20)];
        [cellTitle setFont:[UIFont fontWithName:kRubik_Bold size:9]];
        cellTitle.textColor = [UIColor colorWithRed:0.5137 green:0.5216 blue:0.5333 alpha:1.0];
        cellTitle.textAlignment = NSTextAlignmentCenter;
        cellTitle.text = headers[i];
        [self.eiSummeryView addSubview:cellTitle];
        UILabel *cellValue=[[UILabel alloc] initWithFrame:CGRectMake(x, 40, cellWidth,15)];
        [cellValue setFont:[UIFont fontWithName:kRubik_Regular size:16]];
        cellValue.textColor = [UIColor colorWithRed:0.3137 green:0.3294 blue:0.3686 alpha:1.0];
        cellValue.textAlignment = NSTextAlignmentCenter;
        cellValue.text = values[i];
        [self.eiSummeryView addSubview:cellValue];
        if (i!=cellCount-1) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(x+cellWidth-1, 0, 1, self.eiSummeryView.frame.size.height)];
            [self.eiSummeryView addSubview:view];
            view.backgroundColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
        }
        x+=cellWidth;
    }
}

- (void) createWeekDetails:(DashBoardFeed *)useFeed {
    for (UIView *view in [self.eiSummeryView subviews]) {
        [view removeFromSuperview];
    }
    self.eiSummeryView.borderWidthsAll = 1.0;
    self.eiSummeryView.borderColorTop = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
    int cellCount=3;
    float cellWidth=([[UIScreen mainScreen] applicationFrame].size.width-30)/cellCount;
    int x=0;
    NSLog(@"width %f",self.eiSummeryView.frame.size.width);
    NSLog(@"cellWidth %f",cellWidth);
    NSMutableArray *headers=[NSMutableArray new];
    [headers addObject:@"MIN"];
    [headers addObject:@"MAX"];
    [headers addObject:@"AVG"];
    //[headers addObject:@"TODAY"];
    NSMutableArray *values=[NSMutableArray new];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.min]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.max]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.avg]];
    //[values addObject:[NSString stringWithFormat:@"%@",useFeed.today]];
    for (int i=0; i<cellCount; i++) {
        UILabel *cellTitle=[[UILabel alloc] initWithFrame:CGRectMake(x, 13, cellWidth,20)];
        [cellTitle setFont:[UIFont fontWithName:kRubik_Bold size:9]];
        cellTitle.textColor = [UIColor colorWithRed:0.5137 green:0.5216 blue:0.5333 alpha:1.0];
        cellTitle.textAlignment = NSTextAlignmentCenter;
        cellTitle.text = headers[i];
        [self.eiSummeryView addSubview:cellTitle];
        UILabel *cellValue=[[UILabel alloc] initWithFrame:CGRectMake(x, 40, cellWidth,15)];
        [cellValue setFont:[UIFont fontWithName:kRubik_Regular size:16]];
        cellValue.textColor = [UIColor colorWithRed:0.3137 green:0.3294 blue:0.3686 alpha:1.0];
        cellValue.textAlignment = NSTextAlignmentCenter;
        cellValue.text = values[i];
        [self.eiSummeryView addSubview:cellValue];
        if (i!=cellCount-1) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(x+cellWidth-1, 0, 1, self.eiSummeryView.frame.size.height)];
            [self.eiSummeryView addSubview:view];
            view.backgroundColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:1.0];
        }
        x+=cellWidth;
    }
}

#pragma mark - Private Method
- (void)openEaDetails {
    User *member = dashBoardFeed.user;
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.selectedUser=member;
    viewController.eaData = dashBoardFeed;
    viewController.isPast = YES;
    UIViewController *vc=self.delegateView;;
    [vc.navigationController pushViewController:viewController animated:YES];
}

@end
