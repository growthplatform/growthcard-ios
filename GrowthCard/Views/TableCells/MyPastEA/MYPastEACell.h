//
//  MyPastEACell.h
//  GrowthCard
//
//  Created by Pawan Kumar on 27/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPastEACell : UITableViewCell

@property (nonatomic, weak)  UIViewController *delegateView;
+ (MyPastEACell *)cell;
- (void)setData:(DashBoardFeed *)info AndUser:(User *)user;
- (void)setDataWithUser:(User *)user;

@end
