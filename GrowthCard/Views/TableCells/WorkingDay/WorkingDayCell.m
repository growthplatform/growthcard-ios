//
//  WorkingDayCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 24/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "WorkingDayCell.h"

@interface WorkingDayCell () {
    __weak IBOutlet UIImageView *img;
    __weak IBOutlet UILabel     *nmLbl;
}
@end

@implementation WorkingDayCell

+ (WorkingDayCell *)cell {
    WorkingDayCell *cell = (WorkingDayCell *)[UIView viewFromXib:@"WorkingDayCell" classname:[WorkingDayCell class] owner:self];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)showDays:(WorkingDaysInfo *)info {
    nmLbl.text      = info.dayName;
    img.highlighted = info.isSelected;
}

@end


@implementation WorkingDaysInfo
+ (WorkingDaysInfo *)setDayName:(NSString *)name andIsSeleted:(BOOL)isSelect {
    WorkingDaysInfo *info = [WorkingDaysInfo new];
    info.dayName          = name;
    info.isSelected       = isSelect;
    return info;
}
@end









