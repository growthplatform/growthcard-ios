//
//  WorkingDayCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 24/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>


@class WorkingDaysInfo;

@interface WorkingDayCell : UITableViewCell
+ (WorkingDayCell *)cell;
- (void)showDays:(WorkingDaysInfo *)info;
@end


@interface WorkingDaysInfo : NSObject
@property(nonatomic,weak) NSString *dayName;
@property(nonatomic,assign) BOOL   isSelected;
+ (WorkingDaysInfo *)setDayName:(NSString *)name andIsSeleted:(BOOL)isSelect;
@end