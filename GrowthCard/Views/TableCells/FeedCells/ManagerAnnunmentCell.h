//
//  ManagerAnnunmentCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManagerAnnunmentCell : UITableViewCell

@property (nonatomic, weak)  UIViewController *delegate;
+ (ManagerAnnunmentCell *)cell;
- (void)setDataWithFeed:(Feed *)feed :(int)idx;
- (void)setDataWithAnnoucement:(Announcement *)feed;

@end
