//
//  SubordinateAnnunmentCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedLikeDelegate.h"


@interface SubordinateAnnunmentCell : UITableViewCell

@property (nonatomic, assign) id <FeedLikeDelegate> delegate;
@property (nonatomic, weak)  UIViewController *delegateView;
+ (SubordinateAnnunmentCell *)cell;
- (void)setDataWithFeed:(Feed *)feed :(int)idx;
- (void)setDataWithComment:(Comment *)commnt;

@end
