//
//  MarkAdhereCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "MarkAdhereCell.h"
#import "SubSegmentedVC.h"
#import "UILabel+Bold_Color.h"
#import "NIAttributedLabel.h"

#import "ResponsiveLabel.h"

@interface MarkAdhereCell () {
     User *feedUser;
     int index;
    NSString *pastEffectiveActionId;
}
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *rimgView;
@property (weak, nonatomic) IBOutlet UILabel *rNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *rPostLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UIView *sGraphView;
@property (weak, nonatomic) IBOutlet UILabel *rTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *rLikesLbl;
@property (weak, nonatomic) IBOutlet UILabel *rCommntLbl;
@property (weak, nonatomic) IBOutlet UIImageView *sImgView;
@property (weak, nonatomic) IBOutlet UILabel *sCommntLbl;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (strong, nonatomic) Feed *feedData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentHConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentImageHeightConstraint;

@end

@implementation MarkAdhereCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (MarkAdhereCell *)cell {
    MarkAdhereCell *cell = (MarkAdhereCell *)[UIView viewFromXib:@"MarkAdhereCell" classname:[MarkAdhereCell class] owner:self];
    [cell.bgView roundCorner:3.0 border:0 borderColor:nil];
    [cell.bgView roundCorner:3.0 border:0 borderColor:[UIColor redColor]];
    [cell.rimgView roundCorner:cell.rimgView.frame.size.width/2 border:0 borderColor:nil];
    [cell.sImgView roundCorner:cell.sImgView.frame.size.width/2 border:0 borderColor:nil];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor=cell.backgroundColor=[UIColor clearColor];
    cell.contentView.userInteractionEnabled = NO;
    return cell;
}

#pragma mark -
#pragma mark - NIAttributedLabelDelegate
- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    if (result.resultType == NSTextCheckingTypeLink) {
        [[UIApplication sharedApplication] openURL:result.URL];
    }
}

#pragma mark -
#pragma mark Public Method
- (void)setDataWithFeed:(Feed *)feed :(int)idx {
    index = idx;
    [_rimgView sd_setImageWithURL:[NSURL URLWithString:feed.sUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [_sImgView sd_setImageWithURL:[NSURL URLWithString:feed.rUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _rNameLbl.text = [NSString stringWithFormat:@"%@ %@", feed.rUser.userFName, feed.rUser.userLName];
    _rPostLbl.text = [NSString stringWithFormat:@"%@", feed.rDesignationTitle];
    _titleLbl.text = [NSString stringWithFormat:@"%@", feed.rDesignationTitle];
    _descLbl.text = [NSString stringWithFormat:@"%@", feed.rEaDescpton];
    
    _titleLbl.text = [NSString stringWithFormat:@"%@", feed.sUserEffectiveActionTitle];
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)feed.likesCount.integerValue] forState:UIControlStateNormal];
    if ([feed.feedIsLiked boolValue]) {
        self.likeBtn.selected = YES;
    }
    else {
        self.likeBtn.selected = NO;
    }
    _rTimeLbl.text = [NSDate getAgoTimeFromString:feed.sUserEffectiveActionDate];
    _rLikesLbl.text = [NSString stringWithFormat:@"%@ Likes", feed.likesCount];
    _rCommntLbl.text = [NSString stringWithFormat:@"%@ Comments", feed.commntCount];
    
    NSMutableString *strMsg=[NSMutableString new];
    [strMsg appendString:[NSString stringWithFormat:@"%@ %@", feed.sUser.userFName, feed.sUser.userLName]];
    [strMsg appendString:@" "];
    [strMsg appendString:[NSString stringWithFormat:@"%@", feed.objComment.commentDesc]];
    [strMsg appendString:@""];
    
    _sCommntLbl.text=strMsg;
    [self.sCommntLbl boldAndColorSubstring:[NSString stringWithFormat:@"%@ %@", feed.sUser.userFName, feed.sUser.userLName] forColor:[UIColor blackColor]];
    if (feed.objComment.commentDesc == nil || [feed.objComment.commentDesc length] == 0 ) {
        self.commentHConst.constant=0;
        self.commentImageHeightConstraint.constant = 0;
    }
    else {
        self.commentHConst.constant=60;
        self.commentImageHeightConstraint.constant = 36;
    }
    [APPManager addChart:feed.rGraphDetails AndView:_sGraphView];
    self.feedData = feed;
    feedUser = [User new];
    feedUser.userId = feed.rUser.userId;
    feedUser.userTeamId = feed.rTeamId;
    feedUser.objCompany.companyId = feed.objCompany.companyId;
    feedUser.userEffectiveActionId = feed.rEaId;
    pastEffectiveActionId = [NSString stringWithFormat:@"%@", feed.pastEffectiveActionId];
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    [self.rimgView addGestureRecognizer:singleFingerTap];
    self.rimgView.userInteractionEnabled=YES;
    UITapGestureRecognizer *singleFingerTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEaDetails)];
    [singleFingerTap1 setDelegate:self];
    [self.sGraphView addGestureRecognizer:singleFingerTap1];
    self.sGraphView.userInteractionEnabled=YES;
}

#pragma mark -
#pragma mark Control Event
- (IBAction)likeBtnClicked:(UIButton *)sender {
    self.feedData.feedIsLiked = @(![self.feedData.feedIsLiked boolValue]);
    if ([self.feedData.feedIsLiked boolValue]) {
        self.feedData.likesCount = @([self.feedData.likesCount integerValue] + 1);
    }
    else {
        self.feedData.likesCount = @([self.feedData.likesCount integerValue] - 1);
    }
    self.likeBtn.selected = [self.feedData.feedIsLiked boolValue];
    if ([self.feedData.likesCount integerValue] > 0) {
        [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)self.feedData.likesCount.integerValue] forState:UIControlStateNormal];
    }
    else {
        [self.likeBtn setTitle:[NSString stringWithFormat:@" Likes"] forState:UIControlStateNormal];
    }
    if (_delegate && [_delegate respondsToSelector:@selector(didLikeFeed: :)]) {
        [_delegate didLikeFeed:self.feedData :index];
    }
}

#pragma mark -
#pragma mark Private Method
- (void)OpenUserProfile:(id)sender {
    NSLog(@"Open User Profile click Feed");
    User *member = feedUser;
    SubSegmentedVC *viewController = (SubSegmentedVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubSegmentedVC class]];
    viewController.selectedUser=member;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

- (void)openEaDetails {
    User *user = [UserManager sharedManager].activeUser;
    DashBoardFeed *eaData = [DashBoardFeed new];
    eaData.feedId = [NSString stringWithFormat:@"%@",self.feedData.rEaId];
    eaData.title = self.feedData.sUserEffectiveActionTitle;
    eaData.desc = self.feedData.rEaDescpton;
    eaData.graphDetails = self.feedData.rGraphDetails;
    eaData.pastEffectiveActionId = [NSString stringWithFormat:@"%@", pastEffectiveActionId];
    User *member = feedUser;
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    viewController.selectedUser=member;
    viewController.eaData = eaData;
    viewController.isPast = YES;
    if((member.userId.integerValue == user.userId.integerValue) && (user.userEffectiveActionId.integerValue == self.feedData.rEaId.integerValue))
    viewController.isPast = NO;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

@end
