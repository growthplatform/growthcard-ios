//
//  RecentAnnouncementCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 20/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "RecentAnnouncementCell.h"
#import "SubSegmentedVC.h"
#import "NIAttributedLabel.h"

#import "ResponsiveLabel.h"

@interface RecentAnnouncementCell () {
    User *feedUser;
}
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *sImgView;
@property (weak, nonatomic) IBOutlet UILabel *sName;
@property (weak, nonatomic) IBOutlet UILabel *sPost;
@property (weak, nonatomic) IBOutlet UILabel *sTime;
@property (weak, nonatomic) IBOutlet UILabel *sAnnounmnt;
@property (weak, nonatomic) IBOutlet UIView *profileBgView;

@end

@implementation RecentAnnouncementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (RecentAnnouncementCell *)cell {
    RecentAnnouncementCell *cell = (RecentAnnouncementCell *)[UIView viewFromXib:@"RecentAnnouncementCell" classname:[RecentAnnouncementCell class] owner:self];
        
    [cell.bgView roundCorner:3.0 border:0 borderColor:nil];
    [cell.sImgView roundCorner:cell.sImgView.frame.size.width/2 border:2 borderColor:[UIColor colorWithRed:.44 green:.72 blue:.33 alpha:1.0f]];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor=cell.backgroundColor=[UIColor clearColor];
    cell.contentView.userInteractionEnabled = NO;
    return cell;
}

#pragma mark -
#pragma mark - NIAttributedLabelDelegate
- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    if (result.resultType == NSTextCheckingTypeLink) {
        [[UIApplication sharedApplication] openURL:result.URL];
    }
}

#pragma mark -
#pragma mark Public Method
- (void)setDataWithAnnoucement:(Announcement *)feed {
    feedUser = feed.announceUser;
    feedUser = [User new];
    feedUser.userId = feed.announceUser.userId;
    feedUser.userTeamId = feed.announceUser.userTeamId;
    feedUser.objCompany.companyId = feed.announceUser.objCompany.companyId;
    [_sImgView sd_setImageWithURL:[NSURL URLWithString:feed.announceUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _sName.text = [NSString stringWithFormat:@"%@ %@", feed.announceUser.userFName,feed.announceUser.userLName];
    _sPost.text = [NSString stringWithFormat:@"%@", feed.announceUser.userDesignationTitle];
    _sTime.text = [NSDate getAgoTimeFromString:feed.announceTime];

    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    [self.sImgView addGestureRecognizer:singleFingerTap];
    self.sImgView.userInteractionEnabled=YES;
    _sAnnounmnt.text = [NSString stringWithFormat:@"%@", feed.announceDesc];
}

#pragma mark -
#pragma mark Private Method
- (void)OpenUserProfile:(id)sender {
    NSLog(@"Open User Profile click Feed");
    User *member = feedUser;
    SubSegmentedVC *viewController = (SubSegmentedVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubSegmentedVC class]];
    viewController.selectedUser=member;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

@end
