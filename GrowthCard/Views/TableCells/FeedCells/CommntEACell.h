//
//  CommntEACell.h
//  GrowthCard
//
//  Created by Narender Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedLikeDelegate.h"

@interface CommntEACell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentImageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commnetSuperViewHeightContstraint;
@property (nonatomic, assign) id <FeedLikeDelegate> delegate;
@property (nonatomic, weak)  UIViewController *delegateView;
+ (CommntEACell *)cell;
- (void)setDataWithFeed:(Feed *)feed  :(int)idx;

@end
