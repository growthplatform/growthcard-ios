//
//  RecentAnnouncementCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 20/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentAnnouncementCell : UITableViewCell

@property (nonatomic, weak)  UIViewController *delegate;
+ (RecentAnnouncementCell *)cell;
- (void)setDataWithAnnoucement:(Announcement *)feed;

@end
