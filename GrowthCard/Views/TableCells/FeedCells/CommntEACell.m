//
//  CommntEACell.m
//  GrowthCard
//
//  Created by Narender Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "CommntEACell.h"
#import "FeedLikeDelegate.h"
#import "SubSegmentedVC.h"
#import "UILabel+Bold_Color.h"
#import "NIAttributedLabel.h"

#import "ResponsiveLabel.h"

@interface CommntEACell () {
    User *feedUser;
    int index;
    NSString *pastEffectiveActionId;
}
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *rImgView;
@property (weak, nonatomic) IBOutlet UILabel *rNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *rPostLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *rGraphView;
@property (weak, nonatomic) IBOutlet UILabel *rCommtLbl;
@property (weak, nonatomic) IBOutlet UILabel *rTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *rLikeLbl;
@property (weak, nonatomic) IBOutlet UILabel *rCommntLbl;
@property (weak, nonatomic) IBOutlet UIImageView *sImgView;
@property (weak, nonatomic) IBOutlet UILabel *sCommntLbl;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (strong, nonatomic) Feed *feedData;

@end

@implementation CommntEACell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (CommntEACell *)cell {
    CommntEACell *cell = (CommntEACell *)[UIView viewFromXib:@"CommntEACell" classname:[CommntEACell class] owner:self];
    [cell.bgView roundCorner:3.0 border:0 borderColor:nil];
    [cell.rImgView roundCorner:cell.rImgView.frame.size.width/2 border:0 borderColor:nil];
    [cell.sImgView roundCorner:cell.sImgView.frame.size.width/2 border:0 borderColor:nil];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor=cell.backgroundColor=[UIColor clearColor];
    cell.contentView.userInteractionEnabled = NO;
    return cell;
}

#pragma mark -
#pragma mark - NIAttributedLabelDelegate
- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    if (result.resultType == NSTextCheckingTypeLink) {
        [[UIApplication sharedApplication] openURL:result.URL];
    }
}

#pragma mark -
#pragma mark Public Method
- (void)setDataWithFeed:(Feed *)feed :(int)idx {
    index = idx;
    [_rImgView sd_setImageWithURL:[NSURL URLWithString:feed.rUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    [_sImgView sd_setImageWithURL:[NSURL URLWithString:feed.sUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _rNameLbl.text = [NSString stringWithFormat:@"%@ %@", feed.rUser.userFName, feed.rUser.userLName];
    _rPostLbl.text = [NSString stringWithFormat:@"%@", feed.rDesignationTitle];
    _titleLbl.text = [NSString stringWithFormat:@"%@", feed.sUserEffectiveActionTitle];
    
    pastEffectiveActionId = [NSString stringWithFormat:@"%@", feed.pastEffectiveActionId];
    _rCommtLbl.text = [NSString stringWithFormat:@"%@",feed.rEaDescpton];
    
    self.likeBtn.selected = feed.feedIsLiked.integerValue;
    _rTimeLbl.text = [NSDate getAgoTimeFromString:feed.sUserEffectiveActionDate];
    _rLikeLbl.text = [NSString stringWithFormat:@"%@ Likes", feed.likesCount];;
    _rCommntLbl.text = [NSString stringWithFormat:@"%@ Comments", feed.commntCount];    NSMutableString *strMsg=[NSMutableString new];
    
    [strMsg appendString:[NSString stringWithFormat:@"%@ %@", feed.sUser.userFName, feed.sUser.userLName]];
    [strMsg appendString:@" "];
    [strMsg appendString:[NSString stringWithFormat:@"%@", feed.objComment.commentDesc]];
    [strMsg appendString:@""];
    _sCommntLbl.text=strMsg;
    
    [self.sCommntLbl boldAndColorSubstring:[NSString stringWithFormat:@"%@ %@", feed.sUser.userFName, feed.sUser.userLName] forColor:[UIColor blackColor]];
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)feed.likesCount.integerValue] forState:UIControlStateNormal];
    if ([feed.feedIsLiked boolValue]) {
        self.likeBtn.selected = YES;
    }
    else {
        self.likeBtn.selected = NO;
    }
    [APPManager addChartForTopView:feed.rGraphDetails AndView:_rGraphView];
    self.feedData = feed;
    feedUser = [User new];
    feedUser.userId = feed.rUser.userId;
    feedUser.userTeamId = feed.rTeamId;
    feedUser.objCompany.companyId = feed.objCompany.companyId;
    feedUser.userEffectiveActionId = feed.rEaId;
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    [self.rImgView addGestureRecognizer:singleFingerTap];
    self.rImgView.userInteractionEnabled=YES;
    UITapGestureRecognizer *singleFingerTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openEaDetails)];
    [singleFingerTap1 setDelegate:self];
    [self.rGraphView addGestureRecognizer:singleFingerTap1];
    self.rGraphView.userInteractionEnabled=YES;
    
    if (feed.objComment.commentDesc == nil || [feed.objComment.commentDesc length] == 0 ) {
        _commnetSuperViewHeightContstraint.constant = 0;
        _sImgView.hidden = YES;
    }else{
        _commnetSuperViewHeightContstraint.constant = 48;
        _sImgView.hidden = NO;
    }
}

#pragma mark -
#pragma mark Control Event
- (IBAction)likeBtnClicked:(UIButton *)sender {
    self.feedData.feedIsLiked = @(![self.feedData.feedIsLiked boolValue]);
    if ([self.feedData.feedIsLiked boolValue]) {
        self.feedData.likesCount = @([self.feedData.likesCount integerValue] + 1);
    }
    else {
        self.feedData.likesCount = @([self.feedData.likesCount integerValue] - 1);
    }
    self.likeBtn.selected = [self.feedData.feedIsLiked boolValue];
    if ([self.feedData.likesCount integerValue] > 0) {
        [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)self.feedData.likesCount.integerValue] forState:UIControlStateNormal];
    }
    else {
        [self.likeBtn setTitle:[NSString stringWithFormat:@" Likes"] forState:UIControlStateNormal];
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(didLikeFeed: :)]) {
        [_delegate didLikeFeed:self.feedData :index];
    }
}

#pragma mark -
#pragma mark Public Method
- (void)OpenUserProfile:(id)sender {
    NSLog(@"Open User Profile click Feed");
    User *member = feedUser;
    SubSegmentedVC *viewController = (SubSegmentedVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubSegmentedVC class]];
    viewController.selectedUser=member;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

- (void)openEaDetails {
    NSLog(@"CommntEACell Open Profile");
    User *user = [UserManager sharedManager].activeUser;
    DashBoardFeed *eaData = [DashBoardFeed new];
    eaData.feedId = [NSString stringWithFormat:@"%@",self.feedData.rEaId];
    eaData.title = self.feedData.sUserEffectiveActionTitle;
    eaData.desc = self.feedData.rEaDescpton;
    eaData.graphDetails = self.feedData.rGraphDetails;
    eaData.pastEffectiveActionId = [NSString stringWithFormat:@"%@",pastEffectiveActionId];
    User *member = feedUser;
    SubEADetails *viewController = (SubEADetails *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubEADetails class]];
    
    viewController.selectedUser=member;
    viewController.eaData = eaData;
    viewController.isPast = YES;
    if((member.userId.integerValue == user.userId.integerValue) && (user.userEffectiveActionId.integerValue == self.feedData.rEaId.integerValue))
        viewController.isPast = NO;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

@end
