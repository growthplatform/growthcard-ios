//
//  FeedLikeDelegate.h
//  GrowthCard
//
//  Created by Narender Kumar on 05/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FeedLikeDelegate <NSObject>

@optional
- (void)didLikeFeed:(Feed *)feed :(int)idx;

@end
