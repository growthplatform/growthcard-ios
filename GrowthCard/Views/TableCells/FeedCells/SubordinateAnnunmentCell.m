//
//  SubordinateAnnunmentCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 02/04/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "SubordinateAnnunmentCell.h"
#import "SubSegmentedVC.h"
#import "NIAttributedLabel.h"

#import "ResponsiveLabel.h"

@interface SubordinateAnnunmentCell () {
    User *feedUser;
    int index;
    NSString *pastEffectiveActionId;
}
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *sImgView;
@property (weak, nonatomic) IBOutlet UILabel *sNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *sPostLbl;
@property (weak, nonatomic) IBOutlet UILabel *sCommtLbl;
@property (weak, nonatomic) IBOutlet UILabel *sLikeLbl;
@property (weak, nonatomic) IBOutlet UILabel *sAgoTimeLbl;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (strong, nonatomic) Feed *feedData;

@end


@implementation SubordinateAnnunmentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (SubordinateAnnunmentCell *)cell {
    SubordinateAnnunmentCell *cell = (SubordinateAnnunmentCell *)[UIView viewFromXib:@"SubordinateAnnunmentCell" classname:[SubordinateAnnunmentCell class] owner:self];
    [cell.bgView roundCorner:3.0 border:0 borderColor:nil];
    [cell.bgView roundCorner:3.0 border:0 borderColor:[UIColor clearColor]];
    [cell.sImgView roundCorner:cell.sImgView.frame.size.width/2 border:0 borderColor:nil];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor=cell.backgroundColor=[UIColor clearColor];
    cell.contentView.userInteractionEnabled = NO;
    return cell;
}

#pragma mark -
#pragma mark Public Method
- (void)setDataWithFeed:(Feed *)feed :(int)idx{
    index = idx;
    [_sImgView sd_setImageWithURL:[NSURL URLWithString:feed.rUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _sNameLbl.text = [NSString stringWithFormat:@"%@ %@", feed.rUser.userFName, feed.rUser.userLName];
    _sPostLbl.text = [NSString stringWithFormat:@"%@", feed.rDesignationTitle];
    
    pastEffectiveActionId = [NSString stringWithFormat:@"%@", feed.pastEffectiveActionId];
    _sCommtLbl.text = [NSString stringWithFormat:@"%@", feed.postDesc];
    
    _sLikeLbl.text = [NSString stringWithFormat:@"%@ Likes", feed.likesCount];
    _sAgoTimeLbl.text = [NSDate getAgoTimeFromString:feed.postDate];
    if ([feed.feedIsLiked boolValue]) {
        self.likeBtn.selected = YES;
        [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)feed.likesCount.integerValue] forState:UIControlStateNormal];
    }
    else {
        self.likeBtn.selected = NO;
        [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)feed.likesCount.integerValue] forState:UIControlStateNormal];
    }
    self.feedData = feed;
    feedUser = [User new];
    feedUser.userId = feed.rUser.userId;
    feedUser.userTeamId = feed.rTeamId;
    feedUser.objCompany = feed.objCompany;
    feedUser.userFName = feed.rUser.userFName;
    feedUser.userLName = feed.rUser.userLName;
    feedUser.userImageUrl = feed.rUser.userImageUrl;
    feedUser.userDesignationTitle = feed.rDesignationTitle;
    feedUser.userEffectiveActionId = feed.rEaId;
    feedUser.userEffectiveActionTitle = feed.rEaDescpton;
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(OpenUserProfile:)];
    [singleFingerTap setDelegate:self];
    [self.sImgView addGestureRecognizer:singleFingerTap];
    self.sImgView.userInteractionEnabled=YES;
}

#pragma mark -
#pragma mark - NIAttributedLabelDelegate
- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    if (result.resultType == NSTextCheckingTypeLink) {
        [[UIApplication sharedApplication] openURL:result.URL];
    }
}

#pragma mark -
#pragma mark Control Event
- (IBAction)likeBtnClicked:(UIButton *)sender {
    self.feedData.feedIsLiked = @(![self.feedData.feedIsLiked boolValue]);
    if ([self.feedData.feedIsLiked boolValue]) {
        self.feedData.likesCount = @([self.feedData.likesCount integerValue] + 1);
    }
    else {
        self.feedData.likesCount = @([self.feedData.likesCount integerValue] - 1);
    }
    self.likeBtn.selected = [self.feedData.feedIsLiked boolValue];
    if ([self.feedData.likesCount integerValue] > 0) {
        [self.likeBtn setTitle:[NSString stringWithFormat:@"%ld Likes", (long)self.feedData.likesCount.integerValue] forState:UIControlStateNormal];
    }
    else {
        [self.likeBtn setTitle:[NSString stringWithFormat:@"0 Likes"] forState:UIControlStateNormal];
    }
    if (_delegate && [_delegate respondsToSelector:@selector(didLikeFeed: :)]) {
        [_delegate didLikeFeed:self.feedData :index];
    }
}

#pragma mark -
#pragma mark Private Method
- (void)OpenUserProfile:(id)sender {
    NSLog(@"Open User Profile click Feed SubordinateAnnunmentCell");
    User *member = feedUser;
    SubSegmentedVC *viewController = (SubSegmentedVC *)[[UIStoryboard subordinateStoryboard] instantiateViewControllerWithClass:[SubSegmentedVC class]];
    viewController.selectedUser=member;
    UIViewController *vc=(UIViewController*)self.delegate;
    [vc.navigationController pushViewController:viewController animated:YES];
}

- (void)setDataWithComment:(Comment *)commnt {
    [_sImgView sd_setImageWithURL:[NSURL URLWithString:commnt.commentUser.userImageUrl] placeholderImage:[UIImage imageNamed:@"profile_top"]];
    _sNameLbl.text = [NSString stringWithFormat:@"%@ %@", commnt.commentUser.userFName, commnt.commentUser.userLName];
    _sPostLbl.text = [NSString stringWithFormat:@"%@", commnt.commentUser.userDesignationTitle];
    _sAgoTimeLbl.text = [NSDate getAgoFromString:commnt.commentAtTime];
    _sCommtLbl.text = [NSString stringWithFormat:@"%@", commnt.commentDesc];
}

@end
