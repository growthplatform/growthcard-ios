//
//  OtherCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 20/05/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIAttributedLabel.h"

#import "ResponsiveLabel.h"

@interface OtherCell : UITableViewCell {
    
}

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userTitle;
@property (weak, nonatomic) IBOutlet FRHyperLabel *comment;
@property (weak, nonatomic) IBOutlet UILabel *userDate;

@end
