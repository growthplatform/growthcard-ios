//
//  MyCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 20/05/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "MyCell.h"

@implementation MyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
