    //
//  UserProfileCell.m
//  GrowthCard
//
//  Created by Pawan Kumar on 10/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "UserProfileCell.h"
#import "UserInfoView.h"
#import "UILabel+Extra.h"
#import "NAUIViewWithBorders.h"
#import "ViewOtherProfile.h"
#import "NSDate+Additions.h"

@interface UserProfileCell () {
    DashBoardFeed *feedInfo;
}
@property (weak,nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompany;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartment;
@property (weak, nonatomic) IBOutlet UILabel *lblTeam;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak,nonatomic) IBOutlet UILabel *commentLable;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *chartContView;
@property (weak, nonatomic) IBOutlet UIView *chatSummaryView;
@property (weak, nonatomic) IBOutlet UIButton *assignButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphHightConstraint;

@end

@implementation UserProfileCell

+ (UserProfileCell *)cell {
    UserProfileCell *cell = (UserProfileCell *)[UIView viewFromXib:@"UserProfileCell" classname:[UserProfileCell class] owner:self];
    cell.contentView.backgroundColor=cell.backgroundColor=[UIColor clearColor];
    cell.accessoryType   = UITableViewCellAccessoryNone;
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor=cell.backgroundColor=[UIColor clearColor];
    cell.contentView.userInteractionEnabled = NO;
    return cell;
}

- (void) layoutSubviews {
    [super layoutSubviews];
}

- (void)awakeFromNib {
    self.graphHightConstraint.constant=120;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - Public Method
- (void)setCellData:(DashBoardFeed *)info withNumberOfline:(NSInteger )lines withTotalCount:(NSInteger )count {
    feedInfo=info;
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:feedInfo.user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profileDefault"]];
    [self.userImage.layer setCornerRadius:self.userImage.frame.size.height/2];
    [self.userImage.layer setMasksToBounds:YES];
    self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",[feedInfo.user.userFName pascalCased], [feedInfo.user.userLName pascalCased]];
    self.lblCompany.text = [NSString stringWithFormat:@"%@",feedInfo.user.objCompany.name];
    self.lblDepartment.text = [NSString stringWithFormat:@"%@",feedInfo.user.userDeparmentName];
    self.lblTeam.text = [NSString stringWithFormat:@"%@",feedInfo.user.userDesignationTitle];
    self.lblEmail.text = [NSString stringWithFormat:@"%@",feedInfo.user.userEmail];
    self.eaDescLbl.text = feedInfo.title;
    self.dateTimeLabel.text = feedInfo.title;
    self.commentLable.numberOfLines = count;
}

- (void)setCellDataWithSubodnate:(User *)user withFeed:(DashBoardFeed *)info withNumberOfline:(NSInteger )lines withTotalCount:(NSInteger )count {
    feedInfo=info;
    if(user.userImageUrl.length > 10)
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:user.userImageUrl] placeholderImage:[UIImage imageNamed:@"profileDefault"]];
    [self.userImage roundCorner:self.userImage.frame.size.width/2 border:0 borderColor:nil];
    self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",[user.userFName pascalCased], [user.userLName pascalCased]];
    self.lblCompany.text = [NSString stringWithFormat:@"%@",user.objCompany.name];
    self.lblDepartment.text = [NSString stringWithFormat:@"%@",user.userDeparmentName];
    self.lblTeam.text = [NSString stringWithFormat:@"%@",user.userDesignationTitle];
    self.lblEmail.text = [NSString stringWithFormat:@"%@",user.userEmail];
    self.commentLable.text=user.userEffectiveActionTitle;
    self.dateTimeLabel.text=[NSDate getHeaderDateStrFromString:user.startTimeStamp];
    [self createWeekDetails:user];
    self.graphHightConstraint.constant=135;
    [APPManager addChart:user.graphDetails AndView:self.chartContView];
}

- (void)createWeekDetails:(User *)useFeed {
    for (UIView *view in [self.chatSummaryView subviews]) {
        [view removeFromSuperview];
    }
    float cellCount = 4.0;
    float cellWidth=([[UIScreen mainScreen] applicationFrame].size.width-50)/cellCount;
    float x = 0.0;
    NSLog(@"cellWidth- %f",cellWidth);
    NSMutableArray *headers=[NSMutableArray new];
    [headers addObject:@"MIN"];
    [headers addObject:@"MAX"];
    [headers addObject:@"AVG"];
    [headers addObject:@"TODAY"];
    NSMutableArray *values=[NSMutableArray new];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.min]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.max]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.avg]];
    [values addObject:[NSString stringWithFormat:@"%@",useFeed.today]];
    int k = roundf(cellCount);
    for (int i=0 ; i < k ; i++) {
        UILabel *cellTitle=[[UILabel alloc] initWithFrame:CGRectMake(x, 5+7, cellWidth ,15)];
        [cellTitle setFont:[UIFont fontWithName:kRubik_Medium size:7]];
        cellTitle.textColor = [UIColor colorWithRed:0.5137 green:0.5216 blue:0.5333 alpha:1.0];
        cellTitle.textAlignment = NSTextAlignmentCenter;
        cellTitle.text = headers[i];
        [self.chatSummaryView addSubview:cellTitle];
        UILabel *cellValue=[[UILabel alloc] initWithFrame:CGRectMake(x, 20+7, cellWidth,15)];
        [cellValue setFont:[UIFont fontWithName:kRubik_Regular size:12]];
        cellValue.textColor = [UIColor colorWithRed:0.3137 green:0.3294 blue:0.3686 alpha:1.0];
        cellValue.textAlignment = NSTextAlignmentCenter;
        cellValue.text = values[i];
        [self.chatSummaryView addSubview:cellValue];
        
        if (i!=cellCount-1) {
            UIView *view=[[UIView alloc] initWithFrame:CGRectMake(x+cellWidth-1, 0, 1, self.chatSummaryView.frame.size.height)];
            [self.chatSummaryView addSubview:view];
            view.backgroundColor = [UIColor colorWithRed:0.8392 green:0.8392 blue:0.8392 alpha:0.3];
        }
        x = x + cellWidth ;
    }
}

#pragma mark - Control Event
- (IBAction)btnAssignEAClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didBtnAssignEAClick:)]) {
        [_delegate didBtnAssignEAClick:feedInfo];
    }
}

- (IBAction)btnEditEAClick:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didbtnEditEAClick:)]) {
        [_delegate didbtnEditEAClick:feedInfo];
    }
}

@end
