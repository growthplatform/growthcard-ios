//
//  UserProfileCell.h
//  GrowthCard
//
//  Created by Pawan Kumar on 10/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DashBoardFeed;
@protocol UserProfileCellDelegat <NSObject>

- (void)readMoreButtonPressed;
- (void)didBtnAssignEAClick:(DashBoardFeed *)info;
- (void)didbtnEditEAClick:(DashBoardFeed *)info;

@end


@interface UserProfileCell : UITableViewCell

@property (weak,nonatomic) id<UserProfileCellDelegat>delegate;
@property (weak, nonatomic) IBOutlet FRHyperLabel *eaDescLbl;
- (void)setCellData:(DashBoardFeed *)info withNumberOfline:(NSInteger )lines withTotalCount:(NSInteger )count;
- (void)setCellDataWithSubodnate:(User *)user withFeed:(DashBoardFeed *)info withNumberOfline:(NSInteger )lines withTotalCount:(NSInteger )count;
+ (UserProfileCell *)cell;

@end