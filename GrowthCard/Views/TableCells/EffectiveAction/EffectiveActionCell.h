//
//  EffectiveActionCell.h
//  GrowthCard
//
//  Created by Narender Kumar on 08/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EffectiveActionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *spertorLine;
+ (EffectiveActionCell *)cell;
- (void)setEATitle:(NSString *)title;

@end
