//
//  EffectiveActionCell.m
//  GrowthCard
//
//  Created by Narender Kumar on 08/03/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import "EffectiveActionCell.h"

@implementation EffectiveActionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

+ (EffectiveActionCell *)cell {
    EffectiveActionCell *cell = (EffectiveActionCell *)[UIView viewFromXib:@"EffectiveActionCell" classname:[EffectiveActionCell class] owner:self];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Public Method
- (void)setEATitle:(NSString *)title {
    _titleLbl.text = title;
}

@end
