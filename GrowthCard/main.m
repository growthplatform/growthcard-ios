//
//  main.m
//  GrowthCard
//
//  Created by Arvind Singh on 17/02/16.
//  Copyright © 2016 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

// get-team-members
// profile page